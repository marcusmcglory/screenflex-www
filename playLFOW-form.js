// ---------- Do Not Remove ----------
// Copyright 2010 Live Face On Web, LLC
// Version: 9.1.1
// Date: 08/13/2010
// End User License Agreement Location: http://www.livefaceonweb.com/eula.htm
//
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/////////////////// User defined parameters and instructions //////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// Below enter the URL location where the files are residing
// Example: var lf_URL = 'http://www.livefaceonweb.com/';
// In the above example, LiveFaceOnWeb Presentaton files are located at the root level of http://www.livefaceonweb.com
var lf_URL = '//www.screenflex.com/';
// Below select from a choice of Static, Relative, or Dynamic position for the LiveFaceOnWeb Presentation
// 1 = Static Position, LiveFaceOnWeb Presentation would be in a fixed location
// 2 = Relative Position, LiveFaceOnWeb Presentation would be located where the script is called from on the webpage 
// 3 = Dynamic Position, LiveFaceOnWeb Presentation would float as the page scrolls
// Example: var lf_Position = 3;
// In the above example, LiveFaceOnWeb Presentation would float as with page scrolls
var lf_Position = 3;
// Below enter LiveFaceOnWeb Presentation OffSet Position
// OffSet Position specifies the top and the left position for Static and Relative Positions only
// Example: var lf_OffSet_Top_Position = 50; and var lf_OffSet_Left_Position = 0; 
// In the above example, LiveFaceOnWeb Presentation would be placed 50 pixels from the top and 0 pixels to the left of the browser screen
var lf_OffSet_Top_Position = 0;
var lf_OffSet_Left_Position = 0;
// Below select the preset Dynamic position for LiveFaceOnWeb Presentation
// 1 = Top left of the browser screen
// 2 = Top right of the browser screen
// 3 = Bottom left of the browser screen
// 4 = Bottom right of the browser screen
// 5 = Middle of the browser screen
// 6 = Top Middle of the browser screen
// 7 = Right Middle of the browser screen
// 8 = Bottom Middle of the browser screen
// 9 = Left Middle of the browser screen
// Example: var lf_Position = 7;
// In the above example, LiveFaceOnWeb Presentation would be placed at bottom right of browser screen
var lf_Position_Dynamic = 4;
// Below enter the delay lenght in seconds before LiveFaceOnWeb Presentation appears
// Example: var lf_tDLB = 5; 
// In the above example. LiveFaceOnWeb Presentation would appear after 5 seconds
// Example: var lf_tDLB = 0;
// in the above example, LiveFaceOnWeb Presentation would appear instantly
var lf_tDLB = 1;
// Below enter the amount of seconds LiveFaceOnWeb Presentation appears after it has completed playing
// Example: var lf_TimeDelayAfter = 0; 
// In the above example, this option would be disabled
// Example: var lf_tDLA = 5; 
// In the above example, LiveFaceOnWeb Presentation would close 5 seconds after it has completed playing
var lf_tDLA = 5;
// Below select how LiveFaceOnWeb Presentation should appear for the same visitor
// 1 = Play everytime
// 2 = Play ones per visitors session
// 3 = Play in so many days
// Example: var lf_PresPlay = 1; 
// In the above example LiveFaceOnWeb Presentation would play every time
// Example: var lf_PresPlay = 3; and var lf_PresPlayDays = 1; 
// In the above example LiveFaceOnWeb Presentation would play ones every day
var lf_PresPlay = 1;
var lf_PresPlayDays = 0;
// Below enable the Click-On-Me Functionality for LiveFaceOnWeb Presentation
// Example: var lf_cOMW = 1;
// In the above example, Click-On-Me Functionality is enabled for LiveFaceOnWeb Presenation
// Example: var lf_cOMW = 0; 
// In the above example, Click-On-Me Functionality is disabled for LiveFaceOnWeb Presenation
var lf_cOMW = 0;
// Below enter the Click-On-Me Functionality URL
// Example: var lf_cOMURL = 'http://www.livefaceonweb.com'
// In the above example, when a visitor would click on LiveFaceOnWeb Presenation they would be redirected to http://www.livefaceonweb.com
var lf_cOMURL = '';
// Below select Click-On-Me Functionality Window Settings
// Example: var lf_cOMWP = 1;
// In the above example, the redirect window would be opened as a new browser screen
// Example: var lf_cOMWP = 0;
// In the above example, the redirect window would be opened in the same browser screen
var lf_cOMWP = 0;
// Below enable the Fade-In Effect
// Example: var lf_fIE = 1;
// In the above example, Fade-In Effect is enabled on LiveFaceOnWeb Presenation
// Example: var lf_fIE = 0; 
// In the above example, this option would be disabled
var lf_fIE = 0;
// Below enter the duration of Fade-In Effect in seconds
// Example: var lf_fIET = 5;
// In the above example, LiveFaceOnWeb Presenation would Fade-In in 5 seconds
var lf_fIET = 1;
// Below select LiveFaceOnWeb Presenation play status while Fade-In Effect is active
// Example: var lf_fIET = 1;
// In the above example, LiveFaceOnWeb Presenation would play after the Fade-In Effect has completed
// Example: var lf_fIEP = 0;
// In the above example, LiveFaceOnWeb Presenation would play while the Fade-In Effect is occuring
var lf_fIEP = 1;
// Below enable Fade-Out Effect
// Example: var lf_fOE = 1;
// In the above example, Fade-Out Effect is enabled on LiveFaceOnWeb Presenation
// Example: var lf_fOE = 0;
// In the above example, this option would be disabled
var lf_fOE = 1;
// Below enter the duration of Fade-Out Effect in seconds
// Example: var lf_fOET = 5;
// In the above example, LiveFaceOnWeb Presenation would Fade-Out in 5 seconds
var lf_fOET = 1;
// Below enable Play-Button Functionality
// Example: var lf_pBBE = 1;
// In the above example, Play-Button Functionality is enabled on LiveFaceOnWeb Presenation and appear by default first at the bottom left corner
// Example: var lf_pBBE = 0;
// In the above example, this option would be disabled
var lf_pBBE = 0;
// Below enable Play-Button Functionality to appear first on top of the model
// Example: var lf_pBBBOF = 1;
// In the above example, Play-Button Functionality is enabled on LiveFaceOnWeb Presenation and appear first and on top of the model
// Example: var lf_pBBBOF = 0; 
// In the above example, this option would be disabled
var lf_pBBBOF = 0;
// Below enable Play-Button Functionality
// Example: var lf_pBAE = 1;
// In the above example, Play-Button Functionality is enabled on LiveFaceOnWeb Presenation after it has completed and appears by default on the bottom left corner
// Example: var lf_pBAE = 0; disables the Play Button Function after the presentation ends
// In the above example, this option would be disabled
var lf_pBAE = 0;
// Below enable Play-Button Functionality to appear on top of the model after LiveFaceOnWeb Presenation has completed
// Example: var lf_pBAOF = 1;
// In the above example, Play-Button Functionality is enabled on LiveFaceOnWeb Presenation to appear on top of the model after LiveFaceOnWeb Presenation has completed
// Example: var lf_pBAOF = 0;
// In the above example, this option would be disabled
var lf_pBAOF = 0;
// Below enable the Frame-Hold Effect
// Example: var lf_sFRAME = 7; shows the 7th frame while fade in/out, play button, and roll over effects
// In the above example, LiveFaceOnWeb Presentation shows 7th frame while Fade-In/Fade-Out, Play-Button, and Roll-Over-To-Play Effects occur
// Example: var lf_sFRAME = 0;
// In the above example, this option would be disabled
var lf_sFRAME = 0;
// Below enter LiveFaceOnWeb.com Affiliate ID
// Example: var affiliateID = '80961c09';
var lf_AffiliateID = '';
// Below enter the buffer percent of the LiveFaceOnWeb Presentation.
// Example: var lf_Buffer = 10;
// In the above example. LiveFaceOnWeb Presentation will buffer for 10 percent before playing
var lf_Buffer = 10;


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////////  Do not change the parameters below  ///////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

var lf_SWF_File_Width = 240;
var lf_SWF_File_Height = 300;
var lf_ID = '100003859';

var lf_divHorPos = 0;
var lf_divVerPos = 0;
var lfVersion = -1;
var arrNavInfo = [];
var lfString;
var lfParamString;
var divID = 'div' + lf_ID;

arrNavInfo.navprop = navigator.userAgent.toLowerCase();

if (navigator.vendor == 'KDE' || (document.childNodes && (!document.all || navigator.accentColorName) && !navigator.taintEnabled)) {
    arrNavInfo[0] = 'kde';
    arrNavInfo[1] = 'Safari';
} else if (window.opera && document.childNodes) {
    arrNavInfo[0] = 'opr';
    arrNavInfo[1] = 'Opera';
} else if (navigator.appName.indexOf('WebTV') + 1) {
    arrNavInfo[0] = 'wtv';
    arrNavInfo[1] = 'WebTV';
} else if (navigator.product == 'Gecko') {
    arrNavInfo[0] = 'gek';
    arrNavInfo[1] = 'Gecko engine (Mozilla, Netscape 6+ etc.)';
} else if (document.getElementById) {
    arrNavInfo[0] = 'ie';
    arrNavInfo[1] = 'Internet Explorer 5+';
} else {
    arrNavInfo[0] = 'na';
    arrNavInfo[1] = 'an unknown browser';
}
function runLFOW() {
    try {
        if (lf_PresPlay == 1) {
            lf_createCookie('LFOW' + lf_ID, 'on', -1);
            showLFOW_Video();
        } else if (lf_PresPlay == 2) {
            if (lf_readCookie('LFOW' + lf_ID) == 'no') {
                lf_createCookie('LFOW' + lf_ID, 'on');
                showLFOW_Video();
            }
        } else if (lf_PresPlay == 3) {
            if (lf_readCookie('LFOW' + lf_ID) == 'no') {
                lf_createCookie('LFOW' + lf_ID, 'on', lf_PresPlayDays);
                showLFOW_Video();
            }
        }
    } catch (e) {
        showLFOW_Video();
    }
}
if ((lf_Position !== 2) && (arrNavInfo[0] !== 'na')) {
    try {
        if (window.addEventListener) window.addEventListener('load', runLFOW, false);
        else if (window.attachEvent) window.attachEvent('onload', runLFOW);
    } catch (e) {
        runLFOW();
    }
} else {
    runLFOW();
}
function showLFOW_Video() {
    if ((lf_Position !== 2) && (arrNavInfo[0] !== 'na')) {
        try {
            var lfDiv = document.createElement('div');
            if (lfDiv === undefined) {
                lf_Position = 'err';
            } else if (lfDiv == null) {
                lf_Position = 'err';
            } else {
                lfDiv.id = divID;
                lfDiv.style.position = 'absolute';
                lfDiv.style.zIndex = 9900;
                if (lf_Position == 1) {
                    lfDiv.style.left = lf_OffSet_Left_Position + 'px';
                    lfDiv.style.top = lf_OffSet_Top_Position + 'px';
                } else {
                    lfDiv.style.left = 0 + 'px';
                    lfDiv.style.top = 0 + 'px';
                }
                lfDiv.style.height = lf_SWF_File_Height + 'px';
                lfDiv.style.width = lf_SWF_File_Width + 'px';
                lfDiv.style.visibility = 'visible';
            }
        } catch (e) {
            lf_Position = 'err';
        }
    }
    compileLFOW()
    if ((lf_Position !== 2) && (lf_Position !== 'err') && (arrNavInfo[0] !== 'na')) {
        lfDiv.innerHTML = lfString;
        document.body.appendChild(lfDiv);
    } else if (lf_Position !== 2) {
        document.write('<div id="' + divID + '" style="position: absolute;z-index: 9900;left: 0px;top: 0px;height: ' + lf_SWF_File_Height + 'px;width: ' + lf_SWF_File_Width + 'px;">' + lfString + '</div>');
    } else {
        document.write('<div id="' + divID + '" style="position: relative;z-index: 9900;left: ' + lf_OffSet_Left_Position + 'px;top: ' + lf_OffSet_Top_Position + 'px;height: ' + lf_SWF_File_Height + 'px;width: ' + lf_SWF_File_Width + 'px;">' + lfString + '</div>');
    }
    if ((lf_Position !== 1) && lf_Position !== 2) {
        switch (arrNavInfo[0]) {
            case 'ie':
                window.onscroll = ScrollHnd;
                window.onresize = ScrollHnd;
                window.setInterval('ScrollHnd()', 30);
                break;
            case 'gek':
                if (window.onscroll === undefined) {
                    window.setInterval('ScrollHnd()', 30);
                } else {
                    window.onscroll = ScrollHnd;
                    window.onresize = ScrollHnd;
                }
                break;
            case 'opr':
                window.onscroll = ScrollHnd;
                window.onresize = ScrollHnd;
                break;
            case 'kde':
                window.onscroll = ScrollHnd;
                window.onresize = ScrollHnd;
                break;
            case 'wtv':
                if (window.onscroll === undefined) {
                    window.setInterval('ScrollHnd()', 30);
                } else {
                    window.onscroll = ScrollHnd;
                    window.onresize = ScrollHnd;
                }
                break;
        }
        ScrollHnd();
    }
}
function ScrollHnd() {
    var divScroll;
    var wnd;
    var screenWd = 0;
    var screenHt = 0;
    var screenX = 0;
    var screenY = 0;
    var objDoc;
    var divLeft = 0;
    var divTop = 0;

    objDoc = document;
    wnd = window;

    if (objDoc.getElementById) {
        divScroll = objDoc.getElementById(divID);
        if (divScroll.style) {
            divScroll = divScroll.style;
        }
    }
    if (window.innerHeight) {
        screenHt = wnd.innerHeight;
        screenWd = wnd.innerWidth;
    }
    else if (objDoc.documentElement && (objDoc.documentElement.clientWidth || objDoc.documentElement.clientHeight)) {
        screenWd = objDoc.documentElement.clientWidth;
        screenHt = objDoc.documentElement.clientHeight;

    }
    else if (objDoc.body && (objDoc.body.clientWidth || objDoc.body.clientHeight)) {
        screenWd = objDoc.body.clientWidth;
        screenHt = objDoc.body.clientHeight;
    }
    if (window.innerHeight) {
        screenY = wnd.pageYOffset;
        screenX = wnd.pageXOffset;
    }
    else if (objDoc.body && (objDoc.body.scrollLeft || objDoc.body.scrollTop)) {
        screenY = objDoc.body.scrollTop;
        screenX = objDoc.body.scrollLeft;
    }
    else if (objDoc.documentElement && (objDoc.documentElement.scrollLeft || objDoc.documentElement.scrollTop)) {
        screenY = objDoc.documentElement.scrollTop;
        screenX = objDoc.documentElement.scrollLeft;
    }
    divLeft = divLeft + lf_divHorPos;
    divTop = divTop + lf_divVerPos;
    divLeft = divLeft + screenX;
    divTop = divTop + screenY;
    if (lf_Position_Dynamic == 6) {
        divLeft = divLeft + ((screenWd / 2) - (lf_SWF_File_Width / 2));
    } else if (lf_Position_Dynamic == 7) {
        divLeft = divLeft + (screenWd - lf_SWF_File_Width);
        divTop = divTop + ((screenHt / 2) - (lf_SWF_File_Height / 2));
    } else if (lf_Position_Dynamic == 8) {
        divLeft = divLeft + ((screenWd / 2) - (lf_SWF_File_Width / 2));
        divTop = divTop + (screenHt - lf_SWF_File_Height);
    } else if (lf_Position_Dynamic == 9) {
        divTop = divTop + ((screenHt / 2) - (lf_SWF_File_Height / 2));
    } else {
        if (lf_Position_Dynamic == 5) {
            divLeft = divLeft + ((screenWd / 2) - (lf_SWF_File_Width / 2));
            divTop = divTop - ((screenHt / 2) - (lf_SWF_File_Height / 2));
        }
        if ((lf_Position_Dynamic % 2) == 0) {
            divLeft = divLeft + (screenWd - lf_SWF_File_Width);
        }
        if ((lf_Position_Dynamic < 3) == false) {
            divTop = divTop + (screenHt - lf_SWF_File_Height);
        }
    }
    if ((lf_Position_Dynamic == 4) && (arrNavInfo[0] !== 'ie')) {
        divLeft = divLeft - 1;
        divTop = divTop - 1;
    }
    divScroll.left = divLeft + 'px';
    divScroll.top = divTop + 'px';
}
function compileLFOW() {
    lfParamString = 'lfID=' + lf_ID;
    lfParamString += '&cOMW=' + lf_cOMW;
    if (lf_cOMURL == '') {
        lfParamString += '&cOMURL=' + escape('http://www.livefaceonweb.com');
    } else {
        lfParamString += '&cOMURL=' + escape(lf_cOMURL);
    }
    lfParamString += '&cOMWP=' + lf_cOMWP;
    lfParamString += '&tDLB=' + lf_tDLB;
    lfParamString += '&tDLA=' + lf_tDLA;
    lfParamString += '&fIE=' + lf_fIE;
    lfParamString += '&fIET=' + lf_fIET;
    lfParamString += '&fIEP=' + lf_fIEP;
    lfParamString += '&fOE=' + lf_fOE;
    lfParamString += '&fOET=' + lf_fOET;
    lfParamString += '&pBBE=' + lf_pBBE;
    lfParamString += '&pBBBOF=' + lf_pBBBOF;
    lfParamString += '&pBAE=' + lf_pBAE;
    lfParamString += '&pBAOF=' + lf_pBAOF;
    lfParamString += '&sFRAME=' + lf_sFRAME;
    lfParamString += '&pBuffer=' + lf_Buffer;
    if (lf_AffiliateID == '') {
        lfParamString += '&lfAffiliateID=2';
    } else {
        lfParamString += '&lfAffiliateID=' + lf_AffiliateID;
    }
    if (document.location.href == '') {
        lfParamString += '&sURL_Site=none';
    } else {
        lfParamString += '&sURL_Site=' + escape(document.location.href);
    }
    lfString = "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' width='" + lf_SWF_File_Width + "' height='" + lf_SWF_File_Height + "' id='obj" + lf_ID + "' align='middle' codebase='https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,24,0' >";
    lfString += "<param name='movie' value='" + lf_URL + 'lfow.swf' + "' />";
    lfString += "<param name='quality' value='high' />";
    lfString += "<param name='flashvars' value='" + lfParamString + "' />";
    lfString += "<param name='wmode' value='transparent' />";
    lfString += "<param name='allowScriptAccess' value='always' />";
    lfString += "<param name='loop' value='false' />";
    lfString += "<embed src='" + lf_URL + 'lfow.swf' + "' flashvars='" + lfParamString + "' allowScriptAccess='always' quality='high' wmode='transparent' loop='false' width='" + lf_SWF_File_Width + "' height='" + lf_SWF_File_Height + "' name='obj" + lf_ID + "' align='middle' type='application/x-shockwave-flash' pluginspage='https://www.macromedia.com/go/getflashplayer' />";
    lfString += "</object>";
}
function lf_createCookie(strName, strValue, strDays) {
    if (strDays) {
        var date = new Date();
        date.setTime(date.getTime() + (strDays * 24 * 60 * 60 * 1000));
        var expires = '; expires=' + date.toGMTString();
    } else var expires = '';
    document.cookie = strName + '=' + strValue + expires + '; path=/';
}
function lf_readCookie(strName) {
    var strNameplus = strName + '=';
    var arrCk = document.cookie.split(';');
    for (var i = 0; i < arrCk.length; i++) {
        var c = arrCk[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(strNameplus) == 0) return c.substring(strNameplus.length, c.length);
    }
    return 'no';
}

function lf_CNode() {
    var b = document.body;
    var elDiv = document.getElementById(divID);
    if (elDiv) { elDiv.style.visibility = 'hidden'; }
}