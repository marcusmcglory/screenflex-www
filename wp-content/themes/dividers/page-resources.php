<?php
/**
Links Page
*/

add_filter('body_class','my_class_names');
function my_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'narrow';
	$classes[] = 'links';
	// return the $classes array
	return $classes;
}


// remove_action( 'genesis_loop', 'genesis_do_loop' ); //remove the current loop
add_action( 'genesis_loop', 'child_do_bookmarks' ); //loads the child_do_bookmarks() function on the genesis_loop hook
/**displays the site links */
function child_do_bookmarks() {
?>
<div class="bookmarks-gallery">
    <?php $args = array(
    'orderby'          => 'name',
    'order'            => 'ASC',
    'limit'            => -1,
//    'category'         => ,
//    'exclude_category' => ,
//    'category_name'    => ,
    'show_description' => 1,
    'hide_invisible'   => 1,
    'show_updated'     => 0,
    'echo'             => 1,
    'categorize'       => 0,
    'title_li'         => __(''),
//    'title_li'         => __('Bookmarks'),
//    'title_before'     => '<h2>',
//    'title_after'      => '</h2>',
    'between'          => '<br />',
    'category_orderby' => 'name',
    'category_order'   => 'ASC',
    'class'            => 'linkcat',
    'category_before'  => '<li id=%id class=%class>',
    'category_after'   => '</li>' );  
     wp_list_bookmarks($args); ?>
</div>  
<?php

}


remove_action( 'genesis_before_content', 'genesis_do_subnav' );

// Use the Genesis page.php template
genesis(); //the genesis/page.php file just loads this function so might as well save a step

 
 
 


