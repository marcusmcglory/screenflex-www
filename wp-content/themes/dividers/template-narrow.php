<?php

 /*
Template Name: Narrow Single Column Page
*/
 
 //adds new body class for post category
 // Add specific CSS class by filter
add_filter('body_class','my_class_names');
function my_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'narrow';
	// return the $classes array
	return $classes;
}
 
genesis();