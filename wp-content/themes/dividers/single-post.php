<?php


add_filter('genesis_post_meta', 'ss_single_after_entry_widget'); 
function ss_single_after_entry_widget($post_meta) {
    if ( is_active_sidebar('after-entry') ) {
		if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('After Blog Post') ) {
				genesis_widget_area( 'ss-after-entry', array(
					'before' => '<div class="after-entry-widgeted">',
					'after' =>  '</div><!-- end .after-entry-widgeted -->'
				));
		}
    }
    return $post_meta;
}

genesis();
