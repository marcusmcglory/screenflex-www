<?php

add_action( 'genesis_before_loop', 'ss_full_content_specific_categories' );

function ss_full_content_specific_categories() {
	if	( ! is_singular() && genesis_get_option( 'content_archive_thumbnail' ) ) {
		//add_filter( 'genesis_pre_get_option_content_archive_thumbnail', 'ss_no_post_image' );
		add_filter( 'genesis_pre_get_option_content_archive', 'ss_do_full_content' );
		add_filter( 'genesis_pre_get_option_content_archive_limit', 'ss_no_content_limit' );
	}
}

//function ss_no_post_image() {
//	return '0';
//}

function ss_do_full_content() {
	return 'full';
}

function ss_no_content_limit() {
	return 0.1;
}





/**
 * Archive Post Class
 * @since 1.0.0
 *
 * Breaks the posts into three columns
 * @link http://www.billerickson.net/code/grid-loop-using-post-class
 *
 * @param array $classes
 * @return array
 */
function be_archive_post_class( $classes ) {
	$classes[] = 'one-third';
	global $wp_query;
	if( 0 == $wp_query->current_post || 0 == $wp_query->current_post % 3 )
		$classes[] = 'first';
	return $classes;
}
add_filter( 'post_class', 'be_archive_post_class' );



// Customize the post info function, Remove the Post_info
remove_action( 'genesis_before_post_content', 'genesis_post_info' );

add_filter('genesis_post_info', 'post_info_filter');
function post_info_filter($post_info) {
    if ((!is_page()) || (!is_singular())) {
        $post_info = '';
        return $post_info;
    }
}

add_filter('genesis_post_meta', 'ss_post_meta_filter');
function ss_post_meta_filter($post_meta) {
if ('photo' == get_post_type()) {
	if (has_term('before-and-after', 'use')) {
		$post_meta = 'Before And After';
	} else {
		$post_meta = '';	
	}
    
}
return $post_meta;
}



//Customize the Grid Featured Image
/** Change the featured image */
remove_action( 'genesis_post_content', 'genesis_do_post_image' );
add_action('genesis_post_content','ss_post_image', 0) ;
function ss_post_image() {
    if ( ! is_singular() && genesis_get_option( 'content_archive_thumbnail' ) ) {
        $img = genesis_get_image( array( 'format' => 'html', 'size' => genesis_get_option( 'image_size' ), 'attr' => array( 'class' => 'aligncenter post-image' ) ) );
        printf( '<a href="%s" title="Click for More Pictures: %s">%s</a>', get_permalink(), the_title_attribute( 'echo=0' ), $img );
    }
}




// Edit the read more link text
add_filter('get_the_content_more_link', 'custom_read_more_link');
add_filter('the_content_more_link', 'custom_read_more_link');
function custom_read_more_link() {
//    return '<br /><a class="more-link" href="' . get_permalink() . '" rel="nofollow">[More Pictures&rarr;]</a>';
	return '';
}


add_action('genesis_loop', 'ss_add_photo_dropdown', 0);
function ss_add_photo_dropdown() {
    if( function_exists('the_dropdown_taxonomy') ) :?> 
        <h4 class="use">Sort By Use / Facility:  <?php the_dropdown_taxonomy('use'); ?></h4><?php
    endif;			
    
}

remove_action( 'genesis_before_content', 'genesis_do_subnav' );



genesis();