<?php 
 /*
Template Name: Video Single
*/
 
 //adds new body class for post category
 // Add specific CSS class by filter
add_filter('body_class','my_class_names');
function my_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'video-gallery-single';
	// return the $classes array
	return $classes;
}


add_action('genesis_after_content', 'custom_video_page_footer_widget'); 
function custom_video_page_footer_widget() {
    if ( is_active_sidebar('video_gallery_footer') ) : ?>
        <div id="frontpage-footer-widgeted">
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Video Gallery Footer') ) : ?>
	    	<?php endif; ?> 
	   </div><!-- end #footer-widgeted -->
    <?php endif; 
}


remove_action( 'genesis_before_content', 'genesis_do_subnav' );


/** Add Link Tracking code for Form **/
add_action( 'wp_enqueue_scripts', 'ss_child_add_custom_page_script' );
function ss_child_add_custom_page_script() {
               wp_enqueue_script( 'email-link', CHILD_URL . '/js/build-email-link.js', array('jquery'), '1.2', false);           
           
}  


genesis();