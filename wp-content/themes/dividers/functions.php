<?php
/**
 * This file controls customisations to the Dividers Child Theme.
 *
 * @author Scott Stawarz
 * @package Dividers
 */

/**
 * Include Genesis theme files
 */
 require_once(get_template_directory().'/lib/init.php');

add_editor_style();
 
 /** Child theme (do not remove) */
define( 'CHILD_THEME_NAME', 'Dividers' );
define( 'CHILD_THEME_URL', 'http://www.studiopress.com/themes/genesis' );

if ( ! isset( $content_width ) )
    $content_width = 800;

/** Add Viewport meta tag for mobile browsers */
add_action( 'genesis_meta', 'add_viewport_meta_tag' );
function add_viewport_meta_tag() {
    echo '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>';
}

/** Add support for custom background */
add_custom_background();

/** Add support for custom header */
add_theme_support( 'genesis-custom-header', array( 'width' => 718, 'height' => 82 ) );

/** Add support for 3-column footer widgets */
add_theme_support( 'genesis-footer-widgets', 3 );


genesis_register_sidebar(array(
	'name'=>'Front Page Footer',
	'description' => __('This is the Large Footer on the Front Page.', 'dividers'),
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));

genesis_register_sidebar(array(
	'id' => 'video_gallery_footer',
	'name'=>'Video Gallery Footer',
	'description' => __('This is the Large Footer on the Video Gallery Page.', 'dividers'),
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));

genesis_register_sidebar(array(
	'name'=>'FREEstanding Footer',
	'description' => __('This is the Large Footer on the FREEstanding Pages.', 'dividers'),
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));

genesis_register_sidebar(array(
	'name'=>'Special Use Footer',
	'description' => __('This is the Large Footer on the Special Use Pages.', 'dividers'),
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));


genesis_register_sidebar(array(
	'id' => 'ss-after-entry',
	'name'=>'After Blog Post',
	'description' => __('This is the Widget After Entry.', 'dividers'),
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));


// Reposition the footer
remove_action('genesis_footer', 'genesis_footer_markup_open', 5);
remove_action('genesis_footer', 'genesis_do_footer');
remove_action('genesis_footer', 'genesis_footer_markup_close', 15);
add_action('genesis_after', 'genesis_footer_markup_open', 5);
add_action('genesis_after', 'genesis_do_footer');
add_action('genesis_after', 'genesis_footer_markup_close', 15);


//Adding custom navigation area 
register_nav_menus( array(
	'footernav' => __('Custom Menu below footer', 'genesis')
) );

 //And put it below the footer in the theme
add_action('genesis_after_footer', 'genesis_do_footernav');



function genesis_do_footernav() {
    $locations = get_nav_menu_locations();
    if ($locations['footernav'] != 0) {

        if ( has_nav_menu( 'footernav' ) ) {

            $args = array(
                'theme_location' => 'footernav',
                'container' => '',
                'menu_class' => genesis_get_option('nav_superfish') ? 'nav superfish' : 'nav',
                'fallback_cb' => '', // doesn't work here
                'echo' => 0
            );

            $abovenav = wp_nav_menu( $args );

        }

        $abovenav_output = sprintf( '<div id="footernav">%2$s%1$s%3$s</div>', $abovenav, genesis_structural_wrap( 'footernav', 'open', 0 ), genesis_structural_wrap( 'footernav', 'close', 0 ) );

        echo apply_filters( 'genesis_do_footernav', $abovenav_output, $abovenav, $args );
        }
    }

remove_action('genesis_after_header', 'genesis_do_subnav');
add_action('genesis_before_content', 'genesis_do_subnav');

//Clean-up Div for ubermenu
//add_filter( 'genesis_do_nav', 'genesis_child_nav', 10, 3 );
//add_filter( 'genesis_do_subnav', 'genesis_child_nav', 10, 3 );
//function genesis_child_nav($nav_output, $nav, $args){
//    return $nav;
//}




// Move breadcrumbs
remove_action('genesis_before_loop', 'genesis_do_breadcrumbs');
add_action('genesis_before_content_sidebar_wrap', 'genesis_do_breadcrumbs');


add_filter( 'genesis_breadcrumb_args', 'child_breadcrumb_args' );

function child_breadcrumb_args( $args ) {
     $args['sep']  = ' &raquo ';
    return $args;
}

//hook into the init action and call create_taxonomies when it fires
add_action( 'init', 'create_photo_taxonomies', 0 );

function create_photo_taxonomies() 
{

  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Uses', 'taxonomy general name' ),
    'singular_name' => _x( 'Use', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Use' ),
    'popular_items' => __( 'Popular Uses' ),
    'all_items' => __( 'All Uses' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Use' ), 
    'update_item' => __( 'Update Use' ),
    'add_new_item' => __( 'Add New Use' ),
    'new_item_name' => __( 'New Use' ),
    'separate_items_with_commas' => __( 'Separate Uses with commas' ),
    'add_or_remove_items' => __( 'Add or remove Uses' ),
    'choose_from_most_used' => __( 'Choose from the most used Uses' ),
    'menu_name' => __( 'Uses' )
  ); 

  register_taxonomy('use','photo',array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_tagcloud' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'photos/use', 'with_front' => false )
  ));  
  
  
  $labels = array(
    'name' => _x( 'Colors', 'taxonomy general name' ),
    'singular_name' => _x( 'Color', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Colors' ),
    'popular_items' => __( 'Popular Colors' ),
    'all_items' => __( 'All Colors' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Color' ), 
    'update_item' => __( 'Update Color' ),
    'add_new_item' => __( 'Add New Color' ),
    'new_item_name' => __( 'New Color Name' ),
    'separate_items_with_commas' => __( 'Separate colors with commas' ),
    'add_or_remove_items' => __( 'Add or remove colors' ),
    'choose_from_most_used' => __( 'Choose from the most used colors' ),
    'menu_name' => __( 'Colors' )
  ); 

  register_taxonomy('color','photo',array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_tagcloud' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'photos/color', 'with_front' => false )
  ));
  
  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Heights', 'taxonomy general name' ),
    'singular_name' => _x( 'Height', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Heights' ),
    'popular_items' => __( 'Popular Heights' ),
    'all_items' => __( 'All Heights' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Height' ), 
    'update_item' => __( 'Update Height' ),
    'add_new_item' => __( 'Add New Height' ),
    'new_item_name' => __( 'New Height' ),
    'separate_items_with_commas' => __( 'Separate heights with commas' ),
    'add_or_remove_items' => __( 'Add or remove heights' ),
    'choose_from_most_used' => __( 'Choose from the most used heights' ),
    'menu_name' => __( 'Heights' )
  ); 

  register_taxonomy('height','photo',array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_tagcloud' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'photos/height', 'with_front' => false )
  ));
  
    // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Lengths', 'taxonomy general name' ),
    'singular_name' => _x( 'Length', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Length' ),
    'popular_items' => __( 'Popular Lengths' ),
    'all_items' => __( 'All Lengths' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Length' ), 
    'update_item' => __( 'Update Length' ),
    'add_new_item' => __( 'Add New Length' ),
    'new_item_name' => __( 'New Length' ),
    'separate_items_with_commas' => __( 'Separate Lengths with commas' ),
    'add_or_remove_items' => __( 'Add or remove Lengths' ),
    'choose_from_most_used' => __( 'Choose from the most used Lengths' ),
    'menu_name' => __( 'Lengths' )
  ); 

  register_taxonomy('length','photo',array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_tagcloud' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'photos/length', 'with_front' => false )
  ));  
  

 };
 
 function post_type_photos() {
    register_post_type('photo',
        array(
            'labels' => array(
                'name' => __('Photos')
                ,'singular_name' => __('Photo')
                ,'add_new' => __('Add New Photo')
                ,'add_new_item' => __('Add New Photo')
                ,'edit' => __('Edit')
                ,'edit_item' => __('Edit Photo')
                ,'new_item' => __('New Photo')
                ,'view' => __('View Photo')
                ,'view_item' => __('View Photo')
                ,'search_items' => __('Search Photos')
                ,'not_found' => __('No Photos found')
                ,'not_found_in_trash' => __('No Photos found in Trash')
                ,'parent' => __('Parent Photo')
                )
            ,'singular_label' => __('Photo')
            ,'public' => true
            ,'publicly_queryable' => true
            ,'show_ui' => true
            ,'capability_type' => 'post'
            ,'hierarchical' => 'false'
            ,'menu_name' => 'Photos'
            ,'menu_position' => 5
            ,'menu_icon' => get_stylesheet_directory_uri() . '/images/photo-icon.gif'
            , 'query_var' => true
            , 'rewrite' => array('slug' => 'photos',  'with_front' => false)
            , 'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields', 'comments', 'revisions', 'page-attributes', 'genesis-seo','genesis-cpt-archives-settings')
            ,'has_archive' => 'photos'
            ,'taxonomies' => array('use','color','height','length')
            )
        );
   /* add_post_type_support( 'photo', 'post-formats' ); */
 };
 
add_action('init', 'post_type_photos');
 //set up Custom Photo Gallery Types

  
  function post_type_plans() {
    register_post_type('plans',
        array(
            'labels' => array(
                'name' => __('Plans')
                ,'singular_name' => __('Plan')
                ,'add_new' => __('Add New Plan')
                ,'add_new_item' => __('Add New Plan')
                ,'edit' => __('Edit')
                ,'edit_item' => __('Edit Plan')
                ,'new_item' => __('New Plan')
                ,'view' => __('View Plan')
                ,'view_item' => __('View Plan')
                ,'search_items' => __('Search Plans')
                ,'not_found' => __('No Plans found')
                ,'not_found_in_trash' => __('No Plans found in Trash')
                ,'parent' => __('Parent Plan')
                )
            ,'singular_label' => __('Plan')
            ,'public' => true
            ,'publicly_queryable' => true
            ,'show_ui' => true
            ,'capability_type' => 'post'
            ,'hierarchical' => 'false'
            ,'menu_name' => 'Plans'
            ,'menu_position' => 6
            ,'menu_icon' => get_stylesheet_directory_uri() . '/images/polaroid-icon.png'
//            , 'taxonomies' => array('')
            , 'query_var' => true
         , 'rewrite' => array('slug' => 'plans',  'with_front' => false)
            , 'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields', 'comments', 'revisions', 'page-attributes', 'genesis-seo','genesis-cpt-archives-settings' )
            ,'has_archive' => 'plans'
        ) 
    );
  }
  
add_action('init', 'post_type_plans');



function create_plan_taxonomies() {
   // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Rooms', 'taxonomy general name' ),
    'singular_name' => _x( 'Room Quantity', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Room Quantities' ),
    'popular_items' => __( 'Popular Room Quantities' ),
    'all_items' => __( 'All Rooms' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Room' ), 
    'update_item' => __( 'Update Room' ),
    'add_new_item' => __( 'Add New Room' ),
    'new_item_name' => __( 'New Room' ),
    'separate_items_with_commas' => __( 'Separate Rooms with commas' ),
    'add_or_remove_items' => __( 'Add or remove Rooms' ),
    'choose_from_most_used' => __( 'Choose from the most used Rooms' ),
    'menu_name' => __( 'Rooms' )
  ); 

  register_taxonomy('rooms','plans',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_tagcloud' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'plans/rooms' )
  ));  

}

add_action( 'init', 'create_plan_taxonomies', 0 );



add_action( 'init', 'register_cpt_review' );

function register_cpt_review() {

    $labels = array( 
        'name' => _x( 'Reviews', 'reviews' ),
        'singular_name' => _x( 'Review', 'review' ),
        'add_new' => _x( 'Add New', 'review' ),
        'add_new_item' => _x( 'Add New Review', 'review' ),
        'edit_item' => _x( 'Edit Review', 'review' ),
        'new_item' => _x( 'New Review', 'review' ),
        'view_item' => _x( 'View Review', 'review' ),
        'search_items' => _x( 'Search Reviews', 'review' ),
        'not_found' => _x( 'No reviews found', 'review' ),
        'not_found_in_trash' => _x( 'No reviews found in Trash', 'review' ),
        'parent_item_colon' => _x( 'Parent Review:', 'review' ),
        'menu_name' => _x( 'Reviews', 'reviews' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Reviews and Testimonials are unedited comments and feedback from Screenflex Room Divider customers.  ',
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', 'revisions', 'page-attributes', 'genesis-seo','genesis-cpt-archives-settings'  ),
        'taxonomies' => array( 'category', 'post_tag' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 20,
        'menu_icon' => get_stylesheet_directory_uri() . '/images/megaphone-red.png',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => 'reviews',
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'page'
    );

    register_post_type( 'review', $args );
}









/** Add new image sizes **/
add_image_size('grid-thumbnail', 100, 100, TRUE);
add_image_size('Featured', 700, 250, false);
add_image_size('big-grid-thumbnail', 260, 260, TRUE);



// Customize the post meta function
add_filter('genesis_post_meta', 'post_meta_filter');
function post_meta_filter($post_meta) {
if ('photo' == get_post_type()) {
    $post_meta = '[post_terms taxonomy="use" before="Use: "] [post_terms taxonomy="length" before="Length: "] [post_terms taxonomy="height" before="Height: "] [post_terms taxonomy="color" before="Color: "] ';
	if (is_post_type_archive('photo') ){
		$post_meta = '';	
	}
}
elseif ('plans' == get_post_type()) {
	$post_meta = '[post_terms taxonomy="rooms" before="Rooms: "]';
} else {
	$post_meta = '[post_categories] [post_tags]';	
}
return $post_meta;
}







add_filter( 'excerpt_length', 'sp_excerpt_length' );
function sp_excerpt_length( $length ) {
	if( is_post_type_archive('photo') || is_tax()) {
    	if( $query->is_main_query() && $query->is_archive() ) {
	return 0; // pull first 50 words
	}
	}
}


// Modify credits section
add_filter('genesis_footer_creds_text', 'custom_footer_creds_text');
function custom_footer_creds_text($creds) {
    $creds = '[footer_copyright] ' . get_bloginfo('name') . '<br />The World&#39;s Most Versatile Room Dividers&trade;';
    return $creds;
}

add_action('genesis_after_footer', 'do_live_chat');
function do_live_chat(){
	if (("https://www.dealersf.com/" != WP_SITEURL)) {
?>
<div id="livechat" onclick="void(0);" class="not-dealersf">
<div id="room-divider-live-chat-text">
<!-- Begin PHP Live! HTML Code -->
<span id="phplive_btn_1377542106" onclick="phplive_launch_chat_0(0)" >L I V E &nbsp; C H A T</span>
<script type="text/JavaScript">
(function() {
	var phplive_e_1377542106 = document.createElement("script") ;
	phplive_e_1377542106.type = "text/javascript" ;
	phplive_e_1377542106.async = true ;
	phplive_e_1377542106.src = "//www.screenflex.com/phplive/js/phplive_v2.js.php?v=0|1377542106|0|L%20I%20V%20E%20%26nbsp%3B%20C%20H%20A%20T" ;
	document.getElementById("phplive_btn_1377542106").appendChild( phplive_e_1377542106 ) ;
})();
</script>
<!-- End PHP Live! HTML Code -->
</div>
</div>
<div id="tab-container" class="not-dealersf">
<div id="tab-info-request">
<div id="tab-info-request-text">
<a href="/how-to-buy/?location=right-float-tab" class="price-tab">
P R I C E &nbsp; G U I D E
</a>
<div class="tab-info-form" >
<?php gravity_form(1, true, false, false, '', false, 100); ?>


<div id="faces-phone">
<h2 style="text-align:center;">Or Call Us Now At: 800.553.0110</h2>
<div id="attachment_51785" style="width: 25%" class="wp-caption alignleft"><a href="mailto:donald@screenflex.com?subject=Price%20and%20Product%20Guide%20Request" class="price-guide-email" id="price-guide-mailto-image-don" onclick="recordEmailLink(this, 'Price Guide Link', 'mailto-click', 'donald');return false;"><img src="https://www.screenflex.com/wp-content/uploads/2011/12/Don-Austin.jpg" alt="Donald Austin" width="100%"  class="size-full wp-image-51785" scale="0" data-lazy-loaded="true" style="display: inline;"></a><p class="wp-caption-text">Donald Austin<br>Manager<br>Ext #114<br><a class="price-guide-email" id="price-guide-mailto-don" onclick="recordEmailLink(this, 'Price Guide Link', 'mailto-click', 'donald');return false;" href="mailto:donald@screenflex.com?subject=Price%20and%20Product%20Guide%20Request">Email Me</a></p></div><div id="attachment_51784" style="width: 25%" class="wp-caption alignleft"><a href="mailto:kim@screenflex.com?subject=Price%20and%20Product%20Guide%20Request" onclick="recordEmailLink(this, 'Price Guide Link', 'mailto-click', 'kim');return false;" class="price-guide-email" id="price-guide-mailto-image-kim"><img src="https://www.screenflex.com/wp-content/uploads/2011/12/Kim-Abraham.jpg" alt="Kim Abraham" width="100%"  class="size-full wp-image-51784" scale="0" data-lazy-loaded="true" style="display: inline;"></a><p class="wp-caption-text">Kim Abraham<br>Consultant<br>Ext #122<br><a class="price-guide-email" id="price-guide-mailto-kim" onclick="recordEmailLink(this, 'Price Guide Link', 'mailto-click', 'kim');return false;" href="mailto:kim@screenflex.com?subject=Price%20and%20Product%20Guide%20Request">Email Me</a></p></div><div id="attachment_51786" style="width: 25%" class="wp-caption alignleft"><a href="mailto:lourdes@screenflex.com?subject=Price%20and%20Product%20Guide%20Request" class="price-guide-email" id="price-guide-mailto-image-lourdes" onclick="recordEmailLink(this, 'Price Guide Link', 'mailto-click', 'lourdes');return false;"><img src="https://www.screenflex.com/wp-content/uploads/2011/12/Lourdes-Morales.jpg" alt="Lourdes Morales" width="100%"  class="size-full wp-image-51786" scale="0" data-lazy-loaded="true" style="display: inline;"></a><p class="wp-caption-text">Lourdes Morales<br>Consultant<br>Ext #123<br><a class="price-guide-email" id="price-guide-mailto-lourdes" onclick="recordEmailLink(this, 'Price Guide Link', 'mailto-click', 'lourdes');return false;" href="mailto:lourdes@screenflex.com?subject=Price%20and%20Product%20Guide%20Request">Email Me</a></p></div></div>
</div>
</div>
</div>
</div>
<?php
}
}


add_filter( 'get_the_content_limit_allowedtags', 'get_the_content_limit_custom_allowedtags' );
/**
 * Add tags to Genesis content limit in content archives.
 *
 * @return string tags
 */
function get_the_content_limit_custom_allowedtags() {
	// Add custom tags to this string
	if (!is_post_type_archive( 'photo' )) {
		return '<script>,<style>,<br>,<p>,<em>,<strong>,<div>,<i>,<a>,<dl>,<dt>,<dd>'; 
	}
}




/** Add support for post formats */ 
add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video' ) ); 
add_theme_support( 'genesis-post-format-images' ); 


/** Remove elements for post formats */ 
add_action( 'genesis_before_post', 'marketing_remove_elements' ); 
function marketing_remove_elements() { 
     
    if ( ! current_theme_supports( 'post-formats' ) ) 
        return; 

    // Remove if post has format 
    if ( get_post_format() ) { 
        remove_action( 'genesis_post_title', 'genesis_do_post_title' ); 
        remove_action( 'genesis_before_post_content', 'genesis_post_info' ); 
        remove_action( 'genesis_after_post_content', 'genesis_post_meta' ); 
    } 

    // Add back, as post has no format 
    else { 
        add_action( 'genesis_post_title', 'genesis_do_post_title' ); 
        add_action( 'genesis_before_post_content', 'genesis_post_info' ); 
        add_action( 'genesis_after_post_content', 'genesis_post_meta' ); 
    } 

}  



// Modify Footer's back to top text
//remove_filter('genesis_footer_backtotop_text', 'genesis_footer_backtotop_text');
//add_filter('genesis_footer_backtotop_text', 'custom_footer_backtotop_text');
function custom_footer_backtotop_text($backtotop) {
    $backtotop = '<a id="gotop" href="#wrap" rel="nofollow" class="scroll">'. __( "Return to top of page", "genesis" ). '</a>';
    $backtotop .= '<script>
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({
        pageLanguage: \'en\',
        gaTrack: true,
        gaId: \'UA-395272-1\'
      });
    }
    </script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>';
    return $backtotop;
}







add_filter("gform_post_submission", "hide_form", 10, 2);
function hide_form($entry, $form) {
    if($form["confirmation"]["type"] != "message")
        echo '
<style type="text/css"> body { display: none; } </style>

';
}



remove_action( 'genesis_doctype', 'genesis_do_doctype' );
add_action( 'genesis_doctype', 'ss_child_do_doctype' );

function ss_child_do_doctype() {
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head profile="https://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
<?php
}



add_filter("gform_validation_message", "ss_change_message", 10, 2);
function ss_change_message($message, $form){
    switch($form['id']){
        case 5:
        case 4:
        case 3:
        case 1:
            return "<div class='error_message'>Please submit your choice of email address or phone number.  Thank you.</div>";
            break;
        default:
            return "<div class='error_message'>There was a problem with your submission. Errors have been highlighted below.</div>";
            break;
    }

}



//Require a Phone or Email address for the following forms
add_filter('gform_validation_23', 'ss_validate_phone_or_email');
add_filter('gform_validation_22', 'ss_validate_phone_or_email');
add_filter('gform_validation_10', 'ss_validate_phone_or_email'); 
add_filter('gform_validation_5', 'ss_validate_phone_or_email'); 
add_filter('gform_validation_4', 'ss_validate_phone_or_email'); 
add_filter('gform_validation_3', 'ss_validate_phone_or_email'); 
add_filter('gform_validation_1', 'ss_validate_phone_or_email');   


function ss_validate_phone_or_email($validation_result){
        
     //  - Get the current form
     $form = $validation_result["form"];
     
     //  - Get the current page being validated
     $current_page = rgpost('gform_source_page_number_' . $form['id']) ? rgpost('gform_source_page_number_' . $form['id']) : 1;
    
     //   - Initalize Count for the quantity of fields found
     $count = 0;
     
     //  - Loop through the form fields
     foreach($form['fields'] as &$field){
     
     
        //  - If the field does not have our designated CSS class, skip it  Logically, if we are NOT phone or NOT email, skip it.
        if((strpos($field['cssClass'], 'request-phone') === FALSE) && (strpos($field['cssClass'], 'request-email') === FALSE)) {
            continue;
        }
        
        //  - Get the field's page number
        $field_page = $field['pageNumber'];
                
        //  - Check if the field is hidden by Gravity Forms conditional logic
        $is_hidden = RGFormsModel::is_field_hidden($form, $field, array());
        
        //  - If the field is not on the current page OR if the field is hidden, skip it
        if($field_page != $current_page || $is_hidden)
            continue;      
        
        //  - Get the submitted value from the $_POST
        $field_value = rgpost("input_{$field['id']}");
        
        //  - If the field is not empty, skip it.
        if(empty($field_value)== false){
            continue;
        }
            
        //  - Set the validation result as invalid
        $is_valid = false;
        
        //  - The field failed validation, so first we'll need to fail the validation for the entire form
        $validation_result['is_valid'] = $is_valid;
        
        //  - Mark the specific field that failed and add a custom validation message
        $field['failed_validation'] = true;
        $field['validation_message'] = 'Either a Phone Number <strong>OR</strong> an Email Address is required.';
        
        //  - Increment our count
        $count++;
     }
     
     //  - If our count is less than two, then one of our fields was valid, and there is no need to mark the entire form invalid.
     if($count < 2){
        $validation_result['is_valid'] = true;
     }
     
     //  - Assign our modified $form object back to the validation result
     $validation_result['form'] = $form;

    //  - Return the validation result
    return $validation_result;
    
}


add_filter("manage_upload_columns", 'upload_columns');
add_action("manage_media_custom_column", 'media_custom_columns', 0, 2);

function upload_columns($columns) {

	unset($columns['parent']);
	$columns['better_parent'] = "Parent";

	return $columns;

}
 function media_custom_columns($column_name, $id) {

	$post = get_post($id);

	if($column_name != 'better_parent')
		return;

		if ( $post->post_parent > 0 ) {
			if ( get_post($post->post_parent) ) {
				$title =_draft_or_post_title($post->post_parent);
			}
			?>
			<strong><a href="<?php echo get_edit_post_link( $post->post_parent ); ?>"><?php echo $title ?></a></strong>, <?php echo get_the_time(__('Y/m/d')); ?>
			<br />
			<a class="hide-if-no-js" onclick="findPosts.open('media[]','<?php echo $post->ID ?>');return false;" href="#the-list"><?php _e('Re-Attach'); ?></a></td>

			<?php
		} else {
			?>
			<?php _e('(Unattached)'); ?><br />
			<a class="hide-if-no-js" onclick="findPosts.open('media[]','<?php echo $post->ID ?>');return false;" href="#the-list"><?php _e('Attach'); ?></a>
			<?php
		}

}



// ADD NEW COLUMN
function ST4_columns_head($defaults) {
	$defaults['featured_image'] = 'Featured Image';
	return $defaults;
}

// SHOW THE FEATURED IMAGE
function ST4_columns_content($column_name, $post_ID) {
	if ($column_name == 'featured_image') {
    	the_post_thumbnail('thumbnail'); 
	}
}

add_filter('manage_posts_columns', 'ST4_columns_head');
add_action('manage_posts_custom_column', 'ST4_columns_content', 10, 2);

add_filter('manage_pages_columns', 'ST4_columns_head');
add_action('manage_pages_custom_column', 'ST4_columns_content', 10, 2); 





/**
 * Add custom script to a certain  pages.
 */
add_action( 'wp_enqueue_scripts', 'child_add_custom_page_script' );
function child_add_custom_page_script() {
//        if(is_page()) { }
           wp_enqueue_script( 'pagescript', CHILD_URL . '/js/portable-partitions.js', array('jquery'), '3.3', true);
           wp_enqueue_script( 'email-link', CHILD_URL . '/js/build-email-link.js', array('jquery'), '1.2', false);           
           
           //'home',
           
           if(is_page(array( 'home-lfow'))) {
              wp_register_script( 'lfow-home', '/playLFOW.js', false, '1.0', true);
              wp_enqueue_script( 'lfow-home', '/playLFOW.js', false, '1.0', true);
            }
           
           
           if(is_page('colors')) {
               wp_enqueue_script( 'colors', CHILD_URL . '/js/colors.js', array('jquery'), '1', true);
               wp_enqueue_script( 'byord', CHILD_URL . '/js/BYRD.2014.01.08.color.js', array('jquery'), '3.4', true);            
           }
           
            if(is_page(array('sizes','find-your-room-divider'))) {
               wp_enqueue_script( 'sizes', CHILD_URL . '/js/sizes.js', array('jquery'), '1.', true);  
               wp_enqueue_script( 'byord', CHILD_URL . '/js/BYRD.2014.01.08.sizes.js', array('jquery'), '3.5', true);    
           }
            
           if(is_page(array('virtual-showroom', 'current-offers'))) {
              wp_register_script('google_swfobject', 'https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js', array('jquery'), '2.2', false);
               wp_enqueue_script( 'google_swfobject', 'https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js', array('jquery'), '2.2', false);         
			   wp_enqueue_script( 'showroom', CHILD_URL . '/js/load-showroom.js', array('jquery', 'google_swfobject'), '2.6.3', false);                
            }
            
            if(is_page('room-dividers-portable-walls')) {
                wp_enqueue_script( 'products', CHILD_URL . '/js/products.js', array('jquery'), '1', true);            
            }
            
            if(is_page('features')) {
                wp_enqueue_script( 'features', CHILD_URL . '/js/features.js', array('jquery'), '1', true);            
            }
            
}  


/**
* Add theme support for infinite scroll
*
* @uses add_theme_support
* @action after_setup_theme
* @return null
*/

/**
 * Enables Jetpack's Infinite Scroll in search pages, disables it in reviews archives & Blog Posts.  
 *  Add this back to the is_archive || is_search() ||  is_home()  to enable infinite scroll for Blog Posts.
 * @return bool
 */
function ss_theme_jetpack_infinite_scroll_supported() {
	return current_theme_supports( 'infinite-scroll' ) && ( is_archive() || is_search() ) && (! is_post_type_archive( 'review' ));
}
add_filter( 'infinite_scroll_archive_supported', 'ss_theme_jetpack_infinite_scroll_supported' );



add_action( 'after_setup_theme', 'ss_infinite_scroll_init' );
function ss_infinite_scroll_init() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'content',
		'footer' => 'footer',
		'render' => 'genesis_do_loop',
		'wrapper' => true,
		'posts_per_page' => 18
	) );
}



add_action( 'pre_get_posts', 'ss_prefix_reverse_post_order' );
/**
 * Reverse Post Order for Post Archives
 * on the Genesis Framework
 *
 */
function ss_prefix_reverse_post_order( $query ) {
	if ( is_admin() )
		return;
	
	// Only change the query for post archives.
	if ( $query->is_main_query() && is_archive() && is_post_type_archive('plans') ) {
		$query->set( 'orderby', 'date' );
		$query->set( 'order', 'ASC' );
	}
}



//add_action( 'pre_get_posts', 'be_change_event_posts_per_page' );
/**
 * Change Posts Per Page for Event Archive
 * 
 * @author Bill Erickson
 * @link http://www.billerickson.net/customize-the-wordpress-query/
 * @param object $query data
 *
 */
//function be_change_event_posts_per_page( $query ) {
	
//	if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'photo' ) ) {
//		$query->set( 'posts_per_page', '27' );
//	}

 
//}




//** Add support to filter specific pages from the Search Box **/

 function filter_where($where = '') {
	if ( is_search() ) {
		$exclude = array(15373,4708,4691,6300,6910,6976,6980,6986,38873,39322,39950);	

		for($x=0;$x<count($exclude);$x++){
		  $where .= " AND ID != ".$exclude[$x];
		}
	}
	return $where;
}
add_filter('posts_where', 'filter_where');

/** Add custom body class to the head */
add_filter( 'body_class', 'ss_add_body_class' );
function ss_add_body_class( $classes ) {
   if ("https://www.dealersf.com/" == WP_SITEURL) {
	array_push($classes, 'dealersf');
   }
   return $classes;
}
