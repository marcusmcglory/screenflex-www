jQuery(document).ready(function($) { 
    var feature = "";

    
    $(".highlight-button").hover(
        function () {
            feature  = "#" + $(this).attr("id");
            $(".highlight-button").stop().animate({opacity:0}, 500);
            $(feature).stop().animate({opacity:1}, 500).css('background-position', '0px -80px');
            $(feature+ "-content").appendTo("#main-top-description").stop().animate({opacity:0}, 000).animate({opacity:1}, 500);
//            $("#main-feature").fadeIn("slow");

        }, function () {
            feature  = "#" + $(this).attr("id");
            $(feature).css('background-position', '0px 0px');
            
            $(feature+ "-content").insertBefore(feature+ "-footer").stop().animate({opacity:0}, 000).animate({opacity:1}, 500);
            $(".highlight-button").stop().animate({opacity:1}, 500);
            
        }
    );

    
    
});