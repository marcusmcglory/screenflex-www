jQuery(document).ready(function($) { 
    var feature = "";
    
    $(".highlight-button").hover(
        function () {
            feature  = "#" + $(this).attr("id");
            $(".highlight-button").fadeOut("slow");
            $(feature).fadeIn("slow").css('background-position', '0px -80px');
            $(feature+"-description").fadeIn("slow"); 
        
        }, function () {
            feature  = "#" + $(this).attr("id");
            $(feature).css('background-position', '0px 0px');
            $(feature+"-description").fadeOut("fast"); 
            $(".highlight-button").fadeIn("slow");
        }
    );

    
    
});