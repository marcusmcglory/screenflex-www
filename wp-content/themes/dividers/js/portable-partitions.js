jQuery(document).ready(function($) {

    // $() will work as an alias for jQuery() inside of this function
    
        $(".scroll").click(function(event){
        //prevent the default action for the click event
        event.preventDefault();
 
        //get the full url - like mysitecom/index.htm#home
        var full_url = this.href;
 
        //split the url by # and get the anchor target name - home in mysitecom/index.htm#home
        var parts = full_url.split("#");
        var trgt = parts[1];
 
        //get the top offset of the target anchor
        var target_offset = $("#"+trgt).offset();
        var target_top = target_offset.top;
 
        //goto that anchor by setting the body scroll top to anchor top
        $('html, body').animate({scrollTop:target_top}, 500);
    });
    


    $("a#moregrid").click(function(event){
        event.preventDefault();
        $(this).text($(this).text() == '↓ Expand ↓' ? '↑ Collapse ↑' : '↓ Expand ↓');
        $('div.grid-details').slideToggle('slow','linear'); 
    });
    
 
 
 
/* 
      var popupStatus = 0;
    //this code will load popup with jQuery magic!
    var loadPopup = function (){
        //loads popup only if it is disabled
        if(0 == popupStatus){
            $("#livechat").show().css({
                "opacity": "1.0"
                , "width": "200px"
            });
            $("#popupClose").show();
            popupStatus = 1;
        }
    };

    //This code will disable popup when click on x!
    function disablePopup(){
        //disables popup only if it is enabled
        if(popupStatus==1){
            $("#livechat").show().css({
                "opacity": "1.0"
                , "width": "25px"
            });
            $("#popupClose").hide();
            popupStatus = 0;
        }
    }


    $(window).bind('load', function() {

        if ($.cookie("sf-live-chat-popup") != 1) {    
        
       //     setTimeout(loadPopup(),150000);	  
        } 
    
    }); 
          
    //CLOSING POPUP
    //Click the x event!
    $("#popupClose").click(function(){
        disablePopup();
        $.cookie("sf-live-chat-popup", "1", { expires: 7 });
    });
    
    
       
*/

// Find all YouTube videos
var $allVideos = $("iframe[src^='https://www.youtube.com']"),

    // The element that is fluid width
    $fluidEl = $("#content");

// Figure out and save aspect ratio for each video
$allVideos.each(function() {

  $(this)
    .data('aspectRatio', this.height / this.width)

    // and remove the hard coded width/height
    .removeAttr('height')
    .removeAttr('width');

});

// When the window is resized
$(window).resize(function() {

  var newWidth = $fluidEl.width();

  // Resize all videos according to their own aspect ratio
  $allVideos.each(function() {

    var $el = $(this);
    $el
      .width(newWidth)
      .height(newWidth * $el.data('aspectRatio'));

  });

// Kick off one resize to fix all videos on page load
}).resize();



$.get("https://www.screenflex.com/phplive/ajax/status.php?akey=h8w4nucqfs&deptid=0", function(online){

    if(1 == online) {
        $('#livechat').show();
    } else {
        $('#livechat').hide();            
    }
});


 
 });