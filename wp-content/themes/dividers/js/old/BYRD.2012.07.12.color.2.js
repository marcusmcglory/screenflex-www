jQuery(document).ready(function($) {    

$(window).bind('load', function() {
   initializeDividers();

});

  	$("#ddlHeight").change(function() { 
		var selectedHeight = $("#ddlHeight").val();
		var selectedColor = $("#ddlColor").val();
		var selectedLength = $("#ddlLength").val();

		if ((selectedHeight != 'NA') & (selectedColor != 'NA') & (selectedLength != 'NA')) {
			showColorDivider(selectedHeight, selectedLength, selectedColor, false);
			$("#ddlLength").removeAttr('disabled');
            $("#ddlColor").removeAttr('disabled');
			return;
		}

		
  		if ('NA' == selectedHeight)  {
  		//	$("#sizes").show("fast");
  			setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/room-divider-height.png","420","210","Select a Height", true);
  			$("#ddlLength").attr('disabled', 'disabled');

  			$("#ddlLength").val('NA');

  		    $("#ddlLength").attr("class", "");
  			$("#ddlHeight").attr("class", "current");
  			
 			clearColorSwatch();
  		} else {
			setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/Freestanding-Divider-Length-Graphic.jpg","600","215","Select a Length", true);
		//	$("#sizes").hide("fast");
			$("#ddlLength").removeAttr('disabled');
			 $("#ddlLength").attr("class", "current");
  			$("#ddlHeight").attr("class", "");
		}
		

  	});
  	
  	
  	
  	$("#ddlLength").change(function() { 

		var selectedHeight = $("#ddlHeight").val();
		var selectedColor = $("#ddlColor").val();
		var selectedLength = $("#ddlLength").val();

		if ((selectedHeight != 'NA') & (selectedColor != 'NA') & (selectedLength != 'NA')) {
			showColorDivider(selectedHeight, selectedLength, selectedColor, false);
			$("#ddlLength").removeAttr('disabled');
            $("#ddlColor").removeAttr('disabled');
			return;
		}
  		if (selectedLength == 'NA') {	
			setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/Freestanding-Divider-Length-Graphic.jpg","600","215","Select a Length", true);
				$(this).attr("class", "current");
 			clearColorSwatch();		  					
  		} else {
  			setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/Fabric-Swatches-All.gif","600","200","Select a Color", true);		
  			$("#ddlColor").removeAttr('disabled');
            $("#ddlColor").attr("class", "current");
  			$(this).attr("class", "");
  		}
  		
  		
  	});
  	 
	 $("#ddlColor").change(function() { 
	   	var selectedColor = $("#ddlColor").val();
	   	var selectedLength = $("#ddlLength").val();
	   	var selectedHeight = $("#ddlHeight").val();	
	   	
	   //	if ((selectedHeight != 'NA') & (selectedColor != 'NA') & (selectedLength != 'NA')) {
	   //		showColorDivider(selectedHeight, selectedLength, selectedColor, false);
	   //		return;
	   //	}
	   	
	   	
		if ('NA' == selectedColor) {
			setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/Fabric-Swatches-All.gif","600","200","Select a Color", true);	
			setImage("#swatch", "",0,0,"",false);
			$("#swatchCaption").text("");
			$("#roomdividerCaption").text("");
			$("#buttons").hide("fast");
			$("#freeinfo").show("fast");
			$("#scale").hide("fast");
			// $("#quoteBox").hide("fast");
			$(this).attr("class", "current");
			$("#byord-pricebutton").attr("href", "/request-quote/");
			$("#byord-pricebutton").attr("class", "");
		} else {
			if ((selectedLength == 'NA') || (selectedHeight == 'NA')) {
				alert('Select a Height, a Length, and a Color.');
			} else {
				showColorDivider(selectedHeight, selectedLength, selectedColor, true);
				$(this).attr("class", "");
				$("#byord-pricebutton").attr("class", "current");
			}			
		}
	 });
	 
    $("#quoteBtn").change(function() {
        var selectedBtn = $("#quoteBtn").val();
        var ht = $("#ddlHeight").val();
		var clr = $("#ddlColor").val();
		var lngth = $("#ddlLength").val();
        
        
        switch(selectedBtn) {
            case 'brochure':
                //execute code block 1
                window.location.href= "/where-to-buy/?RoomDividerChoice="+"FSL"+ht+lngth+clr;
            break;
            case 'priceguide':
                window.location.href= "/request-quote/?RoomDividerChoice="+"FSL"+ht+lngth+clr;
                //execute code block 2
            break;
            //case 'phone' :
            //break;
            default:
                //default code.
        }      
    });
 

 




function setImage (img,src,width,height,alt, wanttoanimate) {
	(wanttoanimate) ? $(img).hide("fast") : '';
	$(img).attr("src", src);
	$(img).attr("width",width);
	$(img).attr("height",height);
	$(img).attr("alt",alt);
	(wanttoanimate) ?  $(img).show("fast"): '';
}

function showColorDivider (height, length, color, animate) {
        if ("NA" != color) {
            selectedColor = color.charAt(color.length - 1);
            switch (selectedColor) { 
                case "S": 
                    selectedColor = "B";
                    break;
                case "R":
                    selectedColor = "M";
                    break;
                case "T":
                    selectedColor = "G";
                    break;
                case "Y":
                    selectedColor = "W";
                    break;
            }
            
        }


	var currentselection = "/wp-content/uploads/page-images/room-divider-sizes/images/colors/FSL" + height + length + selectedColor + ".jpg";
	setImage("#selectimage", currentselection, 650, 220, "FSL"+height+length+color, animate);
	
	
	var ht = $("#ddlHeight option:selected").text();
	var lgth = $("#ddlLength option:selected").text();
	var clr = $("#ddlColor option:selected").text();
//	$("#roomdividerCaption").text("Height: " + ht + ", Length: " + lgth + ", Color: " + clr );
	
	var swatchselection = "/wp-content/uploads/page-images/room-divider-sizes/images/Fabric-Swatches-" + selectedColor + ".gif";
//	$("#swatch").hide("blind", {direction: "vertical" }, 250);
	$("#swatch").removeAttr("width");
	$("#swatch").removeAttr("height");
	$("#swatch").attr("alt", $("#ddlColor option:selected").text());
	$("#swatch").attr("src", swatchselection);
	// $("#swatchCaption").text("Swatches available for " + $("#ddlColor option:selected").text()); 
//	$("#swatch").show("blind", {direction: "vertical" }, 250);
	$("#scale").show("fast");
	$("#buttons").show("fast");
	$("#freeinfo").hide("fast");
	$("#quoteBox").show("fast");
	$("#quotebutton").attr("href", "/request-quote/?RoomDividerChoice="+"FSL"+height+length+color);
	$("#byord-pricebutton").attr("href", "/request-quote/?RoomDividerChoice="+"FSL"+height+length+color);
	$("#brochurebutton").attr("href", "/where-to-buy/?RoomDividerChoice="+"FSL"+height+length+color);
	
	//$("#brochurebtn").attr("href", "/where-to-buy/?RoomDividerChoice="+"FSL"+height+length+color);	
	//$("#quotebtn").attr("href", "/request-quote/?RoomDividerChoice="+"FSL"+height+length+color);
	$("#numberassigned_center").text($("#numberassigned_right").text());
	$("#phoneStep").text("Questions? "+$("#numberassigned_right").text());	
	

}


function clearColorSwatch() {
	$("#ddlColor").val('NA');
	$("#ddlColor").attr('disabled', 'disabled');
	setImage("#swatch", "",0,0,"",false);
	$("#swatchCaption").text("");
	$("#roomdividerCaption").text("");
	$("#scale").hide("fast");
	$("#buttons").hide("fast");
	$("#freeinfo").show("fast");
	//$("#quoteBox").hide("fast");
	$("#byord-pricebutton").attr("href", "/request-quote/");
	$("#ddlColor").attr("class", "");
}

function getUrlParameters() {
var map = {};
var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
map[key] = value;
});
return map; 
}

function initializeDividers() {

	//$("fieldset.ready").show("blind", {directions: "vertical"}, 1000);
			$("#byord-pricebutton").attr("class", "");
			$("#ddlHeight").attr("class", "");
			$("#ddlLength").attr("class", "");
            $("#ddlColor").attr("class", "");

    var param = getUrlParameters();
    var ht = (param['height'] == undefined) ?  'NA' : param['height'];
    var lth = (param['length'] == undefined) ? 'NA' : param['length'];
    var clr = (param['color'] == undefined) ? 'NA' : param['color']; 
    
    
    if ($.inArray(ht, ['40', '50', '60', '68', '74', '80']) == -1 ){
       ht = 'NA'; 
    }

    if ($.inArray(lth, ['3', '5', '7', '9', '11', '13']) == -1 ){
       lth = 'NA'; 
    }


    currentColorGroup = clr.charAt(clr.length - 1);

    if ($.inArray(currentColorGroup, ['B', 'G', 'M', 'N', 'O', 'W', 'X']) == -1 ){
       clr = 'NA'; 
    }
    
    
    $("#ddlHeight").val(ht);
	$("#ddlLength").val(lth);
	$("#ddlColor").val(clr);   
	
	if (('NA' != ht) & ('NA' != clr) & ('NA' != lth)) {
        $("#ddlLength").removeAttr('disabled');
        $("#ddlColor").removeAttr('disabled');
        $("#ddlColor").change();
    } 	
    else if (('NA' != ht) & ('NA' != lth)) {
        $("#ddlLength").removeAttr('disabled');
        $("#ddlLength").change();
    } else if ('NA' != ht) {
        $("#ddlHeight").change();    
    } else {
        $("#ddlHeight").attr("class", "current");
    }
    
    if (('NA' == ht) || ('NA' == lth) || ('NA' == clr)) {
        $("#byord-pricebutton").attr("href", "/request-quote/");
	} else {
        $("#byord-pricebutton").attr("href", "/request-quote/?RoomDividerChoice="+"FSL"+ht+lth+clr);
	}
	
	//showColorDivider (ht, lth, clr, false);
	
	
}

 });