jQuery(document).ready(function($) {    

$(window).bind('load', function() {
   initializeDividers();

});

  	$("#ddlHeight").change(function() { 
		var selectedHeight = $("#ddlHeight").val();
		var selectedColor = $("#ddlColor").val();
		var selectedLength = $("#ddlLength").val();

		if ((selectedHeight != 'NA') & (selectedColor != 'NA') & (selectedLength != 'NA')) {
			showColorDivider(selectedHeight, selectedLength, selectedColor, false);
			$("#ddlLength").removeAttr('disabled');
            $("#ddlColor").removeAttr('disabled');
			return;
		}

		
  		if ('NA' == selectedHeight)  {
  			setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/room-divider-height.png","420","210","Select a Height", true);
  			$("#ddlLength").attr('disabled', 'disabled');

  			$("#ddlLength").val('NA');

  		    $("#ddlLength").attr("class", "");
  		    $("#lengthBox").attr("class", "selector");
  			$("#ddlHeight").attr("class", "current");
  			$("#heightBox").attr("class", "selector currentTask");
				$("#otherLengthBox").hide();
  			
 			clearColorSwatch();
  		} else {
			setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/Freestanding-Divider-Length-Graphic.jpg","600","215","Select a Length", true);
			$("#ddlLength").removeAttr('disabled');
			$("#ddlLength").attr("class", "current");
  			$("#ddlHeight").attr("class", "");
  		    $("#heightBox").attr("class", "selector");
  		    $("#lengthBox").attr("class", "selector currentTask");
		}
		

  	});
  	
  	
  	
  	$("#ddlLength").change(function() { 
  		
  		var selectedHeight = $("#ddlHeight").val();
		var selectedColor = $("#ddlColor").val();
		var selectedLength = $("#ddlLength").val();

		if ((selectedHeight != 'NA') & (selectedColor != 'NA') & (selectedLength != 'NA')) {
			showColorDivider(selectedHeight, selectedLength, selectedColor, false);
			$("#ddlLength").removeAttr('disabled');
            $("#ddlColor").removeAttr('disabled');
			return;
		}
		
		
		$("#lengthBox").attr("class", "selector currentTask");
				
		
		switch(selectedLength) {
    		case 'NA':
    		    setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/Freestanding-Divider-Length-Graphic.jpg","600","215","Select a Length", true);
				$(this).attr("class", "current");
				clearColorSwatch();	
				$("#otherLengthBox").hide();   					
    		  return;
            case 'other':
                ////If it is other, then Hide Swatches
                if ("none" == $("#otherLengthBox").css("display")) {
                    setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/Fabric-Swatches-All.gif","0","0","Select a Color", false);                    
                    $("#otherLengthBox").show();
                    $("#otherLengthBox").attr("class", "currentTask");
                    $(this).attr("class", "current");
                    clearColorSwatch();	
                } else {
                    $("#ddlLength :selected").text("other: " + $("#otherLengthFeet").val() + "\'\-" + $("#otherLengthInches").val() + "\"");
                    setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/Fabric-Swatches-All.gif","600","200","Select a Color", true);		
                    $("#otherLengthBox").attr("class", "");
                    $("#ddlColor").removeAttr('disabled');
                    $("#ddlColor").attr("class", "current");
                    $("#colorBox").attr("class", "selector currentTask");
                    $(this).attr("class", "");    
                    $("#lengthBox").attr("class", "selector");
                    $("#otherLengthBox").hide();    
                }
                return;
            default:
            
                if ("none" != $("#otherLengthBox").css("display")) {$("#otherLengthBox").hide();  }
                setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/Fabric-Swatches-All.gif","600","200","Select a Color", true);		
                $("#ddlColor").removeAttr('disabled');
                $("#ddlColor").attr("class", "current");
                $(this).attr("class", "");    
                $("#lengthBox").attr("class", "selector");
                $("#colorBox").attr("class", "selector currentTask");
                return;
        }
		
  	});
  	 
	 $("#ddlColor").change(function() { 
	   	var selectedColor = $("#ddlColor").val();
	   	var selectedLength = $("#ddlLength").val();
	   	var selectedHeight = $("#ddlHeight").val();	
	   	
	   //	if ((selectedHeight != 'NA') & (selectedColor != 'NA') & (selectedLength != 'NA')) {
	   //		showColorDivider(selectedHeight, selectedLength, selectedColor, false);
	   //		return;
	   //	}
	   	
	   	
		if ('NA' == selectedColor) {
			setImage ("#selectimage","/wp-content/uploads/page-images/room-divider-sizes/images/Fabric-Swatches-All.gif","600","200","Select a Color", true);	
			setImage("#swatch", "",0,0,"",false);
			$("#swatchCaption").text("");
			$("#roomdividerCaption").text("");
			$("#buttons").hide("fast");
			$("#freeinfo").show("fast");
			$("#scale").hide("fast");
			// $("#quoteBox").hide("fast");
			$(this).attr("class", "current");
			$("#byord-pricebutton").attr("href", "/request-quote/");
			$("#byord-pricebutton").attr("class", "");
			$("#colorBox").attr("class", "selector currentTask");
			$("#quoteBox").attr("class", "selector");
		} else {
			if ((selectedLength == 'NA') || (selectedHeight == 'NA')) {
				alert('Select a Height, a Length, and a Color.');
			} else {
			
			
			
				showColorDivider(selectedHeight, selectedLength, selectedColor, true);
				$(this).attr("class", "");
				$("#byord-pricebutton").attr("class", "current");
				$("#colorBox").attr("class", "selector");
				$("#quoteBox").attr("class", "selector currentTask");
			}			
		}
	 });
	 
    $("#quoteBtn").change(function() {
        var selectedBtn = $("#quoteBtn").val();
        var ht = $("#ddlHeight").val();
		var clr = $("#ddlColor").val();
		var lngth = $("#ddlLength").val();
        
        
        switch(selectedBtn) {
            case 'brochure':
                //execute code block 1
                window.location.href= "/how-to-buy/?RoomDividerChoice="+"FSL"+ht+lngth+clr;
            break;
            case 'priceguide':
                window.location.href= "/request-quote/?RoomDividerChoice="+"FSL"+ht+lngth+clr;
                //execute code block 2
            break;
            default:
                //default code.
        }      
    });
 


function setImage (img,src,width,height,alt, wanttoanimate) {
	(wanttoanimate) ? $(img).hide("fast") : '';
	$(img).attr("src", src);
	$(img).attr("width",width);
	$(img).attr("height",height);
	$(img).attr("alt",alt);
	(wanttoanimate) ?  $(img).show("fast"): '';
}

function showColorDivider (height, length, color, animate) {
    $("#roomdividerCaption").hide(); 
    var otherLength = "";
    //Need to test Length for 'other' and decide which length to pick.
    if ("other" == length) {        
        otherLength = encodeURI(" OTHER: " + $("#otherLengthFeet").val() + "\`\-" + $("#otherLengthInches").val() + "\"");
        var ft = $("#otherLengthFeet").val();
        var inches = $("#otherLengthInches").val();      
        if (ft < 5 ) { length = 3;} 
        else if ((5 == ft) && (inches <= 9)) {length = 3; }
        else if (ft < 9 ) { length = 5;}
        else if ((9 == ft) && (inches <= 5)) {length = 5;  }
        else if (ft < 13 ) {length = 7;}
        else if ((13 == ft) && (inches <= 1)) {length = 7;}        
        else if (ft < 16) {length = 9;}        
        else if ((16 == ft) && (inches <= 9)) {length = 9; }
        else if (ft < 20) {length = 11;}
        else if ((20 == ft) && (inches <= 5)) {length = 11;}
        else { length = 13;
             $("#roomdividerCaption").show(); 
            $("#roomdividerCaption").html("The size you requested requires multiple dividers.<br /><a href='#' id='captionlink'>Click submit</a> for pricing, and a space planning specialist will provide you with the best solution and quote for your needs.");
            $("#captionlink").attr("href", "/request-quote/?RoomDividerChoice="+"FSL"+height+length+color+otherLength);
        }                
    }
    

	var currentselection = "/wp-content/uploads/page-images/room-divider-sizes/images/colors/FSL" + height + length + color + ".jpg";
	setImage("#selectimage", currentselection, 650, 220, "FSL"+height+length+color, animate);
	
	
	var ht = $("#ddlHeight option:selected").text();
	var lgth = $("#ddlLength option:selected").text();
	var clr = $("#ddlColor option:selected").text();
	
	var swatchselection = "/wp-content/uploads/page-images/room-divider-sizes/images/Fabric-Swatches-" + color + ".gif";
	$("#swatch").removeAttr("width");
	$("#swatch").removeAttr("height");
	$("#swatch").attr("alt", $("#ddlColor option:selected").text());
	$("#swatch").attr("src", swatchselection);

	$("#scale").show("fast");
	$("#buttons").show("fast");
	$("#freeinfo").hide("fast");
	$("#quoteBox").show("fast");
	$("#quotebutton").attr("href", "/request-quote/?RoomDividerChoice="+"FSL"+height+length+color+otherLength);
	$("#byord-pricebutton").attr("href", "/request-quote/?RoomDividerChoice="+"FSL"+height+length+color+otherLength);
	$("#brochurebutton").attr("href", "/how-to-buy/?RoomDividerChoice="+"FSL"+height+length+color+otherLength);
}


function clearColorSwatch() {
	$("#ddlColor").val('NA');
	$("#ddlColor").attr('disabled', 'disabled');
	setImage("#swatch", "",0,0,"",false);
	$("#swatchCaption").text("");
	$("#roomdividerCaption").text("");
	$("#scale").hide("fast");
	$("#buttons").hide("fast");
	$("#freeinfo").show("fast");
	$("#byord-pricebutton").attr("href", "/request-quote/");
	$("#ddlColor").attr("class", "");
	$("#colorBox").attr("class", "selector");
	$("#quoteBox").attr("class", "selector");
	$("#roomdividerCaption").text("");
	$("#roomdividerCaption").hide(); 
}

function getUrlParameters() {
var map = {};
var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
map[key] = value;
});
return map; 
}

function initializeDividers() {
			$("#byord-pricebutton").attr("class", "");
			$("#ddlHeight").attr("class", "");
			$("#ddlLength").attr("class", "");
            $("#ddlColor").attr("class", "");
            $("#otherLengthAction").bind("click", function() {$("#ddlLength").change();return false;});


    var param = getUrlParameters();
    var ht = (param['height'] == undefined) ?  'NA' : param['height'];
    var lth =(param['length'] == undefined) ? 'NA' : param['length'];
    var clr = (param['color'] == undefined) ? 'NA' : param['color']; 
    
    
    if ($.inArray(ht, ['40', '50', '60', '68', '74', '80']) == -1 ){
       ht = 'NA'; 
    }

    if ($.inArray(lth, ['3', '5', '7', '9', '11', '13', 'other']) == -1 ){
       lth = 'NA'; 
    }

    if ($.inArray(clr, ['B', 'G', 'M', 'N', 'O', 'W', 'X']) == -1 ){
       clr = 'NA'; 
    }
    
    
    $("#ddlHeight").val(ht);
	$("#ddlLength").val(lth);
	$("#ddlColor").val(clr);   
	
	if (('NA' != ht) & ('NA' != clr) & ('NA' != lth)) {
        $("#ddlLength").removeAttr('disabled');
        $("#ddlColor").removeAttr('disabled');
        $("#ddlColor").change();
    } 	
    else if (('NA' != ht) & ('NA' != lth)) {
        $("#ddlLength").removeAttr('disabled');
        $("#ddlLength").change();
    } else if ('NA' != ht) {
        $("#ddlHeight").change();    
    } else {
        $("#ddlHeight").attr("class", "current");
    }
    
    if (('NA' == ht) || ('NA' == lth) || ('NA' == clr)) {
        $("#byord-pricebutton").attr("href", "/request-quote/");
	} else {
        $("#byord-pricebutton").attr("href", "/request-quote/?RoomDividerChoice="+"FSL"+ht+lth+clr);
	}
	
	$("#scale").hide("fast");
	
}

 });