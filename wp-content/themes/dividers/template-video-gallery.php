<?php
/*
Template Name: Video Gallery
*/
 
 //adds new body class for post category
 // Add specific CSS class by filter
add_filter('body_class','my_class_names');
function my_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'video-gallery';
	// return the $classes array
	return $classes;
}

remove_action( 'genesis_before_content', 'genesis_do_subnav' );

genesis();