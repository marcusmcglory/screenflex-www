<?php

/*
Template Name: A/B Test
*/

//add_filter('body_class','my_class_names');
//function my_class_names($classes) {
	// add 'class-name' to the $classes array
//	$classes[] = 'product a-b';
	// return the $classes array
//	return $classes;
//}

add_action('genesis_after_content', 'custom_front_page_footer_widget');  
function custom_front_page_footer_widget() {
    if ( is_active_sidebar(4) ) : ?>
        <div id="frontpage-footer-widgeted">
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Front Page Footer') ) : ?>
	    	<?php endif; ?> 
	   </div><!-- end #footer-widgeted -->
    <?php endif; 
}

// Remove the page title
//remove_action( 'genesis_post_title', 'genesis_do_post_title' );

//add_action('genesis_before_content', 'genesis_do_subnav');

 
genesis();
