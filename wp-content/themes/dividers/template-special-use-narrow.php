<?php 
 /*
Template Name: Special Use Narrow Product Page
*/
 
 //adds new body class for post category
 // Add specific CSS class by filter
add_filter('body_class','my_class_names');
function my_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'narrow-product special-use narrow';
	// return the $classes array
	return $classes;
}


add_action('genesis_before_content', 'genesis_do_subnav');

add_action('genesis_after_content', 'custom_special_use_page_footer_widget'); 
function custom_special_use_page_footer_widget() {
    if ( is_active_sidebar(10) ) : ?>
        <div id="frontpage-footer-widgeted">
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Special Use Footer') ) : ?>
	    	<?php endif; ?> 
	    	</div><!-- end #footer-widgeted -->
    <?php endif; 

}


genesis();