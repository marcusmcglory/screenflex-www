<?php
/**

 Site Map Generated
*/

//add_filter( 'the_content', 'ss_add_search_form', 10 );
//function ss_add_search_form( $content ) {
//
//        $content = sprintf('<a name="search"></a>' . get_search_form(), $content );
//
//
//    return $content;
//}


add_filter('body_class','my_class_names');
function my_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'site-map';
	// return the $classes array
	return $classes;
}

/** Remove default loop **/
// remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_loop', 'screenflex_create_site_map' );
/**
 * This function outputs a 404 "Not Found" error message
 *
 * @since 1.6
 */
function screenflex_create_site_map() { ?>

	<div class="post hentry">
        <a name="site-map"></a>
	    <h2 class="entry-title">Site Map</h2>
		<div class="entry-content">

			<div class="archive-page">

				<h4><?php _e( 'Pages:', 'genesis' ); ?></h4>
				<ul>
					<?php wp_list_pages( 'exclude=6300,6910,6976,6980,6986,38873,39322,39950,44509&depth=3&sort_column=menu_order,post_title&title_li=' ); ?>
				</ul>

				<h4><?php _e( 'Blog Categories:', 'genesis' ); ?></h4>
				<ul>
					<?php wp_list_categories( 'show_post_count=1&sort_column=name&title_li=' ); ?>
				</ul>

				<h4><?php _e( 'Monthly:', 'genesis' ); ?></h4>
				<ul>
					<?php wp_get_archives( 'show_post_count=1&type=monthly' ); ?>
				</ul>
				
				<h4><?php _e( '40 Recent Blog Posts:', 'genesis' ); ?></h4>
				<ul>
					<?php wp_get_archives( 'type=postbypost&limit=40' ); ?>
				</ul>


			</div><!-- end .archive-page-->

			<div class="archive-page">

				<h4><?php _e( 'Photo Gallery:', 'genesis' ); ?></h4>
				<ul>
				    <?php 
                        $photoargs = array(
                                      'post_type'=>'photo',
                                      'title_li'=> __('Photos')
                                );
                        wp_list_pages( $photoargs ); 
                    ?> 
				</ul>

			</div><!-- end .archive-page-->

		</div><!-- end .entry-content -->

	</div><!-- end .postclass -->

<?php
}

remove_action( 'genesis_before_content', 'genesis_do_subnav' );

genesis();