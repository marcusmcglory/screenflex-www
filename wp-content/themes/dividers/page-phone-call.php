<?php
/**
 * WARNING: This file is part of the core Genesis framework. DO NOT edit
 * this file under any circumstances. Please do all modifications
 * in the form of a child theme.
 *
 * 
 *
 * @package Genesis
 */

 /*

 Phone Track
*/


add_filter('body_class','my_class_names');
function my_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'narrow';
	// return the $classes array
	return $classes;
}


remove_action( 'genesis_before_content', 'genesis_do_subnav' );

genesis();