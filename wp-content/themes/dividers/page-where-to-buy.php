<?php 

add_filter('body_class','my_class_names');
function my_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'narrow';
	// return the $classes array
	return $classes;
}


add_action( 'wp_enqueue_scripts', 'ss_child_add_custom_page_script' );
function ss_child_add_custom_page_script() {
               wp_enqueue_script( 'email-link', CHILD_URL . '/js/build-email-link.js', array('jquery'), '1.2', false);           
           
}  


remove_action( 'genesis_before_content', 'genesis_do_subnav' );

genesis();