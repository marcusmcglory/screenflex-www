<?php


remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'child_grid_loop_helper' );
/** Add support for Genesis Grid Loop **/
function child_grid_loop_helper() {
    if ( function_exists( 'genesis_grid_loop' ) ) {
        genesis_grid_loop( array(
            'features' => 28,
            'feature_image_size' => 0,
            'feature_image_class' => 'alignleft post-image',
            'feature_content_limit' => 0,
            'grid_image_class' => 'alignleft post-image',
//            'grid_content_limit' => 0,
           // 'more' => __( '[Continue reading...]', 'genesis' ),
            'posts_per_page' => 28
        ) );
    } else {
        genesis_standard_loop();
    }
}

//Add Custom Pin-Map of Customers in the zip code radius
add_action('genesis_loop', 'ss_add_review_map', 0);
function ss_add_review_map() {
     ?> 
     <h2 class="section-head">USA Customer List &hellip; Last Two Years</h2>
<iframe style="border: 1px solid #aaa; border-radius: 10px;" src="//batchgeo.com/map/9b1d2f4a9f7a00d46dd14a042613781d" height="550" width="100%" frameborder="0"></iframe>
<small class="review-map-caption">View <a href="//batchgeo.com/map/9b1d2f4a9f7a00d46dd14a042613781d">Customers</a> in a full screen map</small>

<a id="reviewtop" name="reviewtop"></a> 	
<?php gravity_form('18', false, true, false, '', false, 50); ?>


<hr style="clear:both;"/>

<h2 class="section-head">Recent Reviews from our Customers</h2>
    <?php    
}


// Customize the post info function, Remove the Post_info
add_filter('genesis_post_info', 'post_info_filter');
function post_info_filter($post_info) {
    if ('review' == get_post_type()) {
        $post_info = '';
        return $post_info;
    }
}


//* Customize the post meta function
add_filter( 'genesis_post_meta', 'sp_post_meta_filter' );
function sp_post_meta_filter($post_meta) {
if ( 'review' == get_post_type()) {
	$post_meta = '';
	return $post_meta;
}}





genesis();