<?php


add_filter( 'wp_nav_menu_args' , 'ss_home_nav_menu' );

function ss_home_nav_menu( $args ) {
	if ( $args['theme_location'] == 'primary' ) { 
		$args['menu'] = 'MainHomeTop'; 
	}
	return $args;
}




add_action('genesis_after_content', 'custom_front_page_footer_widget');  
function custom_front_page_footer_widget() {
    if ( is_active_sidebar(4) ) : ?>
        <div id="frontpage-footer-widgeted">
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Front Page Footer') ) : ?>
	    	<?php endif; ?> 
	   </div><!-- end #footer-widgeted -->
    <?php endif; 
}


 
genesis();

