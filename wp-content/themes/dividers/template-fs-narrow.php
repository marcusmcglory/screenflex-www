<?php
 /*
Template Name: FREEstanding Product Narrow Page
*/
 
 //adds new body class for post category
 // Add specific CSS class by filter
add_filter('body_class','my_class_names');
function my_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'narrow-product narrow';
	// return the $classes array
	return $classes;
}


add_action('genesis_before_content', 'genesis_do_subnav');

add_action('genesis_after_content', 'custom_freestanding_page_footer_widget'); 
function custom_freestanding_page_footer_widget() {
    if ( is_active_sidebar(9) ) : ?>
        <div id="frontpage-footer-widgeted">
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('FREEstanding Footer') ) : ?>
	    	<?php endif; ?> 
	   </div><!-- end #footer-widgeted -->
    <?php endif; 

}


genesis();