<?php 
 /*
Template Name: Healthflex Narrow Product Page
*/
 
 //adds new body class for post category
 // Add specific CSS class by filter
add_filter('body_class','my_class_names');
function my_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'narrow-product special-use narrow healthflex';
	// return the $classes array
	return $classes;
}

//Remove Menu from HealthFlex page
add_action('get_header', 'ss_child_remove_genesis_do_nav');
function ss_child_remove_genesis_do_nav() {
    remove_action('genesis_after_header', 'genesis_do_nav');
}


add_filter('widget_content', 'ss_widget_content_filter');
function ss_widget_content_filter($content='') {
    $content=preg_replace('/how\-to\-buy/', 'how-to-buy-health-care-room-dividers',$content,1);
    $content=preg_replace('/800\.553\.0110/', '800-553-0746',$content,1);
    return $content;
}

add_action('genesis_before_content', 'genesis_do_subnav');

add_action('genesis_after_content', 'custom_special_use_page_footer_widget'); 
function custom_special_use_page_footer_widget() {
    if ( is_active_sidebar(10) ) : ?>
        <div id="frontpage-footer-widgeted">
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Special Use Footer') ) : ?>
	    	<?php endif; ?> 
	   </div><!-- end #footer-widgeted -->
    <?php endif; 
}


genesis();