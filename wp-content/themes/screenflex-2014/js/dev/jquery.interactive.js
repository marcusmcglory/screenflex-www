jQuery(document).ready(function($) {
  $(window).bind('load', function() {
    swfobject.registerObject("SFVirtualDivider", "10.0.0");
    var flashvars = {
      gaTracker : "UA-395272-1",
      belowFold : "&false"
    };
    var params = {
      base: "https://staging.screenflex.com/wp-content/uploads/virtual-divider/",
      wmode: "opaque",
      allowscriptaccess: "always"
    };
    var attributes = {
      id: "SFVirtualDivider",
      name: "SFVirtualDivider"
    };

    swfobject.embedSWF('https://staging.screenflex.com/wp-content/uploads/virtual-divider/SFVirtualDivider.swf', 'SFVirtualDivider', '600', '450', '10.0.0', false, flashvars, params, attributes);
  });
});