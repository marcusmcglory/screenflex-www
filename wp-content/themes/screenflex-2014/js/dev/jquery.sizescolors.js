(function($) {
	$(document).ready(function() {
		var History = window.History; // Note: We are using a capital H instead of a lower h
		if (!History.enabled) {
			// History.js is disabled for this browser.
			// This is because we can optionally choose to support HTML4 browsers or not.
			return false;
		}
		var colorSelect = $('.color-select');
		var screen = $('#screen');
		var otherLengthBox = $('#otherLengthBox');
		// Sizes and Colors
		$('.height-select li').hover(function() {
			var id = $(this).attr('id');
			$('.screen').attr('data-height', id);
			$(this).addClass('active').removeClass('inactive');
			$(this).siblings().removeClass('active').addClass('inactive');
			$(this).parent('ul').toggleClass('open');
		});
		$('.length-select li').hover(function() {
			var id = $(this).attr('id');
			$('.screen').attr('data-length', id);
			$(this).addClass('active').removeClass('inactive');
			$(this).siblings().removeClass('active').addClass('inactive');
			$(this).parent('ul').toggleClass('open');
			$("#roomdividerCaption").fadeOut();
			otherLengthBox.fadeOut();
		});
		$('.color-select li').hover(function() {
			var id = $(this).attr('id');
			//var link = $(this).attr('data-link');
			var title = $(this).attr('data-title');
			screen.attr('data-color', id);
			$('.color-select li').removeClass('active').addClass('inactive');
			$(this).addClass('active').removeClass('inactive');
			$(this).siblings().removeClass('active').addClass('inactive');
			$('.view-colors').attr('data-color', id);
			$('.zoom').attr('href', '/wp-content/uploads/colors/' + id + '.jpg');
			$('.zoom').attr('title', title);
			$(this).parent('ul').toggleClass('open');
		});
		$('.color-select li, .color-select li i').click(function() {
			var divPosition = screen.offset();
			$('html, body').animate({scrollTop: divPosition.top}, "fast");
		});
		$('.color-select[data-open="false"]').hide();
		$('.view-colors').click(function() {
			var el = $(this).children('.text');
			el.text(el.text() === el.data("text-swap") ? el.data("text-original") : el.data("text-swap"));
			colorSelect.slideToggle('slow');
			colorSelect.attr('data-open', colorSelect.attr('data-open') === 'false' ? 'true' : 'false');
			if (colorSelect.attr('data-open') == 'true') {
				$('.scotchgard').show();
				$('.request-wrapper, .price-cat-cta').addClass('stick');
			}
			if (colorSelect.attr('data-open') == 'false') {
				$('.scotchgard').hide();
				$('.request-wrapper, .price-cat-cta').removeClass('stick');
			}
			$('.arrow').toggleClass('icon-arrow-down-4').toggleClass('icon-arrow-up-2');
		});
		if (colorSelect.attr('data-open') == 'true') {
			$('.scotchgard').show();
			$('.request-wrapper, .price-cat-cta').addClass('stick');
		}
		if (colorSelect.attr('data-open') == 'false') {
			$('.scotchgard').hide();
			$('.request-wrapper, .price-cat-cta').removeClass('stick');
		}
		$('.show-other').click(function() {
			otherLengthBox.fadeIn('slow');
			$("#roomdividerCaption").fadeOut();
		});
		$('.close-other').click(function() {
			otherLengthBox.fadeOut('slow');
		});
		$('#otherLengthAction').click(function() {
			var length = '';
			var ft = $("#otherLengthFeet").val();
			var inches = $("#otherLengthInches").val();
			var size = ft + 'feet-' + inches + 'inches';
			$("#roomdividerCaption").hide();
			if ((ft < 5) || ((5 == ft) && (inches <= 9))) {
				length = 'five_nine';
			} else if ((ft < 9) || ((9 == ft) && (inches <= 5))) {
				length = 'nine_five';
			} else if ((ft < 13) || ((13 == ft) && (inches <= 1))) {
				length = 'thirteen_one';
			} else if ((ft < 16) || ((16 == ft) && (inches <= 9))) {
				length = 'sixteen_nine';
			} else if ((ft < 20) || ((20 == ft) && (inches <= 5))) {
				length = 'twenty_five';
			} else if ((ft < 24) || ((24 == ft) && (inches <= 1))) {
				length = 'twentyfour_one';
			} else {
				length = 'twentyfour_one';
				$("#roomdividerCaption").fadeIn();
			}
			$('.screen').attr('data-length', length);
			$('.screen').attr('data-customsize', size);
			$('#' + length).addClass('active').removeClass('inactive');
			$('#' + length).siblings().removeClass('active').addClass('inactive');
			otherLengthBox.fadeOut('slow');
		});
		$('.color-select li, .length-select li, .height-select li, .view-colors, #otherLengthAction').hover(function() {
			var length = screen.attr('data-length');
			var height = screen.attr('data-height');
			var color = screen.attr('data-color');
			var customsize = screen.attr('data-customsize');
			var colorSelection = colorSelect.attr('data-open');
			var constructState = '?height=' + height + '&length=' + length + '&color=' + color + '&open=' + colorSelection + '&customsize=' + customsize;
			History.replaceState(null, null, constructState);
			$('.request a').attr("href", '/request-quote/' + constructState);
		});

		function sticky_relocate() {
			var window_top = $(window).scrollTop();
			var winwidth = $(window).width();
			var screen = $('.screen-wrapper');
			var div_top = $('.app').offset().top;
			var placehold = $('.app .placeholder');
			if (colorSelect.attr('data-open') == 'true' && winwidth < 768) {
				if (window_top > div_top) {
					screen.addClass('stick');
					placehold.addClass('active');
				} else {
					screen.removeClass('stick');
					placehold.removeClass('active');
				}
			}
		}
		$(window).scroll(sticky_relocate);
		sticky_relocate();
	});
})(jQuery);