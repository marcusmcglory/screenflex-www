(function($) {
  $(document).ready(function() {
      
		// Catalog Requests
		var catRequest = $('.catalog-request');
		//$('.catalog-request').hide();
	$('.priceguide-cta').mouseover(function(){
			catRequest.addClass('open');
    });
    $('.priceguide-cta').click(function(){
			catRequest.toggleClass('open');
    });
    $('.close-catalog').click(function(){
      catRequest.removeClass('open');
    });
    $('.site-inner').click(function(){
      catRequest.removeClass('open');
    });
    $(document).keyup(function(e) {
      if (e.keyCode === 27) {
        catRequest.removeClass('open');
      }
    });
		
		// Speciality products
		$('#showcolors').click(function(){
      $('.color-select').fadeIn('slow');
		});
		$('.close-colors').click(function(){
      $('.color-select').fadeOut('slow');
		});
		
		// Taxonomy Navigation
		$(".page-changer #submit").hide();

		$(".page-changer select").change(function() {
			window.location = $(this).children('select option:selected').val();
		});
  });
})(jQuery);