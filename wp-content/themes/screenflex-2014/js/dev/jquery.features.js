(function($) {
  $(document).ready(function() {
    // Features Page
    $('.circles li').click(function(){
      var id = $(this).attr('id');
      $('.features-canvas').attr('data-active', id);
      $(this).addClass('active');
      $(this).siblings().removeClass('active');
    });
    
    $('.popovers .icon-cross').click(function(){
      $('.features-canvas').attr('data-active', '');
      $('.circles li').removeClass('active');
    });
    
    $('.zoomon').click(function(){
      //$('.feature-image').zoom({url: '/wp-content/themes/screenflex-2014/images/features/features-hero-big.jpg'});
      $('#featureHero').addClass('featurezoom');
       $(this).hide();
      $('.hover').show();
      $('.circles').fadeOut('slow');
      $('.popovers').fadeOut('slow');
      $('.zoomoff').show();
      $('#mlens_target_0').removeClass('hide');
    });
    
    $('.zoomoff').click(function(){
      $('#featureHero').removeClass('featurezoom');
      $(this).hide();
      $('.hover').hide();
      $('.zoomon').show();
      $('.circles').fadeIn('slow');
      $('.popovers').fadeIn('slow');
      $('#mlens_target_0').addClass('hide');
    });
    
    $("#featureHero").mlens( {
        imgSrc: $("#featureHero").attr("data-big"),
        lensShape: "circle",
        lensSize: 280,
        borderSize: 4,
        borderColor: "#fff",
        borderRadius: 0,
        overlayAdapt: true,
        zoomLevel: 1
    });
    $('#mlens_target_0').addClass('hide');

  });
})(jQuery);