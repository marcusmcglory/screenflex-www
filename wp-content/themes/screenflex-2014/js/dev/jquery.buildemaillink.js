

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) {
			return c.substring(nameEQ.length, c.length);
		}
	}
	return null;
}

function googleCookieSource() {
	var feed = readCookie("__utmz");
	var src = "";
	if (null == feed || feed.indexOf("utmcsr") == -1) {
		return null;
	}
	if (feed != null) {
		if (feed.indexOf("%7C") != -1) {
			feed = feed.split("%7C");
			for (counter = 0; counter < feed.length; counter++) {
				feed[counter] = feed[counter].split("%3D");
			}
		} else {
			feed = feed.split("|");
			for (counter = 0; counter < feed.length; counter++) {
				feed[counter] = feed[counter].split("=");
			}
		}
		if (feed[0][1] != "" || feed[0][1] != null) {
			src += feed[0][1];
		}
		for (counter = 0; counter < feed.length; counter++) {
			if ('utmcmd' == feed[counter][0]) {
				src += "|" + feed[counter][1];
			}
		}
		return src;
	}
}

function buildEmailSubject(googlesrc) {
	var subject = "";
	googlesrc = googlesrc.toLowerCase();
	switch (googlesrc) {
	case "google|cpc":
	case "google|ppc":
		subject = "Price and Product Guide Request.  3140";
		break;
	case "google|organic":
		subject = "Price and Product Guide Request.  3141";
		break;
	case "bing|organic":
		subject = "Price and Product Guide Request.  3145";
		break;
	case "msftab|cpc":
	case "msftab|ppc":
		subject = "Price and Product Guide Request.  3143";
		break;
	case "facebook|cpc":
	case "facebook|ppc":
		subject = "Price and Product Guide Request.  3152";
		break;
	case "(direct)|(none)":
		subject = "Price and Product Guide Request.  3148";
		break;
	case "yahoo|organic":
		subject = "Price and Product Guide Request.  3146";
		break;
	case "princetononline|banners":
		subject = "Price and Product Guide Request.  3155";
		break;
	case "aol|organic":
		subject = "Price and Product Guide Request.  3147";
		break;
	case "salem":
		subject = "Price and Product Guide Request.  3153";
		break;
	case "cti":
		subject = "Price and Product Guide Request.  3153";
		break;
	case "cwd":
		subject = "Price and Product Guide Request.  3154";
		break;
	default:
		subject = "Price and Product Guide Request.  3153 " + googlesrc;
	}
	subject = "subject=" + encodeURIComponent(subject);
	return subject;
}

function buildEmailLink(emailsubject, dest) {
	var email = "";
	jQuery(document).ready(function($) {
		var mailto = "mailto:" + dest + "@screenflex.com?";
		email = mailto;
		email += emailsubject + "&body=";
		var toorgname = $("li.request-organization input").val();
		email += ((undefined == toorgname) || (null == toorgname) || ("" == toorgname)) ? "" : "Organization%20Name:%0D%0A%20" + encodeURIComponent(toorgname) + "%0D%0A%20%0D%0A%20%0D%0A%20%0D%0A";
		var toaddress = $("li.request-address textarea").val();
		email += ((undefined == toaddress) || (null == toaddress) || ("" == toaddress)) ? "" : "Mailing%20Address:%0D%0A%20" + encodeURIComponent(toaddress);
		var toname = $("li.request-name input").val();
		email += ((undefined == toname) || (null == toname) || ("" == toname)) ? "" : "%0D%0A%20%0D%0AName:%0D%0A%20" + encodeURIComponent(toname);
		var tophone = $("li.request-phone input").val();
		email += ((undefined == tophone) || (null == tophone) || ("" == tophone)) ? "" : "%0D%0A%20%0D%0APhone:%0D%0A%20" + encodeURIComponent(tophone);
		var tocomments = $("li.request-comments textarea").val();
		email += ((undefined == tocomments) || (null == tocomments) || ("" == tocomments)) ? "" : "%0D%0A%20%0D%0AComments:%0D%0A%20" + encodeURIComponent(tocomments);
	});
	return email;
}

function recordEmailLink(link, category, action, who) {
	var googlecookie = googleCookieSource();
	if (googlecookie != null && googlecookie != "") {
		link.href = buildEmailLink(buildEmailSubject(googlecookie), who);
	}
	_gat._getTrackerByName()._trackEvent(category, action);
	setTimeout('document.location = "' + link.href + '"', 100);
}

