jQuery(document).ready(function($) {
    
    // Expand and collapse grid elements in buyers guide.
    $("a#moregrid").click(function(event){
        event.preventDefault();
        $(this).text($(this).text() == '↓ Expand ↓' ? '↑ Collapse ↑' : '↓ Expand ↓');
        $('div.grid-details').slideToggle('slow','linear'); 
    });
    
    /*
    $.get("https://www.screenflex.com/phplive/ajax/status.php?akey=h8w4nucqfs&deptid=0", function(online){
    
        if(1 == online) {
            $('#livechat').show();
        } else {
            $('#livechat').hide();            
        }
    });
    */
});