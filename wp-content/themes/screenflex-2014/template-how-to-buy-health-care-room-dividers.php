<?php 

 
add_action('wp_head', 'ss_child_inject_js_link_tracking',10);

function ss_child_inject_js_link_tracking() {
 ?>
<script type="text/javascript">

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) {
			return c.substring(nameEQ.length, c.length);
		}
	}
	return null;
}

function googleCookieSource() {
	var feed = readCookie("__utmz");
	var src = "";
	if (null == feed || feed.indexOf("utmcsr") == -1) {
		return null;
	}
	if (feed != null) {
		if (feed.indexOf("%7C") != -1) {
			feed = feed.split("%7C");
			for (counter = 0; counter < feed.length; counter++) {
				feed[counter] = feed[counter].split("%3D");
			}
		} else {
			feed = feed.split("|");
			for (counter = 0; counter < feed.length; counter++) {
				feed[counter] = feed[counter].split("=");
			}
		}
		if (feed[0][1] != "" || feed[0][1] != null) {
			src += feed[0][1];
		}
		for (counter = 0; counter < feed.length; counter++) {
			if ('utmcmd' == feed[counter][0]) {
				src += "|" + feed[counter][1];
			}
		}
		return src;
	}
}

function buildEmailSubject(googlesrc) {
	var subject = "";
	googlesrc = googlesrc.toLowerCase();
	//if ((googlesrc.indexOf("salem2012") != -1) || (googlesrc.indexOf("salem2013") != -1)) {
	//	googlesrc = "salem";
	//} else if ((googlesrc.indexOf("cti2012") != -1) || (googlesrc.indexOf("cti2013") != -1)) {
	//	googlesrc = "cti";
	//} else if ((googlesrc.indexOf("cwd2012") != -1) || (googlesrc.indexOf("cwd2013") != -1) || (googlesrc.indexOf("CWD") != -1) {
	//	googlesrc = "cwd";
	//}
	switch (googlesrc) {
	case "google|cpc":
	case "google|ppc":
		subject = "HEALTHFLEX Price and Product Guide Request.  3140";
		break;
	case "google|organic":
		subject = "HEALTHFLEX Price and Product Guide Request.  3141";
		break;
	case "bing|organic":
		subject = "HEALTHFLEX Price and Product Guide Request.  3145";
		break;
	case "msftab|cpc":
	case "msftab|ppc":
		subject = "HEALTHFLEX Price and Product Guide Request.  3143";
		break;
	case "facebook|cpc":
	case "facebook|ppc":
		subject = "HEALTHFLEX Price and Product Guide Request.  3152";
		break;
	case "(direct)|(none)":
		subject = "HEALTHFLEX Price and Product Guide Request.  3148";
		break;
	case "yahoo|organic":
		subject = "HEALTHFLEX Price and Product Guide Request.  3146";
		break;
	case "princetononline|banners":
		subject = "HEALTHFLEX Price and Product Guide Request.  3155";
		break;
	case "aol|organic":
		subject = "HEALTHFLEX Price and Product Guide Request.  3147";
		break;
	case "salem":
		subject = "HEALTHFLEX Price and Product Guide Request.  3153";
		break;
	case "cti":
		subject = "HEALTHFLEX Price and Product Guide Request.  3153";
		break;
	case "cwd":
		subject = "HEALTHFLEX Price and Product Guide Request.  3154";
		break;
	default:
		subject = "HEALTHFLEX Price and Product Guide Request.  3153 " + googlesrc;
	}
	subject = "subject=" + encodeURIComponent(subject);
	return subject;
}

function buildEmailLink(emailsubject) {
	var email = "";
	jQuery(document).ready(function($) {
		var mailto = "mailto:information@screenflex.com?";
		email = mailto;
		email += emailsubject + "&body=Please%20E-MAIL%20a%20Price%20Guide.%0D%0A%0D%0A";
		var toorgname = $("li.request-organization input").val();
		email += (undefined == toorgname) ? "" : "Organization%20Name:%0D%0A%20" + encodeURIComponent(toorgname) + "%0D%0A%20%0D%0A%20%0D%0A%20%0D%0A";
		var toaddress = $("li.request-address textarea").val();
		email += (undefined == toaddress) ? "" : "Additionally, Please%20MAIL%20a%20Complete%20Brochure%20Packet%20to%0D%0A%20%0D%0AMailing%20Address:%0D%0A%20" + encodeURIComponent(toaddress);
		var toname = $("li.request-name input").val();
		email += (undefined == toname) ? "" : "%0D%0A%20%0D%0AName:%0D%0A%20" + encodeURIComponent(toname);
		var tophone = $("li.request-phone input").val();
		email += (undefined == tophone) ? "" : "%0D%0A%20%0D%0APhone:%0D%0A%20" + encodeURIComponent(tophone);
		var tocomments = $("li.request-comments textarea").val();
		email += (undefined == tocomments) ? "" : "%0D%0A%20%0D%0AComments:%0D%0A%20" + encodeURIComponent(tocomments);
	});
	return email;
}

function recordEmailLink(link, category, action) {
	var googlecookie = googleCookieSource();
	if (googlecookie != null && googlecookie != "") {
		link.href = buildEmailLink(buildEmailSubject(googlecookie));
	}
	_gat._getTrackerByName()._trackEvent(category, action);
	setTimeout('document.location = "' + link.href + '"', 100);
}

</script>

<?php
}


add_filter('body_class','my_class_names');
function my_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'narrow-product special-use narrow';
	// return the $classes array
	return $classes;
}

//Remove Menu from HealthFlex page
add_action('get_header', 'ss_child_remove_genesis_do_nav');
function ss_child_remove_genesis_do_nav() {

remove_action('genesis_after_header', 'genesis_do_nav');
remove_action( 'genesis_before_content', 'genesis_do_subnav' );

}



add_action('genesis_before_content', 'genesis_do_subnav');




add_filter('widget_content', 'ss_widget_content_filter');
function ss_widget_content_filter($content='')
{   
    $content=preg_replace('/how\-to\-buy/', 'how-to-buy-health-care-room-dividers',$content,1);
    $content=preg_replace('/800\-553\-0110/', '800-553-0746',$content,1);

return $content;
}


function custom_special_use_page_footer_widget() {

if ( is_active_sidebar(10) ) : ?>
<div id="frontpage-footer-widgeted">
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Special Use Footer') ) : ?>
	    	<?php endif; ?> 
</div><!-- end #footer-widgeted -->
<?php endif; 

}
add_action('genesis_after_content', 'custom_special_use_page_footer_widget'); 


genesis();