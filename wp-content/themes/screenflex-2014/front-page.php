<?php 
// Template name: Home

add_action( 'genesis_meta', 'home_genesis_meta' );
function home_genesis_meta() {
	remove_action( 'genesis_loop', 'genesis_do_loop' );
	add_action( 'genesis_loop', 'home_loop_helper' );
	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
	add_action('genesis_before_footer', 'custom_content', 2);
	}

function home_loop_helper() { 
  
  // Variables
  $hero = get_field('hero_image');
  $herolink = get_field('hero_link');
  $intro = get_field('intro_content');
  $left = get_field('grid_content_left');
  $middle = get_field('grid_content_middle');
  $right = get_field('grid_content_right');
  $blog = get_field('blog_content');
  $herocta = get_field('hero_cta_content');
?>
  <section class="hero">
    <div class="wrap">
      <?php 
        
        if($hero) {
          echo '<a href="'.$herolink.'"><img src="'.$hero['url'].'" alt="'.$hero['alt'].'" title="'.$hero['title'].'" /></a>';          
        } else {
          echo '<img src="'.get_bloginfo('stylesheet_directory').'/images/site-images/home-hero.jpg" alt="Home Hero"/>';    
        }
      ?>
    </div>
  </section>
  
  
  <div class="cta">
    <div class="wrap">
      <?php echo $herocta; ?>
    </div>
  </div>
  
  <section class="introduction">
    <div class="wrap">
      <?php echo $intro; ?>      
      <div class="grid">
        <div class="one-third first">
          <?php echo $left; ?>
        </div>
        
        <div class="one-third">
          <?php echo $middle; ?>
        </div>
        
        <div class="one-third">
          <?php echo $right; ?>
        </div>
      </div>
    </div>
  </section>
  
  <section class="blog lightbg">
    <div class="wrap">
      <div class="one-third first">
        <?php echo $blog; ?>
      </div>
     
      <div class="two-thirds">
         
      <?php 
        $args = array(
          'posts_per_page' => 1,
        );
        
        $post = new WP_Query($args);
        if($post) {
          while($post->have_posts()): $post->the_post();
          ?>
            <article <?php post_class(); ?>>
              <figure>
                <a href="<?php the_permalink(); ?>">
                  <?php 
                    if(has_post_thumbnail()) {
                      the_post_thumbnail('home-thumbnail');
                    } else {
                      echo '<img src="http://placehold.it/200x200" alt="placeholder"/>';
                    }
                  ?>
                </a>
              </figure>
              <header class="entry-header">
                <p class="entry-meta">
                  <time class="entry-time" itemprop="datePublished" datetime="<?php the_time('c'); ?>"><?php the_time('F j, Y'); ?></time>      
                </p>

                <h2 class="entry-title" itemprop="headline">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                </h2> 
              </header>
              
              <div class="entry-content" itemprop="text">
                <?php the_excerpt(); ?>
              </div>
              <footer class="entry-footer">
                <a href="<?php the_permalink(); ?>" class="alignleft">Continue Reading</a>
                <a href="<?php bloginfo('home'); ?>/blog" class="alignright">More Blog Posts</a>
              </footer>
            </article>
          <?php
          endwhile;
          
          wp_reset_postdata();
        }
        ?>
        
      </div>
    </div>
  </section>
<?php } genesis();




/***
  * Legacy Functions
  *
***/

//add_filter( 'wp_nav_menu_args' , 'ss_home_nav_menu' );
function ss_home_nav_menu( $args ) {
	if ( $args['theme_location'] == 'primary' ) { 
		$args['menu'] = 'MainHomeTop'; 
	}
	return $args;
}




//add_action('genesis_after_content', 'custom_front_page_footer_widget');  
function custom_front_page_footer_widget() {
    if ( is_active_sidebar(4) ) : ?>
        <div id="frontpage-footer-widgeted">
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Front Page Footer') ) : ?>
	    	<?php endif; ?> 
	   </div><!-- end #footer-widgeted -->
    <?php endif; 
}
?>