<?php

// Template Name: Search 

add_action( 'genesis_meta', 'search_genesis_meta' );
function search_genesis_meta() {
	remove_action( 'genesis_loop', 'genesis_do_loop' );
	add_action( 'genesis_loop', 'search_loop_helper' );
	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
	}

function search_loop_helper() {
    echo '<header class="entry-header">';
      genesis_do_post_title();
    echo '</header>';
    
    the_content(); 
    
    echo '<div class="one-half" style="float: none; margin:0 auto;">';
    get_search_form();
    echo '</div>';
    
    echo '<div class="clear full" style="clear:both; margin-top: 30px;margin-top:3rem;">';
    echo '<h2>Site Map</h2>';
    if(has_nav_menu('sitemap')) {
      $args = array(
      	'theme_location'  => 'sitemap',
      	'menu'            => '',
      	'container'       => 'div',
      	'container_class' => 'slickmap',
      	'container_id'    => 'slickmap',
      	'menu_class'      => 'menu slickmap col5',
      	'menu_id'         => '',
      	'echo'            => true,
      	'fallback_cb'     => '',
      	'before'          => '',
      	'after'           => '',
      	'link_before'     => '',
      	'link_after'      => '',
      	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
      	'depth'           => 0,
      	'walker'          => '',
      );
      wp_nav_menu( $args );
    } else {
      echo 'Please set the sitemap menu to the proper location in the menu settings.';
    }
    echo '</div>';

} genesis();