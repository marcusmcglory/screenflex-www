<?php
// Template Name: 404
//* Remove default loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_loop', 'genesis_404' );
/**
 * This function outputs a 404 "Not Found" error message
 *
 * @since 1.6
 */
function genesis_404() {

	echo genesis_html5() ? '<article class="entry">' : '<div class="post hentry">';

		printf( '<h1 class="entry-title">%s</h1>', __( 'Oops! The page cannot be found, error 404', 'genesis' ) );
		echo '<div class="entry-content">';

			if ( genesis_html5() ) :

				echo '<p>' . sprintf( __( 'The page you are looking for no longer exists. Perhaps you can return back to the site\'s <a href="%s">homepage</a> and see if you can find what you are looking for. Or, you can try finding it by using the search form below.', 'genesis' ), home_url() ) . '</p>';

				echo '<p>' . get_search_form() . '</p>';

			else :
	?>
			<p style="text-align:center;"><img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/404.jpg" alt="OOPS, Nothing Found" width="400" height="308" align="right" style="border:1px solid #333; margin:1em;" /></p>
			<h3>Please try the following:</h3>
			<ul> 
				<li>If you typed the page address in the address bar, make sure that it is spelled correctly.</li>
				<li>Click the back button to try another link.</li>
			</ul>
			
			<h3>Maybe you were looking for one of our most viewed pages:</h3>  
				
			<ul>
				<li>Home <a href="%s">https://www.screenflex.com/</a></li>
				<li>Sizes <a href="https://www.screenflex.com/room-dividers-portable-walls/freestanding/sizes/">https://www.screenflex.com/room-dividers-portable-walls/freestanding/sizes</a></li>
				<li>Colors <a href="https://www.screenflex.com/room-dividers-portable-walls/freestanding/colors/">https://www.screenflex.com/room-dividers-portable-walls/freestanding/colors/</a></li>
				<li>Interactive Room Divider <a href="https://www.screenflex.com/room-dividers-portable-walls/virtual-showroom/">https://www.screenflex.com/room-dividers-portable-walls/virtual-showroom/</a></li>
			</ul>
			
			<h3>Or, you can try finding it by using the search form or site-map below:</h3>
			

			
			<p><?php get_search_form(); ?></p>

			<h4><?php _e( 'Site Map Pages:', 'genesis' ); ?></h4>
			<ul>
				<?php wp_list_pages( 'exclude=6300,6910,6976,6980,6986,38873,39322,39950,44509&depth=3&sort_column=menu_order,post_title&title_li=' ); ?>
			</ul>

			<h4><?php _e( 'Categories:', 'genesis' ); ?></h4>
			<ul>
				<?php wp_list_categories( 'show_post_count=1&sort_column=name&title_li=' ); ?>
			</ul>

			<h4><?php _e( 'Monthly:', 'genesis' ); ?></h4>
			<ul>
				<?php wp_get_archives( 'type=monthly&show_post_count=1' ); ?>
			</ul>

			<h4><?php _e( 'Recent Posts:', 'genesis' ); ?></h4>
			<ul>
				<?php wp_get_archives( 'type=postbypost&limit=20' ); ?>
			</ul>

<?php
			endif;

			echo '</div>';

		echo genesis_html5() ? '</article>' : '</div>';

}

genesis();
