=== Based on Genesis Sample Theme ===

Github project link: https://github.com/copyblogger/genesis-sample



=== Installation Instructions ===

	1. Upload the Genesis SCSS 2.0 theme folder via FTP to your wp-content/themes/ directory. (The Genesis parent theme needs to be in the wp-content/themes/ directory as well.)
	
	2. Go to your WordPress dashboard and select Appearance.
	
	3. Activate the Genesis Sample theme.
	
	4. Inside your WordPress dashboard, go to Genesis > Theme Settings and configure them to your liking.



=== Using Genesis SCSS 2.0 Theme ===

This theme is based on the sample theme that ships with Genesis Framework.  The exception is that all of the CSS is broken down into SCSS parts.  As well as many other "extra options" included, for example:

	1. Compass: It is pre configured and ready to roll!  Just bring the file into CodeKit and it'll automatically know what to do. 
	
	2. Icon fonts:  Choose from Entypo or Font Awesome.  Simply uncomment the icon font you would like to use in scss/global.scss.  It is recommended to use only one font at a time, as there can be conflicts.
	
	3. Mixins:  I've loaded this theme with many @mixins from around the web and some of my own (this needs to be cleaned up a little still).  You can find them in scss/_mixins.scss.  This file contains many helpful mixins, including:
	
		A. Font Stacks.  Easily set a font site wide for body and headings, then utilize the font-stacks across your custom code to keep things consistant.  No more find and replace.
		
		B. Inline Media Queries: Break at custom breakpoints by simple adding @include break(break-size) to your code.  Please refer to the mixin to see the pre configured break points. 
		
		C. Key Frame Helper for animations.
		
		D. Image Filters.
	
	Please refer to http://sass-lang.com/documentation/file.SASS_REFERENCE.html#mixins for more information on @mixins
	
	4. Bits: A place to keep all of your variables.  Add common colors, sizes, etc to this file then refer to them in your code. 
	
	5. Standard Dev Plugins:
	
		A. Advanced Custom Fields:  This is the theme that I use to develop custom themes for clients, I use Advanced custom fields quite a bit.  So I preinstalled it into the theme.  There's nothing to do, just activate the theme and the power of ACF is included. 
	
		B. Admin Columns: This is great plugin that adds custom columns to your post types admin panel.
		
		C. Duplicate Posts: This is handy dev tool to quickly populate multiple posts for testing purposes. 
	
	6. jQuery: There is a global.js file to handle all of the themes jquery, turn it off you don't need it.  It's on by default.
	
	7. Custom Archive, Page, Single, Search Templates/Loops.  Just rename the file to your needs and create a custom loop.
	
	8. Custom Post Types.  Add custom post types by uncommenting line 4 in functions.php then set up your custom post type in lib/custom-post-types.php and adding your specific details.  There is a sample Taxonomy as well. 




=== Genesis Support ===

Please visit http://my.studiopress.com/help/ for theme support.




=== Genesis SCSS 2.0 Support ===

Please visit http://studio412design.com/contact for questions regarding this theme.
