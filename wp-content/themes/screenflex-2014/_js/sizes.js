jQuery(document).ready(function($) { 

$("td.productcode").hover(
    function(){
        //when hover, highlight the appropriate height & length.  
        //find out the currently hovered height.
        //apply style to the height header
        
        $(this).css("border","2px solid #f00");

        $(this).children('p').css("display","block").stop().animate({opacity:0}, 000).animate({opacity:1}, 500);
        $(this).parent().find('td.dividerlength').css("background-color", "#ccc");
    
        
        
        $('td.panelheight').eq($(this).index()-1).css("background-color", "#ccc");
        
                
        //find out the length.
        //apply style to the length header
        
    },
    function(){ 
        //turn off hover for Height & length
        $(this).css("border","1px solid #000");

        $(this).children('p').css("display","none").stop().animate({opacity:0}, 000).animate({opacity:1}, 500);
        $(this).parent().find('td.dividerlength').css("background-color", "#EEF5F8");
        $('td.panelheight').eq($(this).index()-1).css("background-color", "#EEF5F8");

    }
);

});