(function($) {
  $(document).ready(function() {
  
    // Toggle body class on navigation open.
    $('#navlist').click(function(){
      $('body').toggleClass('navopen');
    });
    
    // Features Page
    $('.circles li').click(function(){
			var id = $(this).attr('id');
			$('.features-canvas').attr('data-active', id);
			$(this).addClass('active');
			$(this).siblings().removeClass('active');
		});
		
		$('.popovers .icon-cross').click(function(){
      $('.features-canvas').attr('data-active', '');
      $('.circles li').removeClass('active');
		});
		
		// Catalog Requests
		var catRequest = $('.catalog-request');
		//$('.catalog-request').hide();
		$('.priceguide-cta').mouseover(function(){
			//catRequest.addClass('open');
    });
    $('.priceguide-cta').click(function(){
			catRequest.toggleClass('open');
    });
    $('.close-catalog').click(function(){
      catRequest.removeClass('open');
    });
    $('.site-inner').click(function(){
      catRequest.removeClass('open');
    });
    $(document).keyup(function(e) {
      if (e.keyCode === 27) {
        catRequest.removeClass('open');
      }
    });
		
		$('#showcolors').click(function(){
      $('.color-select').fadeIn('slow');
		});
		
		$('.close-colors').click(function(){
      $('.color-select').fadeOut('slow');
		});
		
		// Taxonomy Navigation
		$(".page-changer #submit").hide();

		$(".page-changer select").change(function() {
			window.location = $(this).children('select option:selected').val();
		});

		//
		/*function createImg(url, fn) {
        var img = new Image();
        img.onload = fn;
        img.src = url;
        return(img);
    }*/
    
		/*$(window).bind('load', function() {
      createImg('/wp-content/themes/screenflex-2014/images/features/features-hero-big.jpg');
		});
		*/
		$('.zoomon').click(function(){
      //$('.feature-image').zoom({url: '/wp-content/themes/screenflex-2014/images/features/features-hero-big.jpg'});
      $('#featureHero').addClass('featurezoom');
       $(this).hide();
      $('.hover').show();
      $('.circles').fadeOut('slow');
      $('.popovers').fadeOut('slow');
      $('.zoomoff').show();
      $('#mlens_target_0').removeClass('hide');
      
    });
    
    $('.zoomoff').click(function(){
      $('#featureHero').removeClass('featurezoom');
      $(this).hide();
      $('.hover').hide();
      $('.zoomon').show();
      $('.circles').fadeIn('slow');
      $('.popovers').fadeIn('slow');
      $('#mlens_target_0').addClass('hide');
    });
    
    $("#featureHero").mlens( {
        imgSrc: $("#featureHero").attr("data-big"),
        lensShape: "circle",
        lensSize: 280,
        borderSize: 4,
        borderColor: "#fff",
        borderRadius: 0,
        overlayAdapt: true,
        zoomLevel: 1
    });
    $('#mlens_target_0').addClass('hide');
		
		var History = window.History; // Note: We are using a capital H instead of a lower h
    if ( !History.enabled ) {
         // History.js is disabled for this browser.
         // This is because we can optionally choose to support HTML4 browsers or not.
        return false;
    }
    
    var colorSelect = $('.color-select');
    var screen = $('#screen');
    var otherLengthBox = $('#otherLengthBox');
    // Sizes and Colors
    $('.height-select li').click(function(){
			var id = $(this).attr('id');
			$('.screen').attr('data-height', id);
			$(this).addClass('active');
			$(this).siblings().removeClass('active');
		});
		
		$('.length-select li').click(function(){
			var id = $(this).attr('id');
			$('.screen').attr('data-length', id);
			$(this).addClass('active');
			$(this).siblings().removeClass('active');
			$("#roomdividerCaption").fadeOut();
      otherLengthBox.fadeOut();
		});
		
		$('.color-select li').click(function(){
			var id = $(this).attr('id');
			//var link = $(this).attr('data-link');
			var title = $(this).attr('data-title');
			screen.attr('data-color', id);
			$('.color-select li').removeClass('active');
			$(this).addClass('active');
			$('.view-colors').attr('data-color', id);
			$('.zoom').attr('href', '/wp-content/uploads/colors/'+id+'.jpg');
			$('.zoom').attr('title', title);
		});
				
		$('.color-select[data-open="false"]').hide();
		
		$('.view-colors').click(function(){
      var el = $(this).children('.text');
      el.text(el.text() === el.data("text-swap") ? el.data("text-original") : el.data("text-swap"));
      colorSelect.slideToggle('slow');
      colorSelect.attr('data-open', colorSelect.attr('data-open') === 'false' ? 'true' : 'false');
      $('.scotchgard').slideToggle('slow');
      $('.arrow').toggleClass('icon-arrow-down-3').toggleClass('icon-arrow-up');
      if($('.arrow').hasClass('icon-arrow-up')) {
        $('html,body').animate({scrollTop: 250 } , 'slow');
      }
      if($('.arrow').hasClass('icon-arrow-down-3')) {
        $('html,body').animate({scrollTop: 0 } , 'slow');
      }
    });
    
    
    $('.show-other').click(function(){
      otherLengthBox.slideToggle('slow');
    });
    
    $('.close-other').click(function(){
      otherLengthBox.slideToggle('slow');
    });
    
    $('#otherLengthAction').click(function(){
        var length = '';
        var ft = $("#otherLengthFeet").val();
        var inches = $("#otherLengthInches").val();
        var size = ft+'feet-'+inches+'inches';
        $("#roomdividerCaption").hide();
        if      ((ft < 5 ) || ((5 == ft)  && (inches <= 9))) { length = 'five_nine'; }
        else if ((ft < 9 ) || ((9 == ft)  && (inches <= 5))) { length = 'nine_five';  }
        else if ((ft < 13) || ((13 == ft) && (inches <= 1))) { length = 'thirteen_one';}
        else if ((ft < 16) || ((16 == ft) && (inches <= 9))) { length = 'sixteen_nine'; }
        else if ((ft < 20) || ((20 == ft) && (inches <= 5))) { length = 'twenty_five'; }
        else if ((ft < 24) || ((24 == ft) && (inches <= 1))) { length = 'twentyfour_one'; }
        else {
          length = 'twentyfour_one';
          $("#roomdividerCaption").fadeIn();
        }
        $('.screen').attr('data-length', length);
        $('.screen').attr('data-customsize', size);
        $('#'+length).addClass('active');
        $('#'+length).siblings().removeClass('active');
        otherLengthBox.fadeOut('slow');
    });
		
		$('.color-select li, .length-select li, .height-select li, .view-colors, #otherLengthAction').click(function(){
      var length = screen.attr('data-length');
      var height = screen.attr('data-height');
      var color = screen.attr('data-color');
      var customsize = screen.attr('data-customsize');
      var colorSelection = colorSelect.attr('data-open');
      
      var constructState = '?height='+height+'&length='+length+'&color='+color+'&open='+colorSelection+'&customsize='+customsize;
      
      History.replaceState(null, null, constructState);
      $('.request a').attr("href", '/request-quote/'+constructState);
		});
			
  });
})(jQuery);