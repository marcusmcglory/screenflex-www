<?php
add_action( 'genesis_meta', 'plans_genesis_meta' );
function plans_genesis_meta() {
  remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
  remove_action( 'genesis_entry_header', 'genesis_post_info', 1 );
  add_action('genesis_loop', 'ss_add_plans_dropdown', 1);
  add_action( 'genesis_before_loop', 'ss_full_content_specific_categories' );
  add_filter('get_the_content_more_link', 'empty_read_more_link');
  add_filter('the_content_more_link', 'custom_read_more_link');
  add_filter( 'post_class', 'be_archive_post_class' );
	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
}

  
// Adjust the number of photos to 24 per page
global $query_string;
query_posts( $query_string . '&posts_per_page=24' );

// add custom classes to post_class()
function be_archive_post_class( $classes ) {
	$classes[] = 'one-half';
	global $wp_query;
	
	if( 0 == $wp_query->current_post || 0 == $wp_query->current_post % 2 ) {
		$classes[] = 'first';
  }
  
	return $classes;
}
  
// Edit the read more link text


function ss_add_plans_dropdown() {
    // Auto setup the title of the term archive if we're not on the post type archive. 
    if(!is_post_type_archive()) {
      global $wp_query;
      $tax = $wp_query->get_queried_object();
      echo '<div class="archive-description cpt-archive-description">';
        echo '<h1 class="archive-title">Plans - ' . $tax->name . ' Rooms</h1>';
        //echo '<p></p>';
      echo '</div>';
    }
    echo '<div class="catnavs">';
      if(is_post_type_archive() || is_tax('rooms')) {
        theme_tax_nav('rooms', 'rooms', 'How Many Rooms Do You Need?', 'plans', 'one-fourth first');
      }
    echo '</div>';
}
