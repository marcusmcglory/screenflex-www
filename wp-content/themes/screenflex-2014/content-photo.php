<?php
add_action( 'genesis_meta', 'photos_genesis_meta' );
function photos_genesis_meta() {
  remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
  remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
  add_action('genesis_loop', 'ss_add_photo_dropdown', 1);
  add_action( 'genesis_before_loop', 'ss_full_content_specific_categories' );
  add_filter('get_the_content_more_link', 'empty_read_more_link');
  add_filter('the_content_more_link', 'custom_read_more_link');
  add_filter( 'genesis_pre_get_option_content_archive', 'ss_do_full_content' );
  add_filter( 'genesis_pre_get_option_content_archive_limit', 'ss_no_content_limit' );
  add_filter( 'post_class', 'be_archive_post_class' );
	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
}
  
// Adjust the number of photos to 24 per page
global $query_string;
query_posts( $query_string . '&posts_per_page=24' );

// add custom classes to post_class()
function be_archive_post_class( $classes ) {
	$classes[] = 'one-fourth';
	global $wp_query;
	
	if( 0 == $wp_query->current_post || 0 == $wp_query->current_post % 4 ) {
		$classes[] = 'first';
  }
  
  if(has_term('before-and-after', 'use')) {
    $classes[] = 'before-after';
  }
  
	return $classes;
}
  
// Edit the read more link text
function custom_read_more_link() {
	return '';
}

function ss_add_photo_dropdown() {
    // Auto setup the title of the term archive if we're not on the post type archive. 
    if(!is_post_type_archive()) {
      global $wp_query;
      $tax = $wp_query->get_queried_object();
      echo '<div class="archive-description cpt-archive-description">';
        echo '<h1 class="archive-title">Room Divider Photos - ' . $tax->name . '</h1>';
        echo '<p>Photos provided by our Customers</p>';
      echo '</div>';
    }
    echo '<div class="catnavs">';
      if(is_post_type_archive() || is_tax('use')) {
        theme_tax_nav('use', 'use', 'Sort by Use/Facility', 'photos', 'one-fourth first');
      }
      if(is_tax('length')) { theme_tax_nav('length', 'length', 'Sort by Length', 'photos', 'one-fourth first'); }
      if(is_tax('height')) { theme_tax_nav('height', 'height', 'Sort by Height', 'photos', 'one-fourth first'); }
      if(is_tax('color')) { theme_tax_nav('color', 'color', 'Sort by Color', 'photos', 'one-fourth first'); }
    echo '</div>';
    
    echo '<div class="beforeafter">';
      echo '<span>'.do_shortcode('[svgicon icon="beforeafter"]').'</span> = Before & After';
    echo '</div>';
}