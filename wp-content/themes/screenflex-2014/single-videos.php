<?php  

add_action( 'genesis_meta', 'single_genesis_meta' );
function single_genesis_meta() {
	remove_action( 'genesis_loop', 'genesis_do_loop' );
	add_action( 'genesis_loop', 'single_loop_helper' );
	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
	wp_enqueue_script( 'fitvids', CHILD_URL . '/js/jquery.fitvids.min.js', array('jquery'), '1.0', false);
	}

function single_loop_helper() { 
	wp_reset_query();  
	//$filesrc = get_field('file_download');
	//$filesize = get_field('file_size');
	//$dimensions = get_field('dimensions');
	//$designer = get_field('designer');
	$length = get_field('video_length');
	$video = get_field('video');
	$vheight = get_field('video_height');
	$vwidth = get_field('video_width');
	
	if(!$vwidth) {
  	$vwidth = '620';
	}
	if(!$vheight) {
  	$vheight = '360';
	}
	?>
	<div class="sample-video">
		<div class="wrap">
			<?php
			preg_match( '/[\\?\\&]v=([^\\?\\&]+)/', $video, $matches);
			// $matches[1] should contain the youtube id
			
			echo '<iframe src="https://www.youtube.com/embed/'.$matches[1].'?hd=1&autoplay=1&wmode=transparent&autohide=1&modestbranding=1&showinfo=0&vq=hd1080&rel=0" frameborder="0" width="'.$vwidth.'" height="'.$vheight.'"></iframe>';
			
			?>
			
			<div class="entry-heading">
				<h1><?php the_title(); ?></h1>
				
				<div class="share">
					<a target="_blank" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>" class="icon-twitter"></a>
					<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>%2F&t=<?php the_title(); ?>" class="icon-facebook"></a>
					<a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" class="icon-googleplus"></a>
				</div>
				
			</div>
		</div>
	</div>
		
	<div class="entry-items">
		<div class="wrap">
			<div class="one-third first">
  			<div class="entry-details">
  				<h2>Description</h2>
  				<?php the_field('short_description'); ?>
  				<?php
  				$terms = get_the_terms( $post->ID, 'collections' );
          if ( $terms && ! is_wp_error( $terms ) ) { 
  				  echo '<h2>Catalog</h2>';
  				}
  				?>
  				<?php collections('link'); ?>
  				
  				<?php // <h2>Tags</h2> ?>
  				<?php 
  				$args = array(
  				    'smallest'                  => 16, 
  				    'largest'                   => 16,
  				    'unit'                      => 'px', 
  				    'number'                    => 0,  
  				    'format'                    => 'flat',
  				    'separator'                 => ', ',
  				    'orderby'                   => 'name', 
  				    'order'                     => 'ASC',
  				    'exclude'                   => null, 
  				    'include'                   => null, 
  				    'topic_count_text_callback' => default_topic_count_text,
  				    'link'                      => 'view', 
  				    'taxonomy'                  => 'video-tags', 
  				    'echo'                      => true
  				); 
  				//wp_tag_cloud( $args ); ?>
  				
  				<?php if($length) { ?>
  				<h2>Length: <span><?php echo $length; ?></span></h2>
  				<?php }	?>
  			  
  			</div>
        <h2><a href="/videos"><i class="icon-video"></i> All Videos</a></h2>
			</div>
			
			<div class="similar-items two-thirds">
			  <?php 
			    if( '' !== get_post()->post_content ) {
  			    echo '<h2>Transcription</h2>';
  			    the_content();
  			   }
  			 ?>
			  
				<h2>Similar Videos</h2>
				
				<?php 
					$terms = get_the_terms( $post->ID, 'collections' );
					
          if ( $terms && ! is_wp_error( $terms ) ) { 

  					$terms_array = array();
  					
  					foreach($terms as $term) {
  						$terms_array[] = $term->slug;
  					}
  					
  					$terms_joined = join( ", ", $terms_array );
  					$args = 
  						array(
  							'post_type' => 'videos',
  							'posts_per_page'=> 4,
  							'orderby' => 'rand',
  							'post__not_in' => $post->ID,
  							'tax_query' => 
  								array(
  									array(
  										'taxonomy' => 'collections',
  										'field' => 'slug',
  										'terms' => $terms_joined
  									)
  								)
  						);
  					
  					$theposts = get_posts($args);
  					
  					foreach($theposts as $p) {
  						$ptitle = get_the_title();
  						$rtitle = get_the_title($p->ID);
  
  						$stitle = substr($rtitle, 0, 29);
  						if($rtitle != $ptitle) { 
  							if(strlen($rtitle) >= 30) {
  								$rtitle = $stitle.'&hellip;';
  							} 
  						?>
  							<div class="video related-video video-<?php echo $p->ID; ?>">
  								<h3><a href="<?php echo post_permalink($p->ID); ?>"><?php echo $rtitle; ?></a></h3>
  								<a href="<?php echo post_permalink($p->ID); ?>" class="video-thumb"><span class="icon-play"></span><?php if(has_post_thumbnail($p->ID)) { echo get_the_post_thumbnail($p->ID, 'Video Thumb'); } else {  echo '<img src="'.get_bloginfo('stylesheet_directory').'/images/no-thumb.jpg" alt="'.get_the_title().'" />'; } ?></a>
  							</div>
  							<?php 
  						}
  					}
  				} else {
    				echo '<p>No similar videos at the moment</p>';
  				}
				?>
			</div>
		</div>
	</div>
	
	<script>
  jQuery(document).ready(function(){
    // Target your .container, .wrapper, .post, etc.
    jQuery('.wrap').fitVids();
  });
  </script>
	
<?php  } genesis(); ?>

