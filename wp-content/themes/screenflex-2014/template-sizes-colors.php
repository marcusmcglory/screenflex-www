<?php 

// Template Name: Sizes and Colors

add_action( 'genesis_meta', 'sizes_colors_genesis_meta' );
function sizes_colors_genesis_meta() {
	remove_action( 'genesis_loop', 'genesis_do_loop' );
	add_action( 'genesis_loop', 'sizes_colors_loop_helper' );
	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
	add_action('genesis_before_footer', 'custom_content', 2);
}

function sizes_colors_loop_helper() { 
  // URL Variables
  $length = $_GET['length'];
  $height = $_GET['height'];
  $color  = $_GET['color'];
  $open  = $_GET['open'];
  
  if(!$length) { $length = 'twentyfour_one';}
  if(!$height) { $height = 'eight';}
  if(!$color) { $color = 'db'; }
  if(is_page(4495)){
    $open = 'true';
    $itext = 'icon-arrow-up-2';
  } elseif(!$open || $open == 'false' && is_page(4673)) { 
    $open = 'false'; 
    $itext = 'icon-arrow-down-4';
  } else {
    $open = 'true';
    $itext = 'icon-arrow-up-2';
  }
  
  function is_active_attr($set) {
    $length = $_GET['length'];
    $height = $_GET['height'];
    $color  = $_GET['color'];
    if(!$length) { $length = 'twentyfour_one';}
    if(!$height) { $height = 'eight';}
    if(!$color) { $color = 'db'; }
    if($color == $set || $height == $set || $length == $set) {
      echo 'class="active"';
    } else {
      echo 'class="inactive"';
    }
  }  
?>
    <article <?php post_class(); ?>>
      <div class="screen-canvas">
        <div class="wrap">
          <header class="entry-header">
              <?php genesis_do_post_title(); ?>
          </header>
          
          <div class="app">
            <div class="placeholder"></div>
            <div class="screen-wrapper">
              <div id="screen" class="screen" data-height="<?php echo $height; ?>" data-color="<?php echo $color; ?>" data-length="<?php echo $length; ?>">
            		<div class="panel one">
              		<img src="<?php echo CHILD_URL; ?>/images/sizes-colors/sc-left-upper.png" class="upper" />
              		<img src="<?php echo CHILD_URL; ?>/images/sizes-colors/sc-left-lower.png" class="lower" />
            		</div>
            		<div class="panel two">
              		<img src="<?php echo CHILD_URL; ?>/images/sizes-colors/sc-right-upper.png" class="upper" />
              		<img src="<?php echo CHILD_URL; ?>/images/sizes-colors/sc-right-lower.png" class="lower" />
            		</div>
            		<div class="model">
              		<img src="<?php echo CHILD_URL; ?>/images/sizes-colors/sc-model.png" class="lower" />
            		</div>
            		 
                <a class="zoom thickbox" title="" href="<?php bloginfo('home'); ?>/wp-content/uploads/colors/<?php echo $color;?>.jpg" ><i class="icon-search"></i> Fabric Zoom</a>
                
                <div id="roomdividerCaption" class="request">
                  <p>The size you requested requires multiple dividers.
                  <br />
                  <a href='#'>Click submit</a> for pricing, and a space planning specialist will provide you with the best solution and quote for your needs.</p>
                </div>  
              </div>
            </div>
            
            <form id="otherLengthBox">
                  <label for="otherLengthFeet">Length:</label>
                  <input type="text" value="" size="2" id="otherLengthFeet" name="otherLengthFeet" />
                  <span>(feet)</span>
                  <label for="otherLengthInches"></label>
                  <input type="text" value="" size="2" id="otherLengthInches" name="otherLengthInches" />
                  <span>(inches)</span>
                  <br />
                  <a href="#" id="otherLengthAction" class="button">Next</a>
                  <i class="icon-cross close-other"></i>
              </form>

            
            <div class="height-select">
              <h2 class="tablet mobile">Select a <span>Height</span></h2>
          		<ul>
          			<li id="eight" <?php is_active_attr('eight'); ?>>8' 0"</li>
          			<li id="seven_four" <?php is_active_attr('seven_four'); ?>>7' 4"</li>
          			<li id="six_eight" <?php is_active_attr('six_eight'); ?>>6' 8"</li>
          			<li id="six" <?php is_active_attr('six'); ?>>6' 0"</li>
          			<li id="five" <?php is_active_attr('five'); ?>>5' 0"</li>
          			<li id="four" <?php is_active_attr('four'); ?>>4' 0"</li>
          		</ul>
          		<h2 class="desktop">Select a <span>Height</span></h2>
          	</div>
          	
          	<div class="length-select">
          		<h2>Select a <span>Length</span></h2>
          		<ul>
          		  <li id="closed" <?php is_active_attr('closed'); ?>>Closed Unit (2' x 3')</li>
          			<li id="five_nine" <?php is_active_attr('five_nine'); ?>>5' 9"</li>
           			<li id="nine_five" <?php is_active_attr('nine_five'); ?>>9' 5"</li>
          			<li id="thirteen_one" <?php is_active_attr('thirteen_one'); ?>>13' 1"</li>
          			<li id="sixteen_nine" <?php is_active_attr('sixteen_nine'); ?>>16' 9"</li>
          			<li id="twenty_five" <?php is_active_attr('twenty_five'); ?>>20' 5"</li>
          			<li id="twentyfour_one" <?php is_active_attr('twentyfour_one'); ?>>24' 1"</li>
          			<li class="show-other tablet inactive">Don't see your size?</li>
          		</ul>
          		<p class="show-other desktop">Don't see your size?</p>
          	</div>
          	<a name="swatches"></a>
            <h2 data-color="<?php echo $color; ?>" class="view-colors">Select a <span>Color</span><i class="swatch"><i class="arrow <?php echo $itext; ?>"></i></i></h2>
            
            <div class="request-wrapper">
              <h2 class="request button top"><a href="/request-quote">Submit for Price</a></h2>
            </div>

            <div class="color-select" data-open="<?php echo $open; ?>">
          	  <div class="wrap">
            		<h2>Designer Fabric <span>- 100% Recycled Polyester</span></h2>
            		<?php // sample li:  <li id="" data-color=""><i></i>Name<br /><span>##</span></li> ?>
            		<ul>
            			<li id="db" data-color="db" data-title="Lake"<?php is_active_attr('db'); ?>><i class="swatch"></i>Lake<br /><span>db</span></li>
            			<li id="dg" data-color="dg" data-title="Stone" <?php is_active_attr('dg'); ?>><i class="swatch"></i>Stone<br /><span>dg</span></li>
            			<li id="dm" data-color="dm" data-title="Rose" <?php is_active_attr('dm'); ?>><i class="swatch"></i>Rose<br /><span>dm</span></li>
            			<li id="dn" data-color="dn" data-title="Mallard" <?php is_active_attr('dn'); ?>><i class="swatch"></i>Mallard<br /><span>dn</span></li>
            			<li id="do" data-color="do" data-title="Walnut" <?php is_active_attr('do'); ?>><i class="swatch"></i>Walnut<br /><span>do</span></li>
            			<li id="dw" data-color="dw" data-title="Desert" <?php is_active_attr('dw'); ?>><i class="swatch"></i>Desert<br /><span>dw</span></li>
            			<li id="dx" data-color="dx" data-title="Charcoal Black" <?php is_active_attr('dx'); ?>><i class="swatch"></i>Charcoal Black<br /><span>dx</span></li>
            			<li id="dt" data-color="dt" data-title="White" <?php is_active_attr('dt'); ?>><i class="swatch"></i>White<br /><span>dt</span></li>
            			<li id="ds" data-color="ds" data-title="Primary Blue" <?php is_active_attr('ds'); ?>><i class="swatch"></i>Primary Blue<br /><span>ds</span></li>
            			<li id="dy" data-color="dy" data-title="Primary Yellow" <?php is_active_attr('dy'); ?>><i class="swatch"></i>Primary Yellow<br /><span>dy</span></li>
            			<li id="dr" data-color="dr" data-title="Primary Red" <?php is_active_attr('dr'); ?>><i class="swatch"></i>Primary Red<br /><span>dr</span></li>  
            		</ul>
            		
            		<h2>Excel Fabric <span>- 100% Recycled Polyester</span></h2>
            		<ul>
            		  <li id="eb" data-color="eb" data-title="Summer Blue" <?php is_active_attr('eb'); ?>><i class="swatch"></i>Summer Blue<br /><span>eb</span></li>
            		  <li id="eg" data-color="eg" data-title="Grey Smoke" <?php is_active_attr('eg'); ?>><i class="swatch"></i>Grey Smoke<br /><span>eg</span></li>
            		  <li id="em" data-color="em" data-title="Cranberry" <?php is_active_attr('em'); ?>><i class="swatch"></i>Cranberry<br /><span>em</span></li>
            		  <li id="en" data-color="en" data-title="Sea Green" <?php is_active_attr('en'); ?>><i class="swatch"></i>Sea Green<br /><span>en</span></li>
            		  <li id="eo" data-color="eo" data-title="Beech" <?php is_active_attr('eo'); ?>><i class="swatch"></i>Beech<br /><span>eo</span></li>
            		  <li id="ew" data-color="ew" data-title="Sand" <?php is_active_attr('ew'); ?>><i class="swatch"></i>Sand<br /><span>ew</span></li>
            		</ul>
            		
            		<div class="upgraded-fabrics">
            		  <h2 class="upgrade">Upgraded Fabric and Vinyl Colors</h2>
            		  <h2>Royal Vinyl <span>- 100% Recycled Vinyl</span></h2>
            		  <ul>
            		    <li id="vb" data-color="vb" data-title="Blue Tide" <?php is_active_attr('vb'); ?>><i class="swatch"></i>Blue Tide<br /><span>vb</span></li>
            		    <li id="vg" data-color="vg" data-title="Granite" <?php is_active_attr('vg'); ?>><i class="swatch"></i>Granite<br /><span>vg</span></li>
            		    <li id="vm" data-color="vm" data-title="Raspberry Mist" <?php is_active_attr('vm'); ?>><i class="swatch"></i>Raspberry Mist<br /><span>vm</span></li>
            		    <li id="vn" data-color="vn" data-title="Mint" <?php is_active_attr('vn'); ?>><i class="swatch"></i>Mint<br /><span>vn</span></li>
            		    <li id="vo" data-color="vo" data-title="Sandalwood" <?php is_active_attr('vo'); ?>><i class="swatch"></i>Sandalwood<br /><span>vo</span></li>
            		    <li id="vw" data-color="vw" data-title="Hazelnut" <?php is_active_attr('vw'); ?>><i class="swatch"></i>Hazelnut<br /><span>vw</span></li>
            		    <li id="vx" data-color="vx" data-title="Coal" <?php is_active_attr('vx'); ?>><i class="swatch"></i>Coal<br /><span>vx</span></li>
            		    <li id="vt" data-color="vt" data-title="Ice" <?php is_active_attr('vt'); ?>><i class="swatch"></i>Ice<br /><span>vt</span></li>
            		  </ul>
            		  
            		  <h2>Select Fabric <span>- 100% Recycled Polyester</span></h2>
            		  <ul>
            		    <li id="sb" data-color="sb" data-title="Blue" <?php is_active_attr('sb'); ?>><i class="swatch"></i>Blue<br /><span>sb</span></li>
            		    <li id="sg" data-color="sg" data-title="Grey" <?php is_active_attr('sg'); ?>><i class="swatch"></i>Grey<br /><span>sg</span></li>
            		    <li id="sm" data-color="sm" data-title="Mauve" <?php is_active_attr('sm'); ?>><i class="swatch"></i>Mauve<br /><span>sm</span></li>
            		    <li id="sn" data-color="sn" data-title="Green" <?php is_active_attr('sn'); ?>><i class="swatch"></i>Green<br /><span>sn</span></li>
            		    <li id="so" data-color="so" data-title="Oatmeal" <?php is_active_attr('so'); ?>><i class="swatch"></i>Oatmeal<br /><span>so</span></li>
            		    <li id="sw" data-color="sw" data-title="Wheat" <?php is_active_attr('sw'); ?>><i class="swatch"></i>Wheat<br /><span>sw</span></li>
            		    <li id="sx" data-color="sx" data-title="Black" <?php is_active_attr('sx'); ?>><i class="swatch"></i>Black<br /><span>sx</span></li>
            		  </ul>
            		  
            		  <h2>Pinnacle Fabric <span>- 100% Recycled Polyester</span></h2>
            		  <ul>
            		    <li id="pb" data-color="pb" data-title="Blue Sky" <?php is_active_attr('pb'); ?>><i class="swatch"></i>Blue Sky<br /><span>pb</span></li>
            		    <li id="pg" data-color="pg" data-title="Platinum" <?php is_active_attr('pg'); ?>><i class="swatch"></i>Platinum<br /><span>pg</span></li>
            		    <li id="pn" data-color="pn" data-title="Green Grass" <?php is_active_attr('pn'); ?>><i class="swatch"></i>Green Grass<br /><span>pn</span></li>
            		    <li id="po" data-color="po" data-title="Oak" <?php is_active_attr('po'); ?>><i class="swatch"></i>Oak<br /><span>po</span></li>
            		    <li id="pw" data-color="pw" data-title="Prairie" <?php is_active_attr('pw'); ?>><i class="swatch"></i>Prairie<br /><span>pw</span></li>
            		    <li id="px" data-color="px" data-title="Black Pearl" <?php is_active_attr('px'); ?>><i class="swatch"></i>Black Pearl<br /><span>px</span></li>
            		  </ul>
            		</div>
          	  </div>
          	</div>
            
            <div class="fabric-swatch-form scotchgard screenflex-only">
            <p><a href="/fabric-card-request/?location=middle-module-icon"><?php echo svg_fabricswatch(); ?></a></p><h3><a href="/fabric-card-request/?location=middle-module-header">Request Free Fabric Samples</a></h3><p>We would be happy to mail you actual fabric swatches of any colors you would like to see.</p>
            </div>
                                  
          </div>
        </div>
      </div>
      
    </article>    
<?php } genesis(); ?>