<?php 

// Template Name: Product 

add_action( 'genesis_meta', 'product_genesis_meta' );
function product_genesis_meta() {
	remove_action( 'genesis_loop', 'genesis_do_loop' );
	add_action( 'genesis_loop', 'product_loop_helper' );
	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
	add_action('genesis_before_footer', 'custom_content', 2);
	}

function product_loop_helper() { 
    // Variables
    $hero = get_field('hero_image');
    $heights = get_field('heights');
    $lengths = get_field('lengths');
    $depth = get_field('depth');
    $exclude = get_field('exclude');
    $features = get_field('features');
    $colorObject = get_field_object('colors'); // grab the colors object so we can use the labels. 
    $colorValue = get_field('colors'); // grab just the active set colors value.
    $colorLabel = $colorObject['choices']; // pull the array of all choices to gain access to the labels.
    $colorCount = count($colorValue); 
    $options = get_field('options_link');
    $recuse = get_field('recommended_use');
    $recusesample = get_field('recommended_samples');
?>
  <section <?php post_class(); ?>>
    <div class="wrap">
      <header class="entry-header">
          <?php genesis_do_post_title(); ?>
      </header>
      
       <div class="image-storage two-thirds alignright">
        <?php
          if($hero) {
            echo '<img src="'.$hero['sizes']['large'].'" alt="'.$hero['alt'].'" />';
          }
          
          if( $exclude && in_array('storage', $exclude)) { 
          } else { ?>
            <div class="storage">
        	    <div class="box-module">
          	    <div class="two-feet">3'</div>
                <div class="three-feet">2'</div>
                <div class="box"></div>
        	    </div>
        	    <p>All room dividers fold into approx. 2' x 3' footprint for easy storage & transportation</p>
        	  </div>
    	  <?php } ?>
      </div>


      <?php if($heights || $widths || $features || $colors || $options) { ?>
        <div class="information one-third entry-content alignleft">
        
          <?php 
            if($heights || $lengths || $depth) {
              echo '<h2>Sizes</h2>';
              echo '<p>';
                if($heights) { echo $heights.'<br/>'; }
                if($lengths) { echo $lengths.'<br/>'; }
                if($depth)   { echo $depth; }
              echo '</p>';
            }
            
            if($features) {
              echo '<h2>Features</h2>';
              echo '<ul class="features">';
                foreach($features as $f) {
                  $img = $f['image'];
                  $title = $f['title'];
                  
                  echo '<li>';
                  
                  if($img) { echo '<a href="'.$img['url'].'" title="'.$title.'" class="thickbox"> '; }
                  
                  echo $title;
                  
                  if($img) { echo '</a>'; }
                  
                  echo '</li>';
              }
            echo '</ul>';
            }
            
            if($colorValue) {
              if($colorCount < 5) {
                echo '<h2>Colors</h2>';
                echo '<div class="color-select open">';
                  echo '<ul>';
                    foreach($colorValue as $c) {
                      $label = $colorLabel[$c];
                        echo '<li id="'.$c.'" data-color="'.$c.'" data-title="'.$label.'"><i class="swatch"></i>'.$label.'<br /><span>'.$c.'</span></li>';
                    }
                  echo '</ul>';
                echo '</div>';
              } else {
                echo '<h2 id="showcolors">Colors &raquo;</h2>';
                echo '<div class="color-select">';
                  echo '<div class="wrap">';
                    echo '<h3>Available Colors</h3>';
                    echo '<ul>';
                    foreach($colorValue as $c) {
                      $label = $colorLabel[$c];
                      echo '<li id="'.$c.'" data-color="'.$c.'" data-title="'.$label.'"><i class="swatch"></i>'.$label.'<br /><span>'.$c.'</span></li>';
                    }
                    echo '</ul>';
                    echo '<i class="icon-cross close-colors"></i>';
                  echo '</div>';
                echo '</div>';
              }
            }
            
            if($options) {
              echo '<h2><a href="'.$options.'">Options &raquo;</a></h2>';
            }
            
            if( $exclude && in_array('model', $exclude)) { 
            } else {
              echo '<h2><a href="/products/">Product Comparison Chart &raquo;</a></h2>';
            }
          ?>
        </div>
      <?php 
      
      } // End if sizes, features, colors or options
      
      ?>
    </div>
  </section>
  
  <div class="recommended">
    <div class="wrap">
      <h2>Recommended for use in:</h2>
      <h3><?php echo $recuse; ?><h3>
      
      <?php
        $count = 0;
        foreach($recusesample as $r) {
          $i = $count++;
          if($i == 0 || $i%3 == 0) { $first = 'first'; } else { $first = ''; }
          ?>
          <div class="recommend one-third <?php echo $first; ?>">
            <?php if($r['link']) { ?><a href="<?php echo $r['link']; ?>"><?php } ?>
              <img src="<?php echo $r['image']['sizes']['recommended']; ?>" alt="<?php echo $r['image']['alt']; ?>" />
            <?php if($r['link']) { ?></a><?php } ?>
            <h4>
              <?php if($r['link']) { ?><a href="<?php echo $r['link']; ?>"><?php } ?>
              <?php echo $r['title']; ?>
              <?php if($r['link']) { ?></a><?php } ?></h4>
          </div>
      <?php
        }
      ?>
    </div>
  </div>
<?php   
} 

genesis();

?>