<?php
//* On Your Mark, Get Set, Start the engine
include_once( get_template_directory() . '/lib/init.php' );
include_once( get_stylesheet_directory() . '/lib/shortcodes.php' );
include_once( get_stylesheet_directory() . '/lib/custom-fields.php' );
include_once( get_stylesheet_directory() . '/lib/custom-modules.php' );
include_once( get_stylesheet_directory() . '/lib/custom-loops.php' );
include_once( get_stylesheet_directory() . '/lib/custom-post-types.php' );
include_once( get_stylesheet_directory() . '/lib/gravity-forms.php' );
include_once( get_stylesheet_directory() . '/lib/svg.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Screenflex' );
define( 'CHILD_THEME_URL', 'http://www.screenflex.com' );
define( 'CHILD_THEME_DEV', 'Studio412 Design' );
define( 'CHILD_THEME_VERSION', '1.0.0' );
define( 'CHILD_URL', get_bloginfo('stylesheet_directory'));
define( 'SVG_URL', get_bloginfo('stylesheet_directory').'/images/svg/');

//* Reposition the breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );
//add_action( 'genesis_after_header', 'genesis_do_breadcrumbs' );

//* Add HTML5 markup structure
add_theme_support( 'html5' );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );


//* Add new image sizes
add_image_size('grid-thumbnail', 100, 100, TRUE);
add_image_size('Featured', 700, 250, false);
add_image_size('big-grid-thumbnail', 260, 260, TRUE);
add_image_size('Video Thumb', 300, 168, true);
add_image_size('recommended', 380, 260, true);

add_theme_support( 'genesis-footer-widgets', 4 );

//* Customize the post meta function
add_filter('genesis_post_meta', 'post_meta_filter');
function post_meta_filter($post_meta) {
  if (is_singular('photo')) {
    $post_meta = '[post_terms taxonomy="use" before="Use: "] [post_terms taxonomy="length" before="Length: "] [post_terms taxonomy="height" before="Height: "] [post_terms taxonomy="color" before="Color: "] ';
  } elseif (is_post_type_archive('photo') ){
		  $post_meta = '';	
	} elseif (is_singular('plans')) {
	  $post_meta = '[post_terms taxonomy="rooms" before="Rooms: "]';
  } else {
	  $post_meta = '[post_categories] [post_tags]';	
  }
  
  return $post_meta;
}

function is_blog () {
	global  $post;
	$posttype = get_post_type($post );
	return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
}

//* Adjust Exceprt Length
//add_filter( 'excerpt_length', 'sp_excerpt_length' );
function sp_excerpt_length( $length ) {
	if( is_post_type_archive('photo') || is_tax()) {
    if( $query->is_main_query() && $query->is_archive() ) {
        return 0; // pull first 50 words
    }
	}
}

//* Custom Excerpt Length for Universal Use
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function custom_excerpt_length( $length ) {
	return 50;
}



//* Add Social & Link bar above the footer widgets.
add_action('genesis_before_footer','sf_social_link_footer', 3);
function sf_social_link_footer() {
  get_template_part('lib/content', 'social-footer');
}
//* Modify credits section
add_filter('genesis_footer_creds_text', 'custom_footer_creds_text');
function custom_footer_creds_text($creds) {
    $creds = '<p>Screenflex Room Dividers, <span>585 Capital Drive, Lake Zurich IL 60047</span><br /><span class="phone">P. 800.553.0110</span> <span class="not-dealersf">F. 847.726.2990</span> <p>[footer_copyright] <span>Screenflex Portable Partitions Inc.</span> </p>';
    return $creds;
}


//* Add tags to Genesis content limit in content archives.
//add_filter( 'get_the_content_limit_allowedtags', 'get_the_content_limit_custom_allowedtags' );
function get_the_content_limit_custom_allowedtags() {
	// Add custom tags to this string
	if (!is_post_type_archive( 'photo' )) {
		return '<script>,<style>,<br>,<p>,<em>,<strong>,<div>,<i>,<a>,<dl>,<dt>,<dd>'; 
	}
}

// Add custom modules to all pages 

add_action('genesis_before_footer', 'custom_modules', 1);
//* Add custom script to a certain  pages
add_action( 'wp_enqueue_scripts', 'child_add_custom_page_script' );
function child_add_custom_page_script() {
  global $post;
   wp_enqueue_script( 'jquery' );
   //Global JS
   wp_enqueue_script( 'sf_global ', get_bloginfo('stylesheet_directory').'/js/jquery.global.min.js', false);
   // Thickbox
   wp_enqueue_script('thickbox');
   wp_enqueue_style('thickbox');
   
   // Flash Interactive 
   if(is_page(array('virtual-showroom', 'current-offers', 4677))) {
      wp_register_script('google_swfobject', 'https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js', array('jquery'), '2.2', false);       
      wp_enqueue_script( 'sf_interactive', CHILD_URL . '/js/jquery.interactive.min.js', array('jquery', 'google_swfobject'), '2.6.3', false);                
    }
    
    // Features and Options
    if(is_page_template('template-features.php')) {
      wp_enqueue_script('sf_zoom', CHILD_URL.'/js/jquery.mlens-1.4.min.js', array('jquery'), false);
      wp_enqueue_script('sf_features', CHILD_URL.'/js/jquery.features.min.js', array('jquery', 'sf_zoom'), false);
    }
    
    // Sizes and Colors
    if(is_page_template('template-sizes-colors.php')) {
      wp_enqueue_script('sf_history', CHILD_URL.'/js/jquery.history.min.js', array('jquery'), '1.1', false);
      wp_enqueue_script('sf_sizescolors', CHILD_URL.'/js/jquery.sizescolors.min.js', array('jquery', 'sf_history'), '1.2', false);
    }
    
    // Insert Lifeway script on Lifeway page and all of it's children 
    if(is_page(58007) || $post->post_parent == '58007' ){
      wp_enqueue_script('sf_lifeway', CHILD_URL.'/js/jquery.lifeway.min.js', array('jquery'), '1.2', false);
    }
    
    // Animation on Custom Floor Planning
    if(is_page(4685)) {
      wp_enqueue_script('sf_animationdepend', CHILD_URL.'/js/code.createjs.com.animation-dependencies.min.js', array('jquery'), '1.1', false);
      wp_enqueue_script('sf_animation', CHILD_URL.'/js/diagram-anim.min.js', array('jquery', 'sf_animationdepend'), '1.1', false);
    }
 
}
 /**
 * Add a CSS ID to main element for Infinite Scroll
 **/
add_filter( 'genesis_attr_content', 'lc_custom_attributes_content' );
function lc_custom_attributes_content( $attributes ) {
	$attributes['id'] = 'main-content';
	return $attributes;
 
}

/**
* Add theme support for infinite scroll
*
* @uses add_theme_support
* @action after_setup_theme
* @return null
*/

/**
 * Enables Jetpack's Infinite Scroll in search pages, disables it in reviews archives & Blog Posts.  
 *  Add this back to the is_archive || is_search() ||  is_home()  to enable infinite scroll for Blog Posts.
 * @return bool
 */

function ss_theme_jetpack_infinite_scroll_supported() {
	return current_theme_supports( 'infinite-scroll' ) && (is_blog() || is_post_type_archive(array('review', 'photo', 'videos')) || is_tax(array('color', 'height', 'length', 'use')) );
}
add_filter( 'infinite_scroll_archive_supported', 'ss_theme_jetpack_infinite_scroll_supported' );

add_action( 'after_setup_theme', 'ss_infinite_scroll_init' );
function ss_infinite_scroll_init() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main-content',
		'footer' => 'footer',
		'render' => 'sf_custom_do_loop',
		'wrapper' => true,
		'posts_per_page' => 24
	));
}

/**
 * Modify the footer credits for JetPack Inifite Scroll
 **/
add_filter('infinite_scroll_credit','ss_infinite_scroll_credit');

function ss_infinite_scroll_credit(){
 $content = '<a href="/" title="Home">Home</a><br />';
 return $content;
}
/** End JetPack **/



//* Reverse Post Order for Plans Archive
add_action( 'pre_get_posts', 'ss_prefix_reverse_post_order' );
function ss_prefix_reverse_post_order( $query ) {
	if ( is_admin() )
		return;
	
	// Only change the query for post archives.
	if ( $query->is_main_query() && is_archive() && is_post_type_archive('plans') ) {
		$query->set( 'orderby', 'date' );
		$query->set( 'order', 'ASC' );
	}
}

//* Add custom body class to the head
add_filter( 'body_class', 'ss_add_body_class' );
function ss_add_body_class( $classes ) {
   if ("https://www.dealersf.com/" == WP_SITEURL) {
     array_push($classes, 'dealersf');
   }
   return $classes;
}


// Add custom body class for the width of the page
add_filter( 'body_class', 'sf_add_body_width' );
function sf_add_body_width( $classes ) {
  $width = get_field('page_width');
  if(is_page()) {
     if ($width == 'Full Width') {
       array_push($classes, 'sf_full');
     } elseif($width == 'Wide') {
       array_push($classes, 'sf_wide');
     } elseif($width == 'Medium') {
       array_push($classes, 'sf_medium');
     } else {
       array_push($classes, 'sf_narrow');
     }
  }
  return $classes;
}

add_filter( 'body_class', 'sf_add_nav_exclude' );
function sf_add_nav_exclude( $classes ) {
  $exclude = get_field('exclude_navigation');
   if ($exclude == 'Yes') {
     array_push($classes, 'exclude-nav');
   }
  return $classes;
}

// Force all Pages to Full Width
add_action( 'genesis_meta', 'sf_page_genesis_meta' );
function sf_page_genesis_meta() {
  if(is_page() || is_singular(array('photo', 'plans')) || is_post_type_archive(array('photo', 'plans')) || is_search()) {
	  add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
	}
	
	if(is_blog()) {
  	remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
  	remove_action('genesis_loop', 'genesis_do_loop');
  	add_action('genesis_loop', 'sf_custom_do_loop');
	}
	
}

//add_action( 'genesis_entry_content', 'genesis_prev_next_post_nav', 12 );


//add_action( 'genesis_entry_content', 'ss_custom_single_post_nav', 12 );
function ss_custom_single_post_nav() {

	if ( ! is_singular() )
		return;

		
	echo '<div class="single-pagination" style="clear:both;"><div class="pagination-previous alignleft button">';
	previous_post_link('%link', '<i class="icon-arrow-left"></i> %title', FALSE);
	echo '</div>';

	echo '<div class="pagination-next alignright button">';
	next_post_link('%link', '<i class="icon-arrow-right"></i> %title', FALSE);
	echo '</div></div>';

}

//* Remove the post meta function
//remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

//* Remove the post info function
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
add_action( 'genesis_entry_header', 'genesis_post_info', 1 );

//* Customize the post info function
add_filter( 'genesis_post_info', 'sp_post_info_filter' );
function sp_post_info_filter($post_info) {
if ( !is_page() ) {
	$post_info = '[post_date]';
	return $post_info;
}}


// ALlow shortcodes to parse in sidebar
add_filter('widget_text', 'do_shortcode');

// Add CTA to every page.
add_action('genesis_after_footer', 'priceguide_cta');


// Register Site Map Menu
register_nav_menu( 'sitemap', 'Site Map Menu' );

//* Register widget areas
genesis_register_sidebar( array(
	'id'            => 'footer-primary-left',
	'name'          => __( 'Footer Primary Left', 'screenflex' ),
	'description'   => __( 'This is a widget area that is the primary footer on the left side.  Normally contains social icons.', 'screenflex' ),
) );


genesis_register_sidebar( array(
	'id'            => 'footer-primary-right',
	'name'          => __( 'Footer Primary Right', 'screenflex' ),
	'description'   => __( 'This is a widget area that is in the primary footer on the right side, normally contains links.', 'screenflex' ),
) );



// add custom classes to post_class()
function one_half_post_class( $classes ) {
	$classes[] = 'one-half';
	global $wp_query;
	
	if( 0 == $wp_query->current_post || 0 == $wp_query->current_post % 2 ) {
		$classes[] = 'first';
  }
  
	return $classes;
}

function one_fourth_post_class( $classes ) {
	$classes[] = 'one-fourth';
	global $wp_query;
	
	if( 0 == $wp_query->current_post || 0 == $wp_query->current_post % 4 ) {
		$classes[] = 'first';
  }
  
  if(has_term('before-and-after', 'use')) {
    $classes[] = 'before-after';
  }
  
	return $classes;
}


// Sticky Post Types
function wpb_cpt_sticky_at_top( $posts ) {
    global $wp_query;
    // apply it on the archives only
    if ( is_post_type_archive() && !is_paged() ) {
        $sticky_posts = get_option( 'sticky_posts' );
        $num_posts = count( $posts );
        $sticky_offset = 0;
 
        // Find the sticky posts
        for ($i = 0; $i < $num_posts; $i++) {
 
            // Put sticky posts at the top of the posts array
            if ( in_array( $posts[$i]->ID, $sticky_posts ) ) {
                $sticky_post = $posts[$i];
 
                // Remove sticky from current position
                array_splice( $posts, $i, 1 );
 
                // Move to front, after other stickies
                array_splice( $posts, $sticky_offset, 0, array($sticky_post) );
                $sticky_offset++;
 
                // Remove post from sticky posts array
                $offset = array_search($sticky_post->ID, $sticky_posts);
                unset( $sticky_posts[$offset] );
            }
        }
 
        // Look for more sticky posts if needed
        if ( !empty( $sticky_posts) ) {
 
            $stickies = get_posts( array(
                'post__in' => $sticky_posts,
                'post_type' => $wp_query->query_vars['post_type'],
                'post_status' => 'publish',
                'nopaging' => true
            ) );
 
            foreach ( $stickies as $sticky_post ) {
                array_splice( $posts, $sticky_offset, 0, array( $sticky_post ) );
                $sticky_offset++;
            }
        }
 
    }
 
    return $posts;
}
 
add_filter( 'the_posts', 'wpb_cpt_sticky_at_top' );

// Add sticky class in article title to style sticky posts differently
function cpt_sticky_class($classes) {
		if ( is_sticky() ) : 
			$classes[] = 'sticky';
	        return $classes;
		endif; 
		return $classes;
}
add_filter('post_class', 'cpt_sticky_class');


function ss_no_post_image() {
  return '0';
}

function ss_do_full_content() {
	return 'full';
}

function ss_no_content_limit() {
	return '0.1';
}

function empty_read_more_link() {
  	return '';
  }