<?php

add_action( 'genesis_meta', 'fourofour_genesis_meta' );
function fourofour_genesis_meta() {
	remove_action( 'genesis_loop', 'genesis_do_loop' );
	add_action( 'genesis_loop', 'fourofour_loop_helper' );
	add_filter('body_class', 'narrow_body_class');
	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
	}

function narrow_body_class($classes) {
		$classes[] = 'sf_medium';
    return $classes;
}

function fourofour_loop_helper() { ?>
  <article class="entry">
  
    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/404.jpg" alt="OOPS, Nothing Found" width="400" height="308" class="alignright" />

    <h1 class="entry-title">Oops! The page cannot be found, error 404</h1>
    <div class="entry-content">
      <p>The page you are looking for no longer exists. Perhaps you can return back to the site's <a href="/">homepage</a> and see if you can find what you are looking for. Or, you can try finding it by using the search form below.</p>
    
      <h3>Please try the following:</h3>
      <ul> 
        <li>If you typed the page address in the address bar, make sure that it is spelled correctly.</li>
        <li>Click the back button to try another link.</li>
      </ul>
      
      <h3>Maybe you were looking for one of our most viewed pages:</h3>  
      
      <ul>
      <li>Home <a href="/">https://www.screenflex.com/</a></li>
      <li>Sizes <a href="/room-divider-sizes/">https://www.screenflex.com/room-divider-sizes/</a></li>
      <li>Colors <a href="/room-divider-colors/">https://www.screenflex.com/room-divider-colors/</a></li>
      <li>Interactive Room Divider <a href="/interactive-room-divider/">https://www.screenflex.com/interactive-room-divider/</a></li>
      </ul>
      
      <h3>Or, you can try finding it by using the search form or the <a href="/search">site-map</a>:</h3>
      
      <p><?php get_search_form(); ?></p>
    </div>
  
  </article>
  <?php
} 

genesis();