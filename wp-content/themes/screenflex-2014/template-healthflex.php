<?php
// Template Name: Healthflex
 
// Add specific CSS class by filter
add_filter('body_class','my_class_names');
function my_class_names($classes) {
	$classes[] = 'special-use healthflex';
	return $classes;
}

add_filter('widget_content', 'ss_widget_content_filter');
function ss_widget_content_filter($content='') {
    $content=preg_replace('/how\-to\-buy/', 'how-to-buy-health-care-room-dividers',$content,1);
    $content=preg_replace('/800\.553\.0110/', '800-553-0746',$content,1);
    return $content;
}

genesis();