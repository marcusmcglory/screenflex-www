<?php 

// Template Name: Features

add_action( 'genesis_meta', 'features_genesis_meta' );
function features_genesis_meta() {
	remove_action( 'genesis_loop', 'genesis_do_loop' );
	add_action( 'genesis_loop', 'features_loop_helper' );
	add_action('genesis_before_footer', 'custom_content', 2);
	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
	}

function features_loop_helper() {
  // Variables
  $features = get_field('features');
?>
  <div class="features lightbg">
    <div class="wrap">
      <header class="entry-header">
      	          <?php genesis_do_post_title(); ?>
        <h2>The All Purpose Divider</h2>
      </header>
      
      <section class="features-canvas" data-active="">
        <ul class="circles">
          <?php
            if($features) {
              foreach($features as $f) {
                if( $f['exclude'] && in_array('from circles', $f['exclude'])) { 
                } else {
                  echo '<li id="'.$f['feature_id'].'">'.$f['title'].'</li>';
                }
              }
            }
          ?>
        </ul>
        
        <ul class="popovers">
          <?php
            if($features) {
              foreach($features as $f) {
                if( $f['exclude'] && in_array('from circles', $f['exclude'])) { 
                } else {
                  echo '<li class="'.$f['feature_id'].'">';
                    echo '<i class="icon-cross"></i>';
                    if($f['image']) {
                      echo '<img src="'.$f['image']['sizes']['grid-thumbnail'].'" class="alignleft" alt="'.$f['image']['alt'].'"/>';
                    } else {
                      echo '<img src="http://placehold.it/100x100" class="alignleft" alt="Placeholder"/>';
                    }
                    echo '<h2>'.$f['title'].'</h2>';
                    echo $f['short_description'];
                  echo '</li>';
                }
              }
            }
          ?>
        </ul>
        <div class="hover">Hover the image</div>
        <div class="feature-image"><img id="featureHero" src="<?php echo CHILD_URL; ?>/images/features/features-hero.jpg" data-big="<?php echo CHILD_URL; ?>/images/features/features-hero-big.jpg" alt="Features" /></div>
        <button class="zoom zoomon"><i class="icon-search"></i> Click to Zoom</button>
        <button class="zoom zoomoff"><i class="icon-cross"></i> Exit Zoom</button>
      </section>
    </div>
  </div>

  <section class="full-features">
    <div class="wrap">
      <?php
        if($features) {
            $count = 0;
            foreach($features as $f) {
                if( $f['exclude'] && in_array('from main list', $f['exclude'])) { 
                } else {
                $i = $count++;

                // Every Third div add a class of "First"
                $first = '';
                if($i%3==0) { $first = ' first'; }
                
                echo '<div class="one-third'.$first.' '.$f['feature_id'].'">';
                
                  // If Vine exists, create a wrap for the video and the image. 
                  if($f['vine_video_id']) {
                    echo '<div class="vine">';
                      echo '<a title="'.$f['title'].'" href="https://vine.co/v/'.$f['vine_video_id'].'/embed/simple?audio=1&KeepThis=true&TB_iframe=true&height=600&width=600" class="thickbox">';
                      echo '<iframe class="vine-embed" src="https://vine.co/v/'.$f['vine_video_id'].'/embed/simple" width="200" height="200" frameborder="0"></iframe>';
                  }
                    
                    if($f['image']) {
                      echo '<img src="'.$f['image']['sizes']['big-grid-thumbnail'].'" alt="'.$f['image']['alt'].'"/>';
                    } else {
                      echo '<img src="http://placehold.it/200x200" alt="Placeholder"/>';
                    }
                  
                  // Close vine
                  if($f['vine_video_id']) {
                      echo '<i class="icon-play"></i>';
                      echo '</a>';
                    echo '</div>'; 
                  }
                  
                  echo '<h2>'.$f['title'].'</h2>';
                  if($f['long_description']) {
                    echo $f['long_description'];
                  } else {
                    echo $f['short_description'];
                  }
                  
                echo '</div>';
              }
            }
          }
        ?>
      </div>
    </section>
    
    
    <script async src="//platform.vine.co/static/scripts/embed.js" charset="utf-8"></script>
        
<?php } genesis(); ?>