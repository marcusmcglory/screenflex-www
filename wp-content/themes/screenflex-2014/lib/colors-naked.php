db : Lake
dg : Stone
dm : Rose
dn : Mallard
do : Walnut
dw : Desert
dx : Charcoal Black
dt : White
ds : Primary Blue 
dy : Primary Yellow
dr : Primary Red
eb : Summer Blue
eg : Grey Smoke
em : Cranberry
en : Sea Green
eo : Beech
ew : Sand
vb : Blue Tide
vg : Granite
vm : Raspberry Mist
vn : Mint
vo : Sandalwood
vw : Hazelnut
vx : Coal
vt : Ice
sb : Blue
sg : Grey
sm : Mauve
sn : Green
so : Oatmeal
sw : Wheat
sx : Black
pb : Blue Sky
pg : Platinum
pn : Green Grass
po : Oak
pw : Prairie
px : Black Pearl
wx : Black