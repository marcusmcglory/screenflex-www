<?php

function review_archive() {
  add_action( 'genesis_meta', 'reviews_genesis_meta' );
  function reviews_genesis_meta() {
  	remove_action( 'genesis_loop', 'genesis_do_loop' );
  	add_action( 'genesis_loop', 'sf_custom_do_loop' );
  	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
  }
  genesis();
}


function photo_archive() {
  add_action( 'genesis_meta', 'photos_genesis_meta' );
  function photos_genesis_meta() {
    remove_action( 'genesis_loop', 'genesis_do_loop' );
  	add_action( 'genesis_loop', 'sf_custom_do_loop' );
    add_action('genesis_loop', 'ss_add_photo_dropdown', 1);
    add_action( 'genesis_before_loop', 'ss_full_content_specific_categories' );
  	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
  }
  genesis();
}  // End Photo Archive

function ss_add_photo_dropdown() {
    // Auto setup the title of the term archive if we're not on the post type archive. 
    if(!is_post_type_archive()) {
      global $wp_query;
      $tax = $wp_query->get_queried_object();
      echo '<div class="archive-description cpt-archive-description">';
        echo '<h1 class="archive-title">Room Divider Photos - ' . $tax->name . '</h1>';
        echo '<p>Photos provided by our Customers</p>';
      echo '</div>';
    }
    echo '<div class="catnavs">';
      if(is_post_type_archive() || is_tax('use')) {
        theme_tax_nav('use', 'use', 'Sort by Use/Facility', 'photos', 'one-fourth first');
      }
      if(is_tax('length')) { theme_tax_nav('length', 'length', 'Sort by Length', 'photos', 'one-fourth first'); }
      if(is_tax('height')) { theme_tax_nav('height', 'height', 'Sort by Height', 'photos', 'one-fourth first'); }
      if(is_tax('color')) { theme_tax_nav('color', 'color', 'Sort by Color', 'photos', 'one-fourth first'); }
    echo '</div>';
    
    echo '<div class="beforeafter">';
      echo '<span>'.do_shortcode('[svgicon icon="beforeafter"]').'</span> = Before & After';
    echo '</div>';
}

function photo_archive_post_class( $classes ) {
	$classes[] = 'one-fourth';
	global $wp_query;
	
	if( 0 == $wp_query->current_post || 0 == $wp_query->current_post % 4 ) {
		$classes[] = 'first';
  }
  
  if(has_term('before-and-after', 'use')) {
    $classes[] = 'before-after';
  }
  
	return $classes;
}


// Video Archive
function video_archive() {
	add_action( 'genesis_meta', 'archive_genesis_meta' );
  function archive_genesis_meta() {
  	remove_action( 'genesis_loop', 'genesis_do_loop' );
  	add_action( 'genesis_loop', 'sf_custom_do_loop' );
  	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
  	}
  
  genesis();

} // End Video Archive

function plans_archive() {
  add_action( 'genesis_meta', 'plans_genesis_meta' );
  function plans_genesis_meta() {
    remove_action( 'genesis_loop', 'genesis_do_loop' );
  	add_action( 'genesis_loop', 'sf_custom_do_loop' );
    add_action('genesis_loop', 'ss_add_plans_dropdown', 1);
    add_action( 'genesis_before_loop', 'ss_full_content_specific_categories' );
  	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
  }  
  
  function ss_add_plans_dropdown() {
      // Auto setup the title of the term archive if we're not on the post type archive. 
      if(!is_post_type_archive()) {
        global $wp_query;
        $tax = $wp_query->get_queried_object();
        echo '<div class="archive-description cpt-archive-description">';
          echo '<h1 class="archive-title">Plans - ' . $tax->name . ' Rooms</h1>';
          //echo '<p></p>';
        echo '</div>';
      }
      echo '<div class="catnavs">';
        if(is_post_type_archive() || is_tax('rooms')) {
          theme_tax_nav('rooms', 'rooms', 'How Many Rooms Do You Need?', 'plans', 'one-fourth first');
        }
      echo '</div>';
  }
  
  genesis();
}  // End Plans Archive

// add custom classes to post_class()
function plans_archive_post_class( $classes ) {
	$classes[] = 'one-half';
	global $wp_query;
	
	if( 0 == $wp_query->current_post || 0 == $wp_query->current_post % 2 ) {
		$classes[] = 'first';
  }
  
	return $classes;
}



function sf_custom_do_loop() {
  // -------------------- Archive customizations that mus be fired when Infinite loop kicks in.  -----------------------------
  // Photos & Videos
  if(is_post_type_archive(array('photo', 'videos')) || is_tax(array('color', 'height', 'length', 'use', 'collections'))) {
    add_filter( 'post_class', 'one_fourth_post_class' );
  }
  
  // Plans & Reviews
  if(is_post_type_archive(array('plans', 'review')) || is_tax(array('rooms'))) {
    add_filter( 'post_class', 'one_half_post_class' );
  }
  
  
  // -------------------- The Loop -----------------------------
  while ( have_posts() ) : the_post(); 
    
    if(is_blog()) { ?>
      <article <?php post_class(); ?> itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">
        <p class="entry-meta">
          <?php echo do_shortcode('[post_date]'); ?>
        </p>
        
        <header class="entry-header"><h2 class="entry-title" itemprop="headline">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
        </header>
        
        <div class="entry-content" itemprop="text">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <?php the_post_thumbnail('big-grid-thumbnail', array('class' => 'alignleft')); ?>
          </a>
          <p><?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>" class="more-link" >Continue Reading</a>
            </p>
          </div>
      </article>
    <?php }
    
    // Reviews
    if(is_post_type_archive('review')) { ?>
    	<article <?php post_class(); ?> itemscope="itemscope" itemtype="http://schema.org/CreativeWork">
    		<h2><?php the_title(); ?></h2>
    		<div class="entry-content">
    		<?php the_content(); ?>
    		</div>
    	</article><!-- end .postclass -->
    	<?php
    } // End Reviews
    
    
    // Photos
    if(is_post_type_archive('photo') || is_tax(array('color', 'height', 'length', 'use'))) { ?>
      <article <?php post_class(); ?> itemscope="itemscope" itemtype="http://schema.org/CreativeWork">
        <header class="entry-header">
          <h2 class="entry-title" itemprop="headline">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
          </h2> 
        </header>
        <div class="entry-content" itemprop="text">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <?php the_post_thumbnail('big-grid-thumbnail'); ?>
          </a>
        </div>
      </article>
    <?php }
    
    // Plans
    if(is_post_type_archive('plans') || is_tax(array('rooms'))) { ?>
      <article <?php post_class(); ?> itemscope="itemscope" itemtype="http://schema.org/CreativeWork">
        <header class="entry-header">
          <h2 class="entry-title" itemprop="headline">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
          </h2> 
        </header>
        <div class="entry-content" itemprop="text" id="LuckyAnchor_495961077_140">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <?php the_post_thumbnail('big-grid-thumbnail', array('class' => 'alignleft')); ?>
          </a>
          <?php the_excerpt(); ?>
        </div>
      </article>
    <?php }
    
    
    // Videos
    if(is_post_type_archive('videos') || is_tax(array('collections'))) { ?>
      <div <?php post_class(); ?>>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<a href="<?php the_permalink(); ?>" class="video-thumb">
					<span class="icon-play"></span>
						<?php
						if(has_post_thumbnail()) { 
							the_post_thumbnail('Video Thumb');
						} else {
							echo '<img src="'.get_bloginfo('stylesheet_directory').'/images/no-thumb.jpg" alt="'.get_the_title().'" />';
						} ?>
				</a>
			</div><!-- end .postclass -->
    <?php }
    
    
  endwhile; /** end of one post **/
}

