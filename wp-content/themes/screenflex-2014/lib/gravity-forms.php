<?php 

/****
  *
  *  All gravity form modifications
  *
****/

// Not sure what this is for, Ask Scott. 
//add_filter("gform_post_submission", "hide_form", 10, 2);
function hide_form($entry, $form) {
    if($form["confirmation"]["type"] != "message")
    echo '<style type="text/css"> body { display: none; } </style>';
}

// Change validation message
add_filter("gform_validation_message", "ss_change_message", 10, 2);
function ss_change_message($message, $form){
    switch($form['id']){
        case 5:
        case 4:
        case 3:
        case 1:
            return "<div class='error_message'>Please submit your choice of email address or phone number.  Thank you.</div>";
            break;
        default:
            return "<div class='error_message'>There was a problem with your submission. Errors have been highlighted below.</div>";
            break;
    }

}

//Require a Phone or Email address for the following forms
add_filter('gform_validation_24', 'ss_validate_phone_or_email');
add_filter('gform_validation_21', 'ss_validate_phone_or_email');
add_filter('gform_validation_10', 'ss_validate_phone_or_email'); 
add_filter('gform_validation_5', 'ss_validate_phone_or_email'); 
add_filter('gform_validation_4', 'ss_validate_phone_or_email'); 
add_filter('gform_validation_3', 'ss_validate_phone_or_email'); 
add_filter('gform_validation_1', 'ss_validate_phone_or_email');   


function ss_validate_phone_or_email($validation_result){
        
     //  - Get the current form
     $form = $validation_result["form"];
     
     //  - Get the current page being validated
     $current_page = rgpost('gform_source_page_number_' . $form['id']) ? rgpost('gform_source_page_number_' . $form['id']) : 1;
    
     //   - Initalize Count for the quantity of fields found
     $count = 0;
     
     //  - Loop through the form fields
     foreach($form['fields'] as &$field){
     
     
        //  - If the field does not have our designated CSS class, skip it  Logically, if we are NOT phone or NOT email, skip it.
        if((strpos($field['cssClass'], 'request-phone') === FALSE) && (strpos($field['cssClass'], 'request-email') === FALSE)) {
            continue;
        }
        
        //  - Get the field's page number
        $field_page = $field['pageNumber'];
                
        //  - Check if the field is hidden by Gravity Forms conditional logic
        $is_hidden = RGFormsModel::is_field_hidden($form, $field, array());
        
        //  - If the field is not on the current page OR if the field is hidden, skip it
        if($field_page != $current_page || $is_hidden)
            continue;      
        
        //  - Get the submitted value from the $_POST
        $field_value = rgpost("input_{$field['id']}");
        
        //  - If the field is not empty, skip it.
        if(empty($field_value)== false){
            continue;
        }
            
        //  - Set the validation result as invalid
        $is_valid = false;
        
        //  - The field failed validation, so first we'll need to fail the validation for the entire form
        $validation_result['is_valid'] = $is_valid;
        
        //  - Mark the specific field that failed and add a custom validation message
        $field['failed_validation'] = true;
        $field['validation_message'] = 'Either a Phone Number <strong>OR</strong> an Email Address is required.';
        
        //  - Increment our count
        $count++;
     }
     
     //  - If our count is less than two, then one of our fields was valid, and there is no need to mark the entire form invalid.
     if($count < 2){
        $validation_result['is_valid'] = true;
     }
     
     //  - Assign our modified $form object back to the validation result
     $validation_result['form'] = $form;

    //  - Return the validation result
    return $validation_result;
    
}
