<?php
function custom_modules() {
  $middle = get_field('middle_modules');
  $bottom = get_field('include_bottom_modules');
  $email = get_field('include_email_script');
  $midbg = get_field('middle_background_color');
  $botbg = get_field('bottom_background_color');
  
  if($middle) { ?>
    <section id="middle_modules" class="modules middle-modules <?php echo $midbg; ?>">
      <div class="wrap">
        <?php
          $count = count($middle); // How many items are in the array?
          $class = ''; // setup the helper classes.
          if($count == '1') { // If only one item, don't set a class.
            $class = 'one';
          } elseif($count%3 == '0') { // It's divisable by 3, set class to one-third
            $class = 'one-third';
          } elseif($count%2 == '0') { // It's divisable by 2, set the class to one-half
            $class = 'one-half';
          }
          $iteration = 0; // Count them out for positioning purposes
          foreach($middle as $mid) {
            $i = $interation ++;
            // Find the first of the array.
            $first = '';
            if($i == 0 && $class != 'one') {
              $first = 'first';
            }
            
            ?>
              <div class="module <?php echo $class.' '.$first; ?>">
                <?php 
                  echo $mid['content']; 
                  
                  // Display the proper sales agent.
                  if($mid['select_agent'] == 'Don Austin') {
                    echo $mid['don'];
                  } elseif($mid['select_agent'] == 'Lourdes Morales') {
                    echo $mid['lourdes'];
                  } elseif($mid['select_agent'] == 'Kim Abraham') {
                    echo $mid['kim'];
                  }
                ?>
                
              </div>
            <?php } ?>
      </div>
    </section>  
  <?php 
  }
  
  if($bottom == 'Yes') { ?>
    <section id="bottom_modules" class="modules bottom-modules <?php echo $botbg; ?>">
      <div class="wrap">
        <?php the_field('bottom_module'); ?>
      </div>
    </section> 
  <?php }
  
  if($email == 'Yes') {
    wp_enqueue_script( 'sf_emaillink', CHILD_URL . '/js/jquery.buildemaillink.min.js', array('jquery'), '1.2', false);
  }
}


function custom_content() { ?>
  <article class="entry-content">
    <div class="wrap">
      <?php the_content(); ?>
    </div>
  </article>    

<?php 
}

function priceguide_cta() {
	if (!(is_page_template('template-form.php'))):
 ?>
  <div class="price-cat-cta call-to-action">
    <div class="wrap">
      <div class="catalog-request">
        
        <?php 
          $form = get_field('cta_form', 'options');
          echo do_shortcode('[gravityform id="'.$form->id.'" title="true" description="true" ajax="true"]'); 
        ?>
        <i class="icon-cross close-catalog"></i>
      </div>
      <button id="btn_cta" class="priceguide-cta"><?php the_field('cta_violator_title', 'options'); ?></button>
    </div>
  </div>
<?php endif; }

// Video Collections
function collections($type = 'icons') {

	/** Set up for single post use
		* 'icons' = Displays proper icon in a span (DEFAULT)
		* 'more' = Displays the more link.
	**/
	if($type == 'icons' || $type == 'more' || $type == 'link') {
		$terms = get_the_terms( $post->ID, 'collections' );
							
		if ( $terms && ! is_wp_error( $terms ) ) { 
			$c = 0;
			foreach ( $terms as $term ) {				
				$i = $c++;
				if($i == '0') { // Only allow the first item to output
					if($type == 'icons') {
					
						echo '<span class="icon-'.$icon.'"></span> ';
					} 
					
					elseif($type == 'more') {
						 echo '<a href="'.get_bloginfo('home').'/collection/'.$term->slug.'">More '.$term->name.'</a>';
					}
					
					elseif($type == 'link') {
						 echo '<a href="'.get_bloginfo('home').'/collection/'.$term->slug.'">'.$term->name.'</a>';
					}
				}
			}
		}
	}
	
	/*** Set up for Count
		* 'count' = Displays the number of posts in that term
	***/
	
	if($type == 'count') {
		$args = array( 
			'hide_empty' => false
		);
		$terms = get_terms( 'collections', $args );
		echo '<ul>'; 
			foreach ( $terms as $term) {
				if	  ($term->slug == 'backgrounds') 			{ $icon = 'stack'; }
				elseif($term->slug == 'countdowns-and-timers') 	{ $icon = 'hourglass'; }
				elseif($term->slug == 'lower-third') 			{ $icon = 'layout'; }
				elseif($term->slug == 'objects-and-bullets') 	{ $icon = 'list'; }
				elseif($term->slug == 'touch-gesture') 		{ $icon = 'hand-up'; }
				elseif($term->slug == 'transition') 			{ $icon = 'exchange'; }
				
				echo '<li><a href="'.get_bloginfo('home').'/collection/'.$term->slug.'"><span aria-hidden="true" class="icon-'.$icon.'"></span> '.$term->count.' '.$term->name.'</a></li>';
			}
		echo '</ul>';
	}
	
	if($type == 'similar') {
		
	}
}


function theme_tax_nav($tax = '', $taxslug = '', $title = '', $posttypeslug = '', $classes = '', $exclude = '') {
	$current = $_SERVER["REQUEST_URI"];
	$args = array(
		'orderby' => 'title',
		'order' => 'ASC',
		'hide_empty' => true,
		'exclude' => array($exclude)
	);
	$terms = get_terms($tax, $args);
	$count = count($terms);
	if ( $count > 0 ){
		echo '<div class="catnav nav-'.$taxslug.' '.$classes.'">';
			echo '<span>'.$title.' </span>';
			
			// Check to see if we're on the post type main archive and set the first option to active if it's true.
			$slug = '/'.$posttypeslug.'/';
			if($slug == $current) {
					$active = 'selected="selected"';
				} else {
					$active = '';
				}
				
			echo '<form id="page-changer-'.$taxslug.'" class="page-changer " action"" method="post">';
				echo '<select>';
					echo '<option class="pager pager-all" value="'.get_bloginfo('home').'/'.$posttypeslug.'/" '.$active.'>All</option>';
					
					foreach ( $terms as $term ) {
						$slug = $term->slug;
						$slug = '/'.$posttypeslug.'/'.$taxslug.'/'.$slug.'/';
						
						if($slug == $current) {
							$active = 'selected="selected"';
						} else {
							$active = '';
						}
	
						echo '<option class="pager pager-'.$term->slug.'" value="'.get_bloginfo('home').'/'.$posttypeslug.'/'.$taxslug.'/' . $term->slug. '" '.$active.'>' . $term->name . '</option>';
						
					}
				echo '</select>';
				
				echo '<input type="submit" value="Go" id="submit" />';
			echo '</form>';
		echo '</div>';

	}
}
