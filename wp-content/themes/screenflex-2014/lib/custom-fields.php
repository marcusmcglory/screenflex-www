<?php 
/*** 
  *
  *
  * All Custom fields will be built here
  *
  *
***/



if(function_exists("register_field_group")) {

// Home Page Custom Fields
	register_field_group(array (
		'id' => 'acf_home-content',
		'title' => 'Home Content',
		'fields' => array (
			array (
				'key' => 'field_5367b52346729',
				'label' => 'Hero Image',
				'name' => 'hero_image',
				'type' => 'image',
				'instructions' => '1200 x 600',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array(
			  'key' => 'field_5367b56941234',
			  'label' => 'Hero Link',
			  'name' => 'hero_link',
			  'type' => 'text',
			  'column_width' => '',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5367b56969423',
				'label' => 'Hero CTA Content',
				'name' => 'hero_cta_content',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_5367b5694672a',
				'label' => 'Intro Content',
				'name' => 'intro_content',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_5367b5914672b',
				'label' => 'Grid Content: Left',
				'name' => 'grid_content_left',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_5367b5c24672c',
				'label' => 'Grid Content: Middle',
				'name' => 'grid_content_middle',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_5367b5d34672d',
				'label' => 'Grid Content: Right',
				'name' => 'grid_content_right',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_5367b5ea4672e',
				'label' => 'Blog Content',
				'name' => 'blog_content',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'front-page.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 1,
	));


  // Features and Options
	register_field_group(array (
		'id' => 'acf_features-and-options',
		'title' => 'Features and Options',
		'fields' => array (
			array (
				'key' => 'field_5373eab55cc0c',
				'label' => 'Features',
				'name' => 'features',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_5373eacd5cc0e',
						'label' => 'Title',
						'name' => 'title',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5373eac25cc0d',
						'label' => 'Image',
						'name' => 'image',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_5373eaed5cc10',
						'label' => 'Short Description',
						'name' => 'short_description',
						'type' => 'wysiwyg',
						'column_width' => '',
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'no',
					),
					array (
						'key' => 'field_5373eada5cc0f',
						'label' => 'Long Description',
						'name' => 'long_description',
						'type' => 'wysiwyg',
						'column_width' => '',
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'no',
					),
					array (
						'key' => 'field_5373eb5f5cc11',
						'label' => 'Vine Video ID',
						'name' => 'vine_video_id',
						'type' => 'text',
						'instructions' => 'Insert only the ID of the Vine video.<br />ie: URL is: https://vine.co/v/M63v2g7ZxMb<br />The ID is: M63v2g7ZxMb',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5373ec607f067',
						'label' => 'Feature ID',
						'name' => 'feature_id',
						'type' => 'text',
						'column_width' => '',
						'instructions' => 'This is for syncing all of the moving parts.	Must be different for each feature/option.',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53752f9e1cfb8',
						'label' => 'Exclude',
						'name' => 'exclude',
						'type' => 'checkbox',
						'column_width' => '',
						'choices' => array (
							'from circles' => 'from circles',
							'from main list' => 'from main list',
						),
						'default_value' => '',
						'layout' => 'vertical',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Feature/Option',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-features.php',
					'order_no' => 0,
					'group_no' => 0,
				)
			),array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-options.php',
					'order_no' => 0,
					'group_no' => 0,
				)
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	
	
	// Custom modules
	register_field_group(array (
		'id' => 'acf_middle-modules',
		'title' => 'Middle Modules',
		'fields' => array (
			array (
				'key' => 'field_538642a071e58',
				'label' => 'Top Modules',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_538642b471e59',
				'label' => 'Which modules would you like to include?',
				'name' => 'middle_modules',
				'type' => 'flexible_content',
				'layouts' => array (
					array (
						'label' => 'Fabric Swatches',
						'name' => 'fabric_swatches',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_538643fc71e5c',
								'label' => 'Content',
								'name' => 'content',
								'type' => 'wysiwyg',
								'column_width' => '',
								'default_value' => '<a href="https://www.screenflex.com/fabric-card-request/?location=middle-module-icon">[svgicon icon="fabricswatches"]</a> <h3><a href="https://www.screenflex.com/fabric-card-request/?location=middle-module-header">Fabric Swatches</a></h3> <p>We would be happy to mail you actual fabric swatches of any colors you would like to see.</p>',
								'toolbar' => 'full',
								'media_upload' => 'yes',
							),
						),
					),
					array (
						'label' => 'Interactive Tool',
						'name' => 'interactive_tool',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_5386453a71e61',
								'label' => 'Content',
								'name' => 'content',
								'type' => 'wysiwyg',
								'column_width' => '',
								'default_value' => '<p><a href="https://www.screenflex.com/interactive-room-divider/?location=middle-module-icon">[svgicon icon="interactivetool"]</a></p>
	<h3>https://www.screenflex.com/interactive-room-divider/?location=middle-module-header">Interactive Tool</a></h3><p>Move and position our animated room divider into a configuration that fits your application. Try it! It?s fun and easy to use!</p>',
								'toolbar' => 'full',
								'media_upload' => 'yes',
							),
						),
					),
					array (
						'label' => 'Intro Video',
						'name' => 'intro_video',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_5386450d71e5f',
								'label' => 'Content',
								'name' => 'content',
								'type' => 'wysiwyg',
								'column_width' => '',
								'default_value' => '<a href="https://www.screenflex.com/videos/quick-easy-set-up/?location=middle-module-icon">[svgicon icon="introvideo"]</a>
	<h3><a href="https://www.screenflex.com/videos/quick-easy-set-up/?location=middle-module-header">Introduction Video</a></h3>
	<p>See how easy room divider?s set-up and configure for use. This short video will introduce you to Screenflex partitions and explain their features, benefits and applications.</p>',
								'toolbar' => 'full',
								'media_upload' => 'yes',
							),
						),
					),
					array (
						'label' => 'Plain Content',
						'name' => 'plain_content',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_538648da81e93',
								'label' => 'Content',
								'name' => 'content',
								'type' => 'wysiwyg',
								'column_width' => '',
								'default_value' => '',
								'toolbar' => 'full',
								'media_upload' => 'yes',
							),
						),
					),
					array (
						'label' => 'Price Guide',
						'name' => 'price_guide',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_5386438571e5a',
								'label' => 'Content',
								'name' => 'content',
								'type' => 'wysiwyg',
								'column_width' => '',
								'default_value' => '<a href="https://www.screenflex.com/how-to-buy/?location=icon">[svgicon icon="priceguide"]</a>
	<h3><a href="https://www.screenflex.com/how-to-buy/?location=icon">Price Guide</a></h3>
	<p class="not-dealersf">Receive a complete Room Divider Catalog now.</p>',
								'toolbar' => 'full',
								'media_upload' => 'yes',
							),
						),
					),
					array (
						'label' => 'Sales Agent',
						'name' => 'sales_agent',
						'display' => 'row',
						'min' => '',
						'max' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_5386459771e64',
								'label' => 'Select a Sales Agent',
								'name' => 'select_agent',
								'type' => 'select',
								'column_width' => '',
								'choices' => array (
									'Don Austin' => 'Don Austin',
									'Lourdes Morales' => 'Lourdes Morales',
									'Kim St. Clair' => 'Kim St Clair',
								),
								'default_value' => '',
								'allow_null' => 0,
								'multiple' => 0,
							),
							array (
								'key' => 'field_538645e871e65',
								'label' => 'Don',
								'name' => 'don',
								'type' => 'wysiwyg',
								'conditional_logic' => array (
									'status' => 1,
									'rules' => array (
										array (
											'field' => 'field_5386459771e64',
											'operator' => '==',
											'value' => 'Don Austin',
										),
									),
									'allorany' => 'all',
								),
								'column_width' => '',
								'default_value' => '<div class="agent alignleft">
	<img src="/wp-content/themes/screenflex-2014/images/sales/don-austin.jpg" alt="Don Austin" />
	<h3>Don Austin</h3>
	<p>Sales Manager</p>
	</div>
	
	<div class="quote">
	"MultiUint Connectors secure dividers together tightly for semi- permanent set-ups and leave no visible gaps."
	</div>',
								'toolbar' => 'full',
								'media_upload' => 'yes',
							),
							array (
								'key' => 'field_538646fe81e8d',
								'label' => 'Lourdes',
								'name' => 'lourdes',
								'type' => 'wysiwyg',
								'conditional_logic' => array (
									'status' => 1,
									'rules' => array (
										array (
											'field' => 'field_5386459771e64',
											'operator' => '==',
											'value' => 'Lourdes Morales',
										),
									),
									'allorany' => 'all',
								),
								'column_width' => '',
								'default_value' => '<div class="agent alignleft">
	<img src="/wp-content/themes/screenflex-2014/images/sales/lourdes-morales.jpg" alt="Lourdes Morales" />
	<h3>Lourdes Morales</h3>
	<p>Consultant</p>
	</div>
	
	<div class="quote">
	"MultiUint Connectors secure dividers together tightly for semi- permanent set-ups and leave no visible gaps."
	</div>',
								'toolbar' => 'full',
								'media_upload' => 'yes',
							),
							array (
								'key' => 'field_5386471d81e8e',
								'label' => 'Kim',
								'name' => 'kim',
								'type' => 'wysiwyg',
								'conditional_logic' => array (
									'status' => 1,
									'rules' => array (
										array (
											'field' => 'field_5386459771e64',
											'operator' => '==',
											'value' => 'Kim St. Clair',
										),
									),
									'allorany' => 'all',
								),
								'column_width' => '',
								'default_value' => '<div class="agent alignleft">
	<img src="/wp-content/themes/screenflex-2014/images/sales/kim-abraham.jpg" alt="Kim St. Clair" />
	<h3>Kim St. Clair</h3>
	<p>Consultant</p>
	</div>
	
	<div class="quote">
	"MultiUint Connectors secure dividers together tightly for semi- permanent set-ups and leave no visible gaps."
	</div>',
								'toolbar' => 'full',
								'media_upload' => 'yes',
							),
						),
					),
				),
				'button_label' => 'Add Module',
				'min' => '',
				'max' => '',
			),
			array (
				'key' => 'field_53864c3aad9a3',
				'label' => 'Background Color',
				'name' => 'middle_background_color',
				'type' => 'select',
				'choices' => array (
					'blue' => 'blue',
					'light-grey' => 'light-grey',
					'dark-grey' => 'dark-grey',
					'white' => 'white',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5386455471e62',
				'label' => 'Bottom Modules',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_538648e881e94',
				'label' => 'Include Bottom Modules?',
				'name' => 'include_bottom_modules',
				'type' => 'select',
				'choices' => array (
					'No' => 'No',
					'Yes' => 'Yes',
				),
				'default_value' => 'No',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_538648e881695',
				'label' => 'Include Email Script?',
				'name' => 'include_email_script',
				'type' => 'select',
				'choices' => array (
					'No' => 'No',
					'Yes' => 'Yes',
				),
				'default_value' => 'No',
				'allow_null' => 0,
				'multiple' => 0,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_538648e881e94',
							'operator' => '==',
							'value' => 'Yes',
						),
					),
					'allorany' => 'all',
				),
			),

			array (
				'key' => 'field_5386479c81e8f',
				'label' => 'Content',
				'name' => 'bottom_module',
				'type' => 'wysiwyg',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_538648e881e94',
							'operator' => '==',
							'value' => 'Yes',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '<h2>Call Us Now!</h2>
	<h3>800-553-0110</h3>
	
	<div class="first agent one-third">
	<img src="/wp-content/themes/screenflex-2014/images/sales/lourdes-morales.jpg" alt="Lourdes Morales" />
	<h3>Lourdes Morales</h3>
	<p>Consultant<br/>Ext. 123<br/><a href="mailto:lourdes@screenflex.com">lourdes@screenflex.com</a></p>
	</div>
	
	<div class="agent one-third">
	<img src="/wp-content/themes/screenflex-2014/images/sales/kim-abraham.jpg" alt="Kim St. Clair" />
	<h3>Kim St. Clair</h3>
	<p>Consultant<br/>Ext. 122<br/><a href="mailto:kim@screenflex.com">kim@screenflex.com</a></p>
	</div>
	
	<div class="agent one-third">
	<img src="/wp-content/themes/screenflex-2014/images/sales/don-austin.jpg" alt="Donald Austin" />
	<h3>Donald Austin</h3>
	<p>Sales Manager<br/>Ext. 114<br/><a href="mailto:donald@screenflex.com">donald@screenflex.com</a></p>
	</div>',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_53864bf6ad9a2',
				'label' => 'Background Color',
				'name' => 'bottom_background_color',
				'type' => 'select',
				'choices' => array (
					'blue' => 'blue',
					'light-grey' => 'light-grey',
					'dark-grey' => 'dark-grey',
					'white' => 'white',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	
	
	// Page Width
	register_field_group(array (
		'id' => 'acf_page-options',
		'title' => 'Page Options',
		'fields' => array (
			array (
				'key' => 'field_538772a213ded',
				'label' => 'Select a page width',
				'name' => 'page_width',
				'type' => 'select',
				'choices' => array (
					'Full Width' => 'Full Width (No Margins)',
					'Wide' => 'Wide (1200px)',
					'Medium' => 'Medium (960px)',
					'Narrow' => 'Narrow (640px)',
				),
				'default_value' => 'Narrow',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_538772a213efe',
				'label' => 'Exclude Navigation',
				'name' => 'exclude_navigation',
				'type' => 'select',
				'column_width' => '',
				'choices' => array (
					 'No' => 'No',
					 'Yes' => 'Yes',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),

		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 1,
	));
	
	
	// Site Wide Options
	register_field_group(array (
		'id' => 'acf_site-wide-options',
		'title' => 'Site Wide Options',
		'fields' => array (
			array (
				'key' => 'field_5388ca69851db',
				'label' => 'CTA Violator Title',
				'name' => 'cta_violator_title',
				'type' => 'text',
				'default_value' => 'Current Offers & Price Guide',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5388ca85851dc',
				'label' => 'CTA Form',
				'name' => 'cta_form',
				'type' => 'gravity_forms_field',
				'allow_null' => 0,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	
	// Video Options
	register_field_group(array (
		'id' => 'acf_video-options',
		'title' => 'Video Options',
		'fields' => array (
			array (
				'key' => 'field_5388e053e70d5',
				'label' => 'Video',
				'name' => 'video',
				'type' => 'text',
				'instructions' => 'Insert the full link to the Youtube video. <br/><small>Example: http://www.youtube.com/watch?v=XXXXXXXXXXX</small>',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5388e053e70e2',
				'label' => 'Video Width',
				'name' => 'video_width',
				'type' => 'text',
				'instructions' => 'Insert the width of the Youtube video',
				'default_value' => '620',
				'placeholder' => '',
				'prepend' => '',
				'append' => 'px',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5388e053e70e1',
				'label' => 'Video Height',
				'name' => 'video_height',
				'type' => 'text',
				'instructions' => 'Insert the height of the Youtube video',
				'default_value' => '360',
				'placeholder' => '',
				'prepend' => '',
				'append' => 'px',
				'formatting' => 'none',
				'maxlength' => '',
			),
      array (
				'key' => 'field_5388e06ce70d6',
				'label' => 'Video Length',
				'name' => 'video_length',
				'type' => 'text',
				'instructions' => 'The length of the video in time.  example: 11:30',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5367b56948541',
				'label' => 'Short Description',
				'name' => 'short_description',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),

		),
		
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'videos',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	
	// Products info
	register_field_group(array (
		'id' => 'acf_products-info',
		'title' => 'Products Info',
		'fields' => array (
			array (
				'key' => 'field_538e317c1b378',
				'label' => 'Hero Image',
				'name' => 'hero_image',
				'type' => 'image',
				'instructions' => 'The large image to the right of the page. Size: 800px x 550px',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_53864c3aa5473',
				'label' => 'Exclude',
				'name' => 'exclude',
				'type' => 'checkbox',
				'instructions' => 'Use this to turn off elements on the page.',
				'choices' => array (
					'storage' => 'Storage Footprint',
					'model' => 'Model Comparison',
				),
				'default_value' => 'on',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_538e426c97a63',
				'label' => 'Heights',
				'name' => 'heights',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_538e427597a64',
				'label' => 'Lengths',
				'name' => 'lengths',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_538e427598521',
				'label' => 'Thickness',
				'name' => 'depth',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_538e2f6aaa5be',
				'label' => 'Features',
				'name' => 'features',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_538e2f79aa5bf',
						'label' => 'Title',
						'name' => 'title',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_538e2f8eaa5c0',
						'label' => 'Image',
						'name' => 'image',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Feature',
			),
			array (
				'key' => 'field_538e2fa1aa5c1',
				'label' => 'Colors',
				'name' => 'colors',
				'type' => 'select',
				'choices' => array (
					'db' => 'Lake',
					'dg' => 'Stone',
					'dm' => 'Rose',
					'dn' => 'Mallard',
					'do' => 'Walnut',
					'dw' => 'Desert',
					'dx' => 'Charcoal Black',
					'dt' => 'White',
					'ds' => 'Primary Blue',
					'dy' => 'Primary Yellow',
					'dr' => 'Primary Red',
					'eb' => 'Summer Blue',
					'eg' => 'Grey Smoke',
					'em' => 'Cranberry',
					'en' => 'Sea Green',
					'eo' => 'Beech',
					'ew' => 'Sand',
					'vb' => 'Blue Tide',
					'vg' => 'Granite',
					'vm' => 'Raspberry Mist',
					'vn' => 'Mint',
					'vo' => 'Sandalwood',
					'vw' => 'Hazelnut',
					'vx' => 'Coal',
					'vt' => 'Ice',
					'sb' => 'Blue',
					'sg' => 'Grey',
					'sm' => 'Muave',
					'sn' => 'Green',
					'so' => 'Oatmeal',
					'sw' => 'Wheat',
					'sx' => 'Black',
					'pb' => 'Blue Sky',
					'pg' => 'Platinum',
					'pn' => 'Green Grass',
					'po' => 'Oak',
					'pw' => 'Prairie',
					'px' => 'Black Pearl',
					'bb' => 'Mist Blue',
					'bg' => 'Light Grey',
					'bo' => 'Tan',
					'wx' => 'Black'
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 1,
			),
			array (
				'key' => 'field_538e306faa5c2',
				'label' => 'Options Link',
				'name' => 'options_link',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_538e3078aa5c3',
				'label' => 'Recommended Use',
				'name' => 'recommended_use',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_538e3090aa5c4',
				'label' => 'Recommended Samples',
				'name' => 'recommended_samples',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_538e309daa5c5',
						'label' => 'Title',
						'name' => 'title',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_538e30aaaa5c6',
						'label' => 'Image',
						'name' => 'image',
						'type' => 'image',
						'instructions' => 'Size: 380px x 260px',
						'column_width' => '',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_538e30b3aa5c7',
						'label' => 'Link',
						'name' => 'link',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Sample',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-product.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

}


?>