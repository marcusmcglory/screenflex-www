<?php 

// SVG Icon Shortcodes
function sf_icon_func( $atts ) {
	extract( shortcode_atts( array(
		'icon' => ''
	), $atts ) );
  
  if($icon == 'blog') {
    return svg_blog();
  } elseif($icon == 'buynow') {
    return svg_buynow();
  } elseif($icon == 'interactivetool') {
    return svg_interactivetool();
  } elseif($icon == 'introvideo') {
    return svg_introvideo();
  } elseif($icon == 'priceguide') {
    return svg_priceguide();
  } elseif($icon == 'seegallery') {
    return svg_seegallery();
  } elseif ($icon == 'fabricswatches') {
    return svg_fabricswatch();
  } elseif ($icon == 'beforeafter') {
    return svg_beforeafter();
  } elseif ($icon == 'planning') {
    return svg_planning();
  }
}
add_shortcode( 'svgicon', 'sf_icon_func' );

?>