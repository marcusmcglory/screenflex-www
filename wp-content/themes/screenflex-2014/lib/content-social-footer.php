<?php
if(is_active_sidebar('Footer Primary Left') || is_active_sidebar('Footer Primary Right')) {
?>
  <div class="social-link-footer">
    <div class="wrap">
      <?php
        if(is_active_sidebar('Footer Primary Left')) {
          !dynamic_sidebar('Footer Primary Left');
        }
        
        if(is_active_sidebar('Footer Primary Right')) {
          !dynamic_sidebar('Footer Primary Right');
        }
      ?>
    </div>
  </div>
<?php } ?>