<?php 

function svg_blog() {
  if(is_ie() && get_browser_version() <= 8) {  
    $icon = '<img src="'.SVG_URL.'blog.png" alt="Blog Icon" class="icon icon-blog" />';
  } else  {
    $icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58.2 56.5" enable-background="new 0 0 58.2 56.5" xml:space="preserve" class="icon icon-blog"><path d="M29.6 54.8C14.9 54.8 3 42.9 3 28.2s12-26.7 26.7-26.7c14.7 0 26.7 12 26.7 26.7S44.3 54.8 29.6 54.8z M29.6 3C15.8 3 4.5 14.3 4.5 28.2s11.3 25.2 25.2 25.2c13.9 0 25.2-11.3 25.2-25.2S43.5 3 29.6 3z" class="style0"/><path d="M42.1 44.8h-25c-0.9 0-1.7-0.8-1.7-1.7V13.3c0-0.9 0.8-1.7 1.7-1.7H34l9.9 9.8V43 C43.9 44 43.1 44.8 42.1 44.8z M17.1 13.1c-0.1 0-0.2 0.1-0.2 0.2V43c0 0.1 0.1 0.2 0.2 0.2h25c0.1 0 0.2-0.1 0.2-0.2v-21l-9-9H17.1 z" class="style0"/><path d="M43.2 22h-8.3c-0.9 0-1.6-0.7-1.6-1.6v-8.3L43.2 22z M34.8 15.6v4.8c0 0.1 0 0.1 0.1 0.1h4.8L34.8 15.6z" class="style0"/><path d="M26.8 23.7h-8.2v-8.9h8.2V23.7z M20.2 22.2h5.2v-5.9h-5.2V22.2z" class="style0"/><rect x="27.5" y="15.9" width="5.1" height="1.5" class="style0"/><rect x="27.5" y="20.7" width="5.1" height="1.5" class="style0"/><rect x="18.3" y="30.2" width="22.4" height="1.5" class="style0"/><rect x="18.4" y="25.4" width="22.4" height="1.5" class="style0"/><rect x="18.3" y="34.9" width="22.4" height="1.5" class="style0"/><rect x="18.3" y="39.7" width="22.4" height="1.5" class="style0"/></svg>';
  }
  return $icon;
}

function svg_buynow() {
  if(is_ie() && get_browser_version() <= 8) {  
    $icon = '<img src="'.SVG_URL.'buynow.png" alt="Buy Now Icon" class="icon icon-buynow" />';
  } else  {
    $icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 68.3 67.7" enable-background="new 0 0 68.3 67.7" xml:space="preserve" class="icon icon-buynow"><path d="M34.7 60.7c-14.7 0-26.7-12-26.7-26.7c0-14.7 12-26.7 26.7-26.7s26.7 12 26.7 26.7 C61.4 48.8 49.4 60.7 34.7 60.7z M34.7 8.9C20.9 8.9 9.6 20.2 9.6 34.1c0 13.9 11.3 25.2 25.2 25.2s25.2-11.3 25.2-25.2 C59.9 20.2 48.6 8.9 34.7 8.9z" class="style0"/><path d="M56.9 34.1L35.8 21.8v11.5H13.2v1.5h22.6v11.5L56.9 34.1z M37.3 24.4l16.7 9.6l-16.7 9.6V24.4z" class="style0"/></svg>';
  }
  return $icon;
}


function svg_interactivetool() {
  if(is_ie() && get_browser_version() <= 8) {  
    $icon = '<img src="'.SVG_URL.'interactivetool.png" alt="Interactive Tool Icon" class="icon icon-interactivetool" />';
  } else  {
    $icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 60.7 61.3" enable-background="new 0 0 60.7 61.3" xml:space="preserve" class="icon icon-interactivetool"><path d="M30.4 57.7c-14.7 0-26.7-12-26.7-26.7s12-26.7 26.7-26.7c14.7 0 26.7 12 26.7 26.7S45.1 57.7 30.4 57.7z M30.4 5.9C16.5 5.9 5.2 17.2 5.2 31.1s11.3 25.2 25.2 25.2c13.9 0 25.2-11.3 25.2-25.2S44.3 5.9 30.4 5.9z" class="style0"/><path d="M26 48.3H14.6V17.8H26V48.3z M16.1 46.8h8.4V19.3h-8.4V46.8z" class="style0"/><path d="M35.9 48.3H24.5V17.8h11.4V48.3z M26 46.8h8.4V19.3H26V46.8z" class="style0"/><path d="M34.4 18v29.7l1.5 0.6l5.6-3.2c-0.2-0.2-0.8 0.3-1 0.1c-0.4-0.5-0.2-0.8-0.4-1.3c0 0 0.6 1.3 0.1 0.1 L40 44.1l-4.1 2.1V18.9l8.4-4.4v20.3c0 0.5 0.1 0.8 0 0.8c0.8 0.1 0.7 0.6 1.5 0.7V12L34.4 18z" class="style0"/><path d="M43.9 36.1h1.3v-0.6h-1.3h0h-0.6V38h0.6V36.1z M39.5 40.5h0.6v-8.3h-0.6V38h-0.6v0.6h0.6V40.5z M41.4 38H42 v-2.5h1.3v-0.6H42v-2.5h-0.6v-0.6h-1.3v0.6h1.3V38z M38.8 41.1v-1.3h-0.6v-0.6h-0.6V38h1.3v-0.6h-1.9v0.6V38v1.3h0.6v0.6h0.6v1.3 H38.8z M46.4 36.1h-0.6h0h-0.6v2.5h0.6v-1.9h0.6v0.6h0.6v-0.6h-0.6V36.1z M46.4 43.7h0.6v-1.9h-0.6V43.7z M45.8 44.9h-5.1v-1.3h-0.6 v-1.3h-0.6v1.3h0.6v1.9h0.6l0 0h5.1l0 0h0.6v-1.9h-0.6V44.9z M39.5 41.1h-0.6v1.3h0.6V41.1z M47.1 37.3v4.4h0.6v-4.4H47.1z" class="style0"/><g><path d="M39 14.5c-0.6-2-3-3.2-6-3l-0.1-1.5c3.7-0.2 6.7 1.4 7.6 4L39 14.5z" class="style0"/><g><polygon points="33.1,9 30.2,11.3 33.7,12.6" class="style0"/></g></g></svg>';
  }
  return $icon;
}

function svg_introvideo() {
  if(is_ie() && get_browser_version() <= 8) {  
    $icon = '<img src="'.SVG_URL.'introvideo.png" alt="Intro Video Icon" class="icon icon-introvideo" />';
  } else  {
    $icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 60 59.3" enable-background="new 0 0 60 59.3" xml:space="preserve" class="icon icon-introvideo"><path d="M30.4 55.9c-14.7 0-26.7-12-26.7-26.7s12-26.7 26.7-26.7c14.7 0 26.7 12 26.7 26.7S45.1 55.9 30.4 55.9z M30.4 4.1C16.5 4.1 5.2 15.4 5.2 29.3s11.3 25.2 25.2 25.2c13.9 0 25.2-11.3 25.2-25.2S44.2 4.1 30.4 4.1z" class="style0"/><path d="M22 43.5V16.4l23.6 13.6L22 43.5z M23.5 18.9v22l19.1-11L23.5 18.9z" class="style0"/></svg>';
  }
  return $icon;
}

function svg_priceguide() {
  if(is_ie() && get_browser_version() <= 8) {  
    $icon = '<img src="'.SVG_URL.'priceguide.png" alt="Price Guide Icon" class="icon icon-priceguide" />';
  } else  {
    $icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 63.5 64" enable-background="new 0 0 63.5 64" xml:space="preserve" class="icon icon-priceguide"><g><path d="M31.6 4.6c-14.7 0-26.7 12-26.7 26.7s12 26.7 26.7 26.7s26.7-12 26.7-26.7S46.3 4.6 31.6 4.6z M31.6 56.4 c-13.9 0-25.2-11.3-25.2-25.2S17.7 6.1 31.6 6.1s25.2 11.3 25.2 25.2S45.4 56.4 31.6 56.4z" class="style0"/><path d="M38.1 31.7c-0.6-0.6-1.4-1.1-2.4-1.6c-0.5-0.2-1.6-0.5-3.2-0.9v-9.1c1 0.2 1.8 0.6 2.4 1.3s1 1.6 1.2 2.9 l3.3-0.5c-0.3-2-1.1-3.6-2.5-4.7c-1.1-0.9-2.5-1.4-4.4-1.6v-1.5h-1.9v1.5c-2.1 0.2-3.7 0.8-4.7 1.7c-1.6 1.4-2.4 3.2-2.4 5.5 c0 1.3 0.3 2.4 0.8 3.4s1.3 1.8 2.3 2.3c1.4 0.8 2.7 1.3 4 1.5v10.1c-1.2-0.1-2.3-0.8-3.2-1.9c-0.6-0.8-1.1-2-1.3-3.7L23 36.9 c0.1 1.6 0.5 3 1.3 4.2s1.6 2 2.6 2.5s2.3 0.9 3.9 1.1v3.2h1.9v-3.2c2.2-0.1 4-0.9 5.4-2.4s2.1-3.3 2.1-5.5c0-1-0.2-1.9-0.5-2.8 S38.7 32.3 38.1 31.7z M30.7 28.8c-1.4-0.4-2.5-1-3.1-1.7s-0.9-1.6-0.9-2.7c0-1.1 0.4-2 1.1-2.8s1.7-1.3 2.9-1.5V28.8z M35.6 40.3 c-0.8 0.9-1.8 1.4-3 1.6v-9.6c1.7 0.6 2.8 1.2 3.4 1.9s0.9 1.6 0.9 2.8C36.8 38.2 36.4 39.4 35.6 40.3z" class="style0"/></g></svg>';
  }
  return $icon;
}

function svg_seegallery() {
  if(is_ie() && get_browser_version() <= 8) {  
    $icon = '<img src="'.SVG_URL.'seegallery.png" alt="See The Gallery Icon" class="icon icon-seegallery" />';
  } else  {
    $icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 68.3 67.7" enable-background="new 0 0 68.3 67.7" xml:space="preserve" class="icon icon-seegallery"><path d="M34.7 60.9C20 60.9 8.1 49 8.1 34.3c0-14.7 12-26.7 26.7-26.7s26.7 12 26.7 26.7 C61.4 49 49.4 60.9 34.7 60.9z M34.7 9.1C20.9 9.1 9.6 20.4 9.6 34.3c0 13.9 11.3 25.2 25.2 25.2s25.2-11.3 25.2-25.2 C59.9 20.4 48.6 9.1 34.7 9.1z" class="style0"/><g><path d="M33.8 33.3H18.7V18.2h15.1V33.3z M20.2 31.8h12.1V19.7H20.2V31.8z" class="style0"/><path d="M50.8 33.3H35.7V18.2h15.1V33.3z M37.2 31.8h12.1V19.7H37.2V31.8z" class="style0"/><path d="M33.8 50.3H18.7V35.2h15.1V50.3z M20.2 48.8h12.1V36.7H20.2V48.8z" class="style0"/><path d="M50.8 50.3H35.7V35.2h15.1V50.3z M37.2 48.8h12.1V36.7H37.2V48.8z" class="style0"/></g></svg>';
  }
  return $icon;
}

function svg_fabricswatch() {
  if(is_ie() && get_browser_version() <= 8) {
    $icon = '<img src="'.SVG_URL.'fabricswatch.png" alt="See The Gallery Icon" class="icon icon-fabricswatch" />';
  } else {
    $icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 68 65.2" enable-background="new 0 0 68 65.2" xml:space="preserve" class="icon icon-fabricswatch"><path fill="#0E76BC" d="M34 59.3c-14.7 0-26.7-12-26.7-26.7C7.3 17.9 19.3 6 34 6s26.7 12 26.7 26.7C60.7 47.3 48.7 59.3 34 59.3z M34 7.5C20.1 7.5 8.8 18.7 8.8 32.6c0 13.9 11.3 25.2 25.2 25.2s25.2-11.3 25.2-25.2C59.2 18.7 47.9 7.5 34 7.5z"/><rect x="17.2" y="21.1" transform="matrix(0.9848 -0.1737 0.1737 0.9848 -5.3471 5.6248)" fill="#8BBCDF" width="24.6" height="24.6"/><g><image overflow="visible" opacity="0.4" width="121" height="121" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH0AAAB9CAYAAACPgGwlAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA GXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABGVJREFUeNrs2QtP2zAUhuHcKJQO 7Yb2///gNgEFeo3nSJ+1I89JHVjSwd5POgq9oz72sZMWBSGEkHeY8kyvJa+Pe+kLm1cAg/5GB0KT iW2PJfD/FK5LYLuXopdRVdGxBP6s4AG7jY7uFHxzArxS1abC7fA4bX/+lh2Aj/r7aKpV9cI3GeDd cy5UCx3tAKDlz9/KLfTe107Hrg5mYCThh2Z6AL9UXfla6rjQYxYe9HnQA/hB2BtfzzqGznvoWeuT 6HYNrzWrO/CVrw+qlfAv9XhqtpNp8AN6N6O3wn70tZZXEa3xZWq29830yqBfCfuTr8++Pur2UjO+ jgYLmQ48rOM7gXfYd8YxdAC7vo9a02uhLoXcgd/6+ir4lQZEk2jxZBr4VqgbzfA7TczCtPuwvgdH l4seNnFhpq8E3YF/8/XF140GBC1+/tbezfIHLbGF7nsyVQ1NxKGNXJ2Y7R8Ffqu/r/U46POh7wQb wLdq83a5rYcsmkRbj1t82MEvNeNvBP5Jt+N1nUyHHtbzhQG/NxvrJmHxx2auGbi4UkUbuoU5bbsW +MqcvnGFbtpTNqd1O6zhu+gU+sKcQldDFkPtPV7fa3OhZmHqkpk+20yvzRoeoJsI++Tky/nBJb7+ Hg8Ce3WOTJfWmNWRRTnmtHnMT6tDxUZu+plemQsufZWVMbOzTAyAgrV8lpSZBq9GzxlBQJ9vALzU jHX4fwzooBPQCegEdAI6AZ2ATkAnoBPQCegEdAI6AZ2ATkAnoINOQCegE9AJ6AR0AjoBnYBOQCeg E9AJ6AR0AjoBnYAOOgGdgE5AJ6AT0AnoBHQCOgGdgE5AJ6AT0AnoBHQCOugEdAI6AZ2ATkAnoBPQ CegEdAI6AZ2ATkAnoBPQCeigE9AJ6AR0AjoBnYBOQCegE9AJ6AR0AjqZCt2ZGnoOmTfulWajZrrr eWPHAJgN+pRBVpoRHzpUrQYQ8NOlzXD4K+gp3FBHXwdftfmnSmwmm+nOfOfHyGIUfpPRTmLkva+d 6sK8TwAHfrrWfjDf/U4W8SA42fKbgQ+x2AF64+vZ15OvhZ6312wvAZ9lpncOjzJ4lkkYABa/F77p 2RXG7WSrD+g+7MHXpZ63E34F+izorb7zDvxOFo+y2ZpZ7xKbvqz2fjQjq3vTtT4ogHcfslSLB30+ 9L08OvAfMlnrvp1xG93eW/MBG42mO7OGd+D3vq70HhXgs8C3ms3W5LuOj7p/n1jfs9BdYpY3Zg1f a5YvovUc+Glnuuvpvj8Ts92NQS+iDdzGnJYdtJ4s1eZp7edr8XaftVZtog1dVnt30Zq+L35ftTua TcRCr60TrR38aU7ZbIs/mtO3jdnFbw1673l7k7F+FObvgF5H4KDPh27h7en03py3t0MbuXLg/lKo lUGuze2ySF+7B306dLv8OtPGj6biq3SjgMqoqujIFbjzDgI7610x4nLsKbAycaSV/5stvygyf3XL RStp42+i7Wf90lbz3b0rdEIIUX4JMACPBZ1BTXbwHAAAAABJRU5ErkJggg==" transform="matrix(0.24 0 0 0.24 18.3655 19.6281)"></image><g><rect x="21.2" y="21.1" fill="#BCBEC0" width="24.6" height="24.6"/></g></g><g><image overflow="visible" opacity="0.4" width="137" height="137" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI4AAACOCAYAAADn/TAIAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA GXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACcNJREFUeNrsnAtT4zgQhGXHCY/l bu9Z9/9/4d0uC4SQxOdUjWqHYeSnxnicVpUqdgiQxB/dM1KHEDAwMDAwMDAwMKxGgbcAF2zI70z9 /rrjHOOKwCnYrZwaJLU4BlBXCE4EpBRTg6dm8yzO+wAFmFYCjgRm08yKTQ4QB0bOWhxrE+q0InA4 MNtm7mje0PmWwXMZJzHPynEKKk2ZAJRDcAoGTYTlrpn3dHvHAIqKEyE5Jib/Wh+QYHcOweFKc0PA /NLMX+n2F4JnR4/j4LyxeRTn/H4Ok1Qn2J3RqGYqiKPi3BEsvzfzj2b+1syHZt7ScykYBBGOA7s9 iHMNIgkT7M4xOBWBc09qc4Hm72b+1cyvzfzC7IpbkQZNamrqBLtzCE4Q4ESreiCluUDzD6nPA4FV sgup2dMFkNcWcA6KUsHunIOzJUv6QqoT7epPsq8bsrSgFMj8wksVelNU59ByC7tbMDhFosaJ4Dyw IvkrHd/S42R3pXVZbQqiqRTszrniyJb8C5u8QA7sjZZ/7RIkrXVvAwt250RxNqIlvyFIbtl5XBTc MnDkGynf/JR9HJVb2J0zxSmF4vCV4x27b0uP2YTuTU/5xso3PKVQsDsn4JQCnEqAw7cb5MZnaPlL Sv0lamDB7ozUqTK2KWlVHJqKqYwEplB+Zi2+VidUqQso2F0GdbJeAOS74bsOtSl6/LwumIYABbvr B9anKA7fq9LqG17XpIJdQ2AaAhTsLm175y4VslacjVLbxFmJ9jv37w6wu6TdafDz84JBpMJTzWBT W6E4N0xthliVBVDXanfa75TgcZX6AI8VODLx12ZTc0EDu9O/d0+3LzT3dE3i44s5FIfXKF1t+FLA uTa7i8DE+dzMp2b+aOYjqzm5gn54rXNZVZvalCML46UA5dHuotJEaL7RrMRe4Vlp600Vp2hZ/JPg rGF4s7sIzgWa77RvuKPncRIqVdJ9sxTHbdBoNrXWT3Iu0e4iFC8Ezj1dk8Duf6a5SV2fyhCaUrTj UnHWDs1S7S4qzwWQO7oWNSnQI1OfStSfhXWNU4g1nJRNLbkwXrPdnUhZ4gcEzgRRjLZs+1ybygAY XhhvQvf+FMCZ1+5OzIJOZEm7odfFssbRFv+2jOiNw45qDXYXH6d1tppqBesFwK6OaitUB6DMr04h 6Cv1qZZfbcWti+NUR6Wt32DMCxS3rZOy1hPvV6EJBusoZYvaoKP6/FG3QMNXm49CcczAyRHewpgX oLNY15HbFLOAwwHKGd7CsFGbc4vaaIrzwbIsFMc6vIUxbcjVZg2e1sLYSnE+K7yF0V91zuH9Pha3 qk61yQ3OUsNbGP3qGxn24iEuU8XxFN5CjfNTdbhVSWhqS3DWFN66JsU5hvSnKM6faVVrCm+tqbaR u+RvSht+Ch0fj8mpONcW3vIM0ElRnEPQV43NuyqEt/ypjoQmwtQKTU5wEN5aR0clP7wXrMFBeMtX R3Wa0lHlAAfhLV+jC5peHZVFjYPw1rJrm3NIrxr3ilNYKQ7CW77Wb+Su+FG04rMVxwhv+eiojuHj R4aP4eOH8EyLY4S3fFkV76hew8d9qk5opoKD8JY/gE4tNtUZ3sqtOAhv+WrDR4e3LBQH4a3lt+GT w1sWioPwlo8aZ1J4K3c7jvCWn1Z8UngrJzgIb/mqcSaFt3KAg/CWT8WZFN6ytCqEt5ZZ22QJb+VU HIS3/AA0ObxlUeMgvOVHdUaHt3KBg/DWOjqq3uGtXOAgvOWro5oc3poKDsJbvka28FbuGgfhrWXX NtnCW7kVB+EtX+s3o8NbuYtjhLd8dFSTw1s5wEF4y5dVZQlvTQEH4S1/AGULb+VQHIS3fLXhWcJb uRQH4a3lt+FZw1u5FAfhLR81TrbwVo52HOEtP614tvDWVHAQ3vJV42QLb40FB+Etn4qTLbyV26oQ 3lpmbZM9vDVVcRDe8gNQ1vBWrhoH4S0/qpMlvDUFHIS31tFRjQpvTQEH4S1fHVXW8NYYcBDe8jVM wls5ahyEt5Zd25iEt3IoDsJbvtZvsoS3chTHCG/56KiyhrfGgoPwli+ryh7eGgoOwlv+ADIJb41V HIS3fLXh2cNbUxQH4a3lt+Fm4a0pioPwlo8axyS8NbYdR3jLTytuEt4aAw7CW75qHJPw1hBwEN7y qTgm4a0cVoXw1jJrG9Pw1hjFQXjLD0Bm4a0pNQ7CW35UJ3t4ayg4CG+to6OaHN4aCg7CW746KrPw Vl9wEN7yNczDW2NrHIS3ll3bmIe3xioOwlu+1m+yh7fGFscIb/noqMzCW0PAQUfly6pMw1tDwJHF caWs3UjFwfgcgMzDW3xUHcCE8D71t2EAxWP+2FThBaDmacNNw1t9wOlqybk1Sbks2QsrlGMAlb8N nyW8NQQceYG1rXu+bsAh0VKAhfLzCsCUpcYxD2+NAUc+wVh8vdDc0uNk9rhQ1nZSQEGdprXi5uGt oeBIYPYEy2Mz7wmUM31tF94vCG6YxZViTahoAauvOl0zULOHt4aAw7fq30hlnpr5rZm3BMSJQLoL P2OkvOvit2UCqDLoWR7Y3bA/bNPwVh9wtGo9QvOdlKWkJ/XczAcC5za8X1Xesha+EnMjjiVQBeyu 96LfLOGtoYpzpiewJ3Di90RovhM0UXFkpLRramBtYHeDAJolvNUXHPmkXuniBXb+Q0Bzw6CRtzKn vEs8RoIEu+uvOqbhraHgRP+Mb+SJKdCPDpXZKcc7Zd50qBTsblxHlTW8Ncaq+DkHR+6QaxdXXvxd j7mF3Q1eNZ6to+oLTmDwcOs6JC6MtjWRgklToS3sbtCq8WzhrTHrOIFJXknHhbLQp015gSRMbQoC u2svIWYNb41dOQ4MIPkmyfNSuZUwlcpFqxITdtdv/cY0vDUGHAlQfOG18qLboOoDkjyG3XV3VLOE t6aAk1Ihfp564RpQRU/Lg92lrWq28FYucKYAlYIJdtdtd9re1CHMEN6yBqcPUGPUCXb3szzQcjev THHM4hSfAQ7sbrrd8UXYCMqezXifSXhrSeDA7obZXXwdJ1KVJ9ovvMwXBk9UIxO1WTI4sDsdpA2z qzfa8rlsMj/S8TOzK77dEK5FcWB3aZUq6TUeSWUu2ah/CaAnuu9g2VGtBZxrsTv+fdGu9qQ2F3D+ o+OoOkdYFexup4BTk7LETFRUnWcoDuxOszv+Of2Yi3pi9c6zaMdNuip8cmD6+za33fF/mRfTmfET J7HD2iuqUwOc5cNkaXebxGKgtldlsk8FcJYHVB+7kwDKwLrpijHA8Wt32n8FiZCcg1FwC+D4V6fU dasVWOq5niiGL6C6ukIMDAwMDAwMDIwB438BBgBm5Bwerm/gewAAAABJRU5ErkJggg==" transform="matrix(0.24 0 0 0.24 20.9299 17.7009)"></image><g><rect x="25.8" y="21.1" transform="matrix(0.9848 0.1736 -0.1736 0.9848 6.3766 -6.1018)" fill="#0B77B3" width="24.6" height="24.6"/></g></g></svg>';
  }
  return $icon;
}

function svg_beforeafter() {
  if(is_ie() && get_browser_version() <= 8) {  
    $icon = '<img src="'.SVG_URL.'beforeafter.png" alt="Before and After Icon" class="icon icon-beforeafter" />';
  } else  {
    $icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="icon-beforeafter" class="icon icon-beforeafter" x="0px" y="0px" viewBox="0 0 59.3 58.8" enable-background="new 0 0 59.3 58.8" xml:space="preserve"><ellipse fill="#8BBCDF" cx="29.8" cy="29.2" rx="29.5" ry="29.4"/><g><path fill="#FFFFFF" d="M6.8 16.4h6.7c2.1 0 3.7 0.2 4.8 0.5c1.1 0.3 2 1 2.6 2c0.7 1 1 2.6 1 4.8c0 1.5-0.2 2.5-0.7 3.1 s-1.4 1-2.8 1.4c1.5 0.3 2.6 0.9 3.1 1.7c0.5 0.8 0.8 2 0.8 3.7v2.4c0 1.7-0.2 3-0.6 3.8c-0.4 0.8-1 1.4-1.9 1.7 c-0.9 0.3-2.6 0.5-5.3 0.5H6.8V16.4z M13.4 20.7v5.7c0.3 0 0.5 0 0.7 0c0.7 0 1.1-0.2 1.3-0.5s0.3-1.2 0.3-2.8 c0-0.8-0.1-1.4-0.2-1.7s-0.3-0.5-0.6-0.6C14.6 20.8 14.1 20.7 13.4 20.7z M13.4 30.4v7.2c0.9 0 1.5-0.2 1.8-0.4 c0.3-0.3 0.4-0.9 0.4-1.9v-2.4c0-1.1-0.1-1.8-0.3-2S14.4 30.4 13.4 30.4z"/><path fill="#FFFFFF" d="M36 15.9l-7.6 26.6h-4.9l7.6-26.6H36z"/><path fill="#FFFFFF" d="M49.1 16.4L52.9 42h-6.8l-0.4-4.6h-2.4L42.9 42H36l3.4-25.6H49.1z M45.5 32.8c-0.3-2.9-0.7-6.5-1-10.8 c-0.7 4.9-1.1 8.5-1.3 10.8H45.5z"/></g></svg>';
  }
  return $icon;
}
function svg_planning() {
  if(is_ie() && get_browser_version() <= 8) {  
    $icon = '<i class="icon-book-2" style="font-size:50px;font-size:5rem;padding:10px;padding:1rem;"></i>';
  } else  {
    $icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 63.5 64" enable-background="new 0 0 63.5 64" xml:space="preserve" class="icon icon-planning" id="icon-planning"><style>.style00{stroke:	#0E76BC;stroke-width:	1.5;stroke-miterlimit:	10;fill:	none;}.style11{fill:	#0E76BC;}</style><g><circle cx="31.8" cy="31.2" r="25.9" class="style00"/><path d="M13.7 15.4v31.7h36.2V15.4H13.7z M47.5 45.1H16V17.4h31.6V45.1z" class="style11"/><line x1="21.2" y1="26.9" x2="42.3" y2="26.9" class="style00"/><line x1="21.2" y1="36.8" x2="42.3" y2="36.8" class="style00"/><line x1="31.7" y1="27.4" x2="31.7" y2="36.3" class="style00"/><line x1="27.8" y1="26.4" x2="27.8" y2="21.1" class="style0"/><line x1="27.8" y1="37.3" x2="27.8" y2="41.8" class="style00"/></g></svg>';
  }
  return $icon;
}



?>