<?php 

//hook into the init action and call create_taxonomies when it fires
add_action( 'init', 'create_photo_taxonomies', 0 );

function create_photo_taxonomies() 
{

  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Uses', 'taxonomy general name' ),
    'singular_name' => _x( 'Use', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Use' ),
    'popular_items' => __( 'Popular Uses' ),
    'all_items' => __( 'All Uses' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Use' ), 
    'update_item' => __( 'Update Use' ),
    'add_new_item' => __( 'Add New Use' ),
    'new_item_name' => __( 'New Use' ),
    'separate_items_with_commas' => __( 'Separate Uses with commas' ),
    'add_or_remove_items' => __( 'Add or remove Uses' ),
    'choose_from_most_used' => __( 'Choose from the most used Uses' ),
    'menu_name' => __( 'Uses' )
  ); 

  register_taxonomy('use','photo',array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_tagcloud' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'photos/use', 'with_front' => false )
  ));  
  
  
  $labels = array(
    'name' => _x( 'Colors', 'taxonomy general name' ),
    'singular_name' => _x( 'Color', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Colors' ),
    'popular_items' => __( 'Popular Colors' ),
    'all_items' => __( 'All Colors' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Color' ), 
    'update_item' => __( 'Update Color' ),
    'add_new_item' => __( 'Add New Color' ),
    'new_item_name' => __( 'New Color Name' ),
    'separate_items_with_commas' => __( 'Separate colors with commas' ),
    'add_or_remove_items' => __( 'Add or remove colors' ),
    'choose_from_most_used' => __( 'Choose from the most used colors' ),
    'menu_name' => __( 'Colors' )
  ); 

  register_taxonomy('color','photo',array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_tagcloud' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'photos/color', 'with_front' => false )
  ));
  
  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Heights', 'taxonomy general name' ),
    'singular_name' => _x( 'Height', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Heights' ),
    'popular_items' => __( 'Popular Heights' ),
    'all_items' => __( 'All Heights' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Height' ), 
    'update_item' => __( 'Update Height' ),
    'add_new_item' => __( 'Add New Height' ),
    'new_item_name' => __( 'New Height' ),
    'separate_items_with_commas' => __( 'Separate heights with commas' ),
    'add_or_remove_items' => __( 'Add or remove heights' ),
    'choose_from_most_used' => __( 'Choose from the most used heights' ),
    'menu_name' => __( 'Heights' )
  ); 

  register_taxonomy('height','photo',array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_tagcloud' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'photos/height', 'with_front' => false )
  ));
  
    // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Lengths', 'taxonomy general name' ),
    'singular_name' => _x( 'Length', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Length' ),
    'popular_items' => __( 'Popular Lengths' ),
    'all_items' => __( 'All Lengths' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Length' ), 
    'update_item' => __( 'Update Length' ),
    'add_new_item' => __( 'Add New Length' ),
    'new_item_name' => __( 'New Length' ),
    'separate_items_with_commas' => __( 'Separate Lengths with commas' ),
    'add_or_remove_items' => __( 'Add or remove Lengths' ),
    'choose_from_most_used' => __( 'Choose from the most used Lengths' ),
    'menu_name' => __( 'Lengths' )
  ); 

  register_taxonomy('length','photo',array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_tagcloud' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'photos/length', 'with_front' => false )
  ));  
  

 };
 
 function post_type_photos() {
    register_post_type('photo',
        array(
            'labels' => array(
                'name' => __('Photos')
                ,'singular_name' => __('Photo')
                ,'add_new' => __('Add New Photo')
                ,'add_new_item' => __('Add New Photo')
                ,'edit' => __('Edit')
                ,'edit_item' => __('Edit Photo')
                ,'new_item' => __('New Photo')
                ,'view' => __('View Photo')
                ,'view_item' => __('View Photo')
                ,'search_items' => __('Search Photos')
                ,'not_found' => __('No Photos found')
                ,'not_found_in_trash' => __('No Photos found in Trash')
                ,'parent' => __('Parent Photo')
                )
            ,'singular_label' => __('Photo')
            ,'public' => true
            ,'publicly_queryable' => true
            ,'show_ui' => true
            ,'capability_type' => 'post'
            ,'hierarchical' => 'false'
            ,'menu_name' => 'Photos'
            ,'menu_position' => 5
            ,'menu_icon' => 'dashicons-images-alt'
            , 'query_var' => true
            , 'rewrite' => array('slug' => 'photos',  'with_front' => false)
            , 'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields', 'comments', 'revisions', 'page-attributes', 'genesis-seo','genesis-cpt-archives-settings')
            ,'has_archive' => 'photos'
            ,'taxonomies' => array('use','color','height','length')
            )
        );
   /* add_post_type_support( 'photo', 'post-formats' ); */
 };
 
add_action('init', 'post_type_photos');
 //set up Custom Photo Gallery Types

  
  function post_type_plans() {
    register_post_type('plans',
        array(
            'labels' => array(
                'name' => __('Plans')
                ,'singular_name' => __('Plan')
                ,'add_new' => __('Add New Plan')
                ,'add_new_item' => __('Add New Plan')
                ,'edit' => __('Edit')
                ,'edit_item' => __('Edit Plan')
                ,'new_item' => __('New Plan')
                ,'view' => __('View Plan')
                ,'view_item' => __('View Plan')
                ,'search_items' => __('Search Plans')
                ,'not_found' => __('No Plans found')
                ,'not_found_in_trash' => __('No Plans found in Trash')
                ,'parent' => __('Parent Plan')
                )
            ,'singular_label' => __('Plan')
            ,'public' => true
            ,'publicly_queryable' => true
            ,'show_ui' => true
            ,'capability_type' => 'post'
            ,'hierarchical' => 'false'
            ,'menu_name' => 'Plans'
            ,'menu_position' => 6
            ,'menu_icon' => 'dashicons-networking'
//            , 'taxonomies' => array('')
            , 'query_var' => true
         , 'rewrite' => array('slug' => 'plans',  'with_front' => false)
            , 'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields', 'comments', 'revisions', 'page-attributes', 'genesis-seo','genesis-cpt-archives-settings' )
            ,'has_archive' => 'plans'
        ) 
    );
  }
  
add_action('init', 'post_type_plans');



function create_plan_taxonomies() {
   // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Rooms', 'taxonomy general name' ),
    'singular_name' => _x( 'Room Quantity', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Room Quantities' ),
    'popular_items' => __( 'Popular Room Quantities' ),
    'all_items' => __( 'All Rooms' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Room' ), 
    'update_item' => __( 'Update Room' ),
    'add_new_item' => __( 'Add New Room' ),
    'new_item_name' => __( 'New Room' ),
    'separate_items_with_commas' => __( 'Separate Rooms with commas' ),
    'add_or_remove_items' => __( 'Add or remove Rooms' ),
    'choose_from_most_used' => __( 'Choose from the most used Rooms' ),
    'menu_name' => __( 'Rooms' )
  ); 

  register_taxonomy('rooms','plans',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_tagcloud' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'plans/rooms' )
  ));  

}

add_action( 'init', 'create_plan_taxonomies', 0 );



add_action( 'init', 'register_cpt_review' );

function register_cpt_review() {

    $labels = array( 
        'name' => _x( 'Reviews', 'reviews' ),
        'singular_name' => _x( 'Review', 'review' ),
        'add_new' => _x( 'Add New', 'review' ),
        'add_new_item' => _x( 'Add New Review', 'review' ),
        'edit_item' => _x( 'Edit Review', 'review' ),
        'new_item' => _x( 'New Review', 'review' ),
        'view_item' => _x( 'View Review', 'review' ),
        'search_items' => _x( 'Search Reviews', 'review' ),
        'not_found' => _x( 'No reviews found', 'review' ),
        'not_found_in_trash' => _x( 'No reviews found in Trash', 'review' ),
        'parent_item_colon' => _x( 'Parent Review:', 'review' ),
        'menu_name' => _x( 'Reviews', 'reviews' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Reviews and Testimonials are unedited comments and feedback from Screenflex Room Divider customers.  ',
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', 'revisions', 'page-attributes', 'genesis-seo','genesis-cpt-archives-settings'  ),
        'taxonomies' => array( 'category', 'post_tag' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-megaphone',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => 'reviews',
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'page',
    );

    register_post_type( 'review', $args );
}


// Add Videos CPT
add_action( 'init', 'register_videos_post_type' );
function register_videos_post_type() {
  $singular = 'Video';
  $plural = 'Videos';

  $labels = array(
    'name' => $plural,
    'singular_name' => $singular,
    'add_new' => "Add New",
    'add_new_item' => "Add New $singular",
    'edit_item' => "Edit $singular",
    'new_item' => "New $singular",
    'all_items' => "All $plural",
    'view_item' => "View $singular",
    'search_items' => "Search $plural",
    'not_found' =>  "No $plural found",
    'not_found_in_trash' => "No $plural found in Trash", 
    'parent_item_colon' => '',
    'menu_name' => $plural
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => strtolower($plural) ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-video-alt2',
    'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields', 'comments', 'revisions', 'page-attributes', 'genesis-seo','genesis-cpt-archives-settings' )

  ); 

  register_post_type( 'videos', $args );
}

add_action( 'init', 'register_collections_taxonomies', 0 );
function register_collections_taxonomies() {
  $singular = 'Collection';
  $plural = 'Collections';
  $cpt = 'videos';

  $labels = array(
    'name'                => _x( $plural, 'taxonomy general name' ),
    'singular_name'       => _x( $singular, 'taxonomy singular name' ),
    'search_items'        => __( "Search $plural" ),
    'all_items'           => __( "All $plural" ),
    'parent_item'         => __( "Parent $singular" ),
    'parent_item_colon'   => __( "Parent $singular:" ),
    'edit_item'           => __( "Edit $singular" ), 
    'update_item'         => __( "Update $singular" ),
    'add_new_item'        => __( "Add New $singular" ),
    'new_item_name'       => __( "New $singular Name" ),
    'menu_name'           => __( $plural )
  ); 	

  $args = array(
    'hierarchical'        => true,
    'labels'              => $labels,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => strtolower($singular) )
  );

  register_taxonomy( 'collections', $cpt, $args );
}

add_action( 'init', 'register_video_tags_taxonomies', 0 );
function register_video_tags_taxonomies() {
  $singular = 'Tag';
  $plural = 'Tags';
  $cpt = 'videos';

  $labels = array(
    'name'                => _x( $plural, 'taxonomy general name' ),
    'singular_name'       => _x( $singular, 'taxonomy singular name' ),
    'search_items'        => __( "Search $plural" ),
    'all_items'           => __( "All $plural" ),
    'parent_item'         => __( "Parent $singular" ),
    'parent_item_colon'   => __( "Parent $singular:" ),
    'edit_item'           => __( "Edit $singular" ), 
    'update_item'         => __( "Update $singular" ),
    'add_new_item'        => __( "Add New $singular" ),
    'new_item_name'       => __( "New $singular Name" ),
    'menu_name'           => __( $plural )
  ); 	

  $args = array(
    'hierarchical'        => false,
    'labels'              => $labels,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'video-tag' )
  );

  register_taxonomy( 'video-tags', $cpt, $args );
}

