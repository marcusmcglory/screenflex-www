<?php


/**
 * Archive Post Class
 * @since 1.0.0
 *
 * Breaks the posts into three columns
 * @link http://www.billerickson.net/code/grid-loop-using-post-class
 *
 * @param array $classes
 * @return array
 */
 
add_action( 'genesis_meta', 'plans_genesis_meta' );
function plans_genesis_meta() {
	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
}

function be_archive_post_class( $classes ) {

//Default to signle column
    if (is_post_type_archive('plans') || is_tax('rooms'))
        return $classes;

	$classes[] = 'one-third';
	global $wp_query;
	if( 0 == $wp_query->current_post || 0 == $wp_query->current_post % 3 )
		$classes[] = 'first';
	return $classes;

}
add_filter( 'post_class', 'be_archive_post_class' );



// Customize the post info function, Remove the Post_info
remove_action( 'genesis_before_post_content', 'genesis_post_info' );

add_filter('genesis_post_info', 'ss_post_info_filter');
function ss_post_info_filter($post_info) {
    if (!is_page()) {
        $post_info = '';
        return $post_info;
    }
}


//Customize the Grid Featured Image
/** Change the featured image */
remove_action( 'genesis_post_content', 'genesis_do_post_image' );
add_action('genesis_post_content','ss_post_image', 0) ;
function ss_post_image() {
    if (is_post_type_archive('plans') || is_tax('rooms')) {
        genesis_do_post_image();
        return;
    }
        

    if ( ! is_singular() && genesis_get_option( 'content_archive_thumbnail' ) ) {
        $img = genesis_get_image( array( 'format' => 'html', 'size' => genesis_get_option( 'image_size' ), 'attr' => array( 'class' => 'aligncenter post-image' ) ) );
        printf( '<a href="%s" title="%s">%s</a>', get_permalink(), the_title_attribute( 'echo=0' ), $img );
    }
}

// Edit the read more link text
add_filter('get_the_content_more_link', 'ss_custom_read_more_link');
add_filter('the_content_more_link', 'ss_custom_read_more_link');
function ss_custom_read_more_link($txt) {

    if (is_post_type_archive('plans') || is_tax('rooms'))
        return $txt;

    return '<br /><a class="more-link" href="' . get_permalink() . '" rel="nofollow">[More Pictures&rarr;]</a>';
}


remove_action( 'genesis_before_content', 'genesis_do_subnav' );

//add_action('genesis_before_content', 'custom_genesis_do_nav'); 
//function custom_genesis_do_nav() { 
//    if (is_post_type_archive('plans') || is_tax('rooms')) {    
//
//        $nav = wp_nav_menu(array( 
//            'theme_location' => 'secondary', 
//            'menu' => 'Plan Secondary', 
//            'container' => '', 
//            'menu_class' => genesis_get_option('nav_superfish') ? 'nav superfish' : 'nav', 
//            'echo' => 0 
//        )); 
//        printf( '<div id="subnav"><div class="wrap">%s</div></div>', $nav ); 
//    }     
//} 

add_action('genesis_before_content', 'ss_add_photo_dropdown', 0);
function ss_add_photo_dropdown() {
 
    if (is_post_type_archive('plans') || is_tax('rooms')) { 
        if( function_exists('the_dropdown_taxonomy') ) :?> 
            <h4 class="use">How Many Rooms Do You Need? <?php the_dropdown_taxonomy('rooms'); ?></h4><?php
        endif;
    } else {
        if( function_exists('the_dropdown_taxonomy') ) :?> 
            <h4 class="use">Sort By Use / Facility:  <?php the_dropdown_taxonomy('use'); ?></h4><?php
        endif;			     
    }
 
    
}



genesis();