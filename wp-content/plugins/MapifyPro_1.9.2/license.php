<?php
// set_site_transient( 'update_plugins', null );
define('EDD_MAPIFYPRO_STORE_URL', 'http://mapifypro.com' );
define('EDD_MAPIFYPRO_ITEM_NAME', 'MapifyPro' );

if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
}

// retrieve our license key from the DB
$license_key = trim( get_option( 'edd_mapifypro_license_key' ) );

// setup the updater
$edd_updater = new EDD_SL_Plugin_Updater( EDD_MAPIFYPRO_STORE_URL, dirname(__FILE__) . '/mapifypro.php', array( 
	'version' 	=> MAPIFY_PRO_PLUGIN_VERSION,	// current version number
	'license' 	=> $license_key,				// license key (used get_option above to retrieve from DB)
	'item_name' => EDD_MAPIFYPRO_ITEM_NAME,		// name of this plugin
	'author' 	=> 'Josh Sears'					// author of this plugin
));

function edd_mapifypro_license_menu() {
	add_submenu_page( 'custom-mapping', 'License', 'License', 'manage_options', 'custom-mapping-license', 'edd_mapifypro_license_page' );
}
add_action('admin_menu', 'edd_mapifypro_license_menu', 11);

function edd_mapifypro_license_page() {
	$license 	= get_option( 'edd_mapifypro_license_key' );
	$status 	= get_option( 'edd_mapifypro_license_status' );
	?>
	<div class="wrap">
		<h2><?php _e('Plugin License Options'); ?></h2>
		<form method="post" action="options.php">
		
			<?php settings_fields('edd_mapifypro_license'); ?>
			
			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top">
							<?php _e('License Key'); ?>
						</th>
						<td>
							<input id="edd_mapifypro_license_key" name="edd_mapifypro_license_key" type="text" class="regular-text" value="<?php esc_attr_e( $license ); ?>" />
							<label class="description" for="edd_mapifypro_license_key"><?php _e('Enter your license key'); ?></label>
						</td>
					</tr>
					<?php if( false !== $license ) { ?>
						<tr valign="top">	
							<th scope="row" valign="top">
								<?php _e('Activate License'); ?>
							</th>
							<td>
								<?php if( $status !== false && $status == 'valid' ) { ?>
									<span style="color:green;"><?php _e('active'); ?></span>
									<?php wp_nonce_field( 'edd_mapifypro_nonce', 'edd_mapifypro_nonce' ); ?>
									<input type="submit" class="button-secondary" name="edd_license_deactivate" value="<?php _e('Deactivate License'); ?>"/>
								<?php } else {
									wp_nonce_field( 'edd_mapifypro_nonce', 'edd_mapifypro_nonce' ); ?>
									<input type="submit" class="button-secondary" name="edd_license_activate" value="<?php _e('Activate License'); ?>"/>
								<?php } ?>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>	
			<?php submit_button(); ?>
		
		</form>
	<?php
}

function edd_mapifypro_register_option() {
	// creates our settings in the options table
	register_setting('edd_mapifypro_license', 'edd_mapifypro_license_key', 'edd_sanitize_license' );
}
add_action('admin_init', 'edd_mapifypro_register_option');

function edd_sanitize_license( $new ) {
	$old = get_option( 'edd_mapifypro_license_key' );
	if( $old && $old != $new ) {
		delete_option( 'edd_mapifypro_license_status' ); // new license has been entered, so must reactivate
	}
	return $new;
}



/************************************
* this illustrates how to activate 
* a license key
*************************************/

function edd_mapifypro_activate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['edd_license_activate'] ) ) {

		// run a quick security check 
	 	if( ! check_admin_referer( 'edd_mapifypro_nonce', 'edd_mapifypro_nonce' ) ) 	
			return; // get out if we didn't click the Activate button

		// retrieve the license from the database
		$license = trim( get_option( 'edd_mapifypro_license_key' ) );
			

		// data to send in our API request
		$api_params = array( 
			'edd_action'=> 'activate_license', 
			'license' 	=> $license, 
			'item_name' => urlencode( EDD_MAPIFYPRO_ITEM_NAME ) // the name of our product in EDD
		);
		
		// Call the custom API.
		$response = wp_remote_get( add_query_arg( $api_params, EDD_MAPIFYPRO_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) )
			return false;

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );
		
		// $license_data->license will be either "active" or "inactive"

		update_option( 'edd_mapifypro_license_status', $license_data->license );

	}
}
add_action('admin_init', 'edd_mapifypro_activate_license');


/***********************************************
* Illustrates how to deactivate a license key.
* This will descrease the site count
***********************************************/

function edd_mapifypro_deactivate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['edd_license_deactivate'] ) ) {

		// run a quick security check 
	 	if( ! check_admin_referer( 'edd_mapifypro_nonce', 'edd_mapifypro_nonce' ) ) 	
			return; // get out if we didn't click the Activate button

		// retrieve the license from the database
		$license = trim( get_option( 'edd_mapifypro_license_key' ) );
			

		// data to send in our API request
		$api_params = array( 
			'edd_action'=> 'deactivate_license', 
			'license' 	=> $license, 
			'item_name' => urlencode( EDD_MAPIFYPRO_ITEM_NAME ) // the name of our product in EDD
		);
		
		// Call the custom API.
		$response = wp_remote_get( add_query_arg( $api_params, EDD_MAPIFYPRO_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) )
			return false;

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );
		delete_option( 'edd_mapifypro_license_status' );
		
		// $license_data->license will be either "deactivated" or "failed"
		// if( $license_data->license == 'deactivated' )
			// delete_option( 'edd_mapifypro_license_status' );

	}
}
add_action('admin_init', 'edd_mapifypro_deactivate_license');