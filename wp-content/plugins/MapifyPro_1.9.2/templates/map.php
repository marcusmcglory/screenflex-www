<?php
$mode = get_post_meta($map->ID, '_map_mode', true);
$zoom_level = get_post_meta($map->ID, '_map_google_center_zoom', true);
$zoom_level = $zoom_level ? $zoom_level : 3;
$zoom_enabled = get_post_meta($map->ID, '_map_enable_zoom', true) === 'yes';
$errors = array();

$image_small_size = array(0,0);
$image_big_size = array(0,0);

if ($mode == 'image') {
	$image_big = get_post_meta($map->ID, '_map_image_big', true);
	$status = get_post_meta($map->ID, '_map_tileset_status', true);
	$uploads_dir = wp_upload_dir();
	$tileset_url = $uploads_dir['baseurl'] . '/mpfy/' . $map->ID . '/';
	
	if (!$image_big) {
		$errors[] = 'There was a problem with loading your map images.';
	} elseif ($status != 'ready') {
		$errors[] = 'Your image has not been processed, yet.';
	}
}

$animate_tooltips = carbon_get_post_meta($map->ID, 'map_animate_tooltips');
$animate_tooltips = ($animate_tooltips) ? $animate_tooltips : 'yes';

$animate_pinpoints = carbon_get_post_meta($map->ID, 'map_animate_pinpoints');
$animate_pinpoints = ($animate_pinpoints) ? $animate_pinpoints : 'yes';

$pins = get_posts('post_type=map-location&posts_per_page=-1&meta_key=_map_location_map&meta_value=' . $map->ID);
foreach ($pins as $index => $p) {
	$p->animate_tooltips = (bool) ($animate_tooltips == 'yes');
	$p->animate_pinpoints = (bool) ($animate_pinpoints == 'yes');

	$p->image_coords = explode(',', get_post_meta($p->ID, '_map_location_image_location', true));
	$p->google_coords = explode(',', get_post_meta($p->ID, '_map_location_google_location', true));
	$p->pin_image = get_post_meta($p->ID, '_map_location_pin', true);
	if ($p->pin_image) {
		$p->pin_image = $p->pin_image;
	} else {
		$p->pin_image = get_post_meta($map->ID, '_map_pin', true);
	}

	$p->pin_image_real = get_file_real_url($p->pin_image);

	$p->pin_size = false;
	$p->pin_anchor = array(0,0);
	if ($p->pin_image) {
		$p->pin_size = @getimagesize(get_file_real_path($p->pin_image));
		if ($p->pin_size) {
			$p->pin_anchor = array(
				round($p->pin_size[0] / 2),
				$p->pin_size[1],
			);
		}
	}

	$p->pin_city = strtolower(esc_attr(get_post_meta($p->ID, '_map_location_city', true)));
	$p->pin_zip = strtolower(esc_attr(get_post_meta($p->ID, '_map_location_zip', true)));
	$p->data_tags = array();
	$tags = wp_get_object_terms($p->ID, 'location-tag');
	foreach ($tags as $t) {
		$p->data_tags[$t->term_id] = $t->term_id;
	}

	ob_start();
	include('tooltip.php');
	$p->tooltip_content = ob_get_clean();

	$p->popup_enabled = carbon_get_post_meta($p->ID, 'map_location_tooltip_enabled');
	$p->popup_enabled = ($p->popup_enabled) ? $p->popup_enabled : 'yes';
	$p->popup_enabled = (bool) ($p->popup_enabled == 'yes');

	$p->tooltip_close = carbon_get_post_meta($p->ID, 'map_location_tooltip_close');
	$p->tooltip_close = ($p->tooltip_close) ? $p->tooltip_close : 'auto';

	$pins[$index] = $p;
}
$background = get_post_meta($map->ID, '_map_background_color', true);
if (!$background) {
	$background = '';
}
$tooltip_background = get_post_meta($map->ID, '_map_tooltip_background_color', true);
if (!$tooltip_background) {
	$tooltip_background = '#000000';
}
$tooltip_background = mpfy_hex2rgb($tooltip_background);
$tooltip_background[] = 0.71;

$google_map_mode = get_post_meta($map->ID, '_map_google_mode', true);
if (!$google_map_mode) {
	$google_map_mode = 'ROADMAP';
}

$google_map_style = get_post_meta($map->ID, '_map_google_style', true);
if (!$google_map_style) {
	$google_map_style = 'default';
}
$google_map_style = mpfy_get_snazzymaps_js($google_map_style);

$momentum_percent = get_post_meta($map->ID, '_map_drag_momentum', true);

$map_tags = wp_get_object_terms($map->ID, 'location-tag', 'hide_empty=0');
$search_enabled = (bool) (get_post_meta($map->ID, '_map_enable_search', true) == 'yes');
$search_radius = max(1, intval(get_post_meta($map->ID, '_map_search_radius', true)));
$search_center = (bool) (get_post_meta($map->ID, '_map_search_center', true) == 'yes');
$filters_center = (bool) (get_post_meta($map->ID, '_map_filters_center', true) == 'yes');
$clustering_enabled = (bool) (get_post_meta($map->ID, '_map_enable_clustering', true) == 'yes');

$filters_enabled = get_post_meta($map->ID, '_map_enable_filters', true);
$filters_enabled = (bool) ($filters_enabled == 'yes');
if (!$filters_enabled) {
	$map_tags = array();
}

$main_location = get_post_meta($map->ID, '_map_main_location', true);
$main_location = $main_location ? get_post_meta($main_location, '_map_location_google_location', true) : '';
$main_location = $main_location ? $main_location : '-5,5';
$main_location = explode(',', $main_location);
$main_location = $main_location[0] . ',' . @$main_location[1];
$center = get_post_meta($map->ID, '_map_main_location', true);
$center = (stristr($center, ',')) ? $center : $main_location;

$routes = apply_filters('pretty_routes_load_routes', array(), $map->ID);
?>
<div id="mpfy-map-<?php echo $mpfy_instances; ?>" class="mpfy-fullwrap">
	<?php if ($errors) : ?>
		<p>
			<?php foreach ($errors as $e) : ?>
				<?php echo $e; ?><br />
			<?php endforeach; ?>
		</p>
	<?php else : ?>
		<div class="cl">&nbsp;</div>
		<?php if ($map_tags || $search_enabled) : ?>
			<div class="mpfy-controls <?php echo ($map_tags && $search_enabled) ? 'mpfy-controls-all' : ''; ?>">
				<div class="mpfy-controls-inner">
					<div class="cl">&nbsp;</div>
					<?php if ($map_tags) : ?>
						<label>Filter Results by</label>
						<div class="select">
							<span class="select-value"></span>
							<select name="mpfy_tag" class="mpfy_tag_select">
								<option value="0">All</option>
								<?php foreach ($map_tags as $t) : ?>
									<option value="<?php echo $t->term_id; ?>"><?php echo $t->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					<?php endif; ?>

					<?php if ($search_enabled) : ?>
						<form class="mpfy-search-form" method="post" action="">
							<div class="mpfy-search-wrap">
								<input type="text" name="mpfy_search" class="mpfy_search" value="" placeholder="Enter city or zip code" />
								<a href="#" class="mpfy-clear-search">&nbsp;</a>
								<input type="submit" name="" value="Search" class="mpfy_search_button" />
							</div>
						</form>
					<?php endif; ?>
					<div class="cl">&nbsp;</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="mpfy-mode-<?php echo $mode ?> <?php echo ($map_tags || $search_enabled) ? 'with-controls' : ''; ?>">
			<div style="display: none;">
				<?php foreach ($pins as $p) : ?>
					<?php if (!$p->popup_enabled) { continue; } ?>
					<a href="<?php echo add_query_arg('mpfy-ajax', '1', get_permalink($p->ID)); ?>" data-id="<?php echo $p->ID; ?>" class="mpfy-pin mpfy-pin-id-<?php echo $p->ID; ?>">&nbsp;</a>
				<?php endforeach; ?>
			</div>
			<div id="custom-mapping-google-map-<?php echo $mpfy_instances; ?>" style="height: <?php echo $height; ?>px; overflow: hidden;"></div>
		</div>
	<?php endif; ?>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
	var map_instance = jQuery.MapifyPro.Instance.create(<?php echo $mpfy_instances; ?>);
	map_instance.routes = <?php echo json_encode($routes); ?>;
	map_instance.tooltip_background = <?php echo json_encode($tooltip_background); ?>;
	
	var center = <?php echo json_encode($center); ?>;
	var zoom = {
		'zoom': <?php echo $zoom_level; ?>,
		'enabled': <?php echo ($zoom_enabled) ? 'true' : 'false'; ?>
	};
	var search_radius = <?php echo $search_radius; ?>;
	var settings = {
		'mapTypeId': <?php echo json_encode($google_map_mode); ?>,
		'map_mode': <?php echo json_encode($mode); ?>,
		'search_center': <?php echo json_encode($search_center); ?>,
		'filters_center': <?php echo json_encode($filters_center); ?>,
		'style': <?php echo $google_map_style; ?>,
		'clustering_enabled': <?php echo json_encode($clustering_enabled); ?>,
		'background': <?php echo json_encode($background); ?>
	};
	if (settings.map_mode == 'image') {
		settings['image_source'] = <?php echo json_encode($tileset_url); ?>;
	}
	var inst = new jQuery.MapifyPro.Google(center, zoom, search_radius, <?php echo json_encode($pins); ?>, map_instance, settings);

	jQuery(map_instance.container).find('a.mpfy-pin[href=#]').click(function(e) {
		e.preventDefault();
		return false;
	});

	var is_phone = jQuery.MapifyPro.isPhone();
	var is_tablet = jQuery.MapifyPro.isTablet();
	var is_touch_device = jQuery.MapifyPro.isTouchDevice();

	if (is_phone) {
		jQuery(map_instance.container).find('a.mpfy-pin').not('.mpfy-pin[href=#]').bind('click', function(e) {
			e.preventDefault();
			
			jQuery('#fancybox-loading').show();
			jQuery.get(jQuery(this).attr('href'), {}, function(response) {
				jQuery('body').addClass('mpfy-popup-open');
				jQuery('#fancybox-loading').hide();

				jQuery('.mpfy-popup-cnt').html(response);
				jQuery('.mpfy-popup').show();

				jQuery('.mpfy-pin-popup .gallery .list li a').click(function() {
					jQuery('.mpfy-pin-popup .gallery .list li a').removeClass('active');
					jQuery(this).addClass('active');

					var index = jQuery('.mpfy-pin-popup .gallery .list li a').index(this);
					jQuery('.mpfy-pin-popup .gallery .holder .item').hide();
					jQuery('.mpfy-pin-popup .gallery .holder .item:eq(' + index.toString() + ')').show();
					jQuery('.mpfy-pin-popup .caption').show().html(jQuery(this).next().html());
					if (!jQuery(this).next().html()) {
						jQuery('.mpfy-pin-popup .caption').hide();
					}
					return false;
				});

				jQuery('.mpfy-pin-popup .gallery .list li a:first').click();

				if (jQuery('.popup-slider .list li').length > 1) {
					jQuery('.popup-slider .list').jcarousel({
				        scroll: 1
				    });
				}

				var gallery_holder = jQuery('.mpfy-popup .gallery').get(0);
				var hammertime = Hammer(gallery_holder, {
			        drag: false,
			        prevent_mouseevents: true,
			        transform: false,
			        swipe_velocity: 0.1
			    });
				hammertime.on("swipeleft", function(e) {
					var target = jQuery('.mpfy-pin-popup .gallery .list li a.active').parent().next().find('a:first');
					if (target.length) {
						target.click();
						var index = jQuery('.mpfy-pin-popup .gallery .list li a').index(target);
						jQuery('.popup-slider .list').jcarousel('scroll', index + 1);
					}
				    e.preventDefault();
				});
				hammertime.on("swiperight", function(e) {
					var target = jQuery('.mpfy-pin-popup .gallery .list li a.active').parent().prev().find('a:first');
					if (target.length) {
						target.click();
						var index = jQuery('.mpfy-pin-popup .gallery .list li a').index(target);
						jQuery('.popup-slider .list').jcarousel('scroll', index + 1);
					}
				    e.preventDefault();
				});
			});

			return false;
		});
		jQuery('.mpfy-popup .close').click(function() {
			jQuery('.mpfy-popup').hide();
			jQuery('body').removeClass('mpfy-popup-open');
			jQuery.MapifyPro.hideTooltips();
			return false;
		});
	} else {
		jQuery(map_instance.container).find('a.mpfy-pin:not([href=#])').fancybox({
			type: 'ajax',
			autoDimensions: false,
			width: 960,
			height: 595,
			padding: 0,
			scrolling: 'auto',
			overlayColor: "#000",
			overlayOpacity: 0.85,
			beforeLoad: function() {
				jQuery('#fancybox-wrap').addClass('mpfy-override-fancybox');
			},
			afterShow: function(){
				jQuery('body').addClass('mpfy-popup-open');
				jQuery('.mpfy-pin-popup .gallery .list li a').click(function() {
					var index = jQuery('.mpfy-pin-popup .gallery .list li a').index(this);
					jQuery('.mpfy-pin-popup .gallery .holder .item').hide();
					jQuery('.mpfy-pin-popup .gallery .holder .item:eq(' + index.toString() + ')').show();
					jQuery('.mpfy-pin-popup .caption').show().html(jQuery(this).next().html());
					if (!jQuery(this).next().html()) {
						jQuery('.mpfy-pin-popup .caption').hide();
					}
					return false;
				});

				jQuery('.mpfy-pin-popup .gallery .list li a:first').click();

				if (jQuery('.popup-slider .list li').length > 7) {
					jQuery('.popup-slider .list').jcarousel({
				        scroll: 1
				    });
				}

				jQuery.fancybox.reposition();

				jQuery('.mpfy-share').appendTo('.fancybox-outer')
			},
			afterClose: function() {
				jQuery('body').removeClass('mpfy-popup-open');
				jQuery('#fancybox-wrap').removeClass('mpfy-override-fancybox');
			}
		});
		if (is_tablet) {
			jQuery('#fancybox-wrap').addClass('fancybox-tablet');
		}
	}

	<?php if (isset($_GET['mpfy-pin'])) : ?>
		var open_pin = <?php echo $_GET['mpfy-pin']; ?>;
		(function(instance) {
			
			setTimeout(function() {
				var a = jQuery(instance.container).find('a.mpfy-pin[data-id="' + open_pin + '"]');
				if (a.length) {
					for (var i = 0; i < instance.markers.length; i++) {
						var m = instance.markers[i];
						if (m._mpfy.pin_id == open_pin) {
							m.getMap().setCenter(m.getPosition());
							break;
						}
					}
					a.trigger('click');
				}
			}, 1);

		})(map_instance);
	<?php endif; ?>
});
</script>