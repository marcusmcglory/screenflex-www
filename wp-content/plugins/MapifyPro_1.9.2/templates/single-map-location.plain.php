<?php
if (!isset($_GET['mpfy-pin'])) {
	wp_redirect(add_query_arg('mpfy-pin', get_the_ID()));
	exit;
}
wp_enqueue_style('mpfy_css', plugins_url('assets/map.css', MAPIFY_PLUGIN_FILE));
wp_enqueue_script('jquery');
wp_enqueue_script('hammer', plugins_url('assets/js/hammer.min.js', MAPIFY_PLUGIN_FILE), array(), false, true);
wp_enqueue_script('jcarousel', plugins_url('assets/js/jcarousel/lib/jquery.jcarousel.min.js', MAPIFY_PLUGIN_FILE), array(), false, true);

$map_id = carbon_get_post_meta(get_the_ID(), 'map_location_map');
?>
<?php get_header('mapify'); ?>

<?php echo do_shortcode('[custom-mapping map_id="' . $map_id . '" height="400"]'); ?>
<?php /* ?>
<div class="mpfy-share-plain">
	<?php do_action('mpfy_share_links_before', get_the_ID()); ?>
	<?php if (in_array('twitter', $share)) : ?>
		<a href="http://www.twitter.com/share?url=<?php echo urlencode(get_permalink(get_the_ID())); ?>" target="_blank" class="mpfy-share mpfy-share-twitter" style="top: <?php echo 19 + 75 * $share_buttons; ?>px;">Share on Twitter</a>
		<?php $share_buttons ++; ?>
	<?php endif; ?>
	<?php if (in_array('facebook', $share)) : ?>
		<a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink(get_the_ID())); ?>" target="_blank" class="mpfy-share mpfy-share-facebook" style="top: <?php echo 19 + 75 * $share_buttons; ?>px;">Share on Facebook</a>
		<?php $share_buttons ++; ?>
	<?php endif; ?>
	<?php if (in_array('googleplus', $share)) : ?>
		<a href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink(get_the_ID())); ?>" target="_blank" class="mpfy-share mpfy-share-googleplus" style="top: <?php echo 19 + 75 * $share_buttons; ?>px;">Share on Google+</a>
		<?php $share_buttons ++; ?>
	<?php endif; ?>
	<?php do_action('mpfy_share_links_after', get_the_ID()); ?>
</div>
<div class="mpfy-pin-popup mpfy-plain-page" style="background: <?php echo $background; ?>;">
	<?php if ($video_embed_code || !empty($images)) : ?>
		<div class="side-a">
			<div class="gallery">
				<div class="holder">
					<?php if ($video_embed_code) : ?>
						<div class="item" style="display: none;">
							<?php echo mpfy_filter_video($video_embed_code, false, 640, 405); ?>
						</div>
					<?php endif; ?>
					<?php
					if (!empty($images)) {
						foreach ($images as $image) {
							$image = $image['image'];
							echo '<div class="item" style="display: none;">';
							echo '<img src="' . mpfy_get_thumb($image, 640, 405) . '" alt="" />';
							echo '</div>';
						}
					}
					?>
				</div>
				<div class="popup-slider" style="<?php echo ($media_count <= 1) ? 'display: none;' : ''; ?>">
					<ul class="list">
						<?php if ($video_embed_code) : ?>
							<li>
								<a href="#" class="plant_thumbnail video_thumbnail"><img src="<?php echo mpfy_get_thumb($video_thumb, 74, 74); ?>" alt="" /></a>
								<div class="description"><?php echo get_post_meta(get_the_ID(), '_map_location_video_description', true); ?></div>
							</li>
						<?php endif; ?>
						<?php
						if (!empty($images)) {
							foreach ($images as $i => $image) {
								echo '
								<li>
									<a href="#" class="plant_thumbnail">
										<img src="' . mpfy_get_thumb($image['image'], 74, 74) . '" alt="" />
									</a>
								</li>
								';
							}
						}
						?>
					</ul>
				</div>
				<div class="cl">&nbsp;</div>
			</div>
			<?php if ($video_embed_code) : ?>
				<?php if (!empty($images)) : ?>
					<div class="cl separator">&nbsp;</div>
				<?php endif; ?>
				<p class="caption" style="display: none;"></p>
			<?php endif; ?>
		</div>
		<div class="side-b">
	<?php else : ?>
		<div class="side-b side-full-width">
	<?php endif; ?>
		<div class="nfo">
			<?php do_action('mpfy_popup_content_before', get_the_ID()); ?>
			<h3><?php the_title(); ?></h3>
			<?php the_content(); ?>
			<?php do_action('mpfy_popup_content_after', get_the_ID()); ?>
		</div>
	</div>
	<div class="cl">&nbsp;</div>
</div>
<script type="text/javascript">
(function($, $window, $document){
	$document.ready(function(){
		jQuery('.mpfy-pin-popup .gallery .list li a').click(function() {
			jQuery('.mpfy-pin-popup .gallery .list li a').removeClass('active');
			jQuery(this).addClass('active');

			var index = jQuery('.mpfy-pin-popup .gallery .list li a').index(this);
			jQuery('.mpfy-pin-popup .gallery .holder .item').hide();
			jQuery('.mpfy-pin-popup .gallery .holder .item:eq(' + index.toString() + ')').show();
			jQuery('.mpfy-pin-popup .caption').show().html(jQuery(this).next().html());
			if (!jQuery(this).next().html()) {
				jQuery('.mpfy-pin-popup .caption').hide();
			}
			return false;
		});

		jQuery('.mpfy-pin-popup .gallery .list li a:first').click();

		if (jQuery('.popup-slider .list li').length > 1) {
			jQuery('.popup-slider .list').jcarousel({
		        scroll: 1
		    });
		}

		var gallery_holder = jQuery('.mpfy-popup .gallery').get(0);
		var hammertime = Hammer(gallery_holder, {
	        drag: false,
	        prevent_mouseevents: true,
	        transform: false,
	        swipe_velocity: 0.1
	    });
		hammertime.on("swipeleft", function(e) {
			var target = jQuery('.mpfy-pin-popup .gallery .list li a.active').parent().next().find('a:first');
			if (target.length) {
				target.click();
				var index = jQuery('.mpfy-pin-popup .gallery .list li a').index(target);
				jQuery('.popup-slider .list').jcarousel('scroll', index + 1);
			}
		    e.preventDefault();
		});
		hammertime.on("swiperight", function(e) {
			var target = jQuery('.mpfy-pin-popup .gallery .list li a.active').parent().prev().find('a:first');
			if (target.length) {
				target.click();
				var index = jQuery('.mpfy-pin-popup .gallery .list li a').index(target);
				jQuery('.popup-slider .list').jcarousel('scroll', index + 1);
			}
		    e.preventDefault();
		});
	});
})(jQuery, jQuery(window), jQuery(document));
</script>
<?php */ ?>

<?php get_footer('mapify'); ?>