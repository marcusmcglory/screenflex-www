<?php
do_action('mpfy_tooltip_content_before', $p->ID);

$addr = $p->pin_city . ' ' . $p->pin_zip;
$directions_url = 'http://www.google.com/maps/?saddr=&daddr=' . $p->google_coords[0] . ',' . $p->google_coords[1];
$text = get_post_meta($p->ID, '_map_location_tooltip', true);
$text = str_replace('[directions]', '<a href="' . $directions_url . '" target="_blank" class="mpfy-directions-button">Get Directions</a>', $text);
echo wpautop('<strong>' . $p->post_title . '</strong><br />' . $text);

do_action('mpfy_tooltip_content_after', $p->ID);