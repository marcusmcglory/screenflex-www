<?php
the_post();
$video_embed_code = get_post_meta(get_the_ID(), '_map_location_video_embed', true);
if (preg_match('/^http/', $video_embed_code)) {
	// dirty solution
	$video_embed_code = mpfy_create_embedcode($video_embed_code);
}
$video_thumb = mpfy_get_video_thumb($video_embed_code);

$images = carbon_get_post_meta(get_the_ID(), '_map_location_gallery_image', 'complex');

$trees_planted = get_post_meta(get_the_ID(), '_post_trees_planted', true);
$location = get_post_meta(get_the_ID(), '_post_location', true);
$trees_type = get_post_meta(get_the_ID(), '_post_trees_type', true);
$seasonal_information = get_post_meta(get_the_ID(), '_post_seasonal_info', true);
$post_description = get_post_meta(get_the_ID(), '_post_description', true);
$post_website = get_post_meta(get_the_ID(), '_post_website', true);

$background = get_post_meta(get_post_meta(get_the_ID(), '_map_location_map', true), '_map_popup_background_color', true);
$background = $background ? $background : '#FFFFFF';
$share = get_post_meta(get_the_ID(), '_map_location_share', true);
$share = $share ? $share : array();
$share_buttons = 0;

$media_count = $images;
$media_count[] = $video_embed_code;
$media_count = count(array_filter($media_count));

if (!isset($_GET['mpfy-ajax'])) {
	include('single-map-location.plain.php');
} else {
	include('single-map-location.ajax.php');
}