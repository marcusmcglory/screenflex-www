<?php
$class = '';
if (!$video_embed_code && $images) {
	$class = 'mpfy-pin-popup-only-images';
}
if (!$video_embed_code && !$images) {
	$class = 'mpfy-pin-popup-only-text';
}
if ($video_embed_code && !$images) {
	$class = 'mpfy-pin-popup-only-video';
}
?>
<?php do_action('mpfy_share_links_before', get_the_ID()); ?>
<?php if (in_array('twitter', $share)) : ?>
	<a href="http://www.twitter.com/share?url=<?php echo urlencode(add_query_arg('mpfy-pin', get_the_ID(), $_SERVER['HTTP_REFERER'])); ?>" target="_blank" class="mpfy-share mpfy-share-twitter" style="top: <?php echo 19 + 75 * $share_buttons; ?>px;">Share on Twitter</a>
	<?php $share_buttons ++; ?>
<?php endif; ?>
<?php if (in_array('facebook', $share)) : ?>
	<a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(add_query_arg('mpfy-pin', get_the_ID(), $_SERVER['HTTP_REFERER'])); ?>" target="_blank" class="mpfy-share mpfy-share-facebook" style="top: <?php echo 19 + 75 * $share_buttons; ?>px;">Share on Facebook</a>
	<?php $share_buttons ++; ?>
<?php endif; ?>
<?php if (in_array('googleplus', $share)) : ?>
	<a href="https://plus.google.com/share?url=<?php echo urlencode(add_query_arg('mpfy-pin', get_the_ID(), $_SERVER['HTTP_REFERER'])); ?>" target="_blank" class="mpfy-share mpfy-share-googleplus" style="top: <?php echo 19 + 75 * $share_buttons; ?>px;">Share on Google+</a>
	<?php $share_buttons ++; ?>
<?php endif; ?>
<?php do_action('mpfy_share_links_after', get_the_ID()); ?>
<div class="mpfy-pin-popup <?php echo $class; ?>" style="background: <?php echo $background; ?>;">
	<?php if ($video_embed_code || !empty($images)) : ?>
		<div class="side-a">
			<div class="gallery">
				<div class="holder">
					<?php if ($video_embed_code) : ?>
						<div class="item" style="display: none;">
							<?php echo mpfy_filter_video($video_embed_code, false, 640, 405); ?>
						</div>
					<?php endif; ?>
					<?php
					if (!empty($images)) {
						foreach ($images as $image) {
							$image = $image['image'];
							echo '<div class="item" style="display: none;">';
							echo '<img src="' . mpfy_get_thumb($image, 640, 405) . '" alt="" />';
							echo '</div>';
						}
					}
					?>
				</div>
				<div class="popup-slider" style="<?php echo ($media_count <= 1) ? 'display: none;' : ''; ?>">
					<ul class="list">
						<?php if ($video_embed_code) : ?>
							<li>
								<a href="#" class="plant_thumbnail video_thumbnail"><img src="<?php echo mpfy_get_thumb($video_thumb, 74, 74); ?>" alt="" /></a>
								<div class="description"><?php echo get_post_meta(get_the_ID(), '_map_location_video_description', true); ?></div>
							</li>
						<?php endif; ?>
						<?php
						if (!empty($images)) {
							foreach ($images as $i => $image) {
								echo '
								<li>
									<a href="#" class="plant_thumbnail">
										<img src="' . mpfy_get_thumb($image['image'], 74, 74) . '" alt="" />
									</a>
								</li>
								';
							}
						}
						?>
					</ul>
				</div>
				<div class="cl">&nbsp;</div>
			</div>
			<?php if ($video_embed_code) : ?>
				<?php if (!empty($images)) : ?>
					<div class="cl separator">&nbsp;</div>
				<?php endif; ?>
				<p class="caption" style="display: none;"></p>
			<?php endif; ?>
		</div>
		<div class="side-b">
	<?php else : ?>
		<div class="side-b side-full-width">
	<?php endif; ?>
		<div class="nfo">
			<?php do_action('mpfy_popup_content_before', get_the_ID()); ?>
			<h3><?php the_title(); ?></h3>
			<?php the_content(); ?>
			<?php do_action('mpfy_popup_content_after', get_the_ID()); ?>
		</div>
	</div>
	<div class="cl">&nbsp;</div>
</div>