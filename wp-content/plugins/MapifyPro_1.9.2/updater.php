<?php
$versions = array(
	'1.6'=>'mpfy_updater_1_6',
	'1.6.1'=>'', '1.6.2'=>'', '1.6.3'=>'', '1.6.4'=>'', '1.6.5'=>'', '1.6.6'=>'', '1.7.0'=>'', '1.7.1'=>'', '1.7.2'=>'', '1.7.3'=>'', '1.7.4'=>'', '1.7.5'=>'', '1.7.6'=>'', '1.7.7'=>'', '1.7.8'=>'', '1.7.9'=>'', '1.7.10'=>'', '1.7.11'=>'', '1.7.12'=>'', '1.7.13'=>'', '1.8.4'=>'', '1.8.5'=>'', '1.8.6'=>'', '1.8.7'=>'', '1.8.8'=>'', '1.8.9'=>'', '1.9.0'=>'', '1.9.1'=>'', '1.9.2'=>''// no updater
);

function mpfy_updater_1_6() {
	$maps = get_posts('post_type=map&posts_per_page=-1');
	foreach ($maps as $p) {
		$fields = array(
			'map_image_big',
			'map_image_small',
			'map_pin',
		);
		foreach ($fields as $f) {
			$value = get_post_meta($p->ID, '_' . $f, true);
			if ($value) {
				$value = mpfy_updater_1_6_ecf_url($value);
				update_post_meta($p->ID, '_' . $f, $value);
			}
		}
	}

	$map_locations = get_posts('post_type=map-location&posts_per_page=-1');
	foreach ($map_locations as $p) {
		$fields = array(
			'map_location_pin',
		);
		foreach ($fields as $f) {
			$value = get_post_meta($p->ID, '_' . $f, true);
			if ($value) {
				$value = mpfy_updater_1_6_ecf_url($value);
				update_post_meta($p->ID, '_' . $f, $value);
			}
		}

		$gallery_images = get_post_meta($p->ID, '_map_location_gallery_image', false);
		$i = 0;
		foreach ($gallery_images as $img) {
			if ($img) {
				update_post_meta($p->ID, '_map_location_gallery_images_-_' . $i, $img);
			}
			$i ++;
		}

		$map_location_google_location = get_post_meta($p->ID, '_map_location_google_location', true);
		if ($map_location_google_location) {
			$data = explode(',', $map_location_google_location);
			update_post_meta($p->ID, '_map_location_google_location', $data[0] . ',' . $data[1] );
			update_post_meta($p->ID, '_map_location_google_location_lat', $data[0] );
			update_post_meta($p->ID, '_map_location_google_location_lng', $data[1] );
			update_post_meta($p->ID, '_map_location_google_location_zoom', (isset($data[2]) ? $data[2] : '10') );
		}
	}
}

function mpfy_updater_1_6_ecf_url($url) {
	$upload_url_path = get_option('upload_url_path');
	if (!$upload_url_path) {
		$upload_url_path = get_option('home') . '/wp-content/uploads/';
	}
	return $upload_url_path . $url;
}