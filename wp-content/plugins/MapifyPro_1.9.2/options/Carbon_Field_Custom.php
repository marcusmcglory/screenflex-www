<?php
class Carbon_Field_Map_Tags extends Carbon_Field {
	static $attached_scripts = false;

	function admin_init() {
		if ( !self::$attached_scripts ) {
			self::$attached_scripts = true;

			add_action('admin_footer', array('Carbon_Field_Map_Tags', 'admin_enqueue_scripts'));
		}
	}

	function admin_enqueue_scripts() {
		?>
		<script type="text/javascript">
		(function($){
			if ($('.tags-slot').length > 0) {
				$('select[name="_map_location_map"]').change(function() {
					if ($(this).val() == '0') {
						$('.tags-slot').html('Please select a map from the above list.');
						return true;
					}
					$('.tags-slot').html('<em>Please wait ...</em>');

					$.get("<?php echo admin_url('admin-ajax.php'); ?>", {
						'action': 'mpfy_get_map_tags',
						'pid': $(this).val()
					}, function(response) {
						if (response.length) {
							$('.tags-slot').html('');
							for (var i = 0; i < response.length; i++) {
								var t = response[i];
								$('.tags-slot').append('<a href="#" class="map-tag-link" data-val="' + t.name + '">' + t.name + '</a>').append('&nbsp;');
							}
							$('.tags-slot').append('<p class="ecf-description">These are the existing tags associated with this map (added when you created the map). You can add these by clicking, or create more in the right sidebar under "map filter tags".</p>');
						} else {
							$('.tags-slot').html('The selected map has no tags.');
						}
					}, 'json');
					return true;
				}).change();

				$(document).on('click', '.map-tag-link', function() {
					$('input[name="newtag[location-tag]"]').val($(this).attr('data-val')).next().click();
					return false;
				});
			}

			$(document).ready(function(){
				$('#tagsdiv-location-tag .inside:first').append('<p>You may assign filters to this map, which will allow your users to filter location results via a dropdown. When you create specific locations within this map, you can then assign these filters to the location.</p>');
				var map_id = $('input[name="_map_id"]');
				map_id.hide().before('<span style="display: block; line-height: 28px;">' + "<?php echo @$_GET['post']; ?>" + '</span>');
				map_id.next().remove();
			});

			var styles = <?php echo mpfy_get_snazzymaps_js(); ?>;
			for (var k in styles) {
				styles[k] = eval(styles[k]);
			}

			$('select[name="_map_location_map"]').change(function(e) {
				var f = '_map_location_google_location';
				$('.carbon-field[data-name="' + f +'"]').after('<div id="map-loading"><em>Loading your map settings ...</em></div>');

				var promise = $.get("<?php echo admin_url('admin-ajax.php'); ?>", {
					'action': 'mpfy_get_map_settings',
					'pid': $(this).val()
				}, 'json');
				promise.done(function(response) {
					response = $.parseJSON(response);
					
					// reset
					var efo = $('.carbon-Map_With_Address[data-name="' + f +'"]').data('exposed_field_object');
					if (!efo) {
						return false;
					}
					var options = {
						'styles': []
					};
					efo.map.mapTypes.set('mpfy', new mpfy.mapType(response.image_source, response.background_color));

					if (response.mode == 'google_maps') {
						// mode
						efo.map.setMapTypeId(google.maps.MapTypeId[ response.terrain_mode ]);

						// style
						options['styles'] = styles[ response.style ];
					} else if (response.image_status == 'ready') {
						efo.map.setMapTypeId('mpfy');
					}
					
					efo.map.setOptions(options);
				});
				promise.always(function() {
					$('#map-loading').remove();
				});
				promise.error(function() {
					alert('There was a problem loading the selected map\'s settings.');
				});
			});
			$(document).ready(function(){
				$('select[name="_map_location_map"]').trigger('change');
			});
		})(jQuery);
		</script>
		<?php
	}

	function render() {
		echo '<div class="tags-slot"></div>';
	}

	function load() {
		// skip;
	}

	function save() {
		// skip;
	}

	function delete() {
		// skip;
	}
}

class Carbon_Field_Select_Location extends Carbon_Field_Select {
	static $attached_scripts = false;

	function admin_init() {
		if ( !self::$attached_scripts ) {
			self::$attached_scripts = true;

			add_action('admin_footer', array($this, 'admin_enqueue_scripts'));
		}
		parent::admin_init();
	}

	function admin_enqueue_scripts() {
		$raw = get_posts('post_type=map-location&posts_per_page=-1&orderby=title&order=asc');
		$locations = array();
		foreach ($raw as $r) {
			$coords = carbon_get_post_meta($r->ID, 'map_location_google_location');
			if ($coords) {
				$locations[$r->ID] = explode(',', $coords);
			}
		}
		?>
		<script type="text/javascript">
		(function($){
			var location_data = <?php echo json_encode($locations); ?>;
			$(document).ready(function(){
				$('select[name="<?php echo $this->name; ?>"]').change(function() {
					var efo = $('.carbon-Map[data-name="_map_google_center"]').data('exposed_field_object');
					var value = location_data[$(this).val()];
					efo.update_marker_position(new google.maps.LatLng(value[0], value[1]));
				});
			});
		})(jQuery);
		</script>
		<?php
	}
}

class Carbon_Field_Map_Mode extends Carbon_Field_Select {
	static $attached_scripts = false;

	function admin_init() {
		if ( !self::$attached_scripts ) {
			self::$attached_scripts = true;

			add_action('admin_footer', array($this, 'admin_enqueue_scripts'));
		}
		parent::admin_init();
	}

	function admin_enqueue_scripts() {
		$post_id = isset($_GET['post']) ? intval($_GET['post']) : 0;
		$uploads_dir = wp_upload_dir();
		$tileset_url = $uploads_dir['baseurl'] . '/mpfy/' . $post_id . '/';
		$image_big = get_post_meta($post_id, '_map_image_big', true);
		$status = get_post_meta($post_id, '_map_tileset_status', true);
		?>
		<script type="text/javascript">
		(function($){
			var image_status = <?php echo json_encode($status); ?>;
			var image_source = <?php echo json_encode($tileset_url); ?>;

			$('body').on('mpfy_image_mode_status_changed', function(e) {
				image_status = e._response.status;
				mpfy_update_map_display();
			});

			var styles = <?php echo mpfy_get_snazzymaps_js(); ?>;
			for (var k in styles) {
				styles[k] = eval(styles[k]);
			}

			function mpfy_update_map_display() {
				// reset
				var efo = $('.carbon-Map[data-name="_map_google_center"]').data('exposed_field_object');
				var options = {
					'styles': []
				};
				efo.map.mapTypes.set('mpfy', new mpfy.mapType(image_source, $('input[name="_map_background_color"]').val()));

				// mode
				var mode = $('select[name="<?php echo $this->name; ?>"]').val();

				if (mode == 'google_maps') {
					// mode
					efo.map.setMapTypeId(google.maps.MapTypeId[$('select[name="_map_google_mode"]').val()]);

					// style
					options['styles'] = styles[$('select[name="_map_google_style"]').val()];
				} else if (image_status == 'ready') {
					efo.map.setMapTypeId('mpfy');
				}
				
				efo.map.setOptions(options);
			}

			$(document).ready(function(){
				$('select[name="<?php echo $this->name; ?>"], select[name="_map_google_mode"], select[name="_map_google_style"]').change(function() {
					mpfy_update_map_display();
				});
				setTimeout(function() {
					$('select[name="<?php echo $this->name; ?>"]').change();
				},1);
			});
		})(jQuery);
		</script>
		<?php
	}
}

class Carbon_Field_Image_Pin extends Carbon_Field_Image {
	function admin_init() {
		add_action('admin_footer', array($this, 'admin_enqueue_scripts'));
		parent::admin_init();
	}

	function attach_to_map($map_field_name) {
		$this->map_field_name = $map_field_name;
		return $this;
	}

	function admin_enqueue_scripts() {
		?>
		<script type="text/javascript">
		(function($){
			$(document).ready(function(){
				$('input[name="<?php echo $this->name; ?>"]').change(function() {
					var efo = $('.carbon-Map[data-name="_<?php echo $this->map_field_name; ?>"], .carbon-Map_With_Address[data-name="_<?php echo $this->map_field_name; ?>"]').data('exposed_field_object');
					if (!efo) {
						return true;
					}
					for (var i = 0; i < efo.map.crb.markers.length; i++) {
						var m = efo.map.crb.markers[i];
						m.setOptions({
      						icon: $(this).val()
						});
					}
				});
				setTimeout(function() {
					$('input[name="<?php echo $this->name; ?>"]').change();
				},1);
			});
		})(jQuery);
		</script>
		<?php
	}
}

class Carbon_Field_Tileset extends Carbon_Field_Text {
	function render() {
		$post_id = isset($_GET['post']) ? intval($_GET['post']) : 0;
		if (!$post_id) {
			return false;
		}
		?>
		<em class="map_tileset_status">Checking status ...</em>
		<img src="<?php echo plugins_url('assets/images/tileset-loader.gif', MAPIFY_PLUGIN_FILE); ?>" alt="" style="position: relative; top: 1px;" />
		<script type="text/javascript">
		(function($, $window, $document){
			function _check_tileset_status() {
				$.get(<?php echo json_encode(admin_url('admin-ajax.php')); ?>, {
					'action': 'mpfy_ajax_tileset_status'
					, 'id': <?php echo json_encode($post_id); ?>
				}, function(response) {
					$('.map_tileset_status').html(response.message);
					if (response.status != 'processing') {
						$('.map_tileset_status').next().hide();
						clearInterval(_tileset_interval);
						$('body').trigger({
							type: "mpfy_image_mode_status_changed",
							_response: response
						});
					} else {
						$('.map_tileset_status').next().show();
					}
				}, 'json');
			}

			$document.ready(function(){
				_check_tileset_status();
				_tileset_interval = setInterval(_check_tileset_status, 5*1000);
			});
		})(jQuery, jQuery(window), jQuery(document));
		</script>
		<?php
	}
}