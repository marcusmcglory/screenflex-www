<?php
$raw = get_posts('post_type=map&posts_per_page=-1&orderby=title&order=asc');
$maps = array(0=>'Select One');
foreach ($raw as $r) {
	$maps[$r->ID] = $r->post_title;
}

Carbon_Container::factory('custom_fields', 'Map Location Options')
	->show_on_post_type('map-location')
	->add_fields(array(
		Carbon_Field::factory('select', 'map_location_map', 'Map')
			->add_options($maps)
			->help_text('Choose the map where you would like to add this location. Tags associated with the selected maps will appear below.'),
		Carbon_Field::factory('map_tags', 'map_location_tags', 'Map Tags'),

		Carbon_Field::factory('select', 'map_location_tooltip_enabled', 'Enable Popup')
			->add_options(array( 'yes' => 'Yes', 'no' => 'No' )),
		Carbon_Field::factory('select', 'map_location_tooltip_close', 'Close Tooltip')
			->add_options(array( 'auto' => 'Automatically', 'manual' => 'Manually' ))
			->help_text('<strong>Automatically:</strong> This will cause the tooltip to automatically hide when the user hovers off the location.<br /><strong>Manually:</strong> Good if you want to add links to the tooltip.'),

		Carbon_Field::factory('textarea', 'map_location_tooltip', 'Tooltip')
			->help_text('This is the brief text that appears in the tooltip when a user hovers over this location. You may use basic html text formatting, such as &lt;br&gt;, &lt;i&gt;, etc.<br />You can use the [directions] shortcode to display a directions link for Google Maps mode.'),
		Carbon_Field::factory('image_pin', 'map_location_pin', 'Pin Image')
			->attach_to_map('map_location_google_location')
			->help_text('You may select a custom pin image to be used for this location only. Leave blank to use the dafault pin.'),

		Carbon_Field::factory('separator', 'map_location_separator_2', 'Gallery Settings'),	
		Carbon_Field::factory('textarea', 'map_location_video_embed', 'Video Embed Code')
			->help_text('Use Vimeo or YouTube iframe video embed codes to include a video in your location’s pop-up gallery. If left blank, the gallery will revert to an image gallery only.'),
		Carbon_Field::factory('textarea', 'map_location_video_description', 'Video Description')
			->help_text('If you are including a video, you can enter a description here that will appear beneath the video in the gallery.'),
		Carbon_Field::factory('complex', 'map_location_gallery_images', 'Gallery Images')
			->add_fields(array(
				Carbon_Field::factory('image', 'image', 'Image'),
			)),
		Carbon_Field::factory('set', 'map_location_share', 'Share Services')
			->add_options(array(
				'twitter'=>'Twitter',
				'facebook'=>'Facebook',
				'googleplus'=>'Google+',
			)),
		
		Carbon_Field::factory('map_with_address', 'map_location_google_location', 'Location'),

		Carbon_Field::factory('separator', 'map_location_separator_4', 'Image Mode Settings'),
		Carbon_Field::factory('text', 'map_location_city', 'City')
			->help_text('Used for location search in image mode.'),
		Carbon_Field::factory('text', 'map_location_zip', 'Zip')
			->help_text('Used for location search in image mode.'),
	));

$raw = get_posts('post_type=map-location&posts_per_page=-1&orderby=title&order=asc');
$locations = array('0'=>'Select One');
foreach ($raw as $r) {
	$coords = carbon_get_post_meta($r->ID, 'map_location_google_location');

	if ( $coords ) {
		$locations[$r->ID] = $r->post_title;
	} else {
		// Dynamically update coordinates meta field
		$lat = carbon_get_post_meta($r->ID, 'map_location_google_location_lat');
		$lng = carbon_get_post_meta($r->ID, 'map_location_google_location_lng');

		$coords = array( $lat, $lng );
		$coords = array_filter( $coords );

		if ( ! empty( $coords ) ) {
			$coords = implode( ',', $coords );
			update_post_meta( $r->ID, '_map_location_google_location', $coords );

			$locations[$r->ID] = $r->post_title;
		}
	}
}


Carbon_Container::factory('custom_fields', 'Map Options')
	->show_on_post_type('map')
	->add_fields(array(
		Carbon_Field::factory('text', 'map_id', 'Map ID'),
		Carbon_Field::factory('map_mode', 'map_mode', 'Mode')
			->add_options(array( 'image' => 'Image', 'google_maps' => 'Google Maps' )),
		Carbon_Field::factory('select', 'map_enable_search', 'Enable Search')
			->add_options(array( 'no' => 'No', 'yes' => 'Yes' )),
		Carbon_Field::factory('select', 'map_enable_filters', 'Enable Filters')
			->add_options(array( 'no' => 'No', 'yes' => 'Yes' )),
		Carbon_Field::factory('select', 'map_enable_clustering', 'Enable Clustering')
			->add_options(array( 'no' => 'No', 'yes' => 'Yes' )),
		Carbon_Field::factory('image_pin', 'map_pin', 'Default Pin Image')
			->attach_to_map('map_google_center')
			->help_text('This image will serve as a default pinpoint for your map. PNG file with transparent background is suggested (supports jpg, gif, png).'),
		Carbon_Field::factory('color', 'map_tooltip_background_color', 'Tooltip Background Color'),
		Carbon_Field::factory('color', 'map_popup_background_color', 'Popup Background Color'),
		Carbon_Field::factory('color', 'map_background_color', 'Map Background Color'),
		Carbon_Field::factory('select', 'map_enable_zoom', 'Enable Zoom')
			->add_options(array( 'yes' => 'Yes', 'no' => 'No' )),
		Carbon_Field::factory('select', 'map_animate_tooltips', 'Animated Tooltips?')
			->add_options(array( 'yes' => 'Yes', 'no' => 'No' ))
			->help_text('Add a subtle animation to the tooltip that appears when hovering on a location.'),
		Carbon_Field::factory('select', 'map_animate_pinpoints', 'Animated Pinpoints?')
			->add_options(array( 'yes' => 'Yes', 'no' => 'No' ))
			->help_text('Add a subtle animation to the pinpoints as they populate the map.'),
		Carbon_Field::factory('select', 'map_search_center', 'Center Map on Search Results')
			->add_options(array( 'no' => 'No', 'yes' => 'Yes' )),
		Carbon_Field::factory('select', 'map_filters_center', 'Center Map on Filters')
			->add_options(array( 'no' => 'No', 'yes' => 'Yes' )),

		Carbon_Field::factory('separator', 'map_separator_2', 'Image Mode Settings'),
		Carbon_Field::factory('image', 'map_image_big', 'Map Image')
			->help_text('Note: you must update your map and the image will be processed to support multiple zoom levels.'),
		Carbon_Field::factory('tileset', 'map_tileset', 'Status'),

		Carbon_Field::factory('separator', 'map_separator_3', 'Google Maps Mode Settings'),
		Carbon_Field::factory('select', 'map_google_mode', 'Mode')
			->add_options(array( 'ROADMAP' => 'Road', 'SATELLITE' => 'Satellite', 'HYBRID' => 'Hybrid', 'TERRAIN' => 'Terrain' )),
		Carbon_Field::factory('select', 'map_google_style', 'Style')
			->add_options(mpfy_get_snazzymaps())
			->help_text('Add a unique style to your Google Maps (as you can preview below).<br />Note: This will override "Mode" settings.'),
		Carbon_Field::factory('text', 'map_search_radius', 'Search Radius')
			->set_default_value(5)
			->help_text('In miles.'),
		Carbon_Field::factory('select_location', 'map_main_location', 'Main Location')
			->add_options($locations),
		Carbon_Field::factory('map', 'map_google_center', 'Initial Zoom and Style Preview')
			->help_text('This will be the initial zoom level of the map in Google Maps mode.'),
	));