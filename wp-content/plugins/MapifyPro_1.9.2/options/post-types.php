<?php  
register_post_type('map-location', array(
	'labels' => array(
		'name'	 => 'Map Locations',
		'singular_name' => 'Map Location',
		'add_new' => __( 'Add New' ),
		'add_new_item' => __( 'Add new Map Location' ),
		'view_item' => 'View Map Location',
		'edit_item' => 'Edit Map Location',
	    'new_item' => __('New Map Location'),
	    'view_item' => __('View Map Location'),
	    'search_items' => __('Search Map Locations'),
	    'not_found' =>  __('No Map Locations found'),
	    'not_found_in_trash' => __('No Map Locations found in Trash'),
	),
	'public' => true,
	'exclude_from_search' => true,
	'show_ui' => true,
	'capability_type' => 'post',
	'hierarchical' => false,
	'_edit_link' =>  'post.php?post=%d',
	'rewrite' => array(
		"slug" => "map-location",
		"with_front" => false,
	),
	'query_var' => true,
	'supports' => array('title', 'editor'),
	'show_in_menu' => 'edit.php?post_type=map',
	'has_archive' => 'map-locations',
));

register_post_type('map', array(
	'labels' => array(
		'name'	 => 'Maps',
		'singular_name' => 'Map',
		'add_new' => __( 'Add New' ),
		'add_new_item' => __( 'Add new Map' ),
		'view_item' => 'View Map',
		'edit_item' => 'Edit Map',
	    'new_item' => __('New Map'),
	    'view_item' => __('View Map'),
	    'search_items' => __('Search Maps'),
	    'not_found' =>  __('No Maps found'),
	    'not_found_in_trash' => __('No Maps found in Trash'),

	    'menu_name' => 'MapifyPro',
	),
	'public' => false,
	'exclude_from_search' => true,
	'show_ui' => true,
	'capability_type' => 'post',
	'hierarchical' => true,
	'_edit_link' =>  'post.php?post=%d',
	'rewrite' => false,
	'query_var' => true,
	'supports' => array('title'),
));

register_taxonomy('location-tag', array('map', 'map-location'), array(
	'hierarchical'            => false,
	'labels'                  => array(
		'name'                         => _x( 'Map Filter Tags', 'taxonomy general name' ),
		'singular_name'                => _x( 'Location Tag', 'taxonomy singular name' ),
		'search_items'                 => __( 'Search Location Tags' ),
		'popular_items'                => __( 'Popular Location Tags' ),
		'all_items'                    => __( 'All Location Tags' ),
		'parent_item'                  => null,
		'parent_item_colon'            => null,
		'edit_item'                    => __( 'Edit Location Tag' ), 
		'update_item'                  => __( 'Update Location Tag' ),
		'add_new_item'                 => __( 'Add New Location Tag' ),
		'new_item_name'                => __( 'New Location Tag Name' ),
		'separate_items_with_commas'   => __( 'Separate Location Tags with commas' ),
		'add_or_remove_items'          => __( 'Add or remove Location Tags' ),
		'choose_from_most_used'        => __( 'Choose from the most used Location Tags' ),
		'not_found'                    => __( 'No Location Tags found.' ),
		'menu_name'                    => __( 'Location Tags' )
	),
	'show_ui'                 => true,
	'show_admin_column'       => true,
	'update_count_callback'   => '_update_post_term_count',
	'query_var'               => true,
	'rewrite'                 => array( 'slug' => 'Location Tag' )
));