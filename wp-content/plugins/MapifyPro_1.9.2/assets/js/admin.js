(function($, $window, $document){
	window.mpfy = {};
	mpfy.mapType = function(image_source, background_color) {
		this.mpfy = {
    		'image_source': image_source,
    		'background_color': background_color
    	}
	}
    mpfy.mapType.prototype.name = 'mpfy';
    mpfy.mapType.prototype.tileSize = new google.maps.Size(256, 256);
    mpfy.mapType.prototype.minZoom = 0;
    mpfy.mapType.prototype.maxZoom = 4;
    mpfy.mapType.prototype.releaseTile = function(node) {
    	$(node).remove();
    }
    mpfy.mapType.prototype.getTile = function (coord, zoom, ownerDocument) {
        var tilesCount = Math.pow(2, zoom);

        if (coord.x >= tilesCount || coord.x < 0 || coord.y >= tilesCount || coord.y < 0) {
            var div = ownerDocument.createElement('div');
            div.style.width = this.tileSize.width + 'px';
            div.style.height = this.tileSize.height + 'px';
            div.style.backgroundColor = this.mpfy.background_color;
            return div;
        }

        var img = ownerDocument.createElement('img');
        img.width = this.tileSize.width;
        img.height = this.tileSize.height;
        img.style.backgroundColor = this.mpfy.background_color;
        img.src = this.mpfy.image_source + 'z' + zoom + '-tile_' + coord.y + '_' + coord.x + '.png';

        return img;
    }

})(jQuery, jQuery(window), jQuery(document));