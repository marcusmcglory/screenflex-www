(function($){

jQuery.MapifyPro = {
	normalizeTouch: function(evt) {
		if (typeof evt.pageX == 'undefined' && typeof evt.originalEvent.touches[0] != 'undefined') {
			evt.pageX = evt.originalEvent.touches[0].pageX;
			evt.pageY = evt.originalEvent.touches[0].pageY;
		}
		return evt;
	},
	isPhone: function() {
		var phone_dimension = Math.min(jQuery(window).width(), jQuery(window).height());
		return (phone_dimension <= 360);
	},
	isTablet: function() {
		return is_tablet = (jQuery(window).width() <= 1050);
	},
	isTouchDevice: function() {
		return (jQuery.MapifyPro.isPhone() || jQuery.MapifyPro.isTablet());
	}
};

jQuery.MapifyPro.Instance = {
	create: function(instance_start) {
		if (typeof window.mpfy_instances == 'undefined') {
			window.mpfy_instances = {};
			window.mpfy_instances_count = instance_start - 1;
		}
		window.mpfy_instances_count ++;

		window.mpfy_instances[window.mpfy_instances_count] = {
			'instance_id': window.mpfy_instances_count,
			'container': '#mpfy-map-' + window.mpfy_instances_count
		};

		window.mpfy_current_instance = window.mpfy_instances[window.mpfy_instances_count];

		var inst = window.mpfy_instances[window.mpfy_instances_count];
		$(inst.container).find('.mpfy-clear-search').click(function() {
			$(inst.container).find('input[name="mpfy_search"]').val('').change();
			$(inst.container).find('.mpfy-search-form').submit();
			return false;
		});
		$(inst.container).find('input[name="mpfy_search"]').bind('keyup change', function() {
			if ($(this).val()) {
				$(inst.container).find('.mpfy-clear-search').show();
			} else {
				$(inst.container).find('.mpfy-clear-search').hide();
			}
			return true;
		});

		var controls = $(inst.container).find('.mpfy-controls');
		var breaking_width = 500;
		if (controls.width() <= breaking_width) {
			controls.addClass('mpfy-controls-mobile');
		}

		return window.mpfy_current_instance;
	}
};

jQuery.MapifyPro.Google = function(center, zoom, search_radius, pins, map_instance, settings) {
	this.settings = settings;
	map_instance.mpfy_mode = 'google';
	map_instance.mouse = {
		x: 0,
		y: 0
	};
	map_instance.geocoder = new google.maps.Geocoder();

	$(document).mousemove(function(evt){
		map_instance.mouse.x = evt.pageX;
		map_instance.mouse.y = evt.pageY;
	});

	center = center.split(',');
	center = new google.maps.LatLng(center[0], center[1]);
	map_instance.center = center;
	var opts = {
        zoom: zoom.zoom,
        center: center,
        mapTypeId: google.maps.MapTypeId[settings.mapTypeId],
        panControl: true,
        mapTypeControl: true,
        scaleControl: true,
        streetViewControl: false,
        zoomControl: zoom.enabled,
        // disableDoubleClickZoom: !zoom.enabled,
        disableDoubleClickZoom: true,
        scrollwheel: zoom.enabled,
        overviewMapControl: true,
        backgroundColor: settings.background
    };
    if (settings.style.length > 0) {
    	opts.styles = settings.style;
    }

    var mpfy_map = new google.maps.Map(document.getElementById("custom-mapping-google-map-" + map_instance.instance_id), opts);
    var overlay = new google.maps.OverlayView();
	overlay.draw = function() {};
	overlay.setMap(mpfy_map);
	mpfy_map.custom_overlay = overlay;
    map_instance.map = mpfy_map;
    this.map = mpfy_map;

    if (settings.map_mode == 'image') {
    	this.set_image_source(settings.image_source);
    	mpfy_map.setOptions({
    		mapTypeControl: false,
    		overviewMapControl: false,
    		panControl: false,
    		scaleControl: false,
    		streetViewControl: false,
    		zoomControl: false
    	});
    }

    map_instance.markers = [];
    for (var i = 0; i < pins.length; i++) {
    	var p = pins[i];
    	opts = {
            position: new google.maps.LatLng(p.google_coords[0], p.google_coords[1])
            , draggable: false
            , map: mpfy_map
        }
        if (p.pin_size) {
        	opts.icon = p.pin_image_real;
        }
        if (p.animate_pinpoints) {
            opts['animation'] = google.maps.Animation.DROP;
        }
		
    	var marker = new google.maps.Marker(opts);
    	marker._mpfy = {};
    	marker._mpfy.marker = marker;
    	marker._mpfy.pin_id = p.ID;
    	marker._mpfy.pin_anchor = p.pin_anchor;
        marker._mpfy.pin_tags = p.data_tags;
        marker._mpfy.pin_city = p.pin_city;
        marker._mpfy.pin_zip = p.pin_zip;
        marker._mpfy.animate = p.animate_pinpoints;
        if (marker.getIcon()) {
	        marker._mpfy.image_object = new Image();
	        marker._mpfy.image_object.src = marker.getIcon();
        }

    	var tooltip = new jQuery.MapifyPro.Tooltip({
			'rgba': map_instance.tooltip_background,
			'content': p.tooltip_content,
			'close_behavior': p.tooltip_close,
			'animate': p.animate_tooltips
		});
		tooltip.node().addClass('mpfy-tooltip');
		tooltip.setGoogleMap(mpfy_map);
		marker._mpfy.tooltip_object = tooltip;

        marker._mpfy.visibilityOptions = {};
        marker._mpfy.refreshVisibility = function() {
        	var visibility_final = true;
        	for (option in this.visibilityOptions) {
        		var visible = this.visibilityOptions[option];
        		if (!visible) {
        			visibility_final = false;
        			break;
        		}
        	}
        	this.marker.setVisible(visibility_final);
        	if (!settings.clustering_enabled && this.animate) {
        		this.marker.setAnimation(google.maps.Animation.DROP);
        	}
        }

        marker._mpfy.mapifyGetPosition = function() {
        	var proj = map_instance.map.custom_overlay.getProjection();
			var pos = this.getPosition();
			var p = proj.fromLatLngToContainerPixel(pos);

        	var anchor_x = 0;
        	var anchor_y = -10;
        	if (this.getIcon()) {
        		// anchor_x = Math.ceil(this.image_object.width / 2);
        		anchor_y -= this._mpfy.image_object.height;
        	} else {
        		anchor_y -= 40; // default pin size
        	}

        	var position = {
        		'x': $(map_instance.container).find('.mpfy-mode-google_maps:first').offset().left + p.x + anchor_x,
        		'y': $(map_instance.container).find('.mpfy-mode-google_maps:first').offset().top + p.y + anchor_y
        	}
        	return position;
        }

        google.maps.event.addListener(marker, 'click', function() {
        	var settings = this._mpfy;
	    	var tooltip = settings.tooltip_object;

	    	if (jQuery.MapifyPro.isTouchDevice() && tooltip) {
				if ( !tooltip.node().is(':visible') || this._mpfy._mouseover ) {
					google.maps.event.trigger(this, 'mouseover');
					this._mpfy._mouseover = false;
					return;
				}
	    	}
	    	var a = $(map_instance.container).find('.mpfy-pin-id-' + this._mpfy.pin_id + ':first');
	    	if (a.length > 0) {
   				a.trigger('click');
	    	}
        });

        google.maps.event.addListener(marker, 'mouseover', function(){
    		this._mpfy._mouseover = true;

    		var marker = this;
	    	var settings = this._mpfy;
	    	var tooltip = settings.tooltip_object;
	    	var proj = this.getMap().custom_overlay.getProjection();
			var pos = this.getPosition();
			var p = proj.fromLatLngToContainerPixel(pos);
			var map = this.getMap();

	    	var anchor_x = 0;
	    	var anchor_y = -10;
	    	if (this.getIcon()) {
	    		anchor_y -= settings.pin_anchor[1];
	    	} else {
	    		anchor_y -= 40; // default google pin height
	    	}
	    	if (tooltip) {
	    		var shwtltp = function() {
	    			var t = $(map_instance.container).offset().top + p.y - tooltip.node().height() + anchor_y;
		    		var l = $(map_instance.container).offset().left + p.x - Math.ceil(tooltip.node().width() / 2) + anchor_x;
			    	tooltip.node().trigger({
			    		'type': 'tooltip_mouseover',
			    		'settings': {
			    			'left': l,
			    			'top': t
			    		}
			    	});
	    		}

	    		if (jQuery.MapifyPro.isPhone()) {
		    		google.maps.event.addListenerOnce(map, 'center_changed', function() {
						setTimeout(function() {
							// recalculate
							pos = marker.getPosition();
							p = proj.fromLatLngToContainerPixel(pos);
							shwtltp();
						}, 100);
					});
	    			map.setCenter(this.getPosition());
	    		} else {
		    		shwtltp();
	    		}
	    	}
	    });
	    google.maps.event.addListener(marker, 'mouseout', function(){
	    	var settings = this._mpfy;
	    	var tooltip = settings.tooltip_object;
	    	if (tooltip) {
	    		tooltip.node().trigger({
		    		'type': 'tooltip_mouseout'
		    	});
	    	}
	    });

        map_instance.markers.push(marker);
    };
    if (settings.clustering_enabled) {
    	map_instance.clusterer = new MarkerClusterer(mpfy_map, map_instance.markers, {
    		/*styles: [
				{
					textColor: 'white',
					url: '',
					height: 50,
					width: 50
				}
			]*/
    	});
    }

    $(map_instance.container).find('select[name="mpfy_tag"]').change(function() {
		var tag_id = $(this).val();
		var tag_label = $(this).find('option[value="' + tag_id + '"]');
		$(this).closest('.select').find('.select-value').text(tag_label.text());
		var markers = window.mpfy_instances[map_instance.instance_id].markers;
		
		if (tag_id == 0) {
			for (var i = markers.length - 1; i >= 0; i--) {
				var m = markers[i];
				m._mpfy.visibilityOptions['tag'] = true;
				m._mpfy.refreshVisibility();
			};
		} else {
			for (var i = markers.length - 1; i >= 0; i--) {
				var m = markers[i];
				m._mpfy.visibilityOptions['tag'] = (typeof m._mpfy.pin_tags[tag_id.toString()] != 'undefined');
				m._mpfy.refreshVisibility();
			};  

			if (settings.filters_center) {
				var bounds = new google.maps.LatLngBounds();
				var visible_markers = [];
				for (var i = 0; i < markers.length; i++) {
					var m = markers[i];
					if (m.getVisible()) {
						bounds.extend(m.getPosition());
						visible_markers.push(m);
					}
				}
				if (visible_markers.length > 0) {
					var current_zoom = map_instance.map.getZoom();

					if (visible_markers.length == 1) {
						map_instance.map.panTo(visible_markers[0].getPosition());
					} else {
						google.maps.event.addListenerOnce(map_instance.map, 'center_changed', function() {
							setTimeout(function() {
								map_instance.map.setZoom(Math.min(map_instance.map.getZoom(), current_zoom));
							}, 100);
						});
						map_instance.map.fitBounds(bounds);
					}
				}
				if (settings.clustering_enabled) {
					map_instance.clusterer.clearMarkers();
					map_instance.clusterer.addMarkers(visible_markers);
			    }
			}
		}
	}).change();
	
	map_instance.afterSearch = function() {
		var markers = window.mpfy_instances[map_instance.instance_id].markers;
		var bounds = new google.maps.LatLngBounds();
		var visible_markers = [];
		for (var i = 0; i < markers.length; i++) {
			var m = markers[i];
			if (m.getVisible()) {
				bounds.extend(m.getPosition());
				visible_markers.push(m);
			}
		}

		if (settings.search_center) {
			if (visible_markers.length > 0) {
				google.maps.event.addListenerOnce(map_instance.map, 'center_changed', function() {
					setTimeout(function() {
						var z = map_instance.map.getZoom();
						z = Math.min(10, z);
						map_instance.map.setZoom(z);
					}, 100);
				});
				map_instance.map.fitBounds(bounds);
			} else {
				map_instance.map.setCenter(center);
			}
		}

		if (settings.clustering_enabled) {
			map_instance.clusterer.clearMarkers();
			map_instance.clusterer.addMarkers(visible_markers);
	    }
	}

	$(map_instance.container).find('.mpfy-search-form').submit(function() {
		var q = $(this).find('input[name="mpfy_search"]').val().toLowerCase();
		var markers = window.mpfy_instances[map_instance.instance_id].markers;
		var button = $(this).find('input[type="submit"]');

		if (!q) {
			for (var i = 0; i < markers.length; i++) {
				var m = markers[i];
				m._mpfy.visibilityOptions['search'] = true;
				m._mpfy.refreshVisibility();
				mpfy_map.setCenter(center);
				mpfy_map.setZoom(zoom.zoom);
			};

			if (settings.clustering_enabled) {
				map_instance.clusterer.clearMarkers();
				map_instance.clusterer.addMarkers(markers);
		    }
		} else {
			if (settings.map_mode == 'image') {
				for (var i = 0; i < markers.length; i++) {
					var m = markers[i];
					m._mpfy.visibilityOptions['search'] = false;
					if (m._mpfy.pin_city.indexOf(q) !== -1 || m._mpfy.pin_zip.indexOf(q) !== -1) {
						m._mpfy.visibilityOptions['search'] = true;
					}
					m._mpfy.refreshVisibility();
				}
				map_instance.afterSearch();
			} else {
				button.val('Searching').attr('disabled', 'disabled');
			    map_instance.geocoder.geocode( { 'address': q}, function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						var center = results[0].geometry.location;
						for (var i = 0; i < markers.length; i++) {
							var m = markers[i];
							m._mpfy.visibilityOptions['search'] = false;
							var ml = new google.maps.LatLng(m.position.lat(), m.position.lng());
							var distance = google.maps.geometry.spherical.computeDistanceBetween(center, ml);
							distance = distance * 0.000621371192; // to miles
							if (distance <= search_radius) {
								m._mpfy.visibilityOptions['search'] = true;
							}
							m._mpfy.refreshVisibility();
						}
						map_instance.afterSearch();
					} else {
						alert("Could not find the entered address. Please check your spelling and try again.");
					}
					button.val('Search').removeAttr('disabled');
			    });
			}
		}
		return false;
	});
	$('body').trigger({
		'type': 'mpfy_google_map_loaded',
		'map_instance': map_instance
	});
};

jQuery.MapifyPro.Google.prototype.set_image_source = function(image_source) {
	var stngs = this.settings;
	Mpfy = function() {}
    Mpfy.prototype.name = 'mpfy';
    Mpfy.prototype.tileSize = new google.maps.Size(256, 256);
    Mpfy.prototype.minZoom = 0;
    Mpfy.prototype.maxZoom = 4;
    Mpfy.prototype.releaseTile = function(node) {
    	jQuery(node).remove();
    }
    Mpfy.prototype.getTile = function (coord, zoom, ownerDocument) {
        var tilesCount = Math.pow(2, zoom);

        if (coord.x >= tilesCount || coord.x < 0 || coord.y >= tilesCount || coord.y < 0) {
            var div = ownerDocument.createElement('div');
            div.style.width = this.tileSize.width + 'px';
            div.style.height = this.tileSize.height + 'px';
            div.style.backgroundColor = stngs.background;
            return div;
        }

        var img = ownerDocument.createElement('img');
        img.width = this.tileSize.width;
        img.height = this.tileSize.height;
        img.style.backgroundColor = stngs.background;
        img.src = image_source + 'z' + zoom + '-tile_' + coord.y + '_' + coord.x + '.png';

        return img;
    }

    this.map.mapTypes.set('mpfy', new Mpfy());
    this.map.setMapTypeId('mpfy');
}

})(jQuery);