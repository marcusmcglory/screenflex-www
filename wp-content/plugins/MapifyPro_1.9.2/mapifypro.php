<?php
/*
Plugin Name: MapifyPro
Plugin URI: http://www.mapifypro.com
Description: 
Version: 1.9.2
Author: Josh Sears
Author URI: http://www.mapifypro.com
License: 
*/

define('MAPIFY_PLUGIN_FILE', __FILE__);
define('MAPIFY_PLUGIN_URL', plugins_url('MapifyPro'));
define('MAPIFY_PRO_PLUGIN_VERSION', '1.9.2');
function mpfy_get_version() {
	$current_version = get_option('mpfy_pro_plugin_version', '1.0');
	return $current_version;
}

function mpfy_activate() {
	add_action('wp', 'mpfy_activate_flush');
}
register_activation_hook(__FILE__, 'mpfy_activate');

function mpfy_activate_flush() {
	flush_rewrite_rules();
}

function mpfy_deactivate() {
	flush_rewrite_rules();
}
register_deactivation_hook(__FILE__, 'mpfy_deactivate');


include_once('license.php');

include_once('lib/video-functions.php');
include_once('lib/snazzymaps.php');
include_once('lib/tileset.php');

add_action('after_setup_theme', 'mpfy_load_carbon', 11);
function mpfy_load_carbon() {
	if (!defined('CARBON_PLUGIN_ROOT')) {
		define('CARBON_PLUGIN_URL', WP_PLUGIN_URL . '/' . basename(dirname(__FILE__)) . '/lib/carbon-fields');
		include_once('lib/carbon-fields/carbon-fields.php');
	}
}

add_action('carbon_register_fields', 'mpfy_attach_carbon');
function mpfy_attach_carbon() {
	include_once('options/Carbon_Field_Custom.php');
	include_once('options/custom-fields.php');
}

add_action('init', 'mpfy_plugin_init');
function mpfy_plugin_init() {
	wp_enqueue_script('jquery');
	wp_enqueue_style('fancybox-css', plugins_url('assets/js/fancybox/jquery.fancybox.css', __FILE__));

	if (is_admin()) {
		wp_enqueue_script('gmaps', 'http://maps.googleapis.com/maps/api/js?sensor=false&libraries=geometry');
	}
	add_action('wp_footer', 'mpfy_enqueue_assets');
}

function mpfy_enqueue_assets() {
	if (!defined('MPFY_LOAD_ASSETS')) {
		return false;
	}
	if ( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ) {
		wp_enqueue_script('fancybox', plugins_url('assets/js/fancybox/jquery.fancybox.js', __FILE__), array(), false, true);
	} else {
		wp_enqueue_script('fancybox', plugins_url('assets/js/fancybox/jquery.fancybox.pack.js', __FILE__), array(), false, true);
	}

	// wp_enqueue_script('hammer', plugins_url('assets/js/hammer.min.js', __FILE__), array('jquery'), false, true);
	wp_enqueue_script('jcarousel', plugins_url('assets/js/jcarousel/lib/jquery.jcarousel.min.js', __FILE__), array('jquery'), false, true);
	if (!is_admin()) {
		wp_enqueue_script('gmaps', 'http://maps.googleapis.com/maps/api/js?sensor=false&libraries=geometry', array(), false, true);
	}
}

function mpfy_admin_dequeue_maps() {
	wp_dequeue_script('carbon-google-maps');
}
add_action('admin_init', 'mpfy_admin_dequeue_maps', 11);

add_action('admin_menu', 'mpfy_plugin_admin_init');
function mpfy_plugin_admin_init() {
	if (isset($_GET['mpfy-update'])) {
		mpfy_update();
	}

	add_submenu_page('edit.php?post_type=map', 'Batch Upload', 'Batch Upload', 'manage_options', 'mpfy-import', 'mpfy_admin_page');
	wp_enqueue_style('mpfy-admin-css', plugins_url('assets/admin.css' , __FILE__));
	wp_enqueue_script('mpfy-admin', plugins_url('assets/js/admin.js', __FILE__));

	if (version_compare(mpfy_get_version(), MAPIFY_PRO_PLUGIN_VERSION) < 0) {
		$maps = get_posts('post_type=map&posts_per_page=-1');
		$map_locations = get_posts('post_type=map-location&posts_per_page=-1');

		if ($maps || $map_locations) {
			add_action( 'admin_notices', 'mpfy_show_update_notice');
		} else {
			update_option('mpfy_pro_plugin_version', MAPIFY_PRO_PLUGIN_VERSION);
		}
	}
}

function mpfy_update() {
	include_once('updater.php');
	$updates_done = 0;
	foreach ($versions as $version => $updater) {
		if (version_compare(mpfy_get_version(), $version) < 0) {
			if ($updater) {
				$updater();
			}
			update_option('mpfy_pro_plugin_version', $version);
			$updates_done ++;
		}
	}

	if ($updates_done > 0) {
		add_action('admin_notices', 'mpfy_show_update_success_notice');
	}
}

function mpfy_show_update_notice(){
	echo '<div class="error"><p>Warning - your MapifyPro plugin data must be updated. Please backup your data and <a href="' . add_query_arg('mpfy-update', '1', admin_url('/')) . '">click here</a> to proceed.</p></div>';
}

function mpfy_show_update_success_notice(){
	echo '<div class="updated"><p>Your MapifyPro data has been updated.</p></div>';
}

function mpfy_admin_page() {
	include_once('admin/page.main.php');
}

add_action('wp_loaded', 'mpfy_attach_custom_fields', 1000);
function mpfy_attach_custom_fields() {
	include_once('options/post-types.php');

	if (isset($_GET['mpfy-do-import'])) {
		$new_post = array(
			'post_type'=>'map-location',
			'post_status'=>'publish',
			'post_title'=>$_POST['row'][0],
			'post_content'=>$_POST['row'][1],
		);
		$new_post_id = wp_insert_post($new_post);
		$tooltip_enabled = (strtolower($_POST['row'][6]) == 'y') ? 'yes' : 'no';

		update_post_meta($new_post_id, '_map_location_map', $_POST['map_id']);
		update_post_meta($new_post_id, '_map_location_tooltip', $_POST['row'][2]);
		update_post_meta($new_post_id, '_map_location_address', $_POST['row'][3]);
		update_post_meta($new_post_id, '_map_location_city', $_POST['row'][4]);
		update_post_meta($new_post_id, '_map_location_zip', $_POST['row'][5]);
		update_post_meta($new_post_id, '_map_location_tooltip_enabled', $tooltip_enabled);

		$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($_POST['row'][3]) . '&sensor=false';
		$curl = curl_init($url);
		// curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($curl);
		$data = json_decode($response);
		$found_address = false;

		if (@isset($data->results[0])) {
			$found_address = true;
			$latlng = $data->results[0]->geometry->location->lat . ',' . $data->results[0]->geometry->location->lng;
			update_post_meta($new_post_id, '_map_location_google_location', $latlng);
		}

		if ($found_address) {
			echo 'Imported <em>' . $_POST['row'][0] . ' (' . $_POST['row'][3] . ')</em>';
		} else {
			echo 'Imported <em>' . $_POST['row'][0] . ' (' . $_POST['row'][3] . ')</em>. <span style="color: red;">Google Maps failed to retrieve exact location (manual entry is required).</span>';
		}
		exit;
	}
}

// Add compatibility with previous version of MapifyPro
if ( !function_exists('cm_shortcode_custom_mapping') ) {
	function cm_shortcode_custom_mapping() {
		return call_user_func_array('mpfy_shortcode_custom_mapping', func_get_args() );
	}
}

function mpfy_shortcode_custom_mapping($atts, $content) {
	static $mpfy_instances = -1;
	$mpfy_instances ++;

	if (!defined('MPFY_LOAD_ASSETS')) {
		define('MPFY_LOAD_ASSETS', true);
	}
	
	extract( shortcode_atts( array(
		'width'=>500,
		'height'=>300,
		'map_id'=>0,
	), $atts));
	$width = intval($width);
	$width = ($width < 1) ? 500 : $width;
	$height = intval($height);
	$height = ($height < 1) ? 300 : $height;

	$map = get_post(intval($map_id));
	if (!$map || is_wp_error($map) || $map->post_type != 'map') {
		return 'Invalid or no map_id specified.';
	}

	ob_start();
	include('templates/map.php');
	$cnt = ob_get_clean();
	$cnt = preg_replace('~>\s*<~s', '><', $cnt);
	$cnt = preg_replace('~\s*<br \/>\s*~i', '<br />', $cnt);
	// $cnt = str_replace("\n", '', $cnt);
	return $cnt;
}
add_shortcode('custom-mapping', 'mpfy_shortcode_custom_mapping');

function mpfy_add_scripts() {
	if (!defined('MPFY_LOAD_ASSETS')) {
		return false;
	}

	echo '<script type="text/javascript" src="' . plugins_url('assets/js/markerclusterer_compiled.js', __FILE__) . '"></script>';
	echo '<script type="text/javascript" src="' . plugins_url('assets/js/map-instance.js', __FILE__) . '"></script>';
	echo '<script type="text/javascript" src="' . plugins_url('assets/js/tooltip.js', __FILE__) . '"></script>';
	echo '<link rel="stylesheet" type="text/css" href="' . plugins_url('assets/map.css', __FILE__) . '">';
	include_once('templates/popup.php');
}
add_action('wp_footer', 'mpfy_add_scripts');

function mpfy_filter_single_template($template) {
	global $post;
	if ($post->post_type == 'map-location') {
		return dirname(__FILE__) . '/templates/single-map-location.php';
	}

	return $template;
}
add_filter('single_template', 'mpfy_filter_single_template', 1000);

function mpfy_get_thumb($src, $w, $h, $zc = '1') {
	return plugins_url('lib/timthumb.php', __FILE__) . '?src=' . $src . '&amp;w=' . $w . '&amp;h=' . $h . '&amp;zc=' . $zc;
}

function get_file_real_path($path) {
	$dirs = wp_upload_dir();
	$real_path = $path;
	$real_path = str_replace('www.', '', $real_path);
	$home_url = str_replace('www.', '', home_url());
	if (!stristr($path, $home_url)) {
		$real_path = $home_url . $path;
	}
	if (!stristr($dirs['baseurl'], $home_url)) {
		$dirs['baseurl'] = $home_url . $dirs['baseurl'];
	}
	$real_path = str_replace($dirs['baseurl'], $dirs['basedir'], $real_path);
	$real_path = str_replace('\\', DIRECTORY_SEPARATOR, $real_path);
	$real_path = str_replace('/', DIRECTORY_SEPARATOR, $real_path);
	return $real_path;
}

function get_file_real_url($url) {
	if (!stristr($url, home_url())) {
		$url = home_url() . $url;
	}
	return $url;
}

function mpfy_hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}

function mpfy_hide_add_new() {
    global $submenu;
    $submenu['edit.php?post_type=map'][5][0] = 'Maps';
    unset($submenu['edit.php?post_type=map'][10]);
    $submenu['edit.php?post_type=map'][14] = $submenu['edit.php?post_type=map'][16];
    unset($submenu['edit.php?post_type=map'][16]);
    ksort($submenu['edit.php?post_type=map']);
}
add_action('admin_menu', 'mpfy_hide_add_new');

function mpfy_add_shortcode_column($columns) {
	$columns = array_slice($columns, 0, 2) + array('shortcode'=>'Shortcode') + array_slice($columns, 2);
    return $columns;
}
add_filter('manage_edit-map_columns', 'mpfy_add_shortcode_column');
 
function mpfy_shortcode_column_content($column, $post_id) {
	if ($column == 'shortcode') {
		echo '<span style="display: inline-block; padding: 1px 5px; background: #eee; border-radius: 3px;">[custom-mapping map_id="' . $post_id . '" height="300"]</span>';
	}
}
add_action('manage_map_posts_custom_column', 'mpfy_shortcode_column_content', 10, 2);

function mpfy_add_permalink_column($columns) {
	$columns = array_slice($columns, 0, 2) + array('permalink'=>'Permalink', 'url_extension'=>'URL Extension') + array_slice($columns, 2);
    return $columns;
}
add_filter('manage_edit-map-location_columns', 'mpfy_add_permalink_column');
 
function mpfy_permalink_column_content($column, $post_id) {
	if ($column == 'permalink') {
		echo '<span style="display: inline-block; padding: 1px 5px; background: #eee; border-radius: 3px;">' . get_permalink($post_id) . '</span>';
	} elseif ($column == 'url_extension') {
		echo '<span style="display: inline-block; padding: 1px 5px; background: #eee; border-radius: 3px;">?mpfy-pin=' . $post_id . '</span>';
	}
}
add_action('manage_map-location_posts_custom_column', 'mpfy_permalink_column_content', 10, 2);

function mpfy_trigger_tileset_slicing($post_id, $before, $after) {
	if ($after->post_type != 'map') {
		return false;
	}

	if (!$_POST['_map_image_big']) {
		return false;
	}
	$image_url = get_post_meta($post_id, '_map_image_big', true);
	if ($_POST['_map_image_big'] == $image_url) {
		return false; // image not changed
	}

	$response = mpfy_ts_request('create', array('image_url'=>$_POST['_map_image_big']));
	if ($response->success === true) {
		update_post_meta($post_id, '_map_tileset_status', 'processing');
		update_post_meta($post_id, '_map_tileset_progress', '0');
		$result = wp_schedule_single_event(1, 'mpfy_tileset_status', array(rand(0, 99999999999))); // rand arg to skip the 10 minute duplicate limit
	} else {
		update_post_meta($post_id, '_map_tileset_status', 'error');
	}
	update_post_meta($post_id, '_map_tileset_message', $response->message);
	update_post_meta($post_id, '_map_tileset_job_id', $response->job_id);
}
add_action('post_updated', 'mpfy_trigger_tileset_slicing', 10, 3);

function mpfy_ajax_mpfy_get_map_tags() {
	$pid = $_GET['pid'];
	$tags = wp_get_object_terms($pid, 'location-tag');
	echo json_encode($tags);
	exit;
}
add_action('wp_ajax_mpfy_get_map_tags', 'mpfy_ajax_mpfy_get_map_tags');

function mpfy_ajax_mpfy_get_map_settings() {
	$post_id = $_GET['pid'];

	$uploads_dir = wp_upload_dir();
	$return = array(
		'mode'=>get_post_meta($post_id, '_map_mode', true),
		'terrain_mode'=>get_post_meta($post_id, '_map_google_mode', true),
		'style'=>get_post_meta($post_id, '_map_google_style', true),
		'background'=>get_post_meta($post_id, '_map_background_color', true),
		'image_status'=>get_post_meta($post_id, '_map_tileset_status', true),
		'image_source'=>$uploads_dir['baseurl'] . '/mpfy/' . $post_id . '/',
	);
	echo json_encode($return);
	exit;
}
add_action('wp_ajax_mpfy_get_map_settings', 'mpfy_ajax_mpfy_get_map_settings');

function mpfy_single_pin_meta() {
	if (!isset($_GET['mpfy-pin'])) {
		return false;
	}

	$pin_id = intval($_GET['mpfy-pin']);
	$images = carbon_get_post_meta($pin_id, '_map_location_gallery_image', 'complex');
	?>
	<meta property="og:title" content="<?php echo esc_attr(get_the_title($pin_id)); ?>" />
	<?php if ($images) : ?>
		<meta property="og:image" content="<?php echo $images[0]['image']; ?>" />
	<?php endif; ?>
	<link rel='canonical' href='<?php echo remove_query_arg('nonexistant'); ?>' />
	<?php
}
add_action('wp_head', 'mpfy_single_pin_meta');