=== MapifyPro v1.9.2===
Designed and Developed by PaperTelevision.com  
Tags: Custom Mapping, Maps, Google Maps Customization,   
Requires at least: 3.0 , Tested up to: 3.9.1

PLEASE REFER TO THE DOCUMENTATION AND TUTORIALS IN OUR FORUMS: http://mapifypro.freeforums.net/

FULL DOCUMENTATION: http://www.mapifypro.com/documentation

== Description ==
MapifyPro is a plugin for WordPress that implements fully-customized maps on your site. It enhances Google maps with custom pin-point graphics and pop-up galleries, but also allows ANY custom map image of your choosing, all while keeping the great zoom and pan effect of Google maps! Check out some of the features:

Fully Customized Maps
Google maps is great, but severely limits how you can display a map on your site. MapifyPro breaks those barriers and allows you to use a map of your choosing, but maintains the ability to zoom, pan, and add locations. Mapify supports Jpegs, gifs, and PNG files, and even lets you specify the background color if using transparent images. 

Google Map Integration
Want to stick with Google maps but need more control? Mapify fully supports Google maps and address-specific pinpoints. You can toggle from a custom map to a Google map, while maintaining Mapify's extra features: Custom tooltip content, photo/video galleries on each location, and even custom pinpoint graphics. 

Location-Based Information
Mapify allows you to add endless locations to your map. Better yet, it allows you to control what appears when a user hovers over the pinpoints. Photos, videos, and information displayed is 100% under your control. Just click around on the map found on http://mapify.it for a preview, it's all running on Mapify! 


== Installation ==
1. Upload and unzip `MapifyPro.zip` to the `/wp-content/plugins/` directory. Alternatively you can use the Worpdress admin to upload the plugin via the "Plugins" section.
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Several new panels will ne added to your admin:
	-Maps: Add custom maps
	-Locations: Upload and edit locations to maps as needed
	-MapifyPro: Utilize one of Mpaify's most powerful features: Batch uploading of locations via spreadsheet.
4.Further documentation and tutorials available here: http://www.mapifypro.com/forums/

== Frequently Asked Questions ==

NOTE THAT THESE ISSUES ARE COVERED IN MORE DETAIL VIA OUR TUTORIALS: http://mapifypro.freeforums.net/thread/2/mapify-tutorials

= How do I add a map? =

After installing the plugin, there will be a section called 'Maps' added to your Wordpress Admin section (typically it is found in the lower half of your sidebar). Within this page, you can add as many maps as you like, in a very similar manner as adding blog posts. When creating a new map, or editing an existing map, you may apply the following settings:

-Choose whether you would like Google Maps or Custom Image Mode.
-Specify whether you would like to include search and/or filtering capability to your map. You may also specify the search radius in miles.
-Choose a default pin image for the map (though note you can use a unique image for every location. If not specified, it will default to the image you upload on your map setting.)
-Drag momentum for image mode
-Upload specific images to be used for the map (Custom image Mode)
-Choose the default view mode in Google maps (terrain, hybrid, etc.)
-Specify the default zoom level of your map.


= How do I create a location on the map? =

After activating the plugin a new administration section called "Map Locations" will be available, and you can add/edit map location from that section by following the instructions.

To add a location to Google Maps: Enter the address to the exact location of your choosing and Mapify will automatically add the pinpoint. You can also double click to add a location directly to the map, or drag and drop an existing location. This is great for locations without an address, or on a body of water.

To add a location to a Custom Map: Simply double click on the location of your choosing. After adding a location, you may also drag and drop the pinpoint and MapifyPro will self-adjust to the new location.
Note: All Locations are stored separately in the section called "Map Locations", located in the sidebar of the Wordpress admin. These are organized separately to keep things nice and organized, and also so it's easy to edit locations in the future.


= How do I add images and video to a map location's Pop-up window? =

To add images to a locations pop-up window: You can upload as many images as you like to each map location through the "Upload/Insert" buttons located in the location settings. Upload your locations via standard Wordpress media uploads, and each will automatically be added to the gallery, thumbnails created, and gallery functions added.

To add a video to the pop-up window: Each location allows you to include an optional video in the pop-up window. To do so, simply enter the embed code from your favorite video site into the field called "Video Embed Code", which is found on the Map Location entry. 

For example, if you want to include a video from Youtube, it would look like this:
<iframe width="472" height="316" src="http://www.youtube.com/embed/u0fk6syQ7iY" frameborder="0" allowfullscreen></iframe>


= Can I edit the tooltip that appears when someone hovers over a location? =

Yes, and it's easy! In the Map Location screen, there is a field called "Tooltip". Edit this field to specify the description of each location. This is what appears when someone hovers over those location pinpoints. 


= What if i don't want the gallery popup? =

For each location, there is a toggle to include/exclude the popup. This allows you to have galleries only on the locations that you specify.


= How do I display a map on a page/post? =

Use the following shortcode  within your blog post or Wordpress Page: `[custom-mapping height="400" width="600" map_id="11"]

You may adjust the height/width as needed within the short code to display your map at any size. The map id is found under your specific map settings (this is created and assigned after you publish a new map). To properly display a custom map image on your site, make sure the height/width in this setting is the same as the default version of the map image that you've uploaded in the custom mapping section, otherwise it will stretch and blur.



Good things to know about custom maps: 

-For custom maps, you need to provide 2 images: a small one and a big one. This allows you to zoom into the map on the live version of the site. 

-Mapify supports any image format (jpg, png, gif). You can also use a transparent image, which will then allow you to specify the background color of the map via the Mapify settings found on this page. 

-Keep file size in mind! We recommend optimized images under 200kb for quick loading.

-DON'T FORGET TO CLICK "INSERT INTO POST WHEN UPLOADING NEW IMAGES"

= How do I add a custom pinpoint images? =

On the Custom Mapping page, there is an option to upload a custom pinpoint. This pinpoint will appear whether you use google maps or the custom image modes of Mapify. When uploading or selecting a new pinpoint image, don't forget to click "Insert into Post" at the bottom of the Meida pop-up.

= What if I just want to use Google maps, but with Mapify's added features? =

On the Custom Mapping page, there is a toggle under the "General" section called "Mode". Simply make your choice whether you want to use Google Maps or a custom map using this option.

Using Google maps allows all of those great familiar features, but also lets you use Mapify's best features, such as custom tooltip descriptions, custom pinpoint images, and pop-up galleries for each location! You can also specify the size and location of your google map, which is much easier than editing code.




== Changelog for 1.7.5==

= 1.0 =
* Initial release