<?php
class TI_Pro extends TI_Frontend {

	function view_template($import) {
		// Multisite
		if (is_multisite() && is_main_site()) {
			$this->postbox_start(__('Multisite'), 'multisite_settings');
			?>
			<table class="form-table">
				<?php $this->optionp($import, 'template[blog_id]'); ?>
			</table>
			<?php
			$this->postbox_end();
		}

		// Normal installation
		?>
		<?php $this->postbox_start(__('Layout'), 'layout_settings'); ?>
		<table class="form-table">
			<?php
				$this->optionp($import, 'template_id');
				$this->optionp($import, 'template[post_title]');
				$this->optionp($import, 'template[post_content]');
				$this->optionp($import, 'template[post_excerpt]');
			?>
		</table>
		<?php $this->postbox_end(); ?>

		<?php $this->postbox_start(__('Post/Page Settings'), 'postpage_settings'); ?>
			<table class="form-table">
				<?php
					$this->optionp($import, 'template[post_type]');
					$this->option($import, 'template[post_parent]');
					$this->option($import, 'template[page_template]');
					$this->option($import, 'template[menu_order]');
					$this->option($import, 'template[post_status]');
					$this->option($import, 'template[comment_status]');
					$this->option($import, 'template[post_author_type]');
					$this->optionp($import, 'template[post_name_col]');
					$this->optionp($import, 'template[post_date_type]');
				?>
			</table>
		<?php $this->postbox_end(); ?>

		<?php $this->postbox_start(__('Updates'), 'postpage_settings'); ?>
			<table class="form-table ti-form-table">
				<?php
					$this->optionp($import, 'template[postid_col]');
					$this->optionp($import, 'template[update_field]');
				?>
			</table>
		<?php $this->postbox_end(); ?>

		<?php $this->postbox_start(__('Categories, Tags and Taxonomies'), 'taxonomy_settings'); ?>
			<table class="form-table ti-form-table">
				<?php
					$this->optionp($import, 'template[taxonomies]');
				?>
			</table>
		<?php $this->postbox_end(); ?>

		<?php $this->postbox_start(__('Custom Fields'), 'customfields_settings'); ?>
			<table class="form-table ti-form-table">
				<?php
					$this->optionp($import, 'template[post_custom]');
				?>
			</table>
			<table class="form-table ti-form-table">
				<?php
					$this->optionp($import, 'template[post_custom_new]');
				?>
			</table>
		<?php $this->postbox_end(); ?>
		<?php
	}

	function optionp($import, $field) {
		global $wpdb;

		switch ($field) {

			case 'template_id':
				$template_ids = TI_Template::get_list();
				if ($template_ids)
					$template_ids = array_combine($template_ids, $template_ids);

				if (isset($template_ids))
					$template_ids = array_merge(array('' => 'new template'), (array)$template_ids);
				else
					$template_ids = array('' => 'new template');

				$html = __("Load template: ")
					. $this->option_dropdown('template_load_id', $import->template->_id, $template_ids)
					. "<input type='submit' class='button ti-submit' name='button_load_template' value='" . __('Load') . "' />"
					. "<input type='submit' class='button ti-submit' name='button_delete_template' value='" . __('Delete') . "' />"
					. "<br/>Save as template: <input type='text' name='template_save_id' value='" . esc_attr($import->template->_id) . "' />"
					. "<input type='submit' class='button ti-submit' name='button_save_template' value='" . __('Save') . "' />";

				$tip = __('Settings can be saved as a "template" for future imports.  This is the template currently in use.');
				$this->option_formatted(__('Import template'), $html, $tip);
				break;

			case 'template[post_title]':

				foreach((array)$import->headers as $header => $token)
					$tokens[] = "<a href='#'>" . esc_attr($token) . "</a>";

				$html = __('To insert a column: first click on the title, body or excerpt field, then click on the column name link') . "<br/>"
					. "<div id='ti_tokens'>" . implode(' | ', $tokens) . "</div>";
				$this->option_formatted(__('Input columns'), $html);

				$html = "<input id='post_title' class='ti-token-field ti-post-title' type='text' value='" . esc_attr($import->template->post_title) . "' name='$field'/>";

				$tip = __("Click on the field names to add them to your post title and body.  During the import they'll be replaced with data from your input file.");
				$this->option_formatted(__('Post title'), $html, $tip, '');
				break;


			case 'template[post_content]':
				// Div for the html/visual buttons
				$html = "<div>"
					. "<input type='button' name='button_editor_html' value='HTML' />"
					. "<input type='button' name='button_editor_visual' value='Visual' />"
					. "</div>";

				// Textarea
				$html .= "<div class='ti-post-content-div'>";
				$html .= "<textarea id='post_content' name='$field' class='ti-token-field ti-post-content' rows='12' >"
					. $import->template->post_content
					. "</textarea>"
					. "</div>";

				$tip = __("Click on the field names to add them to your post title and body.  During the import they'll be replaced with data from your input file.");
				$this->option_formatted(__('Post body'), $html, $tip);
				break;

			case 'template[post_excerpt]':
				$html = "<textarea id='post_excerpt' name='$field' class='ti-token-field ti-post-excerpt'  rows='3' >"
					. esc_attr($import->template->post_excerpt)
					. "</textarea>";

				$tip = __("Click on the field names to add them to your post title and body.  During the import they'll be replaced with data from your input file.");
				$this->option_formatted(__('Post excerpt'), $html, $tip);

				break;

			case 'template[blog_id]':
				// Ignore this if it's not a multisite install
				if (!is_multisite())
					return;

				// Blog ID from column
				$checked = checked($import->template->blog_id_type, 'COLUMN', false);
				$html = "<input type='radio' name='template[blog_id_type]' value='COLUMN' $checked />" . __('Column') . " "
					. $this->option_dropdown_headers('template[blog_id_col]', $import->template->blog_id_col, $import->headers);

				// Blog ID from fixed list
				$checked = checked($import->template->blog_id_type, 'FIXED', false);
				$html .= "<br/><input type='radio' name='template[blog_id_type]' value='FIXED' $checked />"  . __('Select from list ');
					
				$query = "SELECT * FROM {$wpdb->blogs} ORDER BY {$wpdb->blogs}.site_id, {$wpdb->blogs}.blog_id ";
				$wp_blogs = $wpdb->get_results( $query, OBJECT );

				$blog_ids = array();
				foreach($wp_blogs as $wp_blog) {
					$key = $wp_blog->blog_id;
					$blog_ids[$key] = $wp_blog->path;
				}

				$html .= $this->option_dropdown($field, $import->template->blog_id, $blog_ids, false);
				$tip = __('Select a site to import into or specify a column containing the site ID.  This feature is only available from the main site.');
				$this->option_formatted(__('Site'), $html, $tip);
				break;
				
				
			case 'template[post_type]':
				$wp_post_types = get_post_types(array('public' => true, '_builtin' => false), 'names', 'and');
				$post_types = array('post' => 'post', 'page' => 'page');
				if (!empty($wp_post_types))
					$post_types = array_merge($post_types, array_combine($wp_post_types, $wp_post_types));

				$html = $this->option_dropdown($field, $import->template->post_type, $post_types, false);
				$tip = __('Select a post type to import.  You can use a standard post type ("post" or "page"), or a <a href="http://codex.wordpress.org/Custom_Post_Types">custom post type</a>.');
				$this->option_formatted(__('Post type'), $html, $tip);
				break;

			case 'template[post_name_col]':
				$html = $this->option_dropdown_headers($field, $import->template->post_name_col, $import->headers, true);
				$tip = __('WordPress post "slug" - see <a href="http://codex.wordpress.org/Writing_Posts">here</a> for details.  Leave blank for default.');
				$this->option_formatted(__('Post name (slug) column'), $html, $tip);
				break;

			case 'template[postid_col]':
				$html = $this->option_dropdown_headers($field, $import->template->postid_col, $import->headers, true);
				$tip = __('<b>Optional</b>: To update existing posts/pages specify the input file column that contains a unique identifier.');
				$this->option_formatted(__('Unique ID column'), $html, $tip);
				break;

			case 'template[update_field]':
				$meta_keys = $this->get_meta_keys();
				$dd = array('' => 'Post ID');
				foreach($meta_keys as $meta_key)
					$dd[$meta_key] = $meta_key;

				$html = $this->option_dropdown("template[update_field]", $import->template->update_field, $dd, false);

				$tip = __('Select the WordPress key field that contains your unique ID numbers.  Using the WordPress Post ID is suggested, but you can also choose a custom field.  During the import if the ID column matches the key field of an existing post, the post will be updated.  If multiple matches are found, only the first post will be updated. And if no match is found, an error will be issued.');
				$this->option_formatted(__('Match to'), $html, $tip);
				break;

			case 'template[post_date_type]':
				// Current date
				$checked = ($import->template->post_date_type == 'TODAY') ? "checked='checked'" : "";
				$html = "<input type='radio' name='$field'' value='TODAY' $checked />" . __('Current date and time');

				// Date range
				$checked = ($import->template->post_date_type == 'FIXED') ? "checked='checked'" : "";
				$html .= "<br/><input type='radio' name='$field' value='FIXED' $checked />" . __('Random dates') . "&nbsp"
					. "<input type='text' name='template[post_date_min]' value='" . esc_attr($import->template->post_date_min) . "' size='10' /> to "
					."<input type='text' name='template[post_date_max]' value='" . esc_attr($import->template->post_date_max) . "' size='10' />";

				// Column
				$checked = ($import->template->post_date_type == 'COLUMN') ? "checked='checked'" : "";
				$html .=  "<br/><input type='radio' name='$field' value='COLUMN' $checked />" . __('Column') . "&nbsp"
					. $this->option_dropdown_headers('template[post_date_col]', $import->template->post_date_col, $import->headers);

				$tip = __('This date will appear as the date the post was published.  To make your blog look like it was written over a period of time, use the "column" or "random dates" options.');
				$tip .= "<br/><br/>" . __('Note that server date and time is used, not your local PC time.');
				$this->option_formatted(__('Post date'), $html, $tip);

				break;

			case 'template[taxonomies]':
				// Output standard taxonomies (categories & tags)
				$this->map_taxonomy($import, 'category');
				$this->map_taxonomy($import, 'post_tag');

				// Output custom taxonomies
				$taxonomies = get_taxonomies(array('_builtin' => false));
				foreach ($taxonomies as $taxonomy_name)
					$this->map_taxonomy($import, $taxonomy_name);
				break;

			case 'template[post_tag_type]':
				$this->map_taxonomy($import, 'post_tag');
				break;

			case 'template[post_custom]':
				$meta_keys = $this->get_meta_keys();

				if (count($meta_keys) == 0) {
					$html = __('There are no existing custom fields to map.  See the WordPress ')
								. '<a href="http://codex.wordpress.org/Using_Custom_Fields">Codex</a>'
								. __(' for more information about custom fields.');
				} else {
					foreach ((array)$meta_keys as $meta_key) {
						$meta_value = (isset($import->template->post_custom[$meta_key])) ? $import->template->post_custom[$meta_key] : null;
						$dd = $this->option_dropdown_headers("template[post_custom][$meta_key]", $meta_value, $import->headers, true);
						$rows[] = array('data' => array($meta_key, $dd));
					}

					$headers = array(__('Field'), __('Input Column'));
					$html = $this->option_table($headers, $rows);

				}
				$tip = __('Use the mapping table to populate existing custom fields with data from the input file.');
				$this->option_formatted(__('Existing custom fields'), $html, $tip);
				break;

			case 'template[post_custom_new]':

				$existing_metas = $this->get_meta_keys();
				$new_metas = array();

                // 'New' custom fields are any headers that are not already mapped and that don't begin with '!' 
				foreach ($import->headers as $key => $header) {
					if (!in_array($header, (array)$existing_metas) && substr($header, 0, 1) != '!')
						$new_metas[] = $header;
				}

				// If there are no new custom fields then don't show this section
				if (count($new_metas) == 0) {
					break;
				}

                // Display the selected custom fields.  The fields are present in the array if selected.
				foreach ((array)$new_metas as $key => $value) {
                    $checked = (isset($import->template->post_custom_new[$value])) ? "checked='checked'" : "";
					$rows[] = array('data' => array("<input type='checkbox' name='template[post_custom_new][$value]' value='$value' " . $checked . "/>", $value));
				}

				$headers = array("<input type='checkbox' class='selectall' />",  __('Input Column'));
				$html = $this->option_table($headers, $rows, 'ti_custom_new_table');

				$tip = __("Use the checkboxes to create new custom fields from the columns in your input file.");
				$this->option_formatted(__('Create new custom fields'), $html, $tip);
				break;
		}
	}

	/**
	* Map a taxonomy to an input column or fixed values
	*
	* @param mixed $import
	* @param mixed $taxonomy_name
	*/
	function map_taxonomy($import, $taxonomy_name) {
		$max_terms = 200;   // MAX # terms to return - some unusual blogs have many thousands
		$taxonomy = (isset($import->template->taxonomies[$taxonomy_name])) ? $import->template->taxonomies[$taxonomy_name] : null;
		$wp_taxonomy = get_taxonomy($taxonomy_name);

		// If the taxonomy is not yet part of the template, add it
		if (!$taxonomy) {
			$taxonomy = $import->template->taxonomies[$taxonomy_name] = array('type' => 'COLUMN', 'col' => null, 'values' => array());
		}

		// If the taxonomy no longer exists in WordPress (i.e. it was deleted), then just skip it
		if (!$wp_taxonomy)
			return;

		// Get all the terms in the taxonomy (up to the maximum #)
		$wp_terms = get_terms($taxonomy_name, array('number' => $max_terms, 'hide_empty' => false, 'orderby' => 'id'));

		// Add a hierarchical description (with dashes) and sort by the 'slug'
		$terms = $this->get_term_list($wp_terms);

		// Term from column
		$checked = ($taxonomy['type'] == 'COLUMN') ? "checked='checked'" : "";
		$html = "<input type='radio' class='ti-taxonomy-type' data='$taxonomy_name' name='template[taxonomies][$taxonomy_name][type]' value='COLUMN' $checked />" . __('Column') . "&nbsp"
			. $this->option_dropdown_headers("template[taxonomies][$taxonomy_name][col]", $taxonomy['col'], $import->headers) . "<br/>";

		// Fixed values (only if terms exist)
		if (!empty($terms)) {
			$checked = ($taxonomy['type'] == 'FIXED') ? "checked='checked'" : "";
			$html .= "<input type='radio' class='ti-taxonomy-type' data='$taxonomy_name' name='template[taxonomies][$taxonomy_name][type]' value='FIXED' $checked />"  . __('Select from list'). "<br/>";

			$rows = array();
			foreach ((array)$terms as $term_id => $term) {
                // Show checkmarks for taxonomy values if the import had the taxonomy using fixed values 
				$checked = ($taxonomy['type'] != 'COLUMN' && in_array($term_id, (array)$taxonomy['values'])) ? "checked='checked'" : "";
				$key = esc_attr($term_id);
				$description = esc_attr($term['description']);
				$name = "template[taxonomies][$taxonomy_name][values][]";
				$rows[] = array('data' => array("<input type='checkbox' name='$name' value='$term_id' $checked />", $description));
			}

			//$headers = array("<input type='checkbox' class='selectall' />",  $wp_taxonomy->labels->singular_name);
            $headers = array('', $wp_taxonomy->labels->singular_name);

			// Hide the table initially unless 'fixed' is already selected
			$class = ($taxonomy['type'] == 'FIXED') ? 'ti-small-scroll' : 'ti-hidden ti-small-scroll';
			$html .= $this->option_table($headers, $rows, '', "ti_taxonomy_table_$taxonomy_name", $class);
		}

		$tip = __('Choose an input column that contains the taxonomy values or select fixed values from the list.');
		$this->option_formatted($wp_taxonomy->labels->singular_name, $html, $tip);
	}


	/**
	* Used for printing hierarchical taxonomies.
	* Takes a list of terms and creates a new array of term_id => (term_slug, term_description)
	* The slug is a concatenation of all parent term names.  Description shows parents replaced by '-'
	* The result is sorted by slug.
	*
	* @param mixed $terms - array of all terms in the taxonomy (as objects)
	*/
	function get_term_list($terms) {
		$result = array();
		foreach($terms as $key=>$term) {
			$result[$term->term_id] = $this->get_term_list_item($terms, $term);
		}

		// Sory by the result array by slug
		uasort($result, array($this, 'sort_term_list'));
		return $result;
		}

	function get_term_list_item($terms, $current_term=null, $level=0) {
		$result = array('description' => '', 'slug' => '');

		if ($current_term->parent) {
			foreach ($terms as $key=>$parent) {
				if ($parent->term_id == $current_term->parent) {
					$result = $this->get_term_list_item($terms, $parent, $level + 1);
					break;
				}
			}
		}

		$result['description'] .= ($level == 0) ? $current_term->name : '&#8212; ';
		$result['slug'] .= $current_term->name;
		return $result;
	}

	/**
	* Sort an array of terms by field ti_slug (a hierarchical slug)
	*
	*/
	function sort_term_list($terma, $termb) {
		if ($terma['slug'] == $termb['slug'])
			return 0;
		if ($terma['slug'] > $termb['slug'])
			return 1;
		return -1;
	}

	/**
	* Undo an entire import.  All posts and associated comments are deleted.
	* Any categories and tags that were created during the import are also
	* deleted, but only if they have no reference count once the posts/comments are gone.
	*
	* The import profile is retained and is not deleted, but the status is set to 'UNDO'.
	*
	* The instance will update itself back to the options database.
	*/
	function undo($id) {
		global $current_user, $blog_id;
		
		$original_blog_id = $blog_id;

		$import = TI_Import::get($id);
		if ($import === false)
			return new WP_Error('ERROR', sprintf(__('Unable to read import id %s for undo'), $id));

		get_currentuserinfo();            
		$import->log(__("Undo started by: $current_user->user_login"), 'INFO');
		$import->import_start();

		foreach ((array)$import->get_imported_posts() as $post) {
			// Switch blogs if needed
			if (is_multisite() && isset($post['blog_id']) && $post['blog_id'] != $blog_id)
				switch_to_blog($post['blog_id']);
		
			// If post was updated during import, and revisions is on, then try to roll back to previous version
            if (defined('WP_POST_REVISIONS') && WP_POST_REVISIONS && isset($post['updated']) && $post['updated'] && isset($post['revision_id'])) {
				$result = wp_restore_post_revision( $post['revision_id'] );
				if (is_wp_error($result))
					$import->log(sprintf(__("Unable to restore original version of post %s (%s): %s"), $post['post_title'], $post['post_id'], $result->get_error_message() ));
				if (!$result)
					$import->log(sprintf(__("Unable to restore original version of post %s (%s)"), $post['post_title'], $post['post_id']));
			} else {
			    // If no revisions, or post was created during import, then delete it
				$result = wp_delete_post($post['post_id']);
				if (is_wp_error($result))
					$import->log(sprintf(__("Error deleting post %s (%s): %s"), $post['post_title'], $post['post_id'], $result->get_error_message() ));
			}
		}

		// NOTE: counts seem to be incorrect for custom taxonomies - they include deleted posts - see wordpress trac #14084, #14073, #14392
		// Delete tags, categories and custom taxonomies
		foreach ((array)$import->imported_terms as $taxonomy => $terms) {
			foreach ($terms as $term) {
				// Switch blogs if needed
				if (is_multisite() && isset($term->blog_id) && $term->blog_id != $blog_id)
					switch_to_blog($term->blog_id);
				
				// Get current name in case it's changed
				$wp_term = get_term($term->term_id, $taxonomy);
				if (!is_wp_error($wp_term) && $wp_term)  // get_term() returns either wp_error or null
					$term->name = $wp_term->name;

				// Term doesn't exist
				if (!$wp_term || is_wp_error($wp_term)) {
					$import->log(sprintf(__('Could not delete term "%s" in taxonomy "%s" because it no longer exists'), $term->name, $taxonomy), 'WARNING');
					continue;
				}

				// Term still in use
				if ($wp_term->count > 0) {
					$import->log(sprintf(__('Term "%s" in taxonomy "%s" was not deleted because it is still in use'), $term->name, $taxonomy), 'WARNING');
					continue;
				}

				// Delete term
				$result = wp_delete_term($term->term_id, $taxonomy);
				if (!$result)
					$import->log(sprintf(__('Uknown error deleting term "%s" in taxonomy "%s"'), $term->name, $taxonomy), 'ERROR');

				if (is_wp_error($result))
					$import->log(sprintf(__('Error deleting term "%s" in taxonomy "%s" : "%s"'), $term->name, $taxonomy, $result->get_error_message() ), 'ERROR');
			}
		}

		// Switch back to original blog before writing out the import logs to the database
		if ($original_blog_id != $blog_id)
			switch_to_blog($original_blog_id);
		
		// Update terms cache
		$import->clean_term_cache();

		// Set status and save back to db
		$import->log(__('Undo finished.'), 'INFO');
		$import->status = 'UNDO';
		$import->save();
		return true;
	}
}
?>