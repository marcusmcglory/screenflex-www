<?php
/**
* Class to perform the import.
* Note:
*   -Template settings are always saved with the import so the import can be repeated.
* 	-List of posts associated with each import is saved in same table.  This is faster, but limits imports by memory size.
*/
class TI_Import extends TI_Obj {
	var $_id,
	$status,
	$template,
	$user_id,
	$timestamp,
	$filepath,
	$fileurl,
	$filename,
	$headers,
	$logs,
	$lines_total,
	$lines_blank,
	$lines_imported,
	$errors,
	$imported_terms,
	$start_memory,
	$max_memory;

	/**
	* Constructor.
	* Create a new import object and immediately save it to the database
	*
	*/
	function defaults() {
		global $current_user;
		get_currentuserinfo();

		return array(
			'user_id' => $current_user->ID,
			'timestamp' => time(),
			'template' => new TI_Template(),
			'status' => 'NEW',
			'lines_total' => 0,
			'lines_blank' => 0,
			'lines_imported' => 0,
			'errors' => 0
		);
	}

	static function create_db() {
		return parent::create_db('ti_import', true);
	}

	static function get($id) {
		return parent::get('ti_import', $id);
	}

	static function get_list() {
		return parent::get_list('ti_import');
	}

	static function delete($id) {
		return parent::delete('ti_import', $id);
	}

	function save() {
		return parent::save('ti_import');
	}

	function get_imported_posts() {
		global $wpdb;

		// Older versions had imported_posts as a property of this class, newer versions use a DB table
		if (property_exists($this, 'imported_posts')) {
			// Convert the key post['ID'] to the newer form, post['post_id'] before returning
			$posts = array();
			foreach ((array)$this->imported_posts as $post) {
				$post['post_id'] = $post['ID'];
				$posts[] = $post;
			}
			return $posts;
		}

		// Newer versions stores the lines in a separate table
		$table = $wpdb->prefix . "ti_import_lines";
		$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table WHERE import_id = {$this->_id}"), ARRAY_A);

		if (!$results)
			return array();
		return $results;
	}

	function import_start() {
		global $wpdb;

		if (!defined('WP_IMPORTING'))
			define( 'WP_IMPORTING', true );
		wp_defer_term_counting(true);
		wp_defer_comment_counting(true);

		// For slow machines, try to increase the time limit to 2 hours
		$max_time = ini_get('max_execution_time');
		if ($max_time < 7200)
			set_time_limit(7200);
	}

	function import_end() {
		// Switch off importing flag and flush rewrite cache
		wp_defer_term_counting(false);
		wp_defer_comment_counting(true);
		global $wp_rewrite;
		$wp_rewrite->flush_rules();
	}

	/**
	* Process the import.
	* $options is passed to subroutines in order to use main site options when importing to alternate blog_id in multisite
	*
	*/
	function import() {
		global $wpdb, $blog_id;

		$original_blog_id = $blog_id;  // Remember the original blog ID - it may be switched during line processing
		$options = get_option('turbocsv');
		$max_skip = $options['max_skip'];
		$encoding = $options['encoding'];
		$commit_rows = $options['commit_rows'];

		// Set status to 'ERROR' in case we get unhandled exception
		$this->status = 'ERROR';

		// Save before starting
		$result = $this->save();

		// Get headers, in case they were modified
		$result = $this->read_headers();
		if (is_wp_error(!$result))
			return $result;

		// Set max run time
		$this->import_start();

		$userd = get_userdata($this->user_id);
		$username = $userd->user_login;
		$this->log(__("Import started by: $username"), 'INFO');

		// Open the file
		$fp = $this->file_open();
		if (is_wp_error($fp))
			return $fp;

		// Throw away the header line
		$this->fgetcsv_plus($fp, $options);

		// Read the input file line-by-line
		while (!feof($fp)) {

			// Read the next line
			$line = $this->fgetcsv_plus($fp, $options);

			// feof() doesn't seem to work properly until *after* we read the last line
			if ($line === false)
				continue;

			// Skip blank lines
			if ($this->is_blank($line)) {
				$this->lines_blank++;
				continue;
			}

			$this->lines_imported++;
			$this->lines_total++;

			// Process a line
			$result = $this->import_line($line, $options);

			// If we had errors
			if (is_wp_error($result)) {
				$this->errors++;

				// Stop at maximum # of error lines
				if ($this->errors >= $max_skip) {
					$this->log(__('Maximum number lines with errors, stopping import.'));
					break;
				}
			}

			// Every time commit count reached: save the imported WP objects and the current import object
			if (($this->lines_total % $commit_rows) == 0) {

				// Switch back to main blog if needed before saving
				if ($original_blog_id != $blog_id)
					switch_to_blog($original_blog_id);

				$this->save();
				$wpdb->query("COMMIT");
			}
		}

		// Close the input file
		fclose($fp);

		// Refresh term cache
		$this->clean_term_cache();

		// Log completion
		$this->log(__('Import finished.'), 'INFO');

		// Set final status
		if ($this->has_errors())
			$this->status = 'ERROR';
		else
			$this->status = 'COMPLETE';

		$this->import_end();

		// Switch back to main blog if needed
		if ($original_blog_id != $blog_id)
			switch_to_blog($original_blog_id);

		// Save import
		$this->save();

		return;
	}

	function import_line($line, $options) {
		global $wpdb, $wp_object_cache;

		$imported_post = new TI_Import_Line();

		// Get blog ID and switch to that blog if needed
		$post_blog_id = $this->import_blog_id($line);
		if (is_wp_error($post_blog_id))
			return $post_blog_id;
		else
			$imported_post->blog_id = $post_blog_id;

		// Replace the header tokens in the post body/title
		foreach($this->headers as $header)
			$tokens[$header] = '#' . $header . '#';

		$post['post_content'] = str_replace($tokens, $line, $this->template->post_content);
		$post['post_title'] = str_replace($tokens, $line, $this->template->post_title);
		$post['post_excerpt'] = str_replace($tokens, $line, $this->template->post_excerpt);

		// Validate post title & content
		if (empty($post['post_content']) && empty($post['post_title']))
			return $this->log_line(__('Post title and body are both empty.  WordPress does not allow empty posts.'));

		// Assign post date
		$result = $this->import_post_date($line);

		if (is_wp_error($result))
			return $result;
		else
			$post['post_date']  = $result;

		// Taxonomies
		$result = $this->import_post_taxonomies($line, $options);
		if (is_wp_error($result))
			return $result;
		else
			$post['tax_input'] = $result;

		// Author
		$post['post_author'] = $this->import_post_author($line);

		// Assign other field values
		$post['comment_status'] = $this->template->comment_status;
		$post['post_status'] = $this->template->post_status;

		// Assign post type
		$result = $this->import_post_type($line);
		if (is_wp_error($result))
			return $result;
		else
			$post['post_type'] = $result;

		// Figure out if revisions are supported and switched on
		$revisions_allowed = defined('WP_POST_REVISIONS') && WP_POST_REVISIONS && post_type_supports($post['post_type'], 'revisions');

		// Some fields only apply to pages
		if ($post['post_type'] == 'page') {
			$post['menu_order'] = $this->template->menu_order;
			$post['post_parent'] = $this->template->post_parent;    // Not currently validated!

			// Validate and assign page template
			if (!empty($this->template->page_template)) {
				$page_templates = get_page_templates();
				if ($this->template->page_template != 'default' && !in_array($this->template->page_template, $page_templates))
					return $this->log_line(sprintf(__('The page template "%s" is invalid.'), $this->template->page_template));
				else
					$post['page_template'] = $this->template->page_template;
			}
		}

		// Assign slug
		if ($this->template->post_name_col)
			$post['post_name'] = $this->read_line_col($line, $this->template->post_name_col);

		// Update or insert the post (update if ID is specified); note that on update we can only get true/false, not WP_ERROR
		if ($this->template->postid_col) {
			// ID value to match
			$update_id = $this->read_line_col($line, $this->template->postid_col);

			// Field to match against
			$update_field = $this->template->update_field;

			// If $update_field is blank, then assume that update_id is matched against post ID
			// Otherwise, the find_post_id() function will locate the correct post ID by the contents of a custom field
			$post['ID'] = $this->find_post_id($update_field, $update_id);

			// Save post ID being updated
			$updated_post_id = $post['ID'];

			// If revisions are turned on, get current maximum revision # (raw query - because wp_get_post_revisions() is filtered)
			if ($revisions_allowed)
				$max_revision_id = $this->get_last_revision($updated_post_id);

			$post_id = wp_update_post($post);
		// Not an update - insert NEW post
		} else {
			$updated_post_id = null;
			$post_id = wp_insert_post($post, true);
		}

		// Record memory usage after update/insert
		$memory_usage = memory_get_usage();
		$this->max_memory = ($memory_usage > $this->max_memory) ? $memory_usage : $this->max_memory;

		// Flush the object cache - if this isn't done WordPress leaks 4k of memory with each new post.
		// Even with the flush, it's about 900B.
		$wp_object_cache->flush();

		// Any error from posting is fatal.  wp_insert gives a full error, wp_update just returns FALSE
		if (is_wp_error($post_id))
			return $this->log_line($post_id);
		elseif ($post_id === false)
			return $this->log_line(sprintf(__("Wordpress error when updating post id=%s"), $post_id));

		// If updating, check revision number if revisions are turned on
		if ($updated_post_id) {
			// Set flag indicating this was an update
			$imported_post->updated = true;

			if ($revisions_allowed) {
				$revision_id = $this->get_last_revision($updated_post_id);
				if ($revision_id <= $max_revision_id || $revision_id == null)
					return $this->log_line(sprintf(__('Post ID %s could not be updated - check that the post still exists!'), $updated_post_id));

				// A revision was created, save it for later UNDO
				$imported_post->revision_id = $revision_id;
			} else {
				// If revision management not turned on, store FALSE in revision
				$imported_post->revision_id = false;
			}
		} else {
			$imported_post->updated = false;
		}



		// Post was OK - save the post data to the import; content and excerpt are NOT saved to conserve memory
		$imported_post->post_id = $post_id;
		$imported_post->import_id = $this->_id;
		$result = $imported_post->save();
		if (is_wp_error($result))
			return $result;

		// Store post metadata
		$result = $this->import_post_meta($line, $post_id, $options, $updated_post_id != null);
		if (is_wp_error($result))
			return $result;

		return true;
	}

	/**
	* Find a post ID by the contents of a field.
	* If the field name is blank, the function will assume it's "Post ID" and immediately return the $id value
	* If the field name is not blank, it will search for a custom field value $id in custom field $field
	*
	* @param mixed $field - A custom field name or blank for Post ID
	* @param mixed $id - unique ID number to search for, either Post ID or contents of a custom field
	*/
	function find_post_id($field, $id) {
		global $wpdb;

		// If field is blank, then assume ID represents a post ID and return it immediately
		if (!$field || empty($field))
			return $id;

		// Search for custom field value matching $id
		$found_id = $wpdb->get_var( $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", $field, $id) );

		return $found_id;
	}

	/**
	* Get blog ID for the post
	* If the input file blog_id is valid, then it's returned.
	* If not multisite then global $blog_id is returned
	* If error, wp_error is returned
	*
	* @return WP_Error if error
	*/
	function import_blog_id($line) {
		global $blog_id;

		// Ignore blog ID if not multisite
		if (!is_multisite())
			return $blog_id;

		switch ($this->template->blog_id_type) {
			case 'FIXED':
				$post_blog_id = (int) $this->template->blog_id;
				break;

		   case 'COLUMN':
				$data = $this->read_line_col($line, $this->template->blog_id_col);
				if ($data===false)
					return $this->log_line(sprintf(__('Invalid column name for blog id: %s'), $this->template->blog_id_col));
				else
					$post_blog_id = (int) $data;
				break;
		}

		// If blog_id was empty, return current blog
		if (!isset($post_blog_id) || !($post_blog_id))
			return $blog_id;

		// If blog_id has changed try to switch to the new blog
		if ($post_blog_id && $post_blog_id != $blog_id) {
			if (switch_to_blog($post_blog_id, true) === false)
				return $this->log_line(sprintf(__('Invalid blog id: %s'), $post_blog_id));
		}

		return $post_blog_id;
	}

	function import_post_author($line) {
		switch ($this->template->post_author_type) {
			case 'FIXED':
				return $this->template->post_author;
				break;

		   case 'COLUMN':
				$data = $this->read_line_col($line, $this->template->post_author_col);
				if ($data===false)
					return $this->log_line(sprintf(__('Invalid column name for post author: %s'), $this->template->post_author_col));
				else
					return $data;
				break;
		}
	}

	function import_post_type($line) {
		// Validate post type - note that this does NOT check if post type valid for current site!
		$index = array_search($this->template->post_type, get_post_types(array('public' => true), 'names'));
		if ($index === false || $index === null)
			return $this->log_line(sprintf(__('Invalid post type: "%s"'), $post_type));
		return $this->template->post_type;
	}

	function import_post_meta($line, $post_id, $options, $update = false) {
		// Add the metadata for new and existing custom fields
		$all_meta = array_merge((array)$this->template->post_custom, (array)$this->template->post_custom_new);

		// Get MapPress options - note that if importing into another blog, this will be the options in the TARGET blog
		if (class_exists('Mappress_Pro')) {
			$map_options = Mappress_Options::get();
			$metaKeyErrors = $map_options->metaKeyErrors;
		}

		// Add metadata; each key should have an associated input column
		foreach ($all_meta as $key => $col) {

			// If no input column specified, then skip this field (this can happen for existing custom fields, if the field wasn't mapped)
			if (empty($col))
				continue;

			// If updating, delete any existing values in the custom field
			if ($update)
				delete_post_meta($post_id, addslashes($key));

			$data = $this->read_line_col($line, $col);
			if ($data===false)
				return $this->log_line(sprintf(__('Invalid column name for custom field: %s'), $key));

			if (!empty($data)) {
				// Split data by custom field values separator (also allows escaped separators)
				$data = $this->explode_escaped($options['values_separator'], $data);

				// Add each data value separately
				foreach((array)$data as $datum) {
					// WP will re-serialize serialized data (turning it into a big string), so unserialize it before adding it
					if ( is_serialized( $datum ) ) {
						$datum = unserialize($datum);
					} else {
						// WP stripslashes on POST when adding metadata so add slashes first!
						$key = addslashes($key);
						$datum = addslashes($datum);
					}
					$result = add_post_meta($post_id, $key, $datum);
				}

				if ($result === false)
					return $this->log_line(sprintf(__('Error in add_post_meta for custom field: id="%s", key="%s", value="%s"'), $post_id, $key, $datum));
			}
		}

		// Check for any mappress errors from geocoding
		if (isset($metaKeyErrors) && !empty($metaKeyErrors)) {
			$errors = get_post_meta($post_id, $metaKeyErrors);
			if ($errors) {
				foreach($errors as $error) {
					$this->log_line(sprintf(__('Error in map creation: %s'), $error));
				}
				return $this->log_line(sprintf(__('Map is incomplete for post id="%s"'), $post_id));
			}
		}

		return true;
	}

	/**
	* Get the post date for the input file line
	*/
	function import_post_date($line) {
		switch ($this->template->post_date_type) {
			case 'TODAY':
				return null;  // Use the default (WP sets to 'Y-m-d H:i:s'
				break;

			case 'FIXED':
				// If type = FIXED then pick a random value between min and max dates
				// Convert min/max dates to a time() format using WordPress localization settings
				// NOTE: WP local time is NOT currently used for the other date/time settings like random dates
				$min = strtotime($this->template->post_date_min);

				// If max date = today, just use current time as the max.
				// Otherwise, we have to add a day (in seconds) to the given 'max' otherwise our range won't include that date
				if (date('m/d/Y', time()) == $this->template->post_date_max)
					$max = time();
				else
					$max = strtotime($this->template->post_date_max) + 86399;

				// Min/max will be false for invalid dates, i.e. if not MM/DD/YYYY
				if ($min === false)
					return $this->log_line(sprintf(__("Invalid mininum post date: '%s'"), $this->template->post_date_min));

				if ($max === false)
					return $this->log_line(sprintf(__("Invalid maximum post date: '%s'"), $this->template->post_date_max));

				// Min/max are OK, return a random date between them
				return date('Y-m-d H:i:s', strtotime('+' . rand(0, $max - $min) . 'seconds', $min));
				break;

		   case 'COLUMN':
				$data = $this->read_line_col($line, $this->template->post_date_col);
				if ($data===false)
					return $this->log_line(sprintf(__('Invalid column name for post date: %s'), $this->template->post_date_col));

				if (strtotime($data) === false)
					return $this->log_line(sprintf(__("Invalid post date in input file: '%s' in column %s"), $data, $this->template->post_date_col));

				return date('Y-m-d H:i:s', strtotime($data));
				break;
		}
	}

	function import_post_taxonomies($line, $options) {
		$post_taxonomies = array();

		// Output custom taxonomies
		$taxonomies = get_taxonomies(array('_builtin' => false));
		$taxonomies[] = 'category';
		$taxonomies[] = 'post_tag';

		foreach ($taxonomies as $taxonomy_name) {
			$result = $this->import_post_taxonomy($taxonomy_name, $line, $options);
			if (is_wp_error($result))
				return $result;

			if (!empty($result))
				$post_taxonomies[$taxonomy_name] = $result;
		}

		return $post_taxonomies;
	}

	function import_post_taxonomy($taxonomy_name, $line, $options) {
		$taxonomy = $this->template->taxonomies[$taxonomy_name];

		// Get WP taxonomy details
		$wp_taxonomy = get_taxonomy($taxonomy_name);
		if (!$wp_taxonomy)
			return $this->log_line(sprintf(__('Unknown taxonomy "%s"'), $taxonomy_name));

		$terms = array();  // Terms to set for this taxonomy

		// If no template data for this taxonomy, return empty array
		if (!$taxonomy)
			return array();

		// For fixed values, just return the terms to set for the post
		if ($taxonomy['type'] == 'FIXED') {
			// For hierarchical taxonomies, just return an array of the values to set
			if ($wp_taxonomy->hierarchical)
				return $taxonomy['values'];

			// For flat taxonomies WP requires a comma-separated string
			foreach((array)$taxonomy['values'] as $tag_ID)
				$terms[] = get_tag($tag_ID)->name;
			return implode(',', $terms);
		}

		// Columnv values; check that column is set, if not return empty array
		if (!$taxonomy['col'])
			return array();

		// Read data from input column
		$data = $this->read_line_col($line, $taxonomy['col']);
		if ($data === false)
			return $this->log_line(sprintf(__('Invalid column name for %s: %s'), $taxonomy_name, $taxonomy['col']));

		$data = trim($data);
		if (empty($data))
			return null;

		// Split by value separator (comma)
		// Removed - this didn't consider escaped commas: $term_names1 = preg_split("/[,]+/", $data);

		// Split data by custom field values separator (also allows escaped separators)
		$term_names1 = $this->explode_escaped($options['values_separator'], $data);

		if ($wp_taxonomy->hierarchical) {

			// Hierarchical taxonomies: also split by "|" separator
			foreach($term_names1 as $term_name1) {
				// Split by hierarchy separator
				$term_names2 = preg_split("/[" . $options['hierarchy_separator'] . "]+/", $term_name1);
				$parent_id = null;

				foreach((array)$term_names2 as $term_name) {
					// Create the term if it doesn't already exist
					$result = $this->insert_term($term_name, $taxonomy_name, $parent_id);
					if (is_wp_error($result))
						return $result;

					// Use this as parent for next term in hierarchy
					$parent_id = $result['term_id'];

					// Store array of terms for current post
					$terms[] = $result['term_id'];
				}
			}
		} else {
			// Flat taxonomies
			$parent_id = null;
			foreach($term_names1 as $term_name) {
				// Create the term if it doesn't already exist
				$result = $this->insert_term($term_name, $taxonomy_name, $parent_id);
				if (is_wp_error($result))
					return $result;

				// Store term for current post
				$terms[] = $term_name;
			}
		}

		return $terms;
	}

	function insert_term($term_name, $taxonomy_name, $parent_id = null) {
		global $blog_id;

		$term_name = trim($term_name);

		// If the term already exists in the given taxonomy, with the given parent, there's nothing to do
		// term_exists returns 0 if term is invalid, null if not found, and an array if valid
		$term = term_exists($term_name, $taxonomy_name, $parent_id);
		if ($term) {
			return $term;
		}

		// Set the parent
		$args = array('parent' => $parent_id, 'description' => $term_name);

		// Insert the term
		$result = wp_insert_term($term_name, $taxonomy_name, $args);

		if (is_wp_error($result))
			return $this->log_line(sprintf(__('Unable to create term "%s" in taxonomy "%s": %s'), $term_name, $taxonomy_name, $result->get_error_message() ));

		if (!$result)
			return $this->log_line(sprintf(__('Unable to create term "%s" in taxonomy "%s"'), $term_name, $taxonomy_name));

		// Get the parent name for future reference
		$parent_name = $parent_id; // Default to parent ID in case name is not available
		if ($parent_id) {
			$wp_parent = get_term($parent_id, $taxonomy_name);
			if (!is_wp_error($wp_parent) && $wp_parent)  // get_term returns either wp_error or null
				$parent_name = $wp_parent->name;
		}

		$this->imported_terms[$taxonomy_name][] = (object) array(
			'blog_id' => $blog_id,      // Remember blog ID for each term - used by undo function
			'parent' => $parent_id,
			'parent_name' => $parent_name,
			'term_id' => $result['term_id'],
			'term_taxonomy_id' => $result['term_taxonomy_id'],
			'name' => $term_name);
		return $result;
	}


	function read_line_col($line, $col) {
		// Get the column index by searching header column names
		$index = array_search($col, $this->headers);
		if ($index === false)
			return false;
		else {
			if (isset($line[$index]))
				return $line[$index];
			else
				return "";          // If column is empty it won't be in the line
		}
	}

	function get_last_revision($postid) {
		global $wpdb;

		// Get maximum revision # using raw query - wp_get_post_revisions() is filtered
		return $wpdb->get_var( $wpdb->prepare( "SELECT max(ID) FROM $wpdb->posts WHERE post_parent = %d AND post_type = 'revision'", $postid ) );
	}


	/**
	* Figure out if a line is blank.
	* According to the docs, fgetcsv() returns 1-element array with that element = null for blank lines
	* However, excel returns lines with just quotes in them, e.g. "", "",...
	*
	* @param mixed $line
	*/
	function is_blank($line) {
		// Basic fgetcsv() check
		if (count($line) == 1 && empty($line[0]))
			return true;

		// Look for array containing ONLY nulls
		if (count($line) == count(array_keys($line, null, true)))
			return true;

		// Look for lines containing ONLY ""
		if (count($line) == count(array_keys($line, "", true)))
			return true;

		// It's not blank
		return false;
	}

	function upload_file($files=null, $server_url=null, $server_path=null) {
		if(empty($files) && empty($server_url) && empty($server_path))
			return $this->log(__('No file specified'));

		if ($server_url)
			$this->fileurl = $server_url;

		elseif ($server_path)
			$this->filepath = $server_path;

		else {
			$filename = $files['name'];
			$tmp_name = $files['tmp_name'];

			// Read file contents from tmp file
			$file = file_get_contents($tmp_name);

			// If the file is in another format, try to convert it to UTF8
			$file = $this->utf8_convert($file);

			// Get wordpress upload directory
			$upload = wp_upload_dir();
			if (isset($upload['ERROR']))
				return new WP_Error('ERROR', __('Could not determine upload directory: ') . $upload['ERROR']);

			// Figure out file name
			$filename = wp_unique_filename( $upload['path'], $filename );
			$new_file = $upload['path'] . "/" . $filename;

			// Create directory if needed
			if(!wp_mkdir_p(dirname($new_file)))
				return $this->log(sprintf( __('Unable to create directory %s. Check permissions.'), dirname($new_file)));

			// Write the file to the upload directory
			$fp = fopen($new_file, 'wb');
			if (is_wp_error($fp))
				return $fp;

			fwrite($fp, $file);
			fclose($fp);

			// Set correct file permissions
			$stat = @ stat(dirname($new_file));
			$perms = $stat['mode'] & 0007777;
			$perms = $perms & 0000666;
			@ chmod($new_file, $perms);

			// Get URL and path
			$this->filepath = $new_file;
			$this->fileurl = $upload['url'] . "/" . $filename;
			$this->filename = $filename;
		}

		// Get the headers
		$result = $this->read_headers();
		if (is_wp_error($result))
			return $result;

		// Save the import object to the database
		$result = $this->save();
		if (is_wp_error($result))
			return $result;
	}

	function file_open() {
		// for Macs
		ini_set('auto_detect_line_endings', true);

		// Open the input file.  If we have a path use it, otherwise try URL
		if ($this->filepath)
			$fp = fopen($this->filepath, 'r');
		else
			$fp = fopen($this->fileurl, 'r');

		if ($fp)
			return $fp;
		else
			return $this->log(sprintf(__('Unable to open file: %s'), $this->fileurl));
	}

	function read_headers() {
		$options = get_option('turbocsv');
		$encoding = isset($options['encoding']) ? $options['encoding'] : null;

		// Read the first line to get headers
		$fp = $this->file_open();
		if (is_wp_error($fp))
			return $fp;

		//$headers = $this->fgetcsv_plus($fp, $options['input_delimiter'], $options['locale']);
		$headers = $this->fgetcsv_plus($fp, $options);

		// It's an error if there are no headers or if any header is empty
		if (empty($headers)) {
			fclose($fp);
			return $this->log(sprintf(__('Unable to read column headers from file %s'), $this->fileurl));
		}

		// Look for any empty headers
		foreach ((array)$headers as $key => $header) {
			if (empty($header)) {
				fclose($fp);
				return $this->log(sprintf(__('Header for column %s is empty.  Empty headers are not allowed'), $key));
			}
		}

		$this->headers = $headers;
		fclose($fp);
	}

	function log_line($message, $code='ERROR') {
		return $this->log($message, $code, $this->lines_total);
	}

	// The log is actually a set of wp_errors where 'msg' is an object with date, time and msg string
	function log($message, $code='ERROR', $line=null) {

		// We can accept a string or a wp_error object
		if (is_wp_error($message))
			$message = $message->get_error_message();

		// Save an enhanced 'message' - an object with the time, line # and the message
		$this->logs[] = (object) array('code' => $code, 'time' => time(), 'line' => $line, 'msg' => $message);

		// Return a singleton WP_error object for the error that was just saved
		return new WP_Error($code, $message);
	}

	function has_errors() {
		foreach((array)$this->logs as $log)
			if ($log->code == 'ERROR')
				return true;
	}

	function get_status($long=false, $icon=false) {
		$statuses = array(
			'NEW' => array('short' => __('New'), 'icon' => 'ready.png', 'long' => __('New')),
			'UNDO' => array('short' => __('Undo Complete'), 'icon' => 'undo.png', 'long' => __('Undo Complete')),
			'COMPLETE' => array('short' => __('Complete'), 'icon' => 'green_light.png', 'long' => __('Complete')),
			'ERROR' => array('short' => __('Error'), 'icon' => 'red_light.png', 'long' => __('Completed with errors')),
		);

		$status = $statuses[$this->status];
		if ($this->status == 'COMPLETE') {
			if ($this->has_errors())
				$status = $statuses['ERROR'];
		}

		if ($icon)
			$icon = "<img width='16' height='16' alt='" . $status['short'] . "' align='middle' src='" . plugins_url("/images/" . $status['icon'], __FILE__) . "'/> ";

		if ($long)
			return $icon . " <b>" . $status['long'] . "</b>";
		else
			return $icon . $status['short'];
	}

	/**
	* Workaround for WP internal bug in standard clean_term_cache() function.
	* That function uses a static variable and so really cleans only on the first term/category/tag insert.  It fails
	* if multiple terms are inserted at once.
	*
	* Code is copied from standard 3.0 clean_term_cache, with following patch applied: http://core.trac.wordpress.org/ticket/14485
	* Many months old and still not fixed in 3.2.1...
	*/

	function clean_term_cache() {
		global $wpdb, $blog_id;

		$original_blog_id = $blog_id;

		// Clear taxonomy caches
		foreach((array)$this->imported_terms as $taxonomy => $terms) {
			foreach ($terms as $term) {
				// Switch blogs if needed
				if ($term->blog_id != $blog_id) {
					$result = switch_to_blog($term->blog_id);

					// If unable to switch to the blog then skip it
					if ($result === false)
						continue;
				}

				wp_cache_delete('all_ids', $taxonomy);
				wp_cache_delete('get', $taxonomy);

				// Begin code from trac 14485 - hopefully it'll work
				$cache_keys = wp_cache_get( 'get_terms:cache_keys', 'terms' );
				if ( ! empty( $cache_keys ) ) {
					foreach ( $cache_keys as $key => $cache_taxonomies ) {
						if ( in_array( $taxonomy, $cache_taxonomies ) ) {
							wp_cache_delete( "get_terms:{$key}", 'terms' );
							unset( $cache_keys[$key] );
						}
					}
				} else {
					$cache_keys = array();
				}
				wp_cache_set( 'get_terms:cache_keys', $cache_keys, 'terms' );

				delete_option("{$taxonomy}_children");
				_get_term_hierarchy($taxonomy);
			}
		}

		// Switch back to original blog if needed
		if ($original_blog_id != $blog_id)
			switch_to_blog($original_blog_id);

		$wpdb->query("COMMIT");
	}

	/**
	* For debugging, dump the file contents to the screen.
	* Useful to determine if character conversion errors occur in PHP or MySQL (since this isn't using MySQL)
	*
	*/
	function dump_file() {
		$options = get_option('turbocsv');
		$encoding = $options['encoding'];

		$fp = $this->file_open();
		while (!feof($fp)) {

			$line = $this->fgetcsv_plus($fp, $options);

			if ($line) {
				foreach ($line as $key => $value)
					echo $key . ":" . $value;
			}
			echo "<br/><br/>";
		}
		fclose($fp);
	}


	/**
	* There are various problems with fgetcsv in different versions of PHP.
	* For example: if an accented character is found (like british pound sign) as first character after delimiter, it is discarded
	*
	* One workaround is to use set_locale() prior to calling fgetcsv, another is to simply replace fgetcsv() with a custom function.
	*
	* This routine provides the ability to do either workaround, or just use the standard fgetcsv().
	*
	* @param mixed $handle - file handle
	* @param mixed $options - the options array from: get_options('turbocsv')
	*/
	function fgetcsv_plus($fp, $options) {
		$input_delimiter = $options['input_delimiter'];
		$locale = $options['locale'];
		$custom_parser = $options['custom_parser'];

		// There's no HTML 'tab' so the tab delimiter is represented by the word 'tab'
		if ($input_delimiter == 'tab')
			$input_delimiter = "\t";

		// Use a completely custom parser
		if ($custom_parser)
			return $this->fgetcsv_custom($fp, $input_delimiter);

		// Use standard fgetcsv()
		if (!$locale || empty($locale))
			return fgetcsv($fp, 9999, $input_delimiter);

		// Use standard fgetcsv() but override the locale just during the fgetcsv() call
		$old_locale = setlocale(LC_ALL, 0);
		setlocale(LC_ALL, $locale);
		$result = fgetcsv($fp, $length, $input_delimiter);          // Don't pass enclosure/escape - even default values cause problems
		setlocale(LC_ALL, $old_locale);

		return $result;
	}

	function fgetcsv_custom($handle, $CSV_SEPARATOR = ',', $CSV_ENCLOSURE = '"') {
		// Note: since this function cannot tell if it's on Windows or Unix, it looks for BOTH \r\n and \n as a line ending!
		$CSV_LINEBREAK = "\n";
		$CSV_LINEBREAK_WIN = "\r";

		$string = fgets($handle);

		$o = array();
		$cnt = strlen($string);
		$esc = false;
		$escesc = false;
		$num = 0;
		$i = 0;
		while ($i < $cnt) {
			$s = $string[$i];
			$s_next = ($i < $cnt - 1) ? $string[$i+1] : null;

			if ($s == $CSV_LINEBREAK || ($s == $CSV_LINEBREAK_WIN && $s_next == $CSV_LINEBREAK)) {
			  if ($esc) {
				$o[$num] .= $s;
			  } else {
				if ($s == $CSV_LINEBREAK)
					$i++;
				else
					$i += 2;    // For windows, skip TWO characters
				break;
			  }
			} elseif ($s == $CSV_SEPARATOR) {
			  if ($esc) {
				$o[$num] .= $s;
			  } else {
				// To conform with fgetcsv() behavior, an empty field should be returned as empty array element!
				if (!isset($o[$num]))
					$o[$num] = '';

				$num++;
				$esc = false;
				$escesc = false;
			  }
			} elseif ($s == $CSV_ENCLOSURE) {
			  if ($escesc) {
				$o[$num] .= $CSV_ENCLOSURE;
				$escesc = false;
			  }

			  if ($esc) {
				$esc = false;
				$escesc = true;
			  } else {
				$esc = true;
				$escesc = false;
			  }
			} else {
			  if ($escesc) {
				$o[$num] .= $CSV_ENCLOSURE;
				$escesc = false;
			  }

			  if (isset($o[$num]))
				$o[$num].= $s;
			  else
				$o[$num] = $s;
			}

			$i++;
		}
		return $o;
	}


	function utf8_convert($file) {
		$options = get_option('turbocsv');

		$encoding = isset($options['encoding']) ? $options['encoding'] : null;

		// Remove BOMs and detect formats
		// Note that this covers only UTF8, and UTF16.  Other esoteric formats aren't covered (http://en.wikipedia.org/wiki/Byte_order_mark)
		if ( substr($file, 0, 3) == pack('CCC',  0xef, 0xbb, 0xbf) ) {
			$file = substr($file, 3);
			$detected = "UTF-8";
		} elseif (substr($file, 0, 2) == pack('CC', 0xfe, 0xff)) {
			$file = substr($file, 2);
			$detected = "UTF-16BE";
		} elseif (substr($file, 0, 2) == pack('CC', 0xff, 0xfe)) {
			$file = substr($file, 2);
			$detected = "UTF-16LE";
		} else {
			$detected = false;
		}

		// If user specified an encoding, always use it
		if ($encoding && $encoding != 'auto')
			return iconv($encoding, 'UTF-8//IGNORE', $file);

		// Auto detect - if something was detected then use it
		if ($detected)
			return iconv($detected, "UTF-8//IGNORE", $file);

		// If it looks like UTF8 without a header, then return the file unchanged
		if (utf8_encode(utf8_decode($file)) == $file)
			return $file;

		// Nothing detected, so just assume 1252
		return iconv("Windows-1252", 'UTF-8//IGNORE', $file);
	}

	/**
	* From php.net: a version of explode() that ignores escaped delimiters (standard version will still explode the delimiter, even if escaped)
	*
	* @param mixed $delimiter
	* @param mixed $string
	* @return string
	*/
	function explode_escaped($delimiter, $string){
		$exploded = explode($delimiter, $string);
		$fixed = array();
		for($k = 0, $l = count($exploded); $k < $l; ++$k){
			if($exploded[$k][strlen($exploded[$k]) - 1] == '\\') {
				if($k + 1 >= $l) {
					$fixed[] = trim($exploded[$k]);
					break;
				}
				$exploded[$k][strlen($exploded[$k]) - 1] = $delimiter;
				$exploded[$k] .= $exploded[$k + 1];
				array_splice($exploded, $k + 1, 1);
				--$l;
				--$k;
			} else $fixed[] = trim($exploded[$k]);
		}
		return $fixed;
	}
}    // End Class TI_Import


class TI_Import_Line extends TI_Obj {
	var $_id;
	var $updated;
	var $import_id;
	var $blog_id;
	var $revision_id;
	var $post_id;

	/**
	* Constructor.
	* Create a new import object and immediately save it to the database
	*
	*/
	function defaults() {
	}

	static function create_db() {
		global $wpdb;
		$table = $wpdb->prefix . 'ti_import_lines';

		// Note - TWO spaces required after PRIMARY KEY or dbdelta blows up!
		$result = $wpdb->query ("CREATE TABLE IF NOT EXISTS $table (
			id INT NOT NULL AUTO_INCREMENT,
			updated BOOLEAN,
			import_id INT NOT NULL,
			blog_id BIGINT(20),
			revision_id BIGINT(20),
			post_id BIGINT(20),
			PRIMARY KEY  (id),
			KEY import_id_key (import_id)
			) CHARACTER SET utf8
		");

		if ($result === false)
			return new WP_Error('fatal', "Error during create_db() on table $table");
	}


	static function delete($import_id) {
		global $wpdb;
		$table = $wpdb->prefix . 'ti_import_lines';
		$result = $wpdb->query($wpdb->prepare("DELETE FROM $table WHERE import_id = %d", $import_id));

		if ($result === false)
			return new WP_Error('fatal', "Error during delete from $table");
	}

	function save() {
		global $wpdb;
		$table = $wpdb->prefix . 'ti_import_lines';

		$result = $wpdb->query($wpdb->prepare("INSERT INTO $table (import_id, updated, blog_id, revision_id, post_id ) VALUES(%d, %d, %d, %d, %d)",
		$this->import_id, $this->updated, $this->blog_id, $this->revision_id, $this->post_id));

		if ($result === false)
			return new WP_Error('fatal', "Error during insert to $table");
	}
}



class TI_Template extends TI_Obj {
	var $_id;
	var $post_title;
	var $post_content;
	var $post_excerpt;
	var $post_type;
	var $blog_id_type;
	var $blog_id_col;
	var $blog_id;
	var $postid_col;
	var $update_field;
	var $post_status;
	var $comment_status;
	var $post_author_type;
	var $post_author;
	var $post_author_col;
	var $post_name_col;
	var $post_parent;
	var $page_template;
	var $menu_order;
	var $post_date_type;
	var $post_date_min;
	var $post_date_max;
	var $post_date_col;
	var $post_custom;
	var $post_custom_new;
	var $taxonomies;

	function defaults() {
		global $current_user;

		$defaults = array (
			'post_type' => 'post',
			'post_status' => 'publish',
			'comment_status' => 'open',
			'blog_id_type' => 'FIXED',
			'post_author_type' => 'FIXED',
			'post_author' => $current_user->ID,
			'page_template' => 'default',   // yes, WP requires the string 'default'
			'menu_order' => 0,
			'post_date_type' => 'TODAY',
			'post_date_min' => date('m/d/Y'),
			'post_date_max' => date('m/d/Y'),
		);
		return $defaults;
	}

	static function create_db() {
		return parent::create_db('ti_template', false);
	}

	static function get($id) {
		return parent::get('ti_template', $id);
	}

	static function get_list() {
		return parent::get_list('ti_template');
	}

	static function delete($id) {
		return parent::delete('ti_template', $id);
	}

	function save() {
		return parent::save('ti_template');
	}
}
?>