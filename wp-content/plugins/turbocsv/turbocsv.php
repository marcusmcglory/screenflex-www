<?php
/*
	Plugin Name: TurboCSV
	Plugin URI: http://www.wphostreviews.com/turbocsv
	Description: TurboCSV
	Version: 2.35
	Author: Chris Richardson
	Author URI: http://www.wphostreviews.com/turbocsv
*/
require_once dirname( __FILE__ ) . '/ti_obj.php';
require_once dirname( __FILE__ ) . '/ti_import.php';
@include_once dirname( __FILE__ ) . '/ti_pro.php';

class TI_Frontend {
	var $version = '2.35',
		$log_file,
		$option_defaults = array(
			'encoding' => 'auto',
			'input_delimiter' => ',',
			'max_skip' => 0,
			'commit_rows' => 100,
			'values_separator' => ',',
			'hierarchy_separator' => '|',
			'locale' => '',
			'custom_parser' => '',
			'max_log_records' => 500
		);

	function TI_Frontend() {
		global $wp_version, $wpdb;

		load_plugin_textdomain( 'turbocsv', false, dirname( plugin_basename( __FILE__ ) ) );

		add_action('admin_init', array(&$this, 'admin_init'));
		add_action('wp_ajax_ti_process', array(&$this, 'ajax_process') );
		add_action('admin_menu', array(&$this,'hook_admin_menu'));
		register_activation_hook(__FILE__, array(&$this, 'activation'));

		// Set log file path
		$this->log_file = trailingslashit(dirname(__FILE__)) . "turbocsv.log";

		// Create db tables with each new blog created if the plugin is network activated (note the workaround in admin_init() hook until 3.1)
		add_action('wpmu_new_blog', array(&$this, 'wpmu_new_blog'));

		$this->debugging();

		// Set option defaults
		$options = get_option('turbocsv');
		$options = shortcode_atts($this->option_defaults, $options);

		update_option('turbocsv', $options);
	}

	// ti_errors -> PHP errors
	// ti_info -> phpinfo + dump
	function debugging() {
		global $wpdb;

		$options = get_option('turbocsv');

		if (isset($_GET['ti_debug'])) {
			// See here in codex: http://codex.wordpress.org/Editing_wp-config.php#Configure_Error_Log
			@error_reporting(E_ALL);
			@ini_set('error_reporting', E_ALL);
			@ini_set('display_errors', 1);

			$wpdb->show_errors();
			if (!defined('WP_DEBUG')) {
				@define('WP_DEBUG', true);
			}
		}

		if (isset($_GET['ti_info'])) {
			$bloginfo = array('version', 'language', 'stylesheet_url', 'wpurl', 'url');
			echo "<br/><b>bloginfo</b><br/>";
			foreach ($bloginfo as $key=>$info)
				echo "$info: " . bloginfo($info) . "<br/>";
			echo "<br/><b>options</b><br/>";
			$options = get_option(turbocsv);
			print_r($options);
			echo "<br/><b>phpinfo</b><br/>";
			phpinfo();
			echo "<br/><b>Imports</b><br/>";
			$import_ids = TI_Import::get_list();
			foreach ($import_ids as $import_id) {
				$import = TI_Import::get($import_id);
				echo $import_id . ' | ' . $import->timestamp . ' | ' . $import->status . ' | ' . get_class($import);
				if (get_class($import) == 'stdClass')
					echo " <span class='error'>INVALID: class is StdClass</span>";
				echo "<br/>";
			}
		}

		// Force deletion of import history (useful if structure changes)
		if (isset($_GET['ti_delete'])) {
			$import_ids = TI_Import::get_list();
			foreach ($import_ids as $import_id) {
				$import = TI_Import::get($import_id);
				echo $import_id . ' | ' . $import->timestamp . ' | ' . $import->status . ' | ' . get_class($import);
				if (get_class($import) == 'stdClass') {
					echo " <span class='error'>DELETING</span>";
					TI_Import::delete($import_id);
					TI_Import_Lines::delete($import_id);
				}
				echo "<br/>";
			}
		}
	}

	function ajax_process() {
		// Read the import object from the database
		$id = (isset ($_POST['id'])) ? $_POST['id'] : null;
		$import = TI_Import::get($id);

		if (!$import) {
			die (sprintf(__("Unable to read the import id = "), $id));
		}

		// Update the template with POST variables
		$_POST = stripslashes_deep($_POST);   // Strip out slashes added by wordpress
		if (isset($_POST['template']))
			$import->template->update($_POST['template']);

		// Process the import
		$import->import();

		die (__('Your import has been processed!  Click to see the results.'));
	}

	/**
	* Activate plugin.
	* For multisite network activation this calls activate_site () for each blog.
	* This is a workaround for a WP bug - not likely to have fix until 3.2: http://core.trac.wordpress.org/ticket/14170
	*
	*/
	function activation() {
		global $wpdb;

		// Handle network activation
		if (is_multisite() && is_plugin_active_for_network(plugin_basename(__FILE__))) {
			// Save current blog
			$old_blog = $wpdb->blogid;

			// Get all blog ids
			$blogids = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs"));

			// Activate each blog
			foreach ($blogids as $blog_id) {
				switch_to_blog($blog_id);
				$this->activate_site();
			}

			// Switch back to original blog
			switch_to_blog($old_blog);
			return;
		} else {
			$this->activate_site();
		}
	}

	/**
	* Activate a single site
	*
	*/
	function activate_site() {
		TI_Import::create_db();
		TI_Import_Line::create_db();
		TI_Template::create_db();

		// Upgrade old templates for 2.21
		$old_version = get_option('turbocsv_version');

		if (!$old_version || $old_version < '2.21')
			$this->activation_221();

		// Remember the current version
		update_option('turbocsv_version', $this->version);
	}

	/**
	* If plugin is network-active in a multisite installation then activate the plugin when a new blog is created
	*
	* @param mixed $blog_id
	*/
	function wpmu_new_blog($blog_id) {
		// If a new blog is created AND the plugin is network activated, then activate it for the new blog as well
		if (is_multisite() && is_plugin_active_for_network(plugin_basename(__FILE__))) {
			switch_to_blog($blog_id);
			$this->activate_site();
			restore_current_blog();
		}
	}

	function activation_221() {
		$import_ids = TI_Import::get_list();
		foreach ($import_ids as $import_id) {
			$import = TI_Import::get($import_id);
			if ($import) {
				$import->template = $this->update_template_221($import->template);
				$import->save();
			}
		}

		$template_ids = TI_Template::get_list();
		foreach ($template_ids as $template_id) {
			$template = TI_Template::get($template_id);
			if ($template) {
				$template = $this->update_template_221($template);
				$template->save();
			}
		}
	}

	function update_template_221($template) {
		if (isset($template->post_category_type)) {

			if ($template->post_category_type == 'COLUMN' && isset($template->post_category_col) && !empty($template->post_category_col))
				$template->taxonomies['category'] = array('type' => 'COLUMN', 'col' => $template->post_category_col);

			if ($template->post_category_type == 'FIXED' && isset($template->post_category) && !empty($template->post_category))
				$template->taxonomies['category'] = array('type' => 'FIXED', 'values' => $template->post_category);
		}

		if (isset($template->post_tag_type)) {
			if ($template->post_tag_type == 'COLUMN' && isset($template->post_tag_col) && !empty($template->post_tag_col))
				$template->taxonomies['post_tag'] = array('type' => 'COLUMN', 'col' => $template->post_tag_col);
			if ($template->post_tag_type == 'FIXED' && isset($template->post_tags) && !empty($template->post_tags))
				$template->taxonomies['post_tag'] = array('type' => 'FIXED', 'values' => $template->post_tags);
		}

		if (isset($template->post_taxonomy_cols) && !empty($template->post_taxonomy_cols)) {
			foreach((array) $template->post_taxonomy_cols as $tax_col_name => $tax_col)
				$template->taxonomies[$tax_col_name] = array('type' => 'COLUMN', 'col' => $tax_col);
		}

		return $template;
	}

	function admin_init() {
		// There's a WP bug - wpmu_new_blog action is not triggered if using activation (WP bug: http://core.trac.wordpress.org/ticket/14718)
		// Workaround below is to check if we need to activate every time the plugin runs
		if (is_plugin_active_for_network(plugin_basename(__FILE__))) {
			if (!get_option('turbocsv_version'))
				$this->activate_site();
		}
	}

	function hook_admin_menu() {
		// Add a single menu (the '' parent slug will create the menu but hide it on-screen)
		$pages[] = add_management_page(__('TurboCSV'), __('TurboCSV'), 'import', 'ti_new', array(&$this, 'control'));
		$pages[] = add_submenu_page('', 'TurboCSV - View Import', 'ti_display', 'import', 'ti_display', array(&$this, 'control'));
		$pages[] = add_submenu_page('', __('TurboCSV - Import History'), __('History'), 'import', 'ti_history', array(&$this, 'control'));
		$pages[] = add_submenu_page('', __('TurboCSV - Settings'), __('Settings'), 'import', 'ti_settings', array(&$this, 'control'));

		// Load scripts/styles for our pages only
		foreach ($pages as $page) {
			add_action('admin_print_scripts-' . $page, array(&$this, 'hook_print_scripts'));
			add_action('admin_print_styles-' . $page, array(&$this, 'hook_print_styles'));
	   }
	}

	function hook_print_scripts() {
		// Normal operation
		$cluetip_js = plugins_url("/cluetip/jquery.cluetip.js", __FILE__);
		$turbocsv_js = plugins_url("/turbocsv.js", __FILE__);

		wp_enqueue_script('cluetip', $cluetip_js, array('jquery', 'jquery-form', 'jquery-ui-dialog', 'jquery-ui-tabs'), '1.0.7');
		wp_enqueue_script('turbocsv', $turbocsv_js, false, $this->version, false);
		wp_enqueue_script('dashboard');  // Collapsible menus
	}

	function hook_print_styles() {
		wp_enqueue_style("turbocsv", plugins_url("/turbocsv.css", __FILE__), FALSE, $this->version);
		wp_enqueue_style("cluetip", plugins_url("/cluetip/jquery.cluetip.css", __FILE__), FALSE, '1.0.7');
	}

	function get_template_from_columns($import) {

		$meta_keys = $this->get_meta_keys();

		foreach($import->headers as $header) {
			// Try to match based on TRIMMED header (i.e. even if user left spaces before/after)
			switch(trim($header)) {
				case '!blog_id':
					// It's only allowed to import a blog_id from the main site, otherwise just ignore it
					if (is_multisite() && is_main_site()) {
						$import->template->blog_id_type = 'COLUMN';
						$import->template->blog_id_col = $header;
					}
					break;

				case '!post_content':
					$import->template->post_content = "#$header#";
					break;

				case '!post_excerpt':
					$import->template->post_excerpt = "#$header#";
					break;

				case '!post_author':
					$import->template->post_author_col = $header;
					$import->template->post_author_type = 'COLUMN';
					break;

				case '!post_name':
					$import->template->post_name_col = $header;
					break;

				case '!id':
					$import->template->postid_col = $header;

				case '!post_title':
					$import->template->post_title = "#$header#";
					break;

				case '!post_category':
					$import->template->taxonomies['category'] = array('type' => 'COLUMN', 'col' => $header, 'values' => array());
					break;

				case '!tags_input':
					$import->template->taxonomies['post_tag'] = array('type' => 'COLUMN', 'col' => $header, 'values' => array());
					break;

				case '!post_date':
					$import->template->post_date_type = 'COLUMN';
					$import->template->post_date_col = $header;
					break;

				default:
					// If first character = '!', it's a custom taxonomy, otherwise it's a custom field
					if (substr($header, 0, 1) == '!') {
						$taxonomy_name = (substr($header, 1));
						$import->template->taxonomies[$taxonomy_name] = array('type' => 'COLUMN', 'col' => $header, 'values' => array());
					} else {
						// Custom field - default it if it already exists, otherwise leave it unselected (for a new custom field)
						if (array_search($header, $meta_keys) !== false)
							$import->template->post_custom[$header] = $header;
//						else
//							$import->template->post_custom_new[$header] = $header;
					}
			}
		}
	}

	function control() {
		global $blog_id;
		$_POST = stripslashes_deep($_POST);     // Strip out slashes added by wordpress
		$page = (isset($_GET['page'])) ? $_GET['page'] : null;
		$id = (isset($_GET['id'])) ? $_GET['id'] : null;
		$cmd = (isset($_GET['cmd'])) ? $_GET['cmd'] : null;
		$noheader = (isset($_GET['noheader'])) ? true : false;

		$to = add_query_arg(array('page' => $page, 'id' => $id));

		switch ($page) {
			case 'ti_new':
				if (isset($_POST['button_file_upload']) || isset($_POST['button_file_url']) || isset($_POST['button_file_path'])) {

					// Create an import object
					$import = new TI_Import();

					// File upload
					$input_file = (isset($_FILES['input_file'])) ? $_FILES['input_file'] : null;
					$server_url = (isset($_POST['server_url'])) ? $_POST['server_url'] : null;
					$server_path = (isset($_POST['server_path'])) ? $_POST['server_path'] : null;
					$result = $import->upload_file($input_file, $server_url, $server_path);
					if(is_wp_error($result)) {
						$error = $result->get_error_message();
						break;
					}

					$this->get_template_from_columns($import);
					$import->save();

					$to = add_query_arg(array('page' => 'ti_display', 'id' => $import->_id), $to);
					break;
				}

				if (!$noheader) {
					$this->view_header($page);
					$this->view_upload();
					$this->view_footer();
				}
				break;

			case 'ti_display':
				$import = TI_Import::get($id);

				if (is_wp_error($import) || !$import)
					wp_die(sprintf(__('Internal error.  Unable to read import with id=%s: %s'), $id, $import->get_error_message() ));

				// If the import has just finished - WP clean_term_cache() has internal bug: it uses a static variable
				// so cache cannot be cleared until a new page is displayed.  See http://core.trac.wordpress.org/ticket/14485
				if (isset($_GET['cache']))
					$import->clean_term_cache();

				// Save template
				if (isset($_POST['button_save_template'])) {
					$save_id = $_POST['template_save_id'];
					if (!$save_id) {
						$error = __('Enter a template name.');
						break;
					}

					// Update import from $_POST and assign ID
					$import->template->update($_POST['template']);
					$import->template->_id = $save_id;

					// Save the import
					$result = $import->save();
					if (is_wp_error($result)) {
						$error = __('Error saving import: ') . $result->get_error_message('ERROR');
						break;
					}

					// Save the template
					$result = $import->template->save();
					if (is_wp_error($result)) {
						$error = __('Error saving template: ') . $result->get_error_message('ERROR');
						break;
					}

					// Everything was OK
					$message = __('Template saved.');
					break;
				}

				// Load template
				if (isset($_POST['button_load_template'])) {
					$load_id = $_POST['template_load_id'];

					// If 'new template' then create a new one; otherwise load the specified template
					if (!$load_id) {
						$import->template = new TI_Template();
						$this->get_template_from_columns($import);
					} else {
						$template = TI_Template::get($load_id);

						if (is_wp_error($template)) {
							$error = __('Error loading template: ') . $template->get_error_messages();
							break;
						} else {
							$import->template = $template;
						}
					}

					// Assign the template to the import and save the import
					$result = $import->save();
					if (is_wp_error($result)) {
						$error = __('Error saving import: ') . $result->get_error_messages();
						break;
					}

					// Everything was OK
					 $message = __('Template loaded');
					 break;
				}

				// Delete template
				if (isset($_POST['button_delete_template'])) {
					$result = TI_Template::delete($_POST['template_load_id']);
					if (is_wp_error($result)) {
						$error = __('Error deleting template: ') . $result->get_error_messages();
						break;
					}

					// Set the import to a blank template and save the import
					$import->template = new TI_Template();
					$result = $import->save();
					if (is_wp_error($result)) {
						$error = __('Error saving import: ') . $result->get_error_messages();
						break;
					}

					// Everything was OK
					 $message = __('Template deleted');
					 break;
				}

				if (!$noheader) {
					$this->view_header($page, $import);
					$this->view_display($import);
					$this->view_footer($import);
				}

				// For TESTING: to check character set conversion dump the file to screen if 'ti_test' is set
				if (isset($_GET['ti_test']))
					$import->dump_file();

				break;

			case 'ti_history':
				// Handle commands from history section
				if ($noheader) {
					$to = add_query_arg(array('cmd' => null, 'id' => null), $to);

					switch ($cmd) {
						case 'delete':

							$result = TI_Import::delete($id);
							if (is_wp_error($result)) {
								$error = $result->get_error_message();
							} else {
								$result = TI_Import_Line::delete($id);
								if (is_wp_error($result))
									$error = $result->get_error_message();
								else
									$message = __('Import deleted.');
							}
							break;

						case 'undo':
							if (method_exists('TI_Pro', 'undo')) {
								$result = $this->undo($id);
								if (is_wp_error($result))
									$error = $result->get_error_message();
								else
									$message = __('Undo complete.  Check import log for any messages.');

								$to = add_query_arg(array('page' => 'ti_display', 'id' => $id), $to);
							} else {
								$error = __('Sorry, undo is only available in the PRO version');
							}

							break;
					}
				} else {
					$this->view_header($page);
					$this->view_history();
					$this->view_footer();
				}

				break;

			case 'ti_settings':
				// Save settings
				if (isset($_POST['save']) && isset($_POST['turbocsv'])) {
					$options = shortcode_atts($this->option_defaults, $_POST['turbocsv']);
					update_option('turbocsv', $options);
					$message = __('Options saved');
					break;
				}

				if (isset($_POST['reset'])) {
					$options = $this->option_defaults;
					update_option('turbocsv', $options);
					$message = __('Options reset');
					break;
				}

				if (!$noheader) {
					$this->view_header($page);
					$this->view_settings();
					$this->view_footer();
				}

				break;
		}

		// Save any error/warning messages for display
		if (isset($error))
			update_option('turbocsv_error', $error);
		if (isset($message))
			update_option('turbocsv_message', $message);

		// Redirect to next page if needed
		if($noheader) {
			$to = add_query_arg(array('noheader' => null), $to);
			wp_redirect($to);
			exit();
		}
	}

	function view_upload() {
		// Max filesize
		$bytes = apply_filters( 'import_upload_size_limit', wp_max_upload_size() );
		$size = round($bytes / 1048576 , 2) . " MB";

		echo $this->memory_usage(true);

		$this->postbox_start(__('New Import'), "newimport");
		?>
			<p>
				<?php
					printf(__('<p>Specify the input file below.  Based on your PHP settings the maximum file size is: <b>%s</b>.</p>' ), $size );
				?>
			</p>
			<p>
				<?php echo __('<p>Files must be in .CSV format with headers in the first row (') . "<a href='" . plugins_url("/sample.csv", __FILE__) . "'>" . __('see a sample file') . "</a>)</p>"; ?>
			</p>

			<form method='post' action='<?php echo esc_attr(add_query_arg( 'noheader', 'true' )); ?>' enctype='multipart/form-data'>
				<table class="form-table">
					<tr valign='top'><th scope='row'><?php _e('PC File:')?></th>
						<td>
							<input type="file" name="input_file" size="25" />
							<input type="hidden" name="max_file_size" value="<?php echo $bytes; ?>" />
							<input type="submit" class="button" name='button_file_upload' value="<?php _e('Load File')?>" />
						</td>
					</tr>
				</table>
			</form>

			<form method='post' action='<?php echo esc_attr(add_query_arg( 'noheader', 'true' )); ?>'>
				<table class="form-table">
					<tr valign='top'><th scope='row'><?php _e('File URL:')?></th>
						<td>
							<input type='text' name='server_url' size='25' />
							<input type='submit' class='button' name='button_file_url' value='<?php _e('Load file')?>' />
						</td>
					</tr>
				</table>
			</form>

			<form method='post' action='<?php echo esc_attr(add_query_arg( 'noheader', 'true' )); ?>'>
				<table class="form-table">
					<tr valign='top'><th scope='row'><?php _e('Server file path:')?></th>
						<td>
							<input type='text' name='server_path' size='25' />
							<input type='submit' class='button' name='button_file_path' value='<?php _e('Load file')?>' />
						</td>
					</tr>
				</table>
			</form>
		<?php
		$this->postbox_end();
	}

	function view_display($import) {
		// If the import is in status 'new' then re-read the file headers in case they've changed
		if ($import->status == 'NEW')
			$import->read_headers();

		?>
			<form method='post' id='form_import' action='<?php echo esc_attr(add_query_arg( array('noheader' => 'true', 'page' => 'ti_display' ))); ?>'>
				<?php $this->view_action($import); ?>
				<?php $this->view_status($import); ?>
				<?php $this->view_template($import); ?>
			</form>
		<?php
	}

	function view_template($import) {
		// Todo: non-pro template
	}

	function view_action($import) {
		if ($import->status == 'NEW') {
			$html = "<input type='submit' class='button-primary' name='button_process' value='" . __('Start the Import!') . "' />"
				."<span id='ti_twizzler' class='ti-hidden ti-twizzler'>"
				. "<img alt='Please wait...' src='" . plugins_url("/images/saving.gif", __FILE__) . "' /> "
				. __('Importing...')
				. "</span><div><br/></div>";
			$this->option_formatted('', $html);
		}
	}

	function view_status($import) {
		global $wpdb, $blog_id;

		$options = get_option('turbocsv');

		$original_blog_id = $blog_id;

		switch ($import->status) {
			case 'NEW':
				$postbox_title = __('New Import');
				break;
			default:
				$postbox_title = __('Import Results');
		}

		$this->postbox_start($postbox_title, "status");
		?>
			<input type='hidden' name='id' value='<?php echo $import->_id?>' />
			<table class="form-table">
				<?php
					$this->option_formatted(__('Status'), $import->get_status(true, true));

					if ($import->fileurl)
						$this->option_formatted(__('File URL'), "<a href='{$import->fileurl}'>{$import->fileurl}</a>");

					if ($import->filepath)
						$this->option_formatted(__('File path'), $import->filepath);

					if ($import->status !== 'NEW') {

						// Memory
						$this->option_formatted(__("Peak memory usage"),  round( $import->max_memory / 1024 / 1024, 2 ) . " MB" );


						// Taxonomies
						foreach((array)$import->imported_terms as $taxonomy => $terms) {

							// If the taxonomy exists in current blog get the label, otherwise show the id
							$tax_obj = get_taxonomy($taxonomy);
							if ($tax_obj)
								$label = $tax_obj->label;
							else
								$label = $taxonomy;
							$this->option_formatted($label, $this->get_term_links($taxonomy, $terms));
						}

						// Imported posts
						foreach((array)$import->get_imported_posts() as $post) {

							// Switch to the imported blog ID if there was one
							if (is_multisite() && $post['blog_id'] && $post['blog_id'] != $blog_id)
								switch_to_blog($post['blog_id']);

							// Get the most recent title, etc. for the post - it may have changed since the import
							$edit_link = get_edit_post_link($post['post_id']);
							$title = get_the_title($post['post_id']);

							// 'revision' = null means no update, but revision= FALSE or revision=postID means updated
							$action = isset($post['updated']) ? __('Updated') : __('Created');

							if ($edit_link)
								$edit_link = "<a href='$edit_link' title='$title'>$title</a>";
							else
								$edit_link = '(deleted)';

							if (is_multisite())
								$post_links[] = array('data' => array($post['blog_id'], $post['post_id'], $edit_link, $action));
							else
								$post_links[] = array('data' => array($post['post_id'], $edit_link, $action));

							// Switch back to original blog ID
							if (is_multisite() && $blog_id != $original_blog_id)
								switch_to_blog($original_blog_id);

							if (count($post_links) >= $options['max_log_records'])
								break;
						}

						if (is_multisite())
							$headers = array(__('Blog ID'), __('Post ID'), __('Title'), __('Action'));
						else
							$headers = array(__('Post ID'), __('Title'), __('Action'));

						if (empty($post_links))
							$html = __('None');
						else
							$html = $this->option_table($headers, $post_links);

						if (count($import->get_imported_posts() ) > $options['max_log_records']) {
							$html .= "<div>";
							$html .= sprintf(__('Displaying %d of %d records'), $options['max_log_records'], count($import->get_imported_posts() ));
							$html .= __(' (you can increase this in the plugin settings)');
							$html .= "</div>";
						}


						$this->option_formatted(__('Posts/pages'), $html);

						// Import log, sorted with most recent first
						$messages = $import->logs;
						if (is_array($messages))
							krsort($messages);

						$rows = null;
						foreach ((array)$messages as $message) {
							switch ($message->code) {
								case "WARNING":
									$class = "updated";
									$msg = "WARNING: " . $message->msg;
									break;

								case "ERROR":
									$class = "error";
									$msg = "ERROR: " . $message->msg;
									break;

								default:
									$class = "";
									$msg = $message->msg;
									break;
							}

							$date = str_replace(' ', '&nbsp;', date("M d, Y G:i:s", $message->time)); // Fill date w/nbsp so it doesn't wrap in table
							$rows[] = array('class' => $class, 'data' => array($date, $message->line, $msg));
						}

						$html = $this->option_table(array(__('Time'), __('Line'), __('Message')), $rows);
						$this->option_formatted(__('Log'), $html);
					}
				?>
			</table>
		<?php
		$this->postbox_end();
	}

	function get_term_links($taxonomy, $terms) {
		global $blog_id;

		$original_blog_id = $blog_id;

		foreach((array)$terms as $term) {
			// Switch blog ID if the term was imported to a different blog, in order to get the correct link
			if (is_multisite() && $term->blog_id)
				switch_to_blog($term->blog_id);

			// If the term still exists show the current name & link, otherwise use the original name
			$wp_term = get_term($term->term_id, $taxonomy);
			if (!is_wp_error($wp_term) && $wp_term)           // get_term() returns either wp_error or null
				$term->name = $wp_term->name;

			// Get parent name
			if ($term->parent) {
				$wp_parent = get_term($term->parent, $taxonomy);
				if (!is_wp_error($wp_parent) && $wp_parent)  // get_term returns either wp_error or null
					$term->parent_name = $wp_parent->name;
			}

			if ($term->parent_name)
				$description = $term->parent_name . " - " . $term->name;
			else
				$description = $term->name;

			$links[] = sprintf("<a href='%s' title='%s (%d)'>%s</a>",
				admin_url('edit-tags.php?taxonomy=' . $taxonomy),
				esc_attr($description),
				$term->term_id,
				esc_attr($description));
		}

		// Switch back to original blog ID
		if (is_multisite() && $blog_id != $original_blog_id)
			switch_to_blog($original_blog_id);

		if (isset($links))
			return implode(", ", $links);
		else
			return 'none';
	}

	function view_history() {
		// Get list of past imports
		$ids = TI_Import::get_list();
		if (!empty($ids))
			krsort($ids);

		$this->postbox_start(__('Import History'), 'history');
		?>
			<?php
				foreach ((array)$ids as $id) {
					$import = TI_Import::get($id);
					$date_link = "<a href='" . esc_attr(add_query_arg(array('noheader'=>null, 'page' => 'ti_display', 'id' => $import->_id))) . "'>"
						. date("M d, Y G:i:s", $import->timestamp) . "</a>";

					$delete_link = "<a class='ti_delete_link' href='" . esc_attr(add_query_arg(array('noheader'=>'true', 'cmd' => 'delete', 'id' => $import->_id))) . "'>" . __('Delete') . "</a>";

					if ($import->status == 'COMPLETE' || $import->status == 'ERROR')
						$undo_link = " | <a class='ti_undo_link' href='" . esc_attr(add_query_arg(array('noheader'=>'true', 'cmd' => 'undo', 'id' => $import->_id))) . "'>" . __('Undo') . "</a>";
					else
						$undo_link = "";

					$rows[] = array('class' => null, 'data' => array(
						$import->get_status(false, true),
						$import->filename,
						$date_link,
						$import->lines_total,
						$delete_link . $undo_link
						)
					);
				}

				if (!isset($rows))
					$rows[] = array('class' => null, 'data' => array(__('No imports', 'turbocsv'), '', '', '', ''));

				$html = $this->option_table(array(__('Status'), __('File Name'), __('Date'), __('Lines'), __('Action')), $rows, '', '', 'ti-large-scroll');
				echo $html;
			?>
		<?php
		$this->postbox_end();
	}

	function view_settings() {
		$options = get_option('turbocsv');
		extract($options);

		$encoding_values = array(
			'auto' => 'Auto-detect',
			'utf8' => 'UTF-8',
			'ISO-8859-1' => 'ISO-8859-1',
			'ISO-8859-2' => 'ISO-8859-2',
			'Windows-1250' => 'Windows-1250 (Polish, Czech, Slovak, Hungarian, Slovene, Serbian, Croatian, Romanian and Albanian)',
			'Windows-1251' => 'Windows-1251 (Cyrillic)',
			'Windows-1252' => 'Windows-1252 (ANSI)',
			'Windows-1253' => 'Windows-1253 (Greek)',
			'Windows-1254' => 'Windows-1254 (Turkish)',
			'Windows-1255' => 'Windows-1255 (Hebrew)',
			'Windows-1256' => 'Windows-1256 (Arabic)',
			'Windows-1257' => 'Windows-1257 (Baltic)',
			'Windows-1258' => 'Windows-1258 (Vietnamese)'
		);

		$input_delimiter_values = array(',' => ',', ';' => ';', '|' => 'pipe (|)', 'tab' => "tab (\\t)");
		$max_skip_values = array('0' => '- none -', '10' => '10', '50' => '50', '100' => '100', '999999' => 'Unlimited');

		$log_file_link = "<a href='" . $this->log_file . "'>Log File</a>";

		$this->postbox_start(__('Options'), 'options');
		?>
			<form method="post" action="<?php echo esc_attr(add_query_arg( 'noheader', 'true' )); ?>">
				<?php wp_nonce_field('ti-options'); ?>

				<table class="form-table">
					<?php
						$html = $this->memory_usage();
						$this->option_formatted(__('Memory usage'), $html);

						$html = $this->option_dropdown('turbocsv[encoding]', $encoding, $encoding_values, false);
						$tip = __("TurboCSV will try to auto-detect both UTF-8 and Windows files or you can use this field to force it to expect a specific encoding.");
						$this->option_formatted(__('Input file encoding'), $html, $tip);

						$html = $this->option_dropdown('turbocsv[input_delimiter]', $input_delimiter, $input_delimiter_values, false);
						$tip = __("This is the field separator - the character that separates input fields in your file.<br/>The default delimiter is the comma.  You will need to select a semicolon for some European-style CSV files.");
						$this->option_formatted(__('Field separator'), $html, $tip);

						$html = $this->option_dropdown('turbocsv[max_skip]', $max_skip, $max_skip_values, false);
						$tip = __("Normally any errors will cause the import to halt.  However, you can also enter a maximum number of line errors to ignore before the import is stopped.  This can be useful if the file may contain many errors and you want to do a full test run before correcting them.");
						$this->option_formatted(__('Skip lines with errors'), $html, $tip);

						$html = $this->option_text('turbocsv[commit_rows]', $commit_rows, 3);
						$tip = __('Number of rows to process before changes are committed to the database during import.  A small number can slow down imports but will also prevent running out of memory for large imports.  The default is to commit every 100 rows.');
						$this->option_formatted(__('Commit rows'), $html, $tip);

						$html = $this->option_text('turbocsv[values_separator]', $values_separator, 1);
						$tip = __('Values separator for multiple values within a single field (used for tags, categories and custom fields).  The default is a comma.  If your data values contains commas (such as addresses) you can change this separator, or escape the commas with a backslash.  For example: "a\,b" is treated as a single value.');
						$this->option_formatted(__('Values separator'), $html, $tip);

						$html = $this->option_text('turbocsv[hierarchy_separator]', $hierarchy_separator, 1);
						$tip = __('Values separator for hierarchical taxonomies.');
						$this->option_formatted(__('Hierarchy separator'), $html, $tip);

						$html = $this->option_text('turbocsv[locale]', $locale, 20);
						$tip = __('Normally this should not be checked!  Some versions of PHP have bugs in the fgetcsv() function.  Forcing the locale may be used as a workaround.  Check this box to call setlocale() with the specified locale just before all fgetcsv() calls.".  ');
						$this->option_formatted(__('Locale'), $html, $tip);

						$html = $this->option_checkbox('turbocsv[custom_parser]', $custom_parser);
						$tip = __('Normally this should not be checked!  Some versions of PHP have bugs in the fgetcsv().  You can check this box to import using the TurboCSV custom parser.  The custom parser is slower but it is consistent on all PHP installations.".  ');
						$this->option_formatted(__('Custom parser'), $html, $tip);

						$html = $this->option_text('turbocsv[max_log_records]', $max_log_records, 4);
						$tip = __('TurboCSV displays a log of all of the posts that were created during an import.  This can be slow for large imports, so you can limit the number of posts displayed using this setting.');
						$this->option_formatted(__('Max. posts to display'), $html, $tip);
					?>
				</table>

				<p class="submit">
					<input type="submit" name="save" class="button-primary" value="<?php _e('Save Changes') ?>">
					<input type="submit" name="reset" class="button" value="<?php _e('Reset Defaults') ?>">
				</p>
			</form>
		<?php
		$this->postbox_end();
	}

	function view_header($page, $import=null) {
		$new_link = "<a href='" . esc_attr(add_query_arg(array('page' => 'ti_new', 'id' => null))) . "'>" . __('New Import') . "</a>";
		$history_link = "<a href='" . esc_attr(add_query_arg(array('page' => 'ti_history', 'id' => null))) . "'>" . __('History') . "</a>";
		$settings_link = "<a href='" . esc_attr(add_query_arg(array('page' => 'ti_settings', 'id'=>null))) . "'>" . __('Settings') . "</a>";
		$bug_link = "<a href='http://wphostreviews.com/chris-contact'>" . __("Support") . "</a>";
		$help_link = "<a href='http://wphostreviews.com/turbocsv/turbocsv-documentation'>" . __("Documentation") . "</a>";

		// Main 'wrap' div and postbox container
		echo "<div class='wrap'>";
		$this->postbox_container_start();

		wp_nonce_field('ti-import');

		echo "<h2>TurboCSV <span style='font-size: 12px'>$this->version</span></h2>";
		echo "<div class='ti-help'>$help_link | $bug_link<br/></div>";

		$new_link = "<a href='" . esc_attr(add_query_arg(array('page' => 'ti_new', 'id' => null))) . "'>" . __('New Import') . "</a>";
		$history_link = "<a href='" . esc_attr(add_query_arg(array('page' => 'ti_history', 'id' => null))) . "'>" . __('History') . "</a>";
		$settings_link = "<a href='" . esc_attr(add_query_arg(array('page' => 'ti_settings', 'id'=>null))) . "'>" . __('Settings') . "</a>";

		switch ($page) {
			case 'ti_new':
				$new_link = "<b>" . $new_link . "</b>";
				break;

			case 'ti_display':
			case 'ti_history':
				if (isset($import) && $import->status == 'NEW')
					$new_link = "<b>" . $new_link . "</b>";
				else
					$history_link = "<b>" . $history_link . "</b>";
				break;

			case 'ti_settings':
				$settings_link = "<b>" . $settings_link . "</b>";
				break;
		}

		echo "<div>" . $new_link . " | " . $history_link . " | " . $settings_link . "</div><br/>";

		$error = get_option('turbocsv_error');
		delete_option('turbocsv_error');
		$message = get_option('turbocsv_message');
		delete_option('turbocsv_message');

		if ($message)
			echo "<div id='message' class='updated fade'><p>$message</p></div>";

		if ($error)
			echo "<div id='error' class='error'><p>$error</p></div>";
	}


	function view_footer($import=null) {
		// Close the 'wrap' div
		$this->postbox_container_end();
		echo "</div>";

		echo "<script type='text/javascript'>        /* <![CDATA[ */\r\n";

		// If display mode, disable all fields except log button
		if (isset($import) && $import->status != 'NEW') {
			echo "jQuery('#form_import :input').attr('disabled', true); \r\n";
		}

		echo 'var til10n = ' . json_encode (array (
			'no_template' => __('No template selected.'),
			'no_template_name' => __('Enter a template name to save.'),
			'no_post_title' => __('Enter the post title.'),
			'confirm_process' => __('Please BACK UP your wordpress database before proceeding! Are you ready to start the import?'),
			'confirm_undo' => __('Please BACK UP your wordpress database before proceeding!  Are you ready to undo?'),
			'confirm_template_delete' => __('Are you sure you want to delete the template "%s"?'),
			'confirm_import_delete' => __('Caution!  If you delete an import you cannot undo it later.  Are you sure you want to delete?')
		)) . ";\r\n" ;

		echo "/* ]]> */ </script>";

		wp_tiny_mce(false);
	}

	function postbox_container_start($echo=true) {
		$html = "<div class='postbox-container'>"
			. "<div class='metabox-holder'>"
			. "<div class='meta-box-sortables'>";
		if ($echo)
			echo $html;
		else
			return $html;
	}

	function postbox_container_end($echo=true) {
		$html = "</div></div></div>";
		if ($echo)
			echo $html;
		else
			return $html;
	}

	function postbox_start($title, $id="", $open=true, $echo=true) {
		$class = ($open) ? "postbox" : "postbox closed";
		$html = "<div id='$id' class='$class'>"
			. "<div class='handlediv' title='" . __('Click to toggle') . "'><br /></div>"
			. "<h3 class='hndle'><span>$title</span></h3>"
			. "<div class='inside'>";
		if ($echo)
			echo $html;
		else
			return $html;
	}

	function postbox_end($echo=true) {
		$html = "</div></div>";
		if ($echo)
			echo $html;
		else
			return $html;
	}


	function option($import, $field) {
		switch ($field) {

			case 'template[post_status]':
				$html = $this->option_dropdown($field, $import->template->post_status, array('publish'=>'Published', 'draft'=>'Draft', 'pending'=>'Pending Review'));
				$tip = __('WordPress post status - see <a href="http://codex.wordpress.org/Writing_Posts">here</a> for details.');
				$this->option_formatted(__('Post status'), $html, $tip);
				break;

			case 'template[comment_status]':
				$html = $this->option_dropdown($field, $import->template->comment_status, array('open'=>'open', 'closed'=>'closed'));
				$tip = __('WordPress comments setting - see <a href="http://codex.wordpress.org/Writing_Posts">here</a> for details.');
				$this->option_formatted(__('Comment status'), $html, $tip);
				break;

			case 'template[post_author_type]':
				global $current_user;

				// Author from column
				$checked = checked($import->template->post_author_type, 'COLUMN', false);
				$html = "<input type='radio' name='$field' value='COLUMN' $checked />" . __('Column') . " "
					. $this->option_dropdown_headers('template[post_author_col]', $import->template->post_author_col, $import->headers);

				// Author from fixed list
				$checked = checked($import->template->post_author_type, 'FIXED', false);
				$html .= "<br/><input type='radio' name='$field' value='FIXED' $checked />"  . __('Select from list ');

				$html .= wp_dropdown_users(array(
					'selected'=>$import->template->post_author,
					'name' => 'template[post_author]',
					'class'=> 'authors',
					'multi' => 1,
					'echo' => 0));
				$tip = __('Author for the imported posts/pages.');
				$this->option_formatted(__('Author'), $html, $tip);
				break;

			case 'template[post_parent]':
				$html = wp_dropdown_pages(array('echo' => false,
					'name' => $field,
					'selected' => $import->template->post_parent,
					'show_option_none' => __('Main Page (no parent)'),
					'sort_column'=> 'menu_order, post_title'));
				if (!$html)
					$html = __('No parent pages available.  See the WordPress <a href="http://codex.wordpress.org/Pages">Codex</a> for details.');

				$tip = __('WordPress parent page - see <a href="http://codex.wordpress.org/Pages">here</a> for details.');
				$this->option_formatted(__('Page parent'), $html, $tip);
				break;

			case 'template[page_template]':
				$templates = get_page_templates();
				ksort($templates);
				if($templates) {
					$templates = array_combine(array_values($templates), array_keys($templates));
					$templates = array_merge(array('default' => 'Default Template'), $templates);
				} else {
					$templates = array('default' => 'Default Template');
				}

				$html = $this->option_dropdown($field, $import->template->page_template, $templates, false);

				$tip = __('WordPress page template - see <a href="http://codex.wordpress.org/Pages">here</a> for details.');
				$this->option_formatted(__('Page template'), $html, $tip);
				break;

			case 'template[menu_order]':
				$html = $this->option_text($field, $import->template->menu_order, 4);
				$tip = __('WordPress page menu order - see <a href="http://codex.wordpress.org/Pages">here</a> for details.');
				$this->option_formatted(__('Page menu order'), $html, $tip);
				break;
		}
	}


	function optionp($import, $field) {
	}

	/**
	* Display html formatted as table row with label, html, tooltip and style(class)
	*/
	function option_formatted($label='', $html='', $tip='', $style='') {
		echo "<tr $style valign='top'>";

		echo "<th scope='row'>";

		if ($label)
			echo "$label: ";

		if ($tip)
			echo "<sup>[<a href='#' class='ti-tooltip' title='" . $label . '|' . esc_attr($tip) . "'>?</a>]</sup>";

		echo "</th>";
		echo "<td>$html</td>";
		echo "</tr>";
	}

	/**
	* Display a text box
	*
	*/
	function option_text($name, $value, $size) {
		$name = esc_attr($name);
		$value = esc_attr($value);
		$html = "<input name='$name' type='text' size='$size' value='$value' />";
		return $html;
	}

	/**
	* Dropdown box listing input file headers
	*
	* @param mixed $name
	* @param mixed $select
	* @param mixed $headers
	* @param mixed $none
	*/
	function option_dropdown_headers($name, $select, $headers, $none=false) {
		// For headers use an array where key = value
		foreach ((array)$headers as $header)
			$dd_headers[$header] = $header;
		return $this->option_dropdown($name, $select, $dd_headers, true);
	}

	/**
	* Dropdown displaying an array of (key => description)
	*
	*/
	function option_dropdown($name, $select, $keys, $none=false) {
		asort($keys);

		if ($none) {
			$none = '- none -';
			$keys = array_merge(array(null => $none), (array)$keys);
		}

		$select = (array) $select;

		$name = esc_attr($name);
		$html = "<select name='$name'>";

		foreach ((array)$keys as $key => $description) {
			$selected = ( in_array($key, $select) || ($select == null && $key == null) ) ? "selected='selected'" : "";
			$key = esc_attr($key);
			$description = esc_attr($description);
			$html .= "<option value='$key' $selected>$description</option>";
		}
		$html .= "</select>";
		return $html;
	}

	/**
	* Single checkbox
	*/
	function option_checkbox($name, $value) {
		$name = esc_attr($name);
		$checked = ($value) ? "checked='checked'" : "";
		$html 	= "<input type='hidden' name='$name' value='0' />"
				. "<input type='checkbox' name='$name' value='1' $checked />";

		return $html;
	}

	/**
	* Outputs a <div> containing an HTML <table>
	*
	* @param mixed array $headers - array of column header strings
	* @param mixed array $rows - array of rows; each row is array(class, data); class is optional, if provided it's applied to <TR>
	* @param mixed $table_id - HTML id of the table
	* @param mixed $div_class - a class to apply to the table's <div>
	* @param mixed $table_class - a class to apply to the <table>
	*/
	function option_table($headers, $rows, $table_id='', $div_id='', $div_class='ti-small-scroll', $table_class='ti-table ti-stripe') {
		$html = "<div id='$div_id' class='$div_class'><table id='$table_id' class='$table_class'><thead><tr>";

		foreach ((array)$headers as $header)
			$html .= "<th>$header</th>";
		$html .= "</tr></thead><tbody>";

		foreach ((array)$rows as $row) {
			if (isset($row['class']))
				$html .= ($row['class']) ? "<tr class='". $row['class'] . "'>" : "";
			else
				$html .= "<tr>";

			if (isset($row['data'])) {
				foreach ((array)$row['data'] as $col)
					$html .= "<td>$col</td>";
			}

			$html .= "</tr>";
		}

		$html .= "</tbody></table></div>";
		return $html;
	}


	/**
	* Returns an array of custom field keys, ignoring most WP standard hidden fields
	* The array is sorted by key
	*
	*/
	function get_meta_keys() {
		global $wpdb;

		// Get list of custom fields from all posts; ignore wordpress standard hidden fields
		$meta_keys = $wpdb->get_col( "
			SELECT DISTINCT meta_key
			FROM $wpdb->postmeta
			WHERE meta_key NOT in ('_edit_last', '_edit_lock', '_encloseme', '_pingme', '_wp_attached_file', '_wp_trash_meta_status', '_wp_trash_meta_time',
			'_wp_trash_meta_comments_status', '_wp_page_template', '_wp_attachment_temp_parent', '_wp_attachment_backup_sizes',
			'_wp_attachment_metadata', '_wp_old_slug')
			AND meta_key NOT LIKE ('\_wp%')" );

		// MySQL cannot be used for group-by or order because postmeta is stored case-insensitive
		if ($meta_keys)
			$meta_keys = array_unique($meta_keys);

		return $meta_keys;
	}

	function memory_usage($show_as_message=false) {
		$php_memory_limit = ini_get('memory_limit');
		$php_memory_limit = str_ireplace('M', '', $php_memory_limit);

		$wp_memory_limit = (defined('WP_MEMORY_LIMIT')) ? WP_MEMORY_LIMIT : "not set";
		$wp_memory_limit = str_ireplace('M', '', $wp_memory_limit);

		$memory_usage = max( memory_get_usage(), memory_get_peak_usage() );
		$memory_usage = round( $memory_usage / 1024 / 1024, 2 );

		$html = sprintf("Current memory usage: <b>%s MB</b>.", $memory_usage)
			. "<br/>"
			. "PHP limit: <strong>$php_memory_limit MB</strong>.<br/>WordPress limit: <strong>$wp_memory_limit MB</strong>";

		if (!$show_as_message)
			return $html;

		$min = 32;      // Amount of FREE memory
		$php_free = $php_memory_limit - $memory_usage;
		$wp_free = $wp_memory_limit - $memory_usage;

		if ( ($php_free) < $min || ($wp_free) < $min ) {
			$html = "<div class='updated fade'>"
				. $html
				. "<br/><br/>" . __("Your free memory looks low.  This may prevent you from doing large imports.") . "<br/><br/>"
				. __("You can increase the WordPress limit by editing the <code>wp-config.php</code> using <code>define('WP_MEMORY_LIMIT', '128M')</code>") . "<br/>"
				. __("PHP memory can be increased by editing the <code>php.ini</code> file using <code>memory_limit = 128M</code>")
				. "<br/>" . __("You may need to contact your hosting service to make these changes.") . "<br/><br/>"
				. "</div>";
		} else {
			$html = '';
		}

		return $html;
	}
} // End class TI_Frontend

if (class_exists('TI_Pro'))
	$tic_frontend = new TI_Pro();
else
	$tic_frontend = new TI_Frontend();
?>