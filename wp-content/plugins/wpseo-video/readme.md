Video SEO
=========
Requires at least: 3.2<br/>
Tested up to: 3.8.1<br/>
Stable tag: 1.7.2
Depends: wordpress-seo

Video SEO adds Video SEO capabilities to WordPress SEO.

Description
-----------

This plugin adds Video XML Sitemaps as well as the necessary OpenGraph markup, Schema.org videoObject markup and mediaRSS for your videos.

Installation
------------

1. Go to Plugins -> Add New.
2. Click "Upload" right underneath "Install Plugins".
3. Upload the zip file that this readme was contained in.
4. Activate the plugin.
5. Go to SEO -> Extensions and enter your license key.
6. Save settings, your license key will be validated. If all is well, you should now see the XML Video Sitemap settings.
7. Make sure to hit the "Re-index video's" button if you have video's in old posts.

Frequently Asked Questions
--------------------------

You can find the FAQ [online here](http://kb.yoast.com/category/8-video-seo).

Changelog
=========

### 1.7.2: July 17th, 2014

Fix added whitespace after content cause in 1.7 update.

### 1.7.1: July 15th, 2014

Fix error in update caused by missing the version number update in 1.7.

### 1.7: July 14th, 2014

* Bug fixes:
	* Fixed: bug where `$content` would be empty for an `mrss_item`.
	* Fixed: minor bug in upgrade routine.
	* Fixed: bugs in Animoto and Screenr oembed provider addition.
	* Fixed: issue with sitemap errors when conflicting http protocols were given.
	* Fixed: video sitemap could show in sitemap index even when no posts with videos were found.
	* Fixed: video description generated from content could break off in the middle of a word or html entity.
	* Fixed: error on plugin activation.
	* Fixed: sitemap conflict when a custom post type named 'video' would exist.
	* Fixed: issue where durations would not be shown correctly in the metabox/

* Enhancements:
	* Add oembed support for wistia.net domain and wistia protocol-relative urls.
	* Moved language file loading to the init hook to allow for translation overloading.
	* Improved clean-up of uploaded files.
	* Update snippet preview to use latest Google design changes in line with the earlier update to WP SEO. This fixes the javascript error some people were experiencing.
	* Auto-deactivate plugin in circumstances that it can't work.
	* Increased size of YouTube thumbnail image being retrieved.

### 1.6.3: March 31st, 2014

* Bug fixes:
	* Fixed a warning for a missing variable in sanitize_rating.

### 1.6.2: March 17th, 2014

* Bug fixes:
	* Fixed a warning for a missing variable.
	* Updated Fitvids.js to fix some issues with it.

* Enhancements:
	* Fitvids will now be included un-minified when `SCRIPT_DEBUG` is on.

### 1.6.1: March 11th, 2014

Fix wrong boolean check.

### 1.6: March 11th, 2014

Compatibility with WPSEO 1.5 and implementation of the same options & meta philosophy

* Bug fixes
	* Fixed: Non-static methods should not be called statically
	* Fixed: noindex setting wasn't being respected properly
	* Fixed: some inconsistent admin form texts
	* Fixed: Warning when loading new post.
    * Fixed: Always re-validate license key on change.

* i18n
    * Updated .pot file
    * Updated it_IT

### 1.5.5.1

* Bug fixes
	* Make sure thumbnail image is available.
	* Move initialisation of plugin to earlier hook to make sure it's there when XML sitemap is generated.

### 1.5.5

* Bug fixes
	* Remove dependency on `WPSEO_URL` constant.
	* Fix use of wrong image in OpenGraph and Schema.org output when a thumbnail is manually selected.
	* Restore $shortcode_tags to original after `index_content()`.

* Enhancements
	* Use media uploader to change video thumbnail.
	* Add setting to allow video playback directly on Facebook (defaults to on).

### 1.5.4.6

* Bug fixes
    * Prevent warning on line 4169, for unset video taxonomies.
    * Prevent issues with custom fields that have spaces in their keys.
    * Added support for more Dailymotion URLs.

* Enhancements
    * Remove CDATA in favor of proper encoding of entities.
    * Force 200 status codes and proper caching on both video sitemap XML and XSL.
    * Add support for WP YouTube Lyte shortcode.

* i18n
    * Renamed wpseo-video.pot to yoast-video-seo.pot
    * Updated fr_FR
    * Added hu_HU


### 1.5.4.5

* To make best use of the new features in this update, please reindex your videos.

* Bug fixes
    * Several i18n namespace fixes.
    * Make video's in taxonomy descriptions pick up properly again.
    * Fix for Wistia popover embeds and Wistia https URLs.
    * Prevent output of hd attribute for video's in XML Video sitemap.
    * Make sure opengraph image is always set to "full" size.
    * Add width and height for Youtube videos.
    * Prevent notice in sitemap when video from taxonomy term is displayed.
    * Prevent wrong or empty dates in XML video sitemap.
* Enhancements
    * Add option to manually add tags per video.
    * Add option to override video category (normally defaults to first post category).
    * Order videos in XML video sitemap by date modified, ascending.
    * Add "proper" Facebook video integration.
    * Added support for [Advanced Responsive Video Embedder](http://wordpress.org/plugins/advanced-responsive-video-embedder/).
    * Added support for muzu.tv.
    * Allow for custom fields that hold arrays to be detected too.
    * Add support for custom Vimeo URLs. (eg http://vimeo.com/yoast/video-seo)
    * Make sure the video thumbnail is always put out as an og:image too.
	* Added support for Instabuilder video shortcodes
	* Added support for Vidyard
	* Set license key with a constant
	* Added support for Cincopa
	* Added support for Brightcove
	* Added support for videos in the 'Archive Intro Text' (Genesis) in the video sitemap
	* Added support for WP OS FLV plugin
	* Added support for [Wordpress Automatic Youtube Video Post] (http://wordpress.org/plugins/automatic-youtube-video-posts/)

### 1.5.4.4

* Bug fixes
    * Spaces in custom fields settings are now properly trimmed.
    * Fix for Vzaar URLs.
    * Wistia embed with extra classes now properly detected.
* Enhancements
    * Video sitemap now adheres to same pagination as post sitemap.
    * Video XML Sitemap date now properly retrieved from last modified post with movie.

### 1.5.4.3

* Enhancements
    * Add support for `fvplayer` shortcode.
    * Add option to manually change or enter duration.

### 1.5.4.2

* Bug fixes:
    * Properly allow normal meta description length when video has been disabled for post.
* Enhancements:
    * Added option to disable RSS enhancements, to prevent clashes with podcasting plugins.

### 1.5.4.1

* Move loading of the plugin to prio 20, in line with upgrades of the core WordPress SEO plugin.

### 1.5.4

* Enhancements:
    * Added support for [fitvids.js](http://fitvidsjs.com/), enable it in the Video SEO settings to make your Youtube / Vimeo / Blip.tv / Viddler / Wistia videos responsive, meaning they'll become fluid. This might not work with all embed codes, let us know when it doesn't work for a particular one.
    * Removed the ping functionality as that's fixed within the core plugin.
    * Added code that forces you to update WordPress to 3.4 or higher and the WordPress SEO plugin to 1.4 or higher to use the plugin.
* Bug fixes:
    * Fixed a bug that would prevent the time last modified of the video sitemap to update.

### 1.5.3

* Enhancements:
    * Improved defaults: now enables all public post-types by default on install.
    * Option to change the basename of the video sitemap, from video-sitemap.xml to whatever-sitemap.xml by setting the `YOAST_VIDEO_SITEMAP_BASENAME` constant.
    * If post meta values are encoded, the plugin now decodes them.
* Bug fixes:
    * No longer override opengraph image when one has already been set.
    * Add extra newlines before video schema to allow oEmbed to work.
    * No longer depends on response from Vzaar servers to create sitemap, properly uses the referrer to authenticate requests and adds option in settings to add your Vzaar CNAME.
    * When there's a post-type with the slug `video`, the plugin now automatically changes the basename to `yoast-video`.
    * No longer print empty `<p>` for empty description in meta box.
    * Improve logic whereby "this image" link is shown correctly and only when the video thumb is not overridden.

### 1.5.2

* Enhancements:
    * Added support for Vzaar video's, embedded with either iframe, object embed or shortcode through 1 of 2 plugins.
    * Added TubePress support.
* Bug fixes
    * Wistia.net support added (not just .com).
    * Fixed bug in parsing youtube_sc shortcodes.

### 1.5.1

* Bug fixes:
    * Improved activation.
* Enhancements:
    * Add support for titan lightbox.
    * Prevented some notices.

### 1.5

* Bug fixes:
    * Make `mrss_gallery_lookup` public to prevent notices.
    * Fix some forms of object detection for youtube and others.
    * Fix detection of [video] shortcodes.
* Enhancements:
    * Allow deactivation of license key so it can be used on another domain.
    * Add link to detected thumbnail on video tab.
    * Changed text-domain from `wordpress-seo` to `yoast-video-seo`.
    * Made sure all the strings are translatable.
    * Touch up admin sections styling.
* i18n:
    * You can now translate the plugin to your native language should you need a translation, check [translate.yoast.com](http://translate.yoast.com/projects/yoast-video-seo) for details.
    * Changed text-domain from `wordpress-seo` to `yoast-video-seo`.
    * Added .pot file to repository.
    * Added Dutch translation.

== Upgrade Notice ==

1.6
---
* Please make sure you also upgrade the WordPress SEO plugin to version 1.5 for compatibility.