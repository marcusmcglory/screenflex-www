<?php
/*
Plugin Name: Video SEO for WordPress SEO by Yoast
Version: 1.7.2
Plugin URI: https://yoast.com/wordpress/plugins/video-seo/
Description: This Video SEO module adds all needed meta data and XML Video sitemap capabilities to the metadata capabilities of WordPress SEO to fully optimize your site for video results in the search results.
Author: Joost de Valk
Author URI: https://yoast.com
Text Domain: yoast-video-seo
Domain Path: /languages/

Copyright 2012-2014 Yoast BV
*/

if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

define( 'WPSEO_VIDEO_VERSION', '1.7.2' );


/**
 * Auto load our class files
 *
 * @param   string  $class  Class name
 * @return  void
 */
function wpseo_video_auto_load( $class ) {
	static $classes    = null;
	static $video_path = null;

	if ( $video_path === null ) {
		$video_path = plugin_dir_path( __FILE__ );
	}

	if ( $classes === null ) {
		$classes = array(
			'yoast_product_wpseo_video'       => $video_path . 'product-wpseo-video.php',
			'wpseo_meta_video'                => $video_path . 'class-wpseo-meta-video.php',
			'wpseo_video_metabox'             => $video_path . 'class-wpseo-video-metabox.php',
			'wpseo_option_video'              => $video_path . 'class-wpseo-option-video.php',
			'wpseo_video_details'             => $video_path . 'video-services/class-wpseo-video-details.php',

			'wpseo_video_details_animoto'     => $video_path . 'video-services/class-animoto.php',
			'wpseo_video_details_blip'        => $video_path . 'video-services/class-blip.php',
			'wpseo_video_details_brightcove'  => $video_path . 'video-services/class-brightcove.php',
			'wpseo_video_details_cincopa'     => $video_path . 'video-services/class-cincopa.php',
			'wpseo_video_details_dailymotion' => $video_path . 'video-services/class-dailymotion.php',
			'wpseo_video_details_evs'         => $video_path . 'video-services/class-evs.php',
			'wpseo_video_details_flickr'      => $video_path . 'video-services/class-flickr.php',
			'wpseo_video_details_metacafe'    => $video_path . 'video-services/class-metacafe.php',
			'wpseo_video_details_muzutv'      => $video_path . 'video-services/class-muzutv.php',
			'wpseo_video_details_screencast'  => $video_path . 'video-services/class-screencast.php',
			'wpseo_video_details_screenr'     => $video_path . 'video-services/class-screenr.php',
			'wpseo_video_details_veoh'        => $video_path . 'video-services/class-veoh.php',
			'wpseo_video_details_viddler'     => $video_path . 'video-services/class-viddler.php',
			'wpseo_video_details_videopress'  => $video_path . 'video-services/class-videopress.php',
			'wpseo_video_details_vidyard'     => $video_path . 'video-services/class-vidyard.php',
			'wpseo_video_details_vimeo'       => $video_path . 'video-services/class-vimeo.php',
			'wpseo_video_details_vippy'       => $video_path . 'video-services/class-vippy.php',
			'wpseo_video_details_vzaar'       => $video_path . 'video-services/class-vzaar.php',
			'wpseo_video_details_wistia'      => $video_path . 'video-services/class-wistia.php',
			'wpseo_video_details_wordpresstv' => $video_path . 'video-services/class-wordpresstv.php',
			'wpseo_video_details_youtube'     => $video_path . 'video-services/class-youtube.php',

			'wpseo_admin_pages'               => WPSEO_PATH . 'admin/class-config.php',
			'wpseo_metabox'                   => WPSEO_PATH . 'admin/class-metabox.php',
			'wpseo_option'                    => WPSEO_PATH . 'inc/class-wpseo-options.php',
			'wpseo_meta'                      => WPSEO_PATH . 'inc/class-wpseo-meta.php',
			'yoast_plugin_license_manager'    => WPSEO_PATH . 'admin/license-manager/class-plugin-license-manager.php',
			'yoast_product'                   => WPSEO_PATH . 'admin/license-manager/class-product.php',
		);
	}

	$cn = strtolower( $class );

	if ( isset( $classes[ $cn ] ) ) {
		require_once( $classes[ $cn ] );
	}
}
if ( function_exists( 'spl_autoload_register' ) ) {
	spl_autoload_register( 'wpseo_video_auto_load' );
}



/**
 * All functionality for fetching video data and creating an XML video sitemap with it.
 *
 * @link       http://codex.wordpress.org/oEmbed oEmbed Codex Article
 * @link       http://oembed.com/ oEmbed Homepage
 *
 * @package    WordPress SEO
 * @subpackage WordPress SEO Video
 */

/**
 * wpseo_video_Video_Sitemap class.
 *
 * @package WordPress SEO Video
 * @since   0.1
 */
class WPSEO_Video_Sitemap {

	/**
	 * The maximum number of entries per sitemap page
	 */
	private $max_entries = 5;

	private $metabox_tab;

	protected $option_instance;

	/**
	 * @var	string	Youtube video ID regex pattern
	 */
	public static $youtube_id_pattern = '[0-9a-zA-Z_-]+';

	/**
	 * @var	string	Video extension list for use in regex pattern
	 */
	protected $video_ext_pattern = 'mpg|mpeg|mp4|m4v|mov|wmv|asf|avi|ra|ram|rm|flv|swf';

	/**
	 * @var	string	Image extension list for use in regex pattern
	 */
	public static $image_ext_pattern = 'jpg|jpeg|jpe|gif|png';

	/**
	 * Compatibility with Youtube Lyte and Smart Youtube Pro
	 * @var	array	Array of alternative Youtube protocol schemes to take into account
	 */
	protected $alt_youtube_schemes = array(
		'httpv://',
		'httpvh://',
		'httpvp://',
		'httpa://',
	);


	/**
	 * Return the plugin file
	 *
	 * @return string
	 */
	public static function get_plugin_file() {
		return __FILE__;
	}

	/**
	 * Constructor for the WPSEO_Video_Sitemap class.
	 *
	 * @todo  upgrade from license constant WPSEO_VIDEO_LICENSE
	 * @since 0.1
	 */
	function __construct() {

		// Initialize the options
		$this->option_instance = WPSEO_Option_Video::get_instance();

		$options = get_option( 'wpseo_video' );

		// run upgrade routine
		$this->upgrade();

		add_filter( 'wpseo_tax_meta_special_term_id_validation__video', array( $this, 'validate_video_tax_meta' ) );


		if ( ! isset( $GLOBALS['content_width'] ) && $options['content_width'] > 0 ) {
			$GLOBALS['content_width'] = $options['content_width'];
		}

		add_action( 'setup_theme', array( $this, 'init' ) );
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'init', array( $this, 'register_sitemap' ), 20 ); // Register sitemap after cpts have been added

		if ( is_admin() ) {

			add_action( 'admin_menu', array( $this, 'register_settings_page' ) );

			add_filter( 'wpseo_admin_pages', array( $this, 'style_admin' ) );

			add_action( 'save_post', array( $this, 'update_video_post_meta' ) );

			if ( in_array( $GLOBALS['pagenow'], array( 'edit.php', 'post.php', 'post-new.php' ) ) ) {
				$this->metabox_tab = new WPSEO_Video_Metabox();
			}

			// Licensing part
			$yoast_product   = new Yoast_Product_WPSEO_Video();
			$license_manager = new Yoast_Plugin_License_Manager( $yoast_product );

			// Setup constant name
			$license_manager->set_license_constant_name( 'WPSEO_VIDEO_LICENSE' );

			// Setup hooks
			$license_manager->setup_hooks();

			// Add form
			add_action( 'wpseo_licenses_forms', array( $license_manager, 'show_license_form' ) );

		} else {

			// OpenGraph
			add_action( 'wpseo_opengraph', array( $this, 'opengraph' ) );
			add_filter( 'wpseo_opengraph_type', array( $this, 'opengraph_type' ), 10, 1 );
			add_filter( 'wpseo_opengraph_image', array( $this, 'opengraph_image' ), 5, 1 );

			// XML Sitemap Index addition
			add_filter( 'wpseo_sitemap_index', array( $this, 'add_to_index' ) );

			// Content filter for non-detected video's
			add_filter( 'the_content', array( $this, 'content_filter' ), 5, 1 );

			if ( $options['fitvids'] === true ) {
				// Fitvids scripting
				add_action( 'wp_head', array( $this, 'fitvids' ) );
			}

			if ( $options['disable_rss'] !== true ) {
				// MRSS
				add_action( 'rss2_ns', array( $this, 'mrss_namespace' ) );
				add_action( 'rss2_item', array( $this, 'mrss_item' ), 10, 1 );
				add_filter( 'mrss_media', array( $this, 'mrss_add_video' ) );
			}
		}
	}

	/**
	 * Load translations
	 */
	static function load_textdomain() {
		load_plugin_textdomain( 'yoast-video-seo', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}


	/**
	 * Adds the fitvids JavaScript to the output if there's a video on the page that's supported by this script.
	 *
	 * @since 1.5.4
	 */
	function fitvids() {
		if ( ! is_singular() ) {
			return;
		}

		global $post;

		$video = WPSEO_Meta::get_value( 'video_meta', $post->ID );

		if ( ! is_array( $video ) || $video === array() ) {
			return;
		}

		// Check if the current post contains a YouTube, Vimeo, Blip.tv or Viddler video, if it does, add the fitvids code.
		if ( in_array( $video['type'], array( 'youtube', 'vimeo', 'blip.tv', 'viddler', 'wistia' ) ) ) {
			if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
				wp_enqueue_script( 'fitvids', plugin_dir_url( __FILE__ ) . 'js/jquery.fitvids.js', array( 'jquery' ) );
			} else {
				wp_enqueue_script( 'fitvids', plugin_dir_url( __FILE__ ) . 'js/jquery.fitvids.min.js', array( 'jquery' ) );
			}
		}

		add_action( 'wp_footer', array( $this, 'fitvids_footer' ) );
	}

	/**
	 * The fitvids instantiation code.
	 *
	 * @since 1.5.4
	 */
	function fitvids_footer() {
		global $post;

		// Try and use the post class to determine the container
		$classes = get_post_class( '', $post->ID );
		$class   = 'post';
		if ( is_array( $classes ) && $classes !== array() ) {
			$class = $classes[0];
		}
		?>
		<script type="text/javascript">
			jQuery(document).ready(function ($) {
				$(".<?php echo esc_attr( $class ); ?>").fitVids({ customSelector: "iframe.wistia_embed"});
			});
		</script>
	<?php
	}

	/**
	 * Make sure the Video SEO plugin receives Yoast admin styling
	 *
	 * @param array $adminpages The array of pages that have Yoast admin styling
	 *
	 * @return array $adminpages
	 */
	function style_admin( $adminpages ) {
		array_push( $adminpages, 'wpseo_video' );

		return $adminpages;
	}

	/**
	 * Register the Video SEO submenu.
	 */
	function register_settings_page() {
		add_submenu_page( 'wpseo_dashboard', esc_html__( 'Yoast WordPress SEO:', 'wordpress-seo' ) . ' ' . esc_html__( 'Video SEO Settings', 'yoast-video-seo' ), esc_html__( 'Video SEO', 'yoast-video-seo' ), 'manage_options', 'wpseo_video', array( $this, 'admin_panel' ) );
	}

	/**
	 * Adds the rewrite for the video XML sitemap
	 *
	 * @since 0.1
	 */
	function init() {
		// Get options to set the entries per page
		$options           = WPSEO_Options::get_all();
		$this->max_entries = $options['entries-per-page'];

		// Add oEmbed providers
		$this->add_oembed();
	}

	/**
	 * Register the video sitemap in the WPSEO sitemap class
	 *
	 * @since 1.7
	 */
	function register_sitemap() {
		// Register the sitemap
		if ( isset( $GLOBALS['wpseo_sitemaps'] ) && is_object( $GLOBALS['wpseo_sitemaps'] ) ) {
			$basename = $this->video_sitemap_basename();
			$GLOBALS['wpseo_sitemaps']->register_sitemap( $basename, array( $this, 'build_video_sitemap' ) );
			if ( method_exists( $GLOBALS['wpseo_sitemaps'], 'register_xsl' ) ) {
				$GLOBALS['wpseo_sitemaps']->register_xsl( $basename, array( $this, 'build_video_sitemap_xsl' ) );
			}
		}
	}


	/**
	 * Execute upgrade actions when needed
	 */
	function upgrade() {

		$options = get_option( 'wpseo_video' );

		$yoast_product   = new Yoast_Product_WPSEO_Video();
		$license_manager = new Yoast_Plugin_License_Manager( $yoast_product );

		// upgrade to license manager
		if ( $license_manager->get_license_key() === '' ) {

			if ( isset( $options['yoast-video-seo-license'] ) ) {
				$license_manager->set_license_key( $options['yoast-video-seo-license'] );
			}

			if ( isset( $options['yoast-video-seo-license-status'] ) ) {
				$license_manager->set_license_status( $options['yoast-video-seo-license-status'] );
			}
			update_option( 'wpseo_video', $options );
		}

		// upgrade to new option & meta classes
		if ( ! isset( $options['dbversion'] ) || version_compare( $options['dbversion'], '1.6', '<' ) ) {
			$this->option_instance->clean();
			WPSEO_Meta::clean_up(); // Make sure our meta values are cleaned up even if WP SEO would have been upgraded already
		}

		// Re-add missing durations
		if ( ! isset( $options['dbversion'] ) || ( version_compare( $options['dbversion'], '1.7', '<' ) && version_compare( $options['dbversion'], '1.6', '>' ) ) ) {
			WPSEO_Meta_Video::re_add_durations();
		}

		// Make sure version nr gets updated for any version without specific upgrades
		$options = get_option( 'wpseo_video' ); // re-get to make sure we have the latest version
		if ( version_compare( $options['dbversion'], WPSEO_VIDEO_VERSION, '<' ) ) {
			update_option( 'wpseo_video', $options );
		}
	}

	/**
	 * Returns the basename of the video-sitemap, the first portion of the name of the sitemap "file".
	 *
	 * Defaults to video, but it's possible to override it by using the YOAST_VIDEO_SITEMAP_BASENAME constant.
	 *
	 * @since 1.5.3
	 *
	 * @return string $basename
	 */
	function video_sitemap_basename() {
		$basename = 'video';

		if ( post_type_exists( 'video' ) ) {
			$basename = 'yoast-video';
		}

		if ( defined( 'YOAST_VIDEO_SITEMAP_BASENAME' ) ) {
			$basename = YOAST_VIDEO_SITEMAP_BASENAME;
		}

		return $basename;
	}

	/**
	 * Return the Video Sitemap URL
	 *
	 * @since 1.2.1
	 *
	 * @return string The URL to the video Sitemap.
	 */
	function sitemap_url() {
		$base = $GLOBALS['wp_rewrite']->using_index_permalinks() ? 'index.php/' : '';

		return home_url( $base . $this->video_sitemap_basename() . '-sitemap.xml' );
	}

	/**
	 * Adds the video XML sitemap to the Index Sitemap.
	 *
	 * @since  0.1
	 *
	 * @param string $str String with the filtered additions to the index sitemap in it.
	 *
	 * @return string $str String with the Video XML sitemap additions to the index sitemap in it.
	 */
	function add_to_index( $str ) {
		$options = get_option( 'wpseo_video' );

		$base = $GLOBALS['wp_rewrite']->using_index_permalinks() ? 'index.php/' : '';

		if ( is_array( $options['videositemap_posttypes'] ) && $options['videositemap_posttypes'] !== array() ) {
			// Use fields => ids to limit the overhead of fetching entire post objects,
			// fetch only an array of ids instead to count
			$args = array(
					'post_type'      => $options['videositemap_posttypes'],
					'post_status'    => 'publish',
					'posts_per_page' => - 1,
					'meta_key'       => '_yoast_wpseo_video_meta',
					'meta_compare'   => '!=',
					'meta_value'     => 'none',
					'fields'         => 'ids',
			);
			// Copy these args to be used and modify later
			$date_args = $args;

			$video_ids = get_posts( $args );
			$count     = count( $video_ids );

			if ( $count > 0 ) {
				$n = ( $count > $this->max_entries ) ? (int) ceil( $count / $this->max_entries ) : 1;
				for ( $i = 0; $i < $n; $i ++ ) {
					$count = ( $n > 1 ) ? $i + 1 : '';

					if ( empty( $count ) || $count == $n ) {
						$date_args['fields']         = 'all';
						$date_args['posts_per_page'] = 1;
						$date_args['offset']         = 0;
						$date_args['order']          = 'DESC';
						$date_args['orderby']        = 'modified';
					} else {
						$date_args['fields']         = 'all';
						$date_args['posts_per_page'] = 1;
						$date_args['offset']         = $this->max_entries * ( $i + 1 ) - 1;
						$date_args['order']          = 'ASC';
						$date_args['orderby']        = 'modified';
					}
					$posts = get_posts( $date_args );
					$date  = date( 'c', strtotime( $posts[0]->post_modified_gmt ) );

					$text = ( $count > 1 ) ? $count : '';
					$str .= '<sitemap>' . "\n";
					$str .= '<loc>' . home_url( $base . $this->video_sitemap_basename() . '-sitemap' . $text . '.xml' ) . '</loc>' . "\n";
					$str .= '<lastmod>' . $date . '</lastmod>' . "\n";
					$str .= '</sitemap>' . "\n";
				}
			}
		}

		return $str;
	}

	/**
	 * Adds oembed endpoints for supported video platforms that are not supported by core.
	 *
	 * @since 1.3.5
	 */
	function add_oembed() {
		// Wistia
		wp_oembed_add_provider( '`(?:http[s]?:)?//[^/]*(wistia\.(com|net)|wi\.st)/(medias|embed)/.*`', 'http://fast.wistia.com/oembed', true );

		// Animoto
		wp_oembed_add_provider( 'http://animoto.com/play/*', 'http://animoto.com/services/oembed?format=json', false );

		// Screenr
		wp_oembed_add_provider( 'http://www.screenr.com/*', 'http://www.screenr.com/api/oembed.{format}', false );

		// EVS
		$evs_location = get_option( 'evs_location' );
		if ( $evs_location && ! empty( $evs_location ) ) {
			wp_oembed_add_provider( $evs_location . '/*', $evs_location . '/oembed.php', false );
		}
	}


	/**
	 * Add the MRSS namespace to the RSS feed.
	 *
	 * @since 0.1
	 */
	function mrss_namespace() {
		echo ' xmlns:media="http://search.yahoo.com/mrss/" ';
	}

	/**
	 * Add the MRSS info to the feed
	 *
	 * Based upon the MRSS plugin developed by Andy Skelton
	 *
	 * @since     0.1
	 * @copyright Andy Skelton
	 */
	function mrss_item() {
		global $mrss_gallery_lookup;
		$media  = array();
		$lookup = array();

		// Honor the feed settings. Don't include any media that isn't in the feed.
		if ( get_option( 'rss_use_excerpt' ) || ! strlen( get_the_content() ) ) {
			ob_start();
			the_excerpt_rss();
			$content = ob_get_clean();
		} else {
			// If any galleries are processed, we need to capture the attachment IDs.
			add_filter( 'wp_get_attachment_link', array( $this, 'mrss_gallery_lookup' ), 10, 5 );
			$content = apply_filters( 'the_content', get_the_content() );
			remove_filter( 'wp_get_attachment_link', array( $this, 'mrss_gallery_lookup' ), 10, 5 );
			$lookup = $mrss_gallery_lookup;
			unset( $mrss_gallery_lookup );
		}

		// img tags
		$images = 0;
		if ( preg_match_all( '`<img ([^>]+)>`', $content, $matches ) ) {
			foreach ( $matches[1] as $attrs ) {
				$item = $img = array();
				// Construct $img array from <img> attributes
				$attributes = wp_kses_hair( $attrs, array( 'http' ) );
				foreach ( $attributes as $attr ) {
					$img[ $attr['name'] ] = $attr['value'];
				}
				unset( $attributes );

				// Skip emoticons and images without source attribute
				if ( ! isset( $img['src'] ) || ( isset( $img['class'] ) && false !== strpos( $img['class'], 'wp-smiley' ) ) ) {
					continue;
				}

				$img['src'] = $this->mrss_url( $img['src'] );

				$id = false;
				if ( isset( $lookup[ $img['src'] ] ) ) {
					$id = $lookup[ $img['src'] ];
				} elseif ( isset( $img['class'] ) && preg_match( '`wp-image-(\d+)`', $img['class'], $match ) ) {
					$id = $match[1];
				}
				if ( $id ) {
					// It's an attachment, so we will get the URLs, title, and description from functions
					$attachment =& get_post( $id );
					$src        = wp_get_attachment_image_src( $id, 'full' );
					if ( ! empty( $src[0] ) ) {
						$img['src'] = $src[0];
					}
					$thumbnail = wp_get_attachment_image_src( $id, 'thumbnail' );
					if ( ! empty( $thumbnail[0] ) && $thumbnail[0] != $img['src'] ) {
						$img['thumbnail'] = $thumbnail[0];
					}
					$title = get_the_title( $id );
					if ( ! empty( $title ) ) {
						$img['title'] = trim( $title );
					}
					if ( ! empty( $attachment->post_excerpt ) ) {
						$img['description'] = trim( $attachment->post_excerpt );
					}
				}
				// If this is the first image in the markup, make it the post thumbnail
				if ( ++$images == 1 ) {
					if ( isset( $img['thumbnail'] ) ) {
						$media[]['thumbnail']['attr']['url'] = $img['thumbnail'];
					} else {
						$media[]['thumbnail']['attr']['url'] = $img['src'];
					}
				}

				$item['content']['attr']['url']    = $img['src'];
				$item['content']['attr']['medium'] = 'image';
				if ( ! empty( $img['title'] ) ) {
					$item['content']['children']['title']['attr']['type'] = 'html';
					$item['content']['children']['title']['children'][]   = $img['title'];
				} elseif ( ! empty( $img['alt'] ) ) {
					$item['content']['children']['title']['attr']['type'] = 'html';
					$item['content']['children']['title']['children'][]   = $img['alt'];
				}
				if ( ! empty( $img['description'] ) ) {
					$item['content']['children']['description']['attr']['type'] = 'html';
					$item['content']['children']['description']['children'][]   = $img['description'];
				}
				if ( ! empty( $img['thumbnail'] ) ) {
					$item['content']['children']['thumbnail']['attr']['url'] = $img['thumbnail'];
				}
				$media[] = $item;
			}
		}

		$media = apply_filters( 'mrss_media', $media );
		$this->mrss_print( $media );
	}


	/**
	 * @todo Properly document
	 *
	 * @param $url
	 *
	 * @return string
	 */
	function mrss_url( $url ) {
		if ( preg_match( '`^http[s]?://`', $url ) ) {
			return $url;
		}
		if ( $url{0} === '/' ) {
			return rtrim( home_url(), '/' ) . $url;
		}

		return home_url() . $url;
	}


	/**
	 * @todo Properly document
	 *
	 * @param $link
	 * @param $id
	 *
	 * @return mixed
	 */
	function mrss_gallery_lookup( $link, $id ) {
		global $mrss_gallery_lookup;
		if ( preg_match( '` src="([^"]+)"`', $link, $matches ) ) {
			$mrss_gallery_lookup[ $matches[1] ] = $id;
		}

		return $link;
	}


	/**
	 * @param $media
	 */
	function mrss_print( $media ) {
		if ( ! empty( $media ) ) {
			foreach ( (array) $media as $element ) {
				$this->mrss_print_element( $element );
			}
		}
		echo "\n";
	}


	/**
	 * @param     $element
	 * @param int $indent
	 */
	function mrss_print_element( $element, $indent = 2 ) {
		echo "\n";
		foreach ( (array) $element as $name => $data ) {
			echo str_repeat( "\t", $indent ) . '<media:' . esc_attr( $name );

			if ( is_array( $data['attr'] ) && $data['attr'] !== array() ) {
				foreach ( $data['attr'] as $attr => $value ) {
					echo ' ' . esc_attr( $attr ) . '="' . esc_attr( ent2ncr( $value ) ) . '"';
				}
			}
			if ( is_array( $data['children'] ) && $data['children'] !== array() ) {
				$nl = false;
				echo '>';
				foreach ( $data['children'] as $_name => $_data ) {
					if ( is_int( $_name ) ) {
						echo ent2ncr( esc_html( $_data ) );
					} else {
						$nl = true;
						$this->mrss_print_element( array( $_name => $_data ), $indent + 1 );
					}
				}
				if ( $nl ) {
					echo "\n" . str_repeat( "\t", $indent );
				}
				echo '</media:' . esc_attr( $name ) . '>';
			} else {
				echo ' />';
			}
		}
	}

	/**
	 * Add the video output to the MRSS feed.
	 *
	 * @since 0.1
	 */
	function mrss_add_video( $media ) {
		global $post;

		$video = WPSEO_Meta::get_value( 'video_meta', $post->ID );

		if ( ! is_array( $video ) || $video === array() ) {
			return $media;
		}

		$video_duration = WPSEO_Meta::get_value( 'videositemap-duration', $post->ID );
		if ( $video_duration == 0 && isset( $video['duration'] ) ) {
			$video_duration = $video['duration'];
		}

		$item['content']['attr']['url']                             = $video['player_loc'];
		$item['content']['attr']['duration']                        = $video_duration;
		$item['content']['children']['player']['attr']['url']       = $video['player_loc'];
		$item['content']['children']['title']['attr']['type']       = 'html';
		$item['content']['children']['title']['children'][]         = esc_html( $video['title'] );
		$item['content']['children']['description']['attr']['type'] = 'html';
		$item['content']['children']['description']['children'][]   = esc_html( $video['description'] );
		$item['content']['children']['thumbnail']['attr']['url']    = $video['thumbnail_loc'];
		$item['content']['children']['keywords']['children'][]      = implode( ',', $video['tag'] );
		array_unshift( $media, $item );

		return $media;
	}

	/**
	 * Checks whether there are oembed URLs in the post that should be included in the video sitemap.
	 *
	 * @since 0.1
	 *
	 * @param string $content the content of the post.
	 *
	 * @return array|boolean returns array $urls with type of video as array key and video URL as content, or false on negative
	 */
	function grab_embeddable_urls( $content ) {
		global $echo;

		$options      = get_option( 'wpseo_video' );
		$evs_location = get_option( 'evs_location' );

		// For compatibility with Youtube Lyte and Smart Youtube Pro
		$content = str_replace( $this->alt_youtube_schemes, 'http://', $content );

		// Catch both the single line embeds as well as the embeds using the [embed] shortcode.
		preg_match_all( '`\[embed(?:[^\]]+)?\](http[s]?://[^\s"]+)\s*\[/embed\]`im', $content, $matches );
		preg_match_all( '`^\s*(?:<p>)?(http[s]?://[^\s"]+)\s*$`im', $content, $matches2 );

		$matched_urls = array();
		if ( isset( $matches[1] ) && is_array( $matches[1] ) ) {
			$matched_urls = array_merge( $matched_urls, $matches[1] );
		}
		if ( isset( $matches2[1] ) && is_array( $matches2[1] ) ) {
			$matched_urls = array_merge( $matched_urls, $matches2[1] );
		}

		if ( preg_match_all( '`(<iframe.*</iframe>)`s', $content, $iframes, PREG_SET_ORDER ) ) {
			foreach ( $iframes as $iframe ) {
				if ( preg_match( '`id=([\'"])vzvd-(\d+)\1`', $iframe[1], $iframesrc ) ) {
					if ( $options['vzaar_domain'] !== '' ) {
						$matched_urls[] = 'http://' . $options['vzaar_domain'] . '/' . $iframesrc[2] . '/video';
					} else {
						$matched_urls[] = 'http://view.vzaar.com/' . $iframesrc[2] . '/video';
					}
				} elseif ( preg_match( '`src=([\'"])([^\s\'"]+)\1`', $iframe[1], $iframesrc ) ) {
					$matched_urls[] = $iframesrc[2];
				}
			}
		}

		if ( preg_match_all( '`(<object.*</object>)`s', $content, $objects, PREG_SET_ORDER ) ) {
			foreach ( $objects as $object ) {
				if ( preg_match( '`<param name=([\'"])src\1 value=([\'"])([^\s\'"]+)\2`', $object[1], $srcmatch ) ) {
					$matched_urls[] = $srcmatch[3];
				} elseif ( preg_match( '`<param name=([\'"])movie\1 value=([\'"])([^\s\'"]+)\2`', $object[1], $moviematch ) ) {
					$matched_urls[] = $moviematch[3];
				}
			}
		}

		if ( preg_match( '`<a href=([\'"])(http[s]?://(?:www\.)?(?:youtube|vimeo)\.com/[^\s\'"]*)\1 rel=([\'"])wp-video-lightbox\3`', $content, $matches ) ) {
			$matched_urls[] = $matches[2];
		}

		if ( preg_match( '`<a class=([\'"])youtubepop\1 href=([\'"])(http[s]?://(?:www\.)?(?:youtube|vimeo)\.com/[^\s\'"]*)\2>`', $content, $matches ) ) {
			$matched_urls[] = $matches[3];
		}

		if ( preg_match( '`<a href=([\'"])([^\s\'"]+)\1 .*?data-titan-lightbox=([\'"])on\3[^>]*>`', $content, $matches ) ) {
			$matched_urls[] = $matches[2];
		}

		if ( $options['vzaar_domain'] !== '' ) {
			$vzaar_info = $this->parse_url( $options['vzaar_domain'] );
		}
		if ( ! isset( $vzaar_info['domain'] ) ) {
			$vzaar_info = array( 'domain' => 'vzaar.com' );
		}

		$evs_info = $this->parse_url( $evs_location );
		if ( ! isset( $evs_info ) || ! isset( $evs_info['domain'] ) ) {
			$evs_info = array( 'domain' => 'easyvideosuite.com' );
		}

		if ( count( $matched_urls ) > 0 ) {
			$urls = array();

			foreach ( $matched_urls as $match ) {
				if ( substr( $match, 0, 4 ) != 'http' ) {
					$match = 'http:' . $match;
				}
				if ( $echo && ( WP_DEBUG || ( defined( 'WPSEO_DEBUG' ) && WPSEO_DEBUG ) ) ) {
					echo esc_html( $match ) . '<br/>';
				}

				$url_info = $this->parse_url( $match );
				if ( ! isset( $url_info['domain'] ) ) {
					continue;
				}
				if ( $echo && ( WP_DEBUG || ( defined( 'WPSEO_DEBUG' ) && WPSEO_DEBUG ) ) ) {
					echo esc_html( $url_info['domain'] ) . '<br/>';
				}
				switch ( $url_info['domain'] ) {
					case 'animoto.com':
						$urls['animoto'] = $match;
						break;
					case 'blip.tv':
						$urls['blip'] = $match;
						break;
					case 'brightcove.com':
						if ( preg_match( '`<param name="flashVars" value="playerID=(\d+)`', $content, $bcmatch ) ) {
							$urls['brightcove'] = $bcmatch[1];
						}

						if ( preg_match( '`<param name="flashVars" value="videoId=(\d+)`', $content, $bcmatch ) ) {
							$urls['brightcove'] = $bcmatch[1];
						}
						break;
					case 'dailymotion.com':
						$urls['dailymotion'] = $match;
						break;
					case 'flickr.com':
						$urls['flickr'] = $match;
						break;
					case 'muzu.tv':
						$urls['muzutv'] = $match;
						break;
					case 'screenr.com':
						$urls['screenr'] = $match;
						break;
					case 'viddler.com':
						$urls['viddler'] = $match;
						break;
					case 'vimeo.com':
						$urls['vimeo'] = $match;
						break;
					case 'vzaar.com':
						$urls['vzaar'] = $match;
						break;
					case $vzaar_info['domain']:
						$urls['vzaar'] = $match;
						break;
					case 'wistia.com':
					case 'wistia.net':
						$urls['wistia'] = $match;
						break;
					case 'wordpress.tv':
						$urls['wordpress.tv'] = $match;
						break;
					case 'youtu.be':
						$urls['youtube'] = $match;
						break;
					case 'youtube.com':
						$urls['youtube'] = $match;
						break;
					case 'youtube-nocookie.com':
						$urls['youtube'] = $match;
						break;
					case $evs_info['domain']:
						$urls['evs'] = $match;
				}
			}

			if ( count( $urls ) > 0 ) {
				return $urls;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Checks whether there are oembed URLs in the post that should be included in the video sitemap.
	 * Uses DOMDocument and XPath to parse the content for url instead of preg matches
	 *
	 * @since 1.5.4.4
	 *
	 * @param string $content the content of the post.
	 *
	 * @return array|boolean returns array $urls with type of video as array key and video URL as content, or false on negative
	 */
	function grab_embeddable_urls_xpath( $content ) {
		global $echo;

		if ( ( ! is_string( $content ) || trim( $content ) === '' ) || ( ! extension_loaded( 'dom' ) || ! class_exists( 'DOMXPath'  ) ) ) {
			return false;
		}

		// For compatibility with Youtube Lyte and Smart Youtube Pro
		$content = str_replace( $this->alt_youtube_schemes, 'http://', $content );

		$dom = new DOMDocument();
		@$dom->loadHTML( $content );
		$xpath = new DOMXPath( $dom );

		$matched_urls = array();

		// for object embeds (i.e screencast.com)
		$objects = $xpath->query( '//object/param[@name="movie"] | //object/param[@name="src"]' );
		if ( is_object( $objects ) && $objects->length > 0 ) {
			foreach ( $objects as $object ) {
				$value          = $object->getAttribute( 'value' );
				$matched_urls[] = $value;
			}
		}

		// for iframe embeds (i.e. vidyard.com)
		$iframes = $xpath->query( '//iframe' );
		if ( is_object( $iframes ) && $iframes->length > 0 ) {
			foreach ( $iframes as $iframe ) {
				$src            = $iframe->getAttribute( 'src' );
				$matched_urls[] = $src;
			}
		}

		// additional check for vidyard embed with javascript and lightbox
		$script = $xpath->query( '//script[contains(@src,"play.vidyard.com")]' );
		if ( is_object( $script ) && $script->length > 0 ) {
			foreach ( $script as $element ) {
				$src            = $element->getAttribute( 'src' );
				$matched_urls[] = $src;
			}
		}

		// additional check for cincopa embed via javascript
		$script = $xpath->query( '//script/text()[contains(.,"cp_load_widget")]' );
		if ( is_object( $script ) && $script->length > 0 ) {
			foreach ( $script as $element ) {
				// Remove CDATA
				$src            = preg_replace( '`//\s*?<!\[CDATA\[\s*|\s*//\s*\]\]>`', '', $element->wholeText );
				$src            = 'http://cincopa.com?' . $src;
				$matched_urls[] = $src;
			}
		}

		// additional check for brightcove
		$script = $xpath->query( '//object/param[contains(@value,"brightcove.com")]/following-sibling::param[@name="flashVars"]' );
		if ( is_object( $script ) && $script->length > 0 ) {
			foreach ( $script as $element ) {
				$src            = $element->getAttribute( 'value' );
				$src            = 'http://brightcove.com?' . $src;
				$matched_urls[] = $src;
			}
		}

		/*
		if ( is_array( $iframes ) && $iframes !== array() ) {
			foreach ( $iframes as $iframe ) {
				if ( preg_match( '`id=([\'"]vzvd-(\d+)\1`', $iframe[1], $iframesrc ) ) {
					if ( $options['vzaar_domain'] !== '' ) {
						$matched_urls[] = 'http://' . $options['vzaar_domain'] . '/' . $iframesrc[2] . '/video';
					}
					else {
						$matched_urls[] = 'http://view.vzaar.com/' . $iframesrc[2] . '/video';
					}
				} elseif ( preg_match( '`src=([\'"])([^\s\'"]+)\1`', $iframe[1], $iframesrc ) ) {
					$matched_urls[] = $iframesrc[2];
				}
			}
		}
		*/

		if ( count( $matched_urls ) > 0 ) {
			$urls = array();

			foreach ( $matched_urls as $match ) {
				if ( substr( $match, 0, 4 ) != 'http' ) {
					$match = 'http:' . $match;
				}
				if ( $echo && ( WP_DEBUG || ( defined( 'WPSEO_DEBUG' ) && WPSEO_DEBUG ) ) ) {
					echo esc_html( $match ) . '<br/>';
				}

				$url_info = $this->parse_url( $match );
				if ( ! isset( $url_info['domain'] ) ) {
					continue;
				}
				if ( $echo && ( WP_DEBUG || ( defined( 'WPSEO_DEBUG' ) && WPSEO_DEBUG ) ) ) {
					echo esc_html( $url_info['domain'] ) . '<br/>';
				}
				switch ( $url_info['domain'] ) {
					case 'animoto.com':
						$urls['animoto'] = $match;
						break;
					case 'brightcove.com':
						$urls['brightcove'] = $match;
						break;
					case 'cincopa.com':
						$urls['cincopa'] = $match;
						break;
					case 'screenr.com':
						$urls['screenr'] = $match;
						break;
					// work around for screencast.com b/c there's no connection between url and the embed code
					case 'screencast.com':
						$urls['screencast']['url']   = $match;
						$urls['screencast']['embed'] = $content;
						break;
					case 'vidyard.com':
						$urls['vidyard'] = $match;
						break;

				}
			}

			if ( count( $urls ) > 0 ) {
				return $urls;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


	/**
	 * Parse a URL and find the host name and more.
	 *
	 * @since 1.1
	 *
	 * @link  http://php.net/manual/en/function.parse-url.php#83875
	 *
	 * @param string $url The URL to parse
	 *
	 * @return array
	 */
	function parse_url( $url ) {
		$r  = "^(?:(?P<scheme>\w+)://)?";
		$r .= "(?:(?P<login>\w+):(?P<pass>\w+)@)?";
		$r .= "(?P<host>(?:(?P<subdomain>[-\w\.]+)\.)?" . "(?P<domain>[-\w]+\.(?P<extension>\w+)))";
		$r .= "(?::(?P<port>\d+))?";
		$r .= "(?P<path>[\w/]*/(?P<file>\w+(?:\.\w+)?)?)?";
		$r .= "(?:\?(?P<arg>[\w=&]+))?";
		$r .= "(?:#(?P<anchor>\w+))?";
		$r  = "!$r!"; // Delimiters

		preg_match( $r, $url, $out );

		return $out;
	}


	/**
	 * Retrieve video details
	 *
	 * @since      1.7.0
	 * @see        WPSEO_Video_Details
	 *
	 * @param string $video_type  The video service the video is hosted on
	 * @param array  $vid         The video array with all the data.
	 * @param array  $old_vid     The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb       The URL to the manually set thumbnail, if available.
	 *
	 * @return array $vid
	 */
	function get_video_details( $video_type, $vid, $old_vid = array(), $thumb = '' ) {
		if ( $video_type === 'blip.tv' || $video_type === 'bliptv' ) {
			$video_type = 'blip';
		}

		$class = 'WPSEO_Video_Details_' . $video_type;
		if ( class_exists( $class ) ) {
			$video = new $class( $vid, $old_vid, $thumb );
			$vid   = $video->get_details();
		}

		return $vid;
	}


	/**
	 * Parse the content of a post or term description.
	 *
	 * @since 1.3
	 *
	 * @param string $content The content to parse for videos.
	 * @param array  $vid     The video array to update.
	 * @param array  $oldvid  The former video array.
	 *
	 * @return array $vid
	 */
	function index_content( $content, $vid, $oldvid = array() ) {
		global $shortcode_tags;
		$old_shortcode_tags = $shortcode_tags;

		$content = apply_filters( 'wpseo_video_index_content', $content, $vid );

		if ( preg_match( '`(<video.*</video>)`s', $content, $html5vid ) ) {

			if ( preg_match( '`src=([\'"])(.*?)\.(' . $this->video_ext_pattern . ')\1`', $html5vid[1], $content_loc ) ) {
				$vid['content_loc'] = $content_loc[2] . '.' . $content_loc[3];

				if ( preg_match( '`poster=([\'"])([^\'"\s]+)\1`', $html5vid[1], $thumbnail_loc ) ) {
					$vid['thumbnail_loc'] = $thumbnail_loc[2];
				}

				$vid['type'] = 'html5vid';
			}
		}

		if ( preg_match( '`<div id=([\'"])wistia_([^\'"\s]+)\1 class=([\'"])wistia_embed[^\'"]*\3`', $content, $matches ) ) {
			$vid['id'] = $matches[2];
			$vid       = $this->get_video_details( 'wistia', $vid, $oldvid );
		}

		if ( preg_match( '`<a(?:.*?)href="http[s]?://fast\.wistia\.(?:com|net)/embed/iframe/([^\?]+)\?`', $content, $matches ) ) {
			$vid['id'] = $matches[1];
			$vid       = $this->get_video_details( 'wistia', $vid, $oldvid );
		}

		if ( isset( $vid['content_loc'] ) || isset( $vid['player_loc'] ) ) {
			$vid = apply_filters( 'wpseo_video_' . $vid['type'] . '_details', $vid );

			return $vid;
		}

		$shortcode_tags = array(
			'bliptv'                 => '',
			'blip.tv'                => '',
			'cincopa'                => '',
			'dailymotion'            => '',
			'embedplusvideo'         => '',
			'ez_video'               => '',
			'ez_youtube'             => '',
			'ez_vimeo'               => '',
			'flickrvideo'            => '',
			'flowplayer'             => '',
			'flv'                    => '',
			'fvplayer'               => '',
			'jwplayer'               => '',
			'lyte'                   => '',
			'metacafe'               => '',
			'pb_vidembed'            => '',
			'sublimevideo'           => '',
			'sublimevideo-lightbox'  => '',
			'tube'                   => '',
			'tubepress'              => '',
			'veoh'                   => '',
			'viddler'                => '',
			'video'                  => '',
			'videojs'                => '',
			'video_lightbox_vimeo5'  => '',
			'video_lightbox_youtube' => '',
			'vimeo'                  => '',
			'vippy'                  => '',
			'vzaarmedia'             => '',
			'weaver_vimeo'           => '',
			'weaver_youtube'         => '',
			'wpvideo'                => '',
			'wposflv'                => '',
			'youtube'                => '',
			'youtubewd'              => '',
			'youtube_sc'             => '',
			'youtube-embed'          => '',
			'youtube-white-label'    => '',
		);
		if ( preg_match( '/' . get_shortcode_regex() . '/', $content, $matches ) ) {
			$thumb = '';
			if ( preg_match( '`image=([\'"])?([^\'"]+)\1?`', $matches[3], $match ) ) {
				$thumb = $match[2];
			}
			unset( $match );

			// support for Advanced Responsive Video Embedder plugin shortcode format
			// [provider id="id"]
			$embedder = shortcode_parse_atts( $matches[3] );

			switch ( $matches[2] ) {
				case 'bliptv':
					$vid['id'] = trim( $matches[3] );
					$vid       = $this->get_video_details( $matches[2], $vid, $oldvid, $thumb );

					/* @todo: embedder uses an embed id but to fetch the details
						requires post_id or url. Need to find the link between them
						[bliptv id="hdljgdbVBwI"]
						http://blip.tv/rss/view/3516963
						http://blip.tv/day9tv/day-9-daily-101-kawaii-rice-tvp-style-3516963

						if ( isset( $embedder['id'] ) && $embedder['id'] ) {
							$vid['id'] = trim( $embedder['id'] );
							$vid       = $this->get_video_details( $matches[2], $vid, $oldvid, $thumb );
						}
					*/

					break;
				case 'blip.tv':
					if ( preg_match( '`posts_id=(\d+)`', $matches[3], $match ) ) {
						$vid['id'] = $match[1];
						$vid       = $this->get_video_details( $matches[2], $vid, $oldvid, $thumb );
					}
					break;
				case 'cincopa':
					if ( $embedder ) {
						$vid['id'] = $embedder[0];
						$vid       = $this->get_video_details( $matches[2], $vid, $oldvid, $thumb );
					}
					break;
				case 'dailymotion':
					// @TODO: SSL certificate problem, verify that the CA cert is OK. Details:
					// error:14090086:SSL routines:SSL3_GET_SERVER_CERTIFICATE:certificate verify failed

					if ( isset( $embedder['id'] ) && $embedder['id'] ) {
						$vid['id'] = $embedder['id'];
					} elseif ( ! empty( $matches[5] ) ) {
						$vid['url'] = $matches[5];
					} elseif ( ! empty( $matches[3] ) ) {
						$url_or_id = trim( $matches[3] );
						if ( strpos( $url_or_id, 'http' ) === 0 ) {
							$vid['url'] = $url_or_id;
						} else {
							$vid['id'] = $url_or_id;
						}
					}
					$vid = $this->get_video_details( $matches[2], $vid, $oldvid, $thumb );
					break;
				case 'embedplusvideo':
					if ( preg_match( '`standard=([\'"])([^\'"]+)\1`', $matches[3], $match ) ) {
						$vid['url'] = $match[2];
						$vid        = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
					}
					break;
				case 'ez_video':
					if ( $embedder['player'] == 'flowplayer' ) {
						$vid['type'] = 'flowplayer';
					} elseif ( $embedder['player'] == 'jwplayer' ) {
						$vid['type'] = 'jwplayer';
					}

					$vid['url']           = $embedder['url'];
					$vid['player_loc']    = $embedder['url'];
					$vid['thumbnail_loc'] = isset( $embedder['splash'] ) ? $embedder['splash'] : '';
					$vid['width']         = $embedder['width'];
					$vid['height']        = $embedder['height'];
					break;
				case 'ez_youtube':
					$vid['url'] = $embedder['url'];
					$vid        = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
					break;
				case 'ez_vimeo':
					$vid['url'] = $embedder['url'];
					$vid        = $this->get_video_details( 'vimeo', $vid, $oldvid, $thumb );
					break;
				case 'flickrvideo':
					$vid['url'] = $matches[5];
					$vid        = $this->get_video_details( 'flickr', $vid, $oldvid, $thumb );
					break;
				case 'flowplayer':
				case 'fvplayer':
					if ( preg_match( '`src=([\'"])?((?:.*?)\.(?:' . $this->video_ext_pattern . '))\1?`', $matches[0], $match ) ) {
						$vid['content_loc'] = $match[2];

						if ( preg_match( '`splash=([\'"])?((?:.*?)\.(?:' . self::$image_ext_pattern . '))\1?`', $matches[0], $match ) ) {
							$vid['thumbnail_loc'] = $match[2];
						}
						$vid['type'] = 'jwplayer';
					}
					break;
				case 'flv':
					$vid['content_loc']   = $matches[5];
					$vid['player_loc']    = plugins_url( '/vipers-video-quicktags/resources/jw-flv-player/player.swf?file=' . urlencode( $matches[5] ) );
					$vid['thumbnail_loc'] = $thumb;
					$vid['id']            = md5( $matches[5] );
					$vid['type']          = 'flv';
					break;
				case 'jwplayer':
					$vid['type'] = 'jwplayer';
					if ( preg_match( '`mediaid=([\'"])?(\d+)\1?`', $matches[0], $match ) ) {
						$vid['content_loc']   = wp_get_attachment_url( $match[2] );
						$vid['duration']      = get_post_meta( $match[2], 'jwplayermodule_duration', true );
						$vid['thumbnail_loc'] = get_post_meta( $match[2], 'jwplayermodule_thumbnail_url', true );
					} else {
						if ( preg_match( '`html5_file=([\'"])?([^\'"\s]+)\1?`', $matches[0], $match ) ) {
							$vid['content_loc'] = $match[2];
						} elseif ( preg_match( '`file=([\'"])?([^\'"\s]+)\1?`', $matches[0], $match ) ) {
							$vid['content_loc'] = $match[2];
						}
						if ( isset( $vid['content_loc'] ) && preg_match( '`image=([\'"])?([^\'"\s]+)\1?`', $matches[0], $match ) ) {
							$vid['thumbnail_loc'] = $match[2];
						}
					}
					break;
				case 'lyte':
					if ( preg_match( '`id=([\'"])([^\'"]+)\1`', $matches[0], $match ) ) {
						$vid['type'] = 'youtube';
						$vid['url']  = $match[2];
						$vid         = $this->get_video_details( $vid['type'], $vid );
					}
					break;
				case 'metacafe':
					if ( isset( $embedder['id'] ) && $embedder['id'] ) {
						$vid['id'] = $embedder['id'];
					} elseif ( ! empty( $matches[5] ) ) {
						$vid['url'] = $matches[5];
					} elseif ( ! empty( $matches[3] ) ) {
						$vid['id'] = trim( $matches[3] );
					}
					$vid = $this->get_video_details( $matches[2], $vid, $oldvid, $thumb );
					break;
				case 'pb_vidembed':
					if ( preg_match( '`url=([\'"])([^\'"]+)\1`', $matches[0], $match ) ) {
						$vid['url'] = $match[2];
						if ( strpos( $vid['url'], 'youtube' ) ) {
							$vid = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
						} elseif ( strpos( $vid['url'], 'vimeo' ) ) {
							$vid = $this->get_video_details( 'vimeo', $vid, $oldvid, $thumb );
						} else {
							$vid['content_loc'] = $vid['url'];
							$vid['type']        = 'pb_embed';
						}
					}
					break;
				case 'sublimevideo':
				case 'sublimevideo-lightbox':
					if ( preg_match( '`src1=([\'"])(?:\(hd\))?([^\'"]+)\1`', $matches[0], $match ) ) {
						$vid['content_loc'] = $match[2];
						if ( preg_match( '`poster=([\'"])([^\'"]+)\1`', $matches[0], $match ) ) {
							$vid['thumbnail_loc'] = $match[2];
						}

						$vid['type'] = 'Sublime';
					}
					break;
				case 'tube':
					$vid['url'] = $matches[5];
					$vid        = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
					break;
				case 'tubepress':
					if ( preg_match( '`video=([\'"])(' . self::$youtube_id_pattern . ')\1`', $matches[0], $match ) ) {
						$vid['id'] = $match[2];
						$vid       = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
					}
					break;
				case 'veoh':
					if ( isset( $embedder['id'] ) && $embedder['id'] ) {
						$vid['id'] = $embedder['id'];
					} elseif ( ! empty( $matches[5] ) ) {
						$vid['url'] = $matches[5];
					} elseif ( ! empty( $matches[3] ) ) {
						$vid['id'] = trim( $matches[3] );
					}
					$vid = $this->get_video_details( $matches[2], $vid, $oldvid, $thumb );
					break;
				case 'viddler':
					if ( isset( $embedder['id'] ) && $embedder['id'] ) {
						$vid['id'] = $embedder['id'];
						$vid       = $this->get_video_details( $matches[2], $vid, $oldvid, $thumb );
					} elseif ( preg_match( '`id=([^&]+)`', $matches[0], $match ) ) {
						$vid['id'] = $match[1];
						$vid       = $this->get_video_details( $matches[2], $vid, $oldvid, $thumb );
					}
					break;
				case 'video':
				case 'videojs':
					if ( preg_match( '`(?:mp4|name|ogg|webm|src)=([\'"])([^\'"]+)\1`', $matches[3], $match ) ) {

						$src         = $match[2];
						$vid['type'] = 'mediaelement-js';

						// If the src has an extension, use it as content_loc, otherwise, see if we can find the file
						if ( substr( $src, 0, 1 ) == '/' ) {
							$info               = parse_url( get_site_url() );
							$vid['content_loc'] = $info['scheme'] . '://' . $info['host'] . $src;
						} else {
							$vid['content_loc'] = $src;
						}

						// If a poster image was specified, use that, otherwise, try and find a suitable .jpg
						if ( preg_match( '`poster=([\'"])([^\'"]+)\1`', $matches[3], $match ) ) {
							if ( substr( $match[2], 0, 1 ) == '/' ) {
								$info                 = parse_url( get_site_url() );
								$vid['thumbnail_loc'] = $info['scheme'] . '://' . $info['host'] . $match[2];
							} else {
								$vid['thumbnail_loc'] = $match[2];
							}
						} else {
							$img_file = preg_replace( '`\.(' . $this->video_ext_pattern . ')`', '.jpg', $vid['content_loc'] );
							if ( file_exists( $img_file ) ) {
								$vid['thumbnail_loc'] = $img_file;
							}
						}
					}
					break;
				case 'video_lightbox_vimeo5':
					if ( preg_match( '`video_id=([\'"])?(\d+)\1?`', $matches[0], $match ) ) {
						$vid['id'] = $match[2];
						$vid       = $this->get_video_details( 'vimeo', $vid, $oldvid, $thumb );
					}
					break;
				case 'video_lightbox_youtube':
					if ( preg_match( '`video_id=([\'"])?(' . self::$youtube_id_pattern . ')\1?`', $matches[0], $match ) ) {
						$vid['id'] = $match[2];
						$vid       = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
					}
					break;
				case 'vimeo':
					if ( isset( $embedder['id'] ) && $embedder['id'] ) {
						$vid['id'] = $embedder['id'];
					} elseif ( ! empty( $matches[5] ) ) {
						$vid['url'] = $matches[5];
					} elseif ( ! empty( $matches[3] ) ) {
						$vid['id'] = trim( $matches[3] );
					} elseif ( preg_match( '`id=([\'"])?([0-9]+)\1?`', $matches[0], $match ) ) {
						$vid['id'] = trim( $match[2] );
					}
					$vid = $this->get_video_details( $matches[2], $vid, $oldvid, $thumb );
					break;
				case 'vippy':
					$atts      = shortcode_parse_atts( $matches[3] );
					$vid['id'] = isset( $atts['id'] ) ? $atts['id'] : 0;
					$vid       = $this->get_video_details( $matches[2], $vid, $oldvid, $thumb );
					break;
				case 'vzaarmedia':
					$atts      = shortcode_parse_atts( $matches[3] );
					$vid['id'] = isset( $atts['vid'] ) ? $atts['vid'] : 0;
					if ( $vid['id'] != 0 ) {
						$vid = $this->get_video_details( 'vzaar', $vid, $oldvid, $thumb );
					}
					break;
				case 'wpvideo':
				case 'videopress':
					if ( preg_match( '`^([^\s]+)`', trim( $matches[3] ), $match ) ) {
						$vid['id'] = $match[1];
						$vid       = $this->get_video_details( 'videopress', $vid, $oldvid, $thumb );
					}
					break;
				case 'weaver_vimeo':
					// [weaver_vimeo vimeo-url id=videoid sd=0 percent=100 ratio=0.5625 center=1 color=#hex autoplay=0 loop=0 portrait=1 title=1 byline=1]
					if ( preg_match( '`\[weaver_vimeo (http[s]?://(?:www\.)?vimeo\.com/\d+)`', $matches[0], $match ) ) {
						$vid['url'] = $match[1];
					} elseif ( preg_match( '`id=(\d+)`', $matches[3], $match ) ) {
						$vid['id'] = $match[1];
					} else {
						break;
					}

					$vid = $this->get_video_details( 'vimeo', $vid, $oldvid, $thumb );
					break;
				case 'weaver_youtube':
					if ( preg_match( '`\[weaver_youtube (http[^\s]+)`', $matches[0], $match ) ) {
						$vid['url'] = $match[1];
					} elseif ( preg_match( '`id=([^\s]+)`', $matches[3], $match ) ) {
						$vid['id'] = $match[1];
					} else {
						break;
					}
					$vid = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
					break;
				case 'wposflv':
					if ( class_exists( 'WPOSFLV' ) ) {
						$vid['type'] = 'wposflv';

						// If the src has an extension, use it as content_loc, otherwise, see if we can find the file
						$src = $embedder['src'];
						if ( substr( $src, strlen( $src ) - 4, 1 ) == '.' ) {
							$vid['content_loc'] = $src;
						} else {
							if ( substr( $src, 0, 4 ) != 'http' ) {
								$filename = WP_CONTENT_DIR . substr( $src, strlen( WP_CONTENT_DIR ) - strrpos( WP_CONTENT_DIR, '/' ) );
							} else {
								$filename = WP_CONTENT_DIR . substr( $src, strlen( WP_CONTENT_URL ) );
							}

							if ( file_exists( $filename . '.mp4' ) ) {
								$vid['content_loc'] = $src . '.mp4';
							} elseif ( file_exists( $filename . '.m4v' ) ) {
								$vid['content_loc'] = $src . '.m4v';
							}
						}

						if ( isset( $embedder['width'] ) && $embedder['width'] ) {
							$vid['width'] = $embedder['width'];
						}

						if ( isset( $embedder['height'] ) && $embedder['height'] ) {
							$vid['height'] = $embedder['height'];
						}


						if ( $thumb != '' ) {
							$vid['thumbnail_loc'] = $thumb;
						} else {
							// If a thumbnail image was specified in the shortcode, use that, otherwise, try and find a suitable .jpg
							$thumbnail = $embedder['previewimage'];
							if ( $thumbnail ) {
								$vid['thumbnail_loc'] = $thumbnail;
							} else {
								$img_file = preg_replace( '`\.(' . $this->video_ext_pattern . ')`', '.jpg', $vid['content_loc'] );
								if ( file_exists( $img_file ) ) {
									$vid['thumbnail_loc'] = $img_file;
								}
							}
						}
					}
					break;
				case 'youtube':
				case 'youtubewd':
				case 'youtube-embed':
				case 'youtube-white-label':
					if ( isset( $embedder['id'] ) && $embedder['id'] ) {
						$vid['id'] = $embedder['id'];
						$vid       = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
					} else {
						if ( ! empty( $matches[5] ) ) {
							if ( preg_match( '`^(' . self::$youtube_id_pattern . ')$`', $matches[5] ) ) {
								$vid['id'] = $matches[5];
							} else {
								$vid['url'] = $matches[5];
							}
							$vid = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
						} elseif ( preg_match( '`id=([\'"])?(' . self::$youtube_id_pattern . ')\1?`', $matches[0], $match ) ) {
							$vid['id'] = trim( $match[2] );
							$vid       = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
						} elseif ( preg_match( '`v=(' . self::$youtube_id_pattern . ')`', $matches[0], $match ) ) {
							$vid['id'] = $match[1];
							$vid       = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
						}
					}
					break;
				case 'youtube_sc':
					if ( preg_match( '`(?:url|v|video)=([\'"])([^\'"]+)\1`', $matches[3], $match ) ) {
						$vid['url'] = $match[2];
						$vid        = $this->get_video_details( 'youtube', $vid, $oldvid, $thumb );
					}
					break;
				default:
					if ( WP_DEBUG || ( defined( 'WPSEO_DEBUG' ) && WPSEO_DEBUG ) ) {
						echo '<pre>' . print_r( $matches, true ) . '</pre>';
						echo '<pre>' . print_r( $vid, true ) . '</pre>';
					}
					$vid = false;
					break;
			}
			if ( isset( $vid['content_loc'] ) || isset( $vid['player_loc'] ) ) {
				$vid = apply_filters( 'wpseo_video_' . $vid['type'] . '_details', $vid );

				return $vid;
			}
		}

		if ( ! isset( $vid['id'] ) && $oembed = $this->grab_embeddable_urls_xpath( $content ) ) {
			if ( is_array( $oembed ) && $oembed !== array() ) {
				$allowed_types = array(
					'animoto',
					'brightcove',
					'cincopa',
					'screencast',
					'vidyard',
				);

				foreach ( $oembed as $type => $url ) {
					$vid['url'] = $url;

					if ( in_array( $type, $allowed_types, true ) ) {
						$vid = $this->get_video_details( $type, $vid, $oldvid );
					}

					if ( isset( $vid['content_loc'] ) || isset( $vid['player_loc'] ) ) {
						$vid = apply_filters( 'wpseo_video_' . $vid['type'] . '_details', $vid );

						return $vid;
					}
				}
			}
			unset( $oembed, $allowed_types );
		}

		if ( ! isset( $vid['id'] ) && $oembed = $this->grab_embeddable_urls( $content ) ) {
			if ( is_array( $oembed ) && $oembed !== array() ) {
				$allowed_types = array(
					'animoto',
					'blip',
					'blip.tv',
					'bliptv',
					'brightcove',
					'dailymotion',
					'evs',
					'flickr',
					'muzutv',
					'screenr',
					'viddler',
					'vimeo',
					'vzaar',
					'wistia',
					'wordpress.tv',
					'youtube',
				);

				foreach ( $oembed as $type => $url ) {
					$vid['url'] = $url;

					if ( in_array( $type, $allowed_types, true ) ) {
						$vid = $this->get_video_details( $type, $vid, $oldvid );
					}

					if ( isset( $vid['content_loc'] ) || isset( $vid['player_loc'] ) ) {
						$vid = apply_filters( 'wpseo_video_' . $vid['type'] . '_details', $vid );

						return $vid;
					}
				}
			}
			unset( $oembed, $allowed_types );
		}

		// support for wordpress-automatic-youtube-video-post plugin
		if ( is_plugin_active( 'automatic-youtube-video-posts/tern_wp_youtube.php' ) ) {
			$youtube_id = get_post_meta( $vid['post_id'], '_tern_wp_youtube_video', true );

			if ( $youtube_id ) {
				$vid['id'] = $youtube_id;
				$vid = $this->get_video_details( 'youtube', $vid, $oldvid );
			}

			if ( isset( $vid['content_loc'] ) || isset( $vid['player_loc'] ) ) {
				$vid = apply_filters( 'wpseo_video_' . $vid['type'] . '_details', $vid );

				return $vid;
			}
		}
		$shortcode_tags = $old_shortcode_tags;

		return 'none';
	}

	/**
	 * Check and, if applicable, update video details for a term description
	 *
	 * @since 1.3
	 *
	 * @param object  $term The term to check the description and possibly update the video details for.
	 * @param boolean $echo Whether or not to echo the performed actions.
	 *
	 * @return mixed $vid The video array that was just stored, or "none" if nothing was stored
	 *                    or false if not applicable.
	 */
	function update_video_term_meta( $term, $echo = false ) {
		$options = array_merge( WPSEO_Options::get_all(), get_option( 'wpseo_video' ) );

		if ( ! is_array( $options['videositemap_taxonomies'] ) || $options['videositemap_taxonomies'] === array() ) {
			return false;
		}

		if ( ! in_array( $term->taxonomy, $options['videositemap_taxonomies'] ) ) {
			return false;
		}

		$tax_meta = get_option( 'wpseo_taxonomy_meta' );
		$oldvid   = array();
		if ( ! isset( $_POST['force'] ) ) {
			if ( isset( $tax_meta[ $term->taxonomy ]['_video'][ $term->term_id ] ) ) {
				$oldvid = $tax_meta[ $term->taxonomy ]['_video'][ $term->term_id ];
			}
		}

		$vid = array();

		$title = WPSEO_Taxonomy_Meta::get_term_meta( $term->term_id, $term->taxonomy, 'wpseo_title' );
		if ( empty( $title ) && isset( $options['title-' . $term->taxonomy] ) && $options['title-' . $term->taxonomy] !== '' ) {
			$title = wpseo_replace_vars( $options['title-' . $term->taxonomy], (array) $term );
		}
		if ( empty( $title ) ) {
			$title = $term->name;
		}
		$vid['title'] = htmlspecialchars( $title );

		$vid['description'] = WPSEO_Taxonomy_Meta::get_term_meta( $term->term_id, $term->taxonomy, 'wpseo_metadesc' );
		if ( ! $vid['description'] ) {
			$vid['description'] = esc_attr( preg_replace( '`\s+`', ' ', wp_html_excerpt( $this->strip_shortcodes( get_term_field( 'description', $term->term_id, $term->taxonomy ) ), 300 ) ) );
		}

		$vid['publication_date'] = date( 'Y-m-d\TH:i:s+00:00' );

		// concatenate genesis intro text and term description to index the videos for both
		$genesis_term_meta = get_option( 'genesis-term-meta' );

		$content = '';
		if ( isset( $genesis_term_meta[ $term->term_id ]['intro_text'] ) && $genesis_term_meta[ $term->term_id ]['intro_text'] ) {
			$content .= $genesis_term_meta[ $term->term_id ]['intro_text'];
		}

		$content .= "\n" . $term->description;
		$content  = stripslashes( $content );

		$vid = $this->index_content( $content, $vid, $oldvid );

		if ( $vid != 'none' ) {
			$tax_meta[ $term->taxonomy ]['_video'][ $term->term_id ] = $vid;
			// Don't bother with the complete tax meta validation
			$tax_meta['wpseo_already_validated'] = true;
			update_option( 'wpseo_taxonomy_meta', $tax_meta );

			if ( $echo ) {
				$link = get_term_link( $term );
				if ( ! is_wp_error( $link ) ) {
					echo 'Updated <a href="' . esc_url( $link ) . '">' . esc_html( $vid['title'] ) . '</a> - ' . esc_html( $vid['type'] ) . '<br/>';
				}
			}
		}

		return $vid;
	}


	/**
	 * (Don't) validate the _video taxonomy meta data array
	 * Doesn't actually validate it atm, but having this function hooked in *does* make sure that the
	 * _video taxonomy meta data is not removed as it otherwise would be (by the normal taxonomy meta validation).
	 *
	 * @since 1.6
	 *
	 * @param  array $tax_meta_data Received _video tax meta data
	 *
	 * @return  array  Validated _video tax meta data
	 */
	function validate_video_tax_meta( $tax_meta_data ) {
		return $tax_meta_data;
	}

	/**
	 * Returns the custom fields to check for posts.
	 *
	 * @param int $post_id The ID of the post to grab the custom fields for.
	 *
	 * @since 1.3.4
	 *
	 * @return array $custom_fields Array of custom field values.
	 */
	function get_custom_fields( $post_id ) {
		$custom_fields = array(
			'videoembed', // Press75 Simple Video Embedder
			'_videoembed_manual', // Press75 Simple Video Embedder
			'_videoembed', // Press75 Simple Video Embedder
			'_premise_settings', // Premise
		);
		$options       = get_option( 'wpseo_video' );
		if ( $options['custom_fields'] !== '' ) {
			$setting       = (array) explode( ',', $options['custom_fields'] );
			$setting       = array_map( 'trim', $setting );
			$custom_fields = array_merge( $custom_fields, $setting );
		}

		$values = array();
		foreach ( (array) $custom_fields as $cf ) {
			$meta_val = get_post_meta( $post_id, $cf, true );
			if ( is_array( $meta_val ) && $meta_val !== array() ) {
				foreach ( $meta_val as $val ) {
					$values[] = $val;
				}
			} elseif ( is_scalar( $meta_val ) ) {
				$values[] = $meta_val;
			}
		}

		return $values;
	}

	/**
	 * Check and, if applicable, update video details for a post
	 *
	 * @since 0.1
	 *
	 * @param object  $post The post to check and possibly update the video details for.
	 * @param boolean $echo Whether or not to echo the performed actions.
	 *
	 * @return mixed $vid The video array that was just stored, or "none" if nothing was stored
	 *                    or false if not applicable.
	 */
	function update_video_post_meta( $post, $echo = false ) {
		global $wp_query;

		if ( is_numeric( $post ) ) {
			$post = get_post( $post );
		}

		if ( ! isset( $post->ID ) ) {
			return false;
		}

		$options = array_merge( WPSEO_Options::get_all(), get_option( 'wpseo_video' ) );

		if ( ! is_array( $options['videositemap_posttypes'] ) || $options['videositemap_posttypes'] === array() ) {
			return false;
		}

		if ( ! in_array( $post->post_type, $options['videositemap_posttypes'] ) ) {
			return false;
		}

		$_GLOBALS['post'] = $post;

		$oldvid = array();
		if ( ! isset( $_POST['force'] ) ) {
			$oldvid = WPSEO_Meta::get_value( 'video_meta', $post->ID );
		}

		$title = WPSEO_Meta::get_value( 'title', $post->ID );
		if ( ( ! is_string( $title ) || $title === '' ) && isset( $options['title-' . $post->post_type] ) && $options['title-' . $post->post_type] !== '' ) {
			$title = wpseo_replace_vars( $options['title-' . $post->post_type], (array) $post );
		} elseif ( ( ! is_string( $title ) || $title === '' ) && ( ! isset( $options['title-' . $post->post_type] ) || $options['title-' . $post->post_type] === '' ) ) {
			$title = wpseo_replace_vars( '%%title%% - %%sitename%%', (array) $post );
		}

		if ( ! is_string( $title ) || $title === '' ) {
			$title = $post->post_title;
		}

		$vid = array();

		// @todo [JRF->Yoast] Verify if this is really what we want. What about non-hierarchical custom post types ? and are we adjusting the main query output now ? could this cause bugs for others ?
		if ( $post->post_type == 'post' ) {
			$wp_query->is_single = true;
			$wp_query->is_page   = false;
		} else {
			$wp_query->is_single = false;
			$wp_query->is_page   = true;
		}

		$vid['post_id'] = $post->ID;

		$vid['title']            = htmlspecialchars( $title );
		$vid['publication_date'] = mysql2date( 'Y-m-d\TH:i:s+00:00', $post->post_date_gmt );

		$vid['description'] = WPSEO_Meta::get_value( 'metadesc', $post->ID );
		if ( ! is_string( $vid['description'] ) || $vid['description'] === '' ) {
			$vid['description'] = esc_attr( preg_replace( '`\s+`', ' ', wp_html_excerpt( $this->strip_shortcodes( $post->post_content ), 300 ) ) );
		}

		$content       = $post->post_content;
		$index         = true;
		$custom_fields = $this->get_custom_fields( $post->ID );
		if ( is_array( $custom_fields ) && $custom_fields !== array() ) {
			foreach ( $custom_fields as $cf_val ) {
				if ( is_array( $cf_val ) ) {
					$cf_val = $cf_val[0];
				}

				// Silly, silly themes _encode_ the value of the post meta field. Yeah it's ridiculous. But this fixes it.
				if ( strpos( $cf_val, '&lt;' ) !== false ) {
					$cf_val = html_entity_decode( $cf_val );
				}

				if ( preg_match( '`\.(?:' . $this->video_ext_pattern . ')$`', $cf_val ) ) {
					$vid['content_loc'] = $cf_val;
					$vid['type']        = 'custom_field';

					$index = false;
				} else {
					$content .= "\n" . $cf_val . "\n";
				}
			}
		}

		if ( $index ) {
			$vid = $this->index_content( $content, $vid, $oldvid );
		}

		if ( $vid == false || ! is_array( $vid ) ) {
			$vid = 'none';
		}

		if ( 'none' != $vid ) {
			if ( ! isset( $vid['thumbnail_loc'] ) || empty( $vid['thumbnail_loc'] ) ) {
				$img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
				if ( strpos( $img[0], 'http' ) !== 0 ) {
					$vid['thumbnail_loc'] = get_site_url( null, $img[0] );
				} else {
					$vid['thumbnail_loc'] = $img[0];
				}
			}

			// Grab the meta data from the post
			if ( isset( $_POST['yoast_wpseo_videositemap-category'] ) && ! empty( $_POST['yoast_wpseo_videositemap-category'] ) ) {
				$vid['category'] = sanitize_text_field( $_POST['yoast_wpseo_videositemap-category'] );
			} else {
				$cats = wp_get_object_terms( $post->ID, 'category', array( 'fields' => 'names' ) );
				if ( isset( $cats[0] ) ) {
					$vid['category'] = $cats[0];
				}
				unset( $cats );
			}

			$tags = wp_get_object_terms( $post->ID, 'post_tag', array( 'fields' => 'names' ) );

			if ( isset( $_POST['yoast_wpseo_videositemap-tags'] ) && ! empty( $_POST['yoast_wpseo_videositemap-tags'] ) ) {
				$extra_tags = explode( ',', sanitize_text_field( $_POST['yoast_wpseo_videositemap-tags'] ) );
				$tags       = array_merge( $extra_tags, $tags );
			}

			$tag = array();
			if ( is_array( $tags ) ) {
				foreach ( $tags as $t ) {
					$tag[] = $t;
				}
			} elseif ( isset( $cats[0] ) ) {
				$tag[] = $cats[0]->name;
			}

			$focuskw = WPSEO_Meta::get_value( 'focuskw', $post->ID );
			if ( ! empty( $focuskw ) ) {
				$tag[] = $focuskw;
			}
			$vid['tag'] = $tag;

			if ( $echo ) {
				echo 'Updated <a href="' . esc_url( add_query_arg( array( 'p' => $post->ID ), home_url() ) ) . '">' . esc_html( $post->post_title ) . '</a> - ' . esc_html( $vid['type'] ) . '<br/>';
			}
		}

		WPSEO_Meta::set_value( 'video_meta', $vid, $post->ID );

		//echo '<pre>' . print_r( $_POST, true ) . '</pre>';
		return $vid;
	}

	/**
	 * Remove both used and unused shortcodes from content.
	 *
	 * @todo [JRF -> Yoast] Why not use the WP native strip_shortcodes function ?
	 * @internal adjusted to prevent stripping of escaped shortcodes which are meant to be displayed literally
	 *
	 * @since 1.3.3
	 *
	 * @param string $content Content to remove shortcodes from.
	 *
	 * @return string
	 */
	function strip_shortcodes( $content ) {
		$regex   = '`(?:^|[^\[])(\[[^\]]+\])(?:.*?(\[/[^\]]+\])(?:[^\]]|$))?`s';
		$content = preg_replace( $regex, '', $content );

		return $content;
	}

	/**
	 * Check whether the current visitor is really Google or Bing's bot by doing a reverse DNS lookup
	 *
	 * @since 1.2.2
	 *
	 * @return boolean
	 */
	function is_valid_bot() {
		if ( preg_match( '`(Google|bing)bot`', sanitize_text_field( $_SERVER['HTTP_USER_AGENT'] ), $match ) ) {
			$hostname = gethostbyaddr( sanitize_text_field( $_SERVER['REMOTE_ADDR'] ) );

			if (
					( $match[1] === 'Google' && preg_match( '`googlebot\.com$`', $hostname ) && gethostbyname( $hostname ) == $_SERVER['REMOTE_ADDR'] ) ||
					( $match[1] === 'bing' && preg_match( '`search\.msn\.com$`', $hostname ) && gethostbyname( $hostname ) == $_SERVER['REMOTE_ADDR'] )
			) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Check to see if the video thumbnail was manually set, if so, update the $video array.
	 *
	 * @param int   $post_id The post to check for.
	 * @param array $video   The video array.
	 *
	 * @return array
	 */
	function get_video_image( $post_id, $video ) {
		// Allow for the video's thumbnail to be overridden by the meta box input
		$videoimg = WPSEO_Meta::get_value( 'videositemap-thumbnail', $post_id );
		if ( $videoimg !== '' ) {
			$video['thumbnail_loc'] = $videoimg;
		}

		return $video;
	}

	/**
	 * Outputs the XSL file
	 */
	function build_video_sitemap_xsl() {

		$protocol = 'HTTP/1.1';
		if ( isset( $_SERVER['SERVER_PROTOCOL'] ) && $_SERVER['SERVER_PROTOCOL'] !== '' ) {
			$protocol = sanitize_text_field( $_SERVER['SERVER_PROTOCOL'] );
		}

		// Force a 200 header and replace other status codes.
		header( $protocol . ' 200 OK', true, 200 );

		// Set the right content / mime type
		header( 'Content-Type: text/xml' );

		// Prevent the search engines from indexing the XML Sitemap.
		header( 'X-Robots-Tag: noindex, follow', true );

		// Make the browser cache this file properly.
		header( 'Pragma: public' );
		header( 'Cache-Control: maxage=31536000' );
		header( 'Expires: ' . gmdate( 'D, d M Y H:i:s', time() + 31536000 ) . ' GMT' );

		require plugin_dir_path( __FILE__ ) . 'xml-video-sitemap.php';
		die();
	}

	/**
	 * The main function of this class: it generates the XML sitemap's contents.
	 *
	 * @since 0.1
	 */
	function build_video_sitemap() {
		$options = get_option( 'wpseo_video' );

		$protocol = 'HTTP/1.1';
		if ( isset( $_SERVER['SERVER_PROTOCOL'] ) && $_SERVER['SERVER_PROTOCOL'] !== '' ) {
			$protocol = sanitize_text_field( $_SERVER['SERVER_PROTOCOL'] );
		}

		// Restrict access to the video sitemap to admins and valid bots
		if ( $options['cloak_sitemap'] === true && ( ! current_user_can( 'manage_options' ) && ! $this->is_valid_bot() ) ) {
			header( $protocol . ' 403 Forbidden', true, 403 );
			wp_die( "We're sorry, access to our video sitemap is restricted to site admins and valid Google & Bing bots." );
		}

		// Force a 200 header and replace other status codes.
		header( $protocol . ' 200 OK', true, 200 );

		$output = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">' . "\n";

		$printed_post_ids = array();

		$steps  = 5;
		$n      = (int) get_query_var( 'sitemap_n' );
		$offset = ( $n > 1 ) ? ( $n - 1 ) * $this->max_entries : 0;
		$total  = $offset + $this->max_entries;

		if ( is_array( $options['videositemap_posttypes'] ) && $options['videositemap_posttypes'] !== array() ) {
			// Set the initial args array to get videos in chunks
			$args = array(
				'post_type'      => $options['videositemap_posttypes'],
				'post_status'    => 'publish',
				'posts_per_page' => $steps,
				'offset'         => $offset,
				'meta_key'       => '_yoast_wpseo_video_meta',
				'meta_compare'   => '!=',
				'meta_value'     => 'none',
				'order'          => 'ASC',
				'orderby'        => 'post_modified',
			);

			// @TODO: add support to tax video to honor pages
			/*
				add a bool to the while loop to see if tax has been processed
				if $items is empty the posts are done so move on to tax

				do some math between $printed_post_ids and $this-max_entries to figure out how many from tax to add to this pagination
			*/

			// Add entries to the sitemap until the total is hit (rounded up by nearest $steps)
			while ( ( $total > $offset ) && ( $items = get_posts( $args ) ) ) {

				if ( is_array( $items ) && $items !== array() ) {
					foreach ( $items as $item ) {
						if ( ! is_object( $item ) || in_array( $item->ID, $printed_post_ids ) ) {
							continue;
						} else {
							$printed_post_ids[] = $item->ID;
						}

						if ( WPSEO_Meta::get_value( 'meta-robots-noindex', $item->ID ) == '1' ) {
							continue;
						}

						$disable = WPSEO_Meta::get_value( 'videositemap-disable', $item->ID );
						if ( $disable === 'on' ) {
							continue;
						}

						$video = WPSEO_Meta::get_value( 'video_meta', $item->ID );

						$video = $this->get_video_image( $item->ID, $video );

						// When we don't have a thumbnail and either a player_loc or a content_loc, skip this video.
						if ( ! isset( $video['thumbnail_loc'] )
							|| ( ! isset( $video['player_loc'] ) && ! isset( $video['content_loc'] ) )
						) {
							continue;
						}

						$video_duration = WPSEO_Meta::get_value( 'videositemap-duration', $item->ID );
						if ( $video_duration > 0 ) {
							$video['duration'] = $video_duration;
						}

						$video['permalink'] = get_permalink( $item );

						$rating = apply_filters( 'wpseo_video_rating', WPSEO_Meta::get_value( 'videositemap-rating', $item->ID ) );
						if ( $rating && WPSEO_Meta_Video::sanitize_rating( null, $rating, WPSEO_Meta_Video::$meta_fields['video']['videositemap-rating'] ) ) {
							$video['rating'] = number_format( $rating, 1 );
						}

						$not_family_friendly = apply_filters( 'wpseo_video_family_friendly', WPSEO_Meta::get_value( 'videositemap-not-family-friendly', $item->ID ), $item->ID );
						if ( is_string( $not_family_friendly ) && $not_family_friendly === 'on' ) {
							$video['family_friendly'] = 'no';
						} else {
							$video['family_friendly'] = 'yes';
						}

						$video['author'] = $item->post_author;

						$output .= $this->print_sitemap_line( $video );
					}
				}

				// Update these args for the next iteration
				$offset = $offset + $steps;
				$args['offset'] += $steps;
			}
		}

		$tax_meta = get_option( 'wpseo_taxonomy_meta' );
		$terms    = array();
		if ( is_array( $options['videositemap_taxonomies'] ) && $options['videositemap_taxonomies'] !== array() ) {
			// Below is a fix for a nasty bug in WooCommerce: https://github.com/woothemes/woocommerce/issues/3807
			$options['videositemap_taxonomies'][0] = '';
			$terms                                 = get_terms( $options['videositemap_taxonomies'] );
		}

		if ( is_array( $terms ) && $terms !== array() ) {
			foreach ( $terms as $term ) {
				if ( is_object( $term ) && isset( $tax_meta[ $term->taxonomy ]['_video'][ $term->term_id ] ) ) {
					$video = $tax_meta[ $term->taxonomy ]['_video'][ $term->term_id ];
					if ( is_array( $video ) ) {
						$video['permalink'] = get_term_link( $term, $term->taxonomy );
						$video['category']  = $term->name;
						$output .= $this->print_sitemap_line( $video );
					}
				}
			}
		}

		$output .= '</urlset>';
		$GLOBALS['wpseo_sitemaps']->set_sitemap( $output );
		$GLOBALS['wpseo_sitemaps']->set_stylesheet( "\n" . '<?xml-stylesheet type="text/xsl" href="' . home_url( 'video-sitemap.xsl' ) . '"?>' );
	}

	/**
	 * Print a full <url> line in the sitemap.
	 *
	 * @since 1.3
	 *
	 * @param $video array The video object to print out
	 *
	 * @return string The output generated
	 */
	function print_sitemap_line( $video ) {
		if ( ! is_array( $video ) || $video === array() ) {
			return '';
		}

		$output  = "\t<url>\n";
		$output .= "\t\t<loc>" . htmlspecialchars( $video['permalink'] ) . '</loc>' . "\n";
		$output .= "\t\t<video:video>\n";


		if ( empty( $video['publication_date'] ) ) {
			$post = get_post( $video['post_id'] );
			if ( is_object( $post ) && $post->post_date_gmt != '0000-00-00 00:00:00' ) {
				$video['publication_date'] = mysql2date( 'Y-m-d\TH:i:s+00:00', $post->post_date_gmt );
			} elseif ( is_object( $post ) && $post->post_date != '0000-00-00 00:00:00' ) {
				$video['publication_date'] = date( 'Y-m-d\TH:i:s+00:00', get_gmt_from_date( $post->post_date ) );
			} else {
				return '<!-- Post with ID ' . $video['post_id'] . 'skipped, because there\'s no valid date in the DB for it. -->';
			} // If we have no valid date for the post, skip the video and don't print it in the XML Video Sitemap.
		}


		foreach ( $video as $key => $val ) {
			if ( in_array( $key, array( 'id', 'url', 'type', 'permalink', 'post_id', 'hd' ) ) ) {
				continue;
			}

			if ( $key == 'author' ) {
				$output .= "\t\t\t<video:uploader info='" . get_author_posts_url( $val ) . "'>" . ent2ncr( esc_html( get_the_author_meta( 'display_name', $val ) ) ) . "</video:uploader>\n";
				continue;
			}

			$xtra = '';
			if ( $key == 'player_loc' ) {
				$xtra = ' allow_embed="yes"';
			}

			if ( $key == 'description' && empty( $val ) ) {
				$val = $video['title'];
			}

			if ( is_scalar( $val ) && ! empty ( $val ) ) {
				$val = $this->clean_string( $val );
				if ( ! empty ( $val ) ) {
					if ( in_array( $key, array( 'description', 'category', 'tag', 'title' ) ) ) {
						$val = ent2ncr( esc_html( $val ) );
					}
					$output .= "\t\t\t<video:" . $key . $xtra . '>' . $val . '</video:' . $key . ">\n";
				}
			} elseif ( is_array( $val ) && $val !== array() ) {
				$i = 1;
				foreach ( $val as $v ) {
					// Only 32 tags are allowed
					if ( $key == 'tag' && $i > 32 ) {
						break;
					}
					$v = $this->clean_string( $v );
					if ( in_array( $key, array( 'description', 'category', 'tag', 'title' ) ) ) {
						$v = ent2ncr( esc_html( $v ) );
					}
					if ( ! empty ( $v ) ) {
						$output .= "\t\t\t<video:" . $key . $xtra . '>' . $v . '</video:' . $key . ">\n";
					}
					$i++;
				}
			}
		}

		// Allow custom implementations with extra tags here
		$output .= apply_filters( 'wpseo_video_item', '', isset( $video['post_id'] ) ? $video['post_id'] : 0 );

		$output .= "\t\t</video:video>\n";

		$output .= "\t</url>\n";

		return $output;
	}

	/**
	 * Cleans a string for XML display purposes.
	 *
	 * @since 1.2.1
	 *
	 * @link  http://php.net/html-entity-decode#98697 Modified for WP from here.
	 *
	 * @param string $in     The string to clean.
	 * @param int    $offset Offset of the string to start the cleaning at.
	 *
	 * @return string Cleaned string.
	 */
	function clean_string( $in, $offset = null ) {
		$out = trim( $in );
		$out = $this->strip_shortcodes( $out );
		$out = html_entity_decode( $out, ENT_QUOTES, 'ISO-8859-15' );
		$out = html_entity_decode( $out, ENT_QUOTES, get_bloginfo( 'charset' ) );
		if ( ! empty( $out ) ) {
			$entity_start = strpos( $out, '&', $offset );
			if ( $entity_start === false ) {
				// ideal
				return _wp_specialchars( $out );
			} else {
				$entity_end = strpos( $out, ';', $entity_start );
				if ( $entity_end === false ) {
					return _wp_specialchars( $out );
				} // zu lang um eine entity zu sein
				elseif ( $entity_end > $entity_start + 7 ) {
					// und weiter gehts
					$out = $this->clean_string( $out, $entity_start + 1 );
				} // gotcha!
				else {
					$clean = substr( $out, 0, $entity_start );
					$subst = substr( $out, $entity_start + 1, 1 );
					// &scaron; => "s" / &#353; => "_"
					$clean .= ( $subst != '#' ) ? $subst : '_';
					$clean .= substr( $out, $entity_end + 1 );
					// und weiter gehts
					$out = $this->clean_string( $clean, $entity_start + 1 );
				}
			}
		}

		return _wp_specialchars( $out );
	}

	/**
	 * Roughly calculate the length of an FLV video.
	 *
	 * @since 1.3.1
	 *
	 * @param string $file The path to the video file to calculate the length for
	 *
	 * @return integer Duration of the video
	 */
	function get_flv_duration( $file ) {
		if ( is_file( $file ) && is_readable( $file ) ) {
			$flv = fopen( $file, 'rb' );
			if ( is_resource( $flv ) ) {
				fseek( $flv, - 4, SEEK_END );
				$arr             = unpack( 'N', fread( $flv, 4 ) );
				$last_tag_offset = $arr[1];
				fseek( $flv, - ( $last_tag_offset + 4 ), SEEK_END );
				fseek( $flv, 4, SEEK_CUR );
				$t0                    = fread( $flv, 3 );
				$t1                    = fread( $flv, 1 );
				$arr                   = unpack( 'N', $t1 . $t0 );
				$milliseconds_duration = $arr[1];

				return $milliseconds_duration;
			}
		}

		return 0;
	}

	/**
	 * Outputs the admin panel for the Video Sitemaps on the XML Sitemaps page with the WP SEO admin
	 *
	 * @since 0.1
	 */
	function admin_panel() {
		$options = get_option( 'wpseo_video' );
		$xmlopt  = get_option( 'wpseo_xml' );


		if ( ! isset( $GLOBALS['wpseo_admin_pages'] ) ) {
			$GLOBALS['wpseo_admin_pages'] = new WPSEO_Admin_Pages;
		}
		$GLOBALS['wpseo_admin_pages']->admin_header( true, $this->option_instance->group_name, $this->option_instance->option_name, false );


		if ( $xmlopt['enablexmlsitemap'] !== true ) {
			echo '<p>' . sprintf( esc_html__( 'Please enable the XML sitemap under the SEO -> %sXML Sitemaps settings%s', 'yoast-video-seo' ), '<a href="' . esc_url( add_query_arg( array( 'page' => 'wpseo_xml' ), admin_url( 'admin.php' ) ) ) . '">', '</a>' ). '</p>';
		} else {
			echo '<h2>' . esc_html__( 'General Settings', 'yoast-video-seo' ) . '</h2>';

			if ( is_array( $options['videositemap_posttypes'] ) && $options['videositemap_posttypes'] !== array() ) {
				// Use fields => ids to limit the overhead of fetching entire post objects,
				// fetch only an array of ids instead to count
				$args          = array(
					'post_type'      => $options['videositemap_posttypes'],
					'post_status'    => 'publish',
					'posts_per_page' => - 1,
					//'offset'         => 0,
					'meta_key'       => '_yoast_wpseo_video_meta',
					'meta_compare'   => '!=',
					'meta_value'     => 'none',
					'fields'         => 'ids',
				);
				$video_ids     = get_posts( $args );
				$count         = count( $video_ids );
				$n             = ( $count > $this->max_entries ) ? (int) ceil( $count / $this->max_entries ) : '';
				$video_lastest = str_replace( 'sitemap.xml', "sitemap$n.xml", $this->sitemap_url() );

				echo '<p>' . esc_html__( 'Please find your video sitemap here:', 'yoast-video-seo' ) . ' <a class="button" target="_blank" href="' . esc_url( $video_lastest ) . '">' . esc_html__( 'Video Sitemap', 'yoast-video-seo' ) . '</a></p>';
			} else {
				echo '<p>' . esc_html__( 'Select at least one post type to enable the video sitemap', 'yoast-video-seo' ) . '</p>';

			}


			echo $GLOBALS['wpseo_admin_pages']->checkbox( 'cloak_sitemap', esc_html__( 'Hide the sitemap from normal visitors?', 'yoast-video-seo' ) );
			echo $GLOBALS['wpseo_admin_pages']->checkbox( 'disable_rss', esc_html__( 'Disable Media RSS Enhancement', 'yoast-video-seo' ) );

			echo $GLOBALS['wpseo_admin_pages']->textinput( 'custom_fields', esc_html__( 'Custom fields:', 'yoast-video-seo' ) );
			echo '<p class="clear description">' . esc_html__( 'Custom fields the plugin should check for video content (comma separated)', 'yoast-video-seo' ) . '</p>';
			echo '<br class="clear"/>';


			echo '<h2>' . esc_html__( 'Embed Settings', 'yoast-video-seo' ) . '</h2>';

			echo $GLOBALS['wpseo_admin_pages']->checkbox( 'facebook_embed', esc_html__( 'Allow video\'s to be played directly on Facebook.', 'yoast-video-seo' ) );
			echo $GLOBALS['wpseo_admin_pages']->checkbox( 'fitvids', sprintf( esc_html__( 'Try to make videos responsive using %sFitVids.js%s?', 'yoast-video-seo' ), '<a href="http://fitvidsjs.com/">', '</a>' ) );

			echo $GLOBALS['wpseo_admin_pages']->textinput( 'content_width', esc_html__( 'Content width:', 'yoast-video-seo' ) );
			echo '<p class="clear description">' . esc_html__( 'This defaults to your themes content width, but if it\'s empty, setting a value here will make sure videos are embedded in the right width.', 'yoast-video-seo' ) . '</p>';

			echo $GLOBALS['wpseo_admin_pages']->textinput( 'vzaar_domain', esc_html__( 'Vzaar domain:', 'yoast-video-seo' ) );
			echo '<p class="clear description">' . esc_html__( 'If you use Vzaar, set this to the domain name you use for your Vzaar videos, no http: or slashes needed.', 'yoast-video-seo' ) . '</p>';
			echo '<br class="clear"/>';


			echo '<h2>' . esc_html__( 'Post Types to include in XML Video Sitemap', 'yoast-video-seo' ) . '</h2>';
			echo '<p>' . esc_html__( 'Determine which post types on your site might contain video.', 'yoast-video-seo' ) . '</p>';

			$post_types = get_post_types( array( 'public' => true ), 'objects' );
			if ( is_array( $post_types ) && $post_types !== array() ) {
				foreach ( $post_types as $posttype ) {
					$sel = '';
					if ( is_array( $options['videositemap_posttypes'] ) && in_array( $posttype->name, $options['videositemap_posttypes'] ) ) {
						$sel = 'checked="checked" ';
					}
					echo '<input class="checkbox double" id="' . esc_attr( 'include' . $posttype->name ) . '" type="checkbox" '
							. 'name="wpseo_video[videositemap_posttypes][' . esc_attr( $posttype->name ) . ']" ' . $sel . 'value="' . esc_attr( $posttype->name ) . '"/> '
							. '<label for="' . esc_attr( 'include' . $posttype->name ) . '">' . esc_html( $posttype->labels->name ) . '</label><br class="clear">';
				}
			}
			unset( $post_types );


			echo '<h2>' . esc_html__( 'Taxonomies to include in XML Video Sitemap', 'yoast-video-seo' ) . '</h2>';
			echo '<p>' . esc_html__( 'You can also include your taxonomy archives, for instance, if you have videos on a category page.', 'yoast-video-seo' ) . '</p>';

			$taxonomies = get_taxonomies( array( 'public' => true ), 'objects' );
			if ( is_array( $taxonomies ) && $taxonomies !== array() ) {
				foreach ( $taxonomies as $tax ) {
					$sel = '';
					if ( is_array( $options['videositemap_taxonomies'] ) && in_array( $tax->name, $options['videositemap_taxonomies'] ) ) {
						$sel = 'checked="checked" ';
					}
					echo '<input class="checkbox double" id="' . esc_attr( 'include' . $tax->name ) . '" type="checkbox" '
							. 'name="wpseo_video[videositemap_taxonomies][' . esc_attr( $tax->name ) . ']" ' . $sel . 'value="' . esc_attr( $tax->name ) . '"/> '
							. '<label for="' . esc_attr( 'include' . $tax->name ) . '">' . esc_html( $tax->labels->name ) . '</label><br class="clear">';
				}
			}
			unset( $taxonomies );

			echo '<br class="clear"/>';
		}
		?>

				<div class="submit">
					<input type="submit" class="button-primary" name="submit"
								 value="<?php esc_attr_e( 'Save Settings', 'yoast-video-seo' ); ?>" />
				</div>
			</form>

		<?php
		if ( isset( $_POST['reindex'] ) ) {
			$this->reindex();
		}
		?>

			<h2><?php esc_html_e( 'Indexation of Video\'s in your content', 'yoast-video-seo' ); ?></h2>

			<p style="max-width: 600px;"><?php esc_html_e( 'This process goes through all the post types specified by you, as well as the terms of each taxonomy, to check for videos in the content. If the plugin finds a video, it updates the meta data for that piece of content, so it can add that meta data and content to the XML Video Sitemap.', 'yoast-video-seo' ); ?></p>

			<p style="max-width: 600px;"><?php esc_html_e( 'By default the plugin only checks content that hasn\'t been checked yet. However, if you check \'Force Re-Index\', it will re-check all content. This is particularly interesting if you want to check for a video embed code that wasn\'t supported before, of if you want to update thumbnail images en masse.', 'yoast-video-seo' ); ?></p>

			<form method="post" action="">
				<input class="checkbox double" type="checkbox" name="force" id="force">
				<label for="force"><?php esc_html_e( "Force reindex of already indexed video's.", 'yoast-video-seo' ); ?></label><br />
				<br />
				<input type="submit" class="button" name="reindex"
							 value="<?php esc_html_e( 'Re-Index Videos', 'yoast-video-seo' ); ?>" />
			</form>
	<?php

		// Add debug info
		$GLOBALS['wpseo_admin_pages']->admin_footer( false, false );
	}

	/**
	 * Based on the video type being used, this content filtering function will automatically optimize the embed codes
	 * to allow for proper recognition by search engines.
	 *
	 * This function also, since version 1.2, adds the schema.org videoObject output.
	 *
	 * @link  http://schema.org/VideoObject
	 * @link  https://developers.google.com/webmasters/videosearch/schema
	 *
	 * @since 0.1
	 *
	 * @param string $content The content of the post.
	 *
	 * @return string $content The content of the post as modified by the function, if applicable.
	 */
	function content_filter( $content ) {
		global $post, $content_width;

		if ( is_feed() || is_home() || is_archive() || is_tax() || is_tag() || is_category() ) {
			return $content;
		}

		if ( ! is_object( $post ) ) {
			return $content;
		}

		$video = WPSEO_Meta::get_value( 'video_meta', $post->ID );

		if ( ! is_array( $video ) || $video === array() ) {
			return $content;
		}

		$disable = WPSEO_Meta::get_value( 'videositemap-disable', $post->ID );
		if ( $disable === 'on' ) {
			return $content;
		}

		if ( ! is_numeric( $content_width ) ) {
			$content_width = 400;
		}

		switch ( $video['type'] ) {
			case 'vimeo':
				$content = str_replace( '<iframe src="http://player.vimeo.com', '<noframes><embed src="http://vimeo.com/moogaloop.swf?clip_id=' . $video['id'] . '" type="application/x-shockwave-flash" width="400" height="300"></embed></noframes><iframe src="http://player.vimeo.com', $content );
				break;

			case 'dailymotion':
				// If dailymotion is embedded using the Viper shortcode, we have to add a noscript version too
				if ( strpos( $content, '<iframe src="http://www.dailymotion' ) === false ) {
					$content = str_replace( '[/dailymotion]', '[/dailymotion]<noscript><iframe src="http://www.dailymotion.com/embed/video/' . $video['id'] . '" width="' . $content_width . '" height="' . floor( $content_width / 1.33 ) . '" frameborder="0"></iframe></noscript>', $content );
				}
				break;
		}

		$desc = trim( WPSEO_Meta::get_value( 'metadesc', $post->ID ) );
		if ( ! is_string( $desc ) || $desc === '' ) {
			$desc = trim( wp_html_excerpt( $this->strip_shortcodes( $post->post_content ), 300 ) );
		}

		$stripped_title = $this->strip_tags( get_the_title() );
		if ( empty( $desc ) ) {
			$desc = $stripped_title;
		}

		$video = $this->get_video_image( $post->ID, $video );

		$content .= '<span itemprop="video" itemscope itemtype="http://schema.org/VideoObject">';
		$content .= '<meta itemprop="name" content="' . esc_attr( $stripped_title ) . '">';
		$content .= '<meta itemprop="thumbnailURL" content="' . esc_attr( $video['thumbnail_loc'] ) . '">';
		$content .= '<meta itemprop="description" content="' . esc_attr( $desc ) . '">';
		$content .= '<meta itemprop="uploadDate" content="' . date( 'c', strtotime( $post->post_date ) ) . '">';

		if ( isset( $video['player_loc'] ) ) {
			$content .= '<meta itemprop="embedURL" content="' . $video['player_loc'] . '">';
		}
		if ( isset( $video['content_loc'] ) ) {
			$content .= '<meta itemprop="contentURL" content="' . $video['content_loc'] . '">';
		}

		$video_duration = WPSEO_Meta::get_value( 'videositemap-duration', $post->ID );
		if ( $video_duration == 0 && isset( $video['duration'] ) ) {
			$video_duration = $video['duration'];
		}

		if ( $video_duration ) {
			$content .= '<meta itemprop="duration" content="' . $this->iso_8601_duration( $video_duration ) . '">';
		}
		$content .= '</span>';

		return $content;
	}

	/**
	 * A better strip tags that leaves spaces intact (and rips out more code)
	 *
	 * @since 1.3.4
	 *
	 * @link  http://php.net/strip-tags#110280
	 *
	 * @param string $string string to strip tags from
	 *
	 * @return string
	 */
	function strip_tags( $string ) {

		// ----- remove HTML TAGs -----
		$string = preg_replace( '/<[^>]*>/', ' ', $string );

		// ----- remove control characters -----
		$string = str_replace( "\r", '', $string ); // --- replace with empty space
		$string = str_replace( "\n", ' ', $string ); // --- replace with space
		$string = str_replace( "\t", ' ', $string ); // --- replace with space

		// ----- remove multiple spaces -----
		$string = trim( preg_replace( '/ {2,}/', ' ', $string ) );

		return $string;
	}

	/**
	 * Convert the duration in seconds to an ISO 8601 compatible output. Assumes the length is not over 24 hours.
	 *
	 * @link http://en.wikipedia.org/wiki/ISO_8601
	 *
	 * @param int $duration The duration in seconds.
	 *
	 * @return string $out ISO 8601 compatible output.
	 */
	function iso_8601_duration( $duration ) {
		if ( $duration <= 0 ) {
			return '';
		}

		$out = 'PT';
		if ( $duration > 3600 ) {
			$hours    = floor( $duration / 3600 );
			$out     .= $hours . 'H';
			$duration = $duration - ( $hours * 3600 );
		}
		if ( $duration > 60 ) {
			$minutes  = floor( $duration / 60 );
			$out     .= $minutes . 'M';
			$duration = $duration - ( $minutes * 60 );
		}
		if ( $duration > 0 ) {
			$out .= $duration . 'S';
		}

		return $out;
	}

	/**
	 * Filter the OpenGraph type for the post and sets it to 'video'
	 *
	 * @since 0.1
	 *
	 * @param string $type The type, normally "article"
	 *
	 * @return string $type Value 'video'
	 */
	function opengraph_type( $type ) {
		$options = get_option( 'wpseo_video' );

		if ( $options['facebook_embed'] !== true ) {
			return $type;
		}

		return $this->type_filter( $type, 'video.movie' );
	}

	/**
	 * Switch the Twitter card type to player if needed.
	 *
	 * @internal [JRF] This method does not seem to be hooked in anywhere
	 *
	 * @param string $type
	 *
	 * @return string
	 */
	function card_type( $type ) {
		return $this->type_filter( $type, 'player' );
	}

	/**
	 * Helper function for Twitter and OpenGraph card types
	 *
	 * @param string $type
	 * @param string $video_output
	 *
	 * @return string
	 */
	function type_filter( $type, $video_output ) {
		if ( is_singular() ) {
			global $post;

			if ( is_object( $post ) ) {
				$video = WPSEO_Meta::get_value( 'video_meta', $post->ID );

				if ( ! is_array( $video ) || $video === array() ) {
					return $type;
				} else {
					$disable = WPSEO_Meta::get_value( 'videositemap-disable', $post->ID );
					if ( $disable === 'on' ) {
						return $type;
					} else {
						return $video_output;
					}
				}
			}
		} else {
			if ( is_tax() || is_category() || is_tag() ) {
				$options = get_option( 'wpseo_video' );
				$term    = get_queried_object();

				if ( is_array( $options['videositemap_taxonomies'] ) && in_array( $term->taxonomy, $options['videositemap_taxonomies'] ) ) {
					$tax_meta = get_option( 'wpseo_taxonomy_meta' );
					if ( isset( $tax_meta[ $term->taxonomy ]['_video'][ $term->term_id ] ) ) {
						return $video_output;
					}
				}
			}
		}

		return $type;
	}

	/**
	 * Filter the OpenGraph image for the post and sets it to the video thumbnail
	 *
	 * @since 0.1
	 *
	 * @param string $image URL to the image
	 *
	 * @return string $image URL to the video thumbnail image
	 */
	function opengraph_image( $image ) {
		if ( is_string( $image ) && $image !== '' ) {
			return $image;
		}

		if ( is_singular() ) {
			global $post;

			if ( is_object( $post ) ) {
				$video = WPSEO_Meta::get_value( 'video_meta', $post->ID );

				if ( ! is_array( $video ) || $video === array() ) {
					return $image;
				}

				$disable = WPSEO_Meta::get_value( 'videositemap-disable', $post->ID );
				if ( $disable === 'on' ) {
					return $image;
				}

				return $video['thumbnail_loc'];
			}
		} else {
			if ( is_tax() || is_category() || is_tag() ) {
				$options = get_option( 'wpseo_video' );

				$term = get_queried_object();

				if ( is_array( $options['videositemap_taxonomies'] ) && in_array( $term->taxonomy, $options['videositemap_taxonomies'] ) ) {
					$tax_meta = get_option( 'wpseo_taxonomy_meta' );
					if ( isset( $tax_meta[ $term->taxonomy ]['_video'][ $term->term_id ] ) ) {
						$video = $tax_meta[ $term->taxonomy ]['_video'][ $term->term_id ];

						return $video['thumbnail_loc'];
					}
				}
			}
		}

		return $image;
	}

	/**
	 * Add OpenGraph video info if present
	 *
	 * @since 0.1
	 */
	function opengraph() {
		$options = get_option( 'wpseo_video' );

		if ( $options['facebook_embed'] !== true ) {
			return false;
		}

		if ( is_singular() ) {
			global $post;

			if ( is_object( $post ) ) {
				$video = WPSEO_Meta::get_value( 'video_meta', $post->ID );

				if ( is_array( $video ) && $video !== array() ) {
					$disable = WPSEO_Meta::get_value( 'videositemap-disable', $post->ID );
					if ( $disable !== 'on' ) {
						$video = $this->get_video_image( $post->ID, $video );
					}
				}
			}
		} else {
			if ( is_tax() || is_category() || is_tag() ) {

				$term = get_queried_object();

				if ( is_array( $options['videositemap_taxonomies'] ) && in_array( $term->taxonomy, $options['videositemap_taxonomies'] ) ) {
					$tax_meta = get_option( 'wpseo_taxonomy_meta' );
					if ( isset( $tax_meta[ $term->taxonomy ]['_video'][ $term->term_id ] ) ) {
						$video = $tax_meta[ $term->taxonomy ]['_video'][ $term->term_id ];
					}
				}
			}
		}

		if ( ! isset( $video ) || ! is_array( $video ) || ! isset( $video['player_loc'] ) ) {
			return false;
		}

		echo '<meta property="og:video" content="' . esc_attr( $video['player_loc'] ) . '" />' . "\n";
		echo '<meta property="og:video:type" content="application/x-shockwave-flash" />' . "\n";
		if ( isset( $video['width'] ) && isset( $video['height'] ) ) {
			echo '<meta property="og:video:width" content="' . esc_attr( $video['width'] ) . '" />' . "\n";
			echo '<meta property="og:video:height" content="' . esc_attr( $video['height'] ) . '" />' . "\n";
		}
		$GLOBALS['wpseo_og']->image_output( $video['thumbnail_loc'] );
	}

	/**
	 * Make the get_terms query only return terms with a non-empty description.
	 *
	 * @since 1.3
	 *
	 * @param $pieces array The separate pieces of the terms query to filter.
	 *
	 * @return mixed
	 */
	function filter_terms_clauses( $pieces ) {
		$pieces['where'] .= " AND tt.description != ''";

		return $pieces;
	}

	/**
	 * Reindex the video info from posts
	 *
	 * @since 0.1
	 */
	function reindex() {
		require_once( ABSPATH . 'wp-admin/includes/media.php' );

		echo '<strong>' . esc_html__( 'Re-index starts....', 'yoast-video-seo' ) . '</strong><br/>';

		$options = get_option( 'wpseo_video' );

		if ( is_array( $options['videositemap_posttypes'] ) && $options['videositemap_posttypes'] !== array() ) {
			$args = array(
				'post_type'   => $options['videositemap_posttypes'],
				'post_status' => 'publish',
				'numberposts' => 100,
				'offset'      => 0,
			);

			global $wp_version;
			if ( ! isset( $_POST['force'] ) ) {
				if ( version_compare( $wp_version, '3.5', '>=' ) ) {
					$args['meta_query'] = array(
						'key'     => '_yoast_wpseo_video_meta',
						'compare' => 'NOT EXISTS',
					);
				}
			}

			$post_count_total = 0;
			foreach ( $options['videositemap_posttypes'] as $post_type ) {
				$post_count_total += wp_count_posts( $post_type )->publish;
			}

			while ( $post_count_total > $args['offset'] ) {
				$results      = get_posts( $args );
				$result_count = count( $results );

				if ( is_array( $results ) && $result_count > 0 ) {
					echo '<br/><strong>' . sprintf( esc_html__( 'Found %d pieces of content to search through', 'yoast-video-seo' ), $result_count ) . '</strong><br/><br/>';

					foreach ( $results as $post ) {
						$this->update_video_post_meta( $post, true );
						flush();
					}
				}

				$args['offset'] += 99;
			}
		}

		// Get all the non-empty terms.
		add_filter( 'terms_clauses', array( $this, 'filter_terms_clauses' ) );
		$terms = array();
		if ( is_array( $options['videositemap_taxonomies'] ) && $options['videositemap_taxonomies'] !== array() ) {
			foreach ( $options['videositemap_taxonomies'] as $val ) {
				$new_terms = get_terms( $val );
				if ( is_array( $new_terms ) ) {
					$terms = array_merge( $terms, $new_terms );
				}
			}
		}
		remove_filter( 'terms_clauses', array( $this, 'filter_terms_clauses' ) );

		if ( count( $terms ) > 0 ) {
			echo '<br/><strong>' . sprintf( esc_html__( 'Found %d terms to search through', 'yoast-video-seo' ), count( $terms ) ) . '</strong><br/><br/>';

			foreach ( $terms as $term ) {
				$this->update_video_term_meta( $term, true );
				flush();
			}
		}

		// Ping the search engines with our updated XML sitemap, we ping with the index sitemap because
		// we don't know which video sitemap, or sitemaps, have been updated / added.
		wpseo_ping_search_engines();

		echo '<br/><strong>' . esc_html__( 'Re-index completed.', 'yoast-video-seo' ) . '</strong>';
	}


	/********************** DEPRECATED METHODS **********************/

	/**
	 * Register the wpseo_video setting
	 *
	 * @deprecated 1.6.0 - now auto-handled by class WPSEO_Option_Video
	 */
	function options_init() {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', null );
	}


	/**
	 * Register defaults for the video sitemap
	 *
	 * @since      0.2
	 * @deprecated 1.6.0 - now auto-handled by class WPSEO_Option_Video
	 */
	function set_defaults() {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', null );
	}

	/**
	 * Adds the header for the Video tab in the WordPress SEO meta box on edit post pages.
	 *
	 * @since      0.1
	 * @deprecated 1.6.0
	 * @deprecated use WPSEO_Video_Metabox::tab_header()
	 * @see        WPSEO_Video_Metabox::tab_header()
	 */
	function tab_header() {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', 'WPSEO_Video_Metabox::tab_header()' );
		$this->metabox_tab->tab_header();
	}

	/**
	 * Outputs the content for the Video tab in the WordPress SEO meta box on edit post pages.
	 *
	 * @since      0.1
	 * @deprecated 1.6.0
	 * @deprecated use WPSEO_Video_Metabox::tab_content()
	 * @see        WPSEO_Video_Metabox::tab_content()
	 */
	function tab_content() {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', 'WPSEO_Video_Metabox::tab_content()' );
		$this->metabox_tab->tab_content();
	}

	/**
	 * Output a tab in the WP SEO Metabox
	 *
	 * @since      0.2
	 * @deprecated 1.6.0
	 * @deprecated use WPSEO_Video_Metabox::do_tab()
	 * @see        WPSEO_Video_Metabox::do_tab()
	 *
	 * @param string $id      CSS ID of the tab.
	 * @param string $heading Heading for the tab.
	 * @param string $content Content of the tab.
	 */
	function do_tab( $id, $heading, $content ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', 'WPSEO_Video_Metabox::do_tab()' );
		$this->metabox_tab->do_tab( $id, $heading, $content );
	}

	/**
	 * Adds a line in the meta box
	 *
	 * @since      0.2
	 * @deprecated 1.6.0
	 * @deprecated use WPSEO_Video_Metabox::do_meta_box()
	 * @see        WPSEO_Video_Metabox::do_meta_box()
	 *
	 * @param array $meta_box Contains the vars based on which output is generated.
	 *
	 * @return string
	 */
	function do_meta_box( $meta_box ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', 'WPSEO_Video_Metabox::do_meta_box()' );
		return $this->metabox_tab->do_meta_box( $meta_box );
	}


	/**
	 * Defines the meta box inputs
	 *
	 * @since      0.1
	 * @deprecated 1.6.0
	 * @deprecated use WPSEO_Meta::get_meta_field_defs()
	 * @see        WPSEO_Meta::get_meta_field_defs()
	 *
	 * @param string $post_type
	 *
	 * @return array $mbs meta box inputs
	 */
	function get_meta_boxes( $post_type = 'post' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', 'WPSEO_Meta::get_meta_field_defs()' );
		return WPSEO_Meta::get_meta_field_defs( 'video', $post_type );
	}

	/**
	 * Save the values from the meta box inputs
	 *
	 * @since      0.1
	 * @deprecated 1.6.0
	 * @deprecated use WPSEO_Video_Metabox::save_meta_boxes()
	 * @see        WPSEO_Video_Metabox::save_meta_boxes()
	 *
	 * @param array $mbs meta boxes to merge the inputs with.
	 *
	 * @return array $mbs meta box inputs
	 */
	function save_meta_boxes( $mbs ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', 'WPSEO_Video_Metabox::save_meta_boxes()' );
		return $this->metabox_tab->save_meta_boxes( $mbs );
	}

	/**
	 * Replace the default snippet with a video snippet by hooking this function into the wpseo_snippet filter.
	 *
	 * @since      0.1
	 * @deprecated 1.6.0
	 * @deprecated use WPSEO_Video_Metabox::snippet_preview()
	 * @see        WPSEO_Video_Metabox::snippet_preview()
	 *
	 * @param string $content The original snippet content.
	 * @param object $post    The post object of the post for which the snippet was generated.
	 * @param array  $vars    An array of variables for use within the snippet, containing title, description, date and slug
	 *
	 * @return string $content The new video snippet if video metadata was found for the post.
	 */
	function snippet_preview( $content, $post, $vars ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', 'WPSEO_Video_Metabox::snippet_preview()' );
		return $this->metabox_tab->snippet_preview( $content, $post, $vars );
	}


	/**
	 * Restricts the length of the meta description in the snippet preview and throws appropriate warnings.
	 *
	 * @since      0.1
	 * @deprecated 1.6.0
	 * @deprecated use WPSEO_Video_Metabox::meta_length()
	 * @see        WPSEO_Video_Metabox::meta_length()
	 *
	 * @param int $length The snippet length as defined by default.
	 *
	 * @return int $length The max snippet length for a video snippet.
	 */
	function meta_length( $length ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', 'WPSEO_Video_Metabox::meta_length()' );
		return $this->metabox_tab->meta_length( $length );
	}

	/**
	 * Explains the length restriction of the meta description
	 *
	 * @since      0.1
	 * @deprecated 1.6.0
	 * @deprecated use WPSEO_Video_Metabox::meta_length_reason()
	 * @see        WPSEO_Video_Metabox::meta_length_reason()
	 *
	 * @param string $reason Input string.
	 *
	 * @return string $reason  The reason why the meta description is limited.
	 */
	function meta_length_reason( $reason ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', 'WPSEO_Video_Metabox::meta_length_reason()' );
		return $this->metabox_tab->meta_length_reason( $reason );
	}

	/**
	 * Filter the Page Analysis results to make sure we're giving the correct hints.
	 *
	 * @since      1.4
	 * @deprecated 1.6.0
	 * @deprecated use WPSEO_Video_Metabox::filter_linkdex_results()
	 * @see        WPSEO_Video_Metabox::filter_linkdex_results()
	 *
	 * @param array  $results The results array to filter and update.
	 * @param array  $job     The current jobs variables.
	 * @param object $post    The post object for the current page.
	 *
	 * @return array $results
	 */
	function filter_linkdex_results( $results, $job, $post ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.6.0', 'WPSEO_Video_Metabox::filter_linkdex_results()' );
		return $this->metabox_tab->filter_linkdex_results( $results, $job, $post );
	}


	/**
	 * Wrapper for the WordPress internal wp_remote_get function, making sure a proper user-agent is sent along.
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Details::remote_get()
	 * @see        WPSEO_Video_Details::remote_get()
	 */
	function remote_get() {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', 'WPSEO_Video_Details::remote_get()' );
	}

	/**
	 * Use the "new" post data with the old video data, to prevent the need for an external video API call when the video hasn't changed.
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Details::use_old_video_data()
	 * @see        WPSEO_Video_Details::use_old_video_data()
	 *
	 */
	function use_old_video_data() {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', 'WPSEO_Video_Details::use_old_video_data()' );
	}

	/**
	 * Retrieve video details from Animoto
	 *
	 * @since 1.4.3
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Animoto
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function animoto_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from Blip.tv
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Blip
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function blip_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from Brightcove
	 *
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Brightcove
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch" from Brightcove, if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function brightcove_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details for cincopa
	 *
	 * @since ?
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Cincopa
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function cincopa_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from Dailymotion
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Dailymotion
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function dailymotion_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details for Easy Video Suite (EVS)
	 *
	 * @since ?
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Evs
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function evs_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from Flickr
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Flickr
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function flickr_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from Metacafe
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Metacafe
	 *
	 * @link  http://help.metacafe.com/?page_id=238 Metacafe API docs - no longer available.
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function metacafe_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details for Muzu.tv
	 *
	 * @since ?
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Muzutv
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function muzutv_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details for Screencast.com
	 *
	 * @since 1.5.4.4
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Screencast
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 *
	 * @TODO: no api or connection from getting video details from the url so we extract the details from the embed code itself
	 */
	function screencast_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details for Screenr
	 *
	 * @since ?
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Screenr
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function screenr_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details for Veoh Video's
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Veoh
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function veoh_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from Viddler
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Viddler
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch" from Viddler, if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function viddler_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from VideoPress
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Videopress
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function videopress_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from Vidyard
	 *
	 * @since 1.3.4.4
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Vidyard
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function vidyard_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from Vimeo
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Vimeo
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function vimeo_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from Vippy
	 *
	 * @since 1.3.4
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Vippy
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function vippy_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from Vzaar
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Vzaar
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function vzaar_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from Wistia
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_Wistia
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function wistia_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from WordPress.tv (well grab the ID and then use the VideoPress API)
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_WordPresstv
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function wordpresstv_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

	/**
	 * Retrieve video details from YouTube
	 *
	 * @since      0.1
	 * @deprecated 1.7.0
	 * @deprecated use WPSEO_Video_Sitemap::get_video_details()
	 * @see        WPSEO_Video_Details_YouTube
	 *
	 * @param array  $vid    The video array with all the data.
	 * @param array  $oldvid The video array with all the data of the previous "fetch", if available.
	 * @param string $thumb  The URL to the manually set thumbnail, if available.
	 *
	 * @return array|boolean $vid Returns a filled video array when successful, false when unsuccessful.
	 */
	function youtube_details( $vid, $oldvid = array(), $thumb = '' ) {
		_deprecated_function( __METHOD__, 'Video SEO 1.7.0', __CLASS__ . '::get_video_details()' );
		$type = str_replace( '_details', '', __FUNCTION__ );
		return $this->get_video_details( $type, $vid, $oldvid, $thumb );
	}

} /* End of class WPSEO_Video_Sitemap */


/**
 * Throw an error if WordPress SEO is not installed.
 *
 * @since 0.2
 */
function yoast_wpseo_missing_error() {
	$url     = add_query_arg(
		array(
			'tab'                 => 'search',
			'type'                => 'term',
			's'                   => 'wordpress+seo',
			'plugin-search-input' => 'Search+Plugins',
		),
		admin_url( 'plugin-install.php' )
	);
	$message = sprintf( esc_html__( 'Please %sinstall & activate WordPress SEO by Yoast%s and then enable its XML sitemap functionality to allow the Video SEO module to work.' ), '<a href="' . esc_url( $url ) . '">', '</a>' );
	yoast_wpseo_video_seo_self_deactivate( $message );
}

/**
 * Throw an error if WordPress is out of date.
 *
 * @since 1.5.4
 */
function yoast_wordpress_upgrade_error() {
	$message = esc_html__( 'Please upgrade WordPress to the latest version to allow WordPress and the Video SEO module to work properly.', 'yoast-video-seo' );
	yoast_wpseo_video_seo_self_deactivate( $message );
}

/**
 * Throw an error if WordPress SEO is out of date.
 *
 * @since 1.5.4
 */
function yoast_wpseo_upgrade_error() {
	$message = esc_html__( 'Please upgrade the WordPress SEO plugin to the latest version to allow the Video SEO module to work.', 'yoast-video-seo' );
	yoast_wpseo_video_seo_self_deactivate( $message );
}

/**
 * Throw an error if the PHP SPL extension is disabled (prevent white screens)
 *
 * @since 1.7
 */
function yoast_phpspl_missing_error() {
	$message = esc_html__( 'The PHP SPL extension seem to be unavailable. Please ask your web host to enable it.', 'yoast-video-seo' );
	yoast_wpseo_video_seo_self_deactivate( $message );
}


/**
 * Initialize the Video SEO module on plugins loaded, so WP SEO should have set its constants and loaded its main classes.
 *
 * @since 0.2
 */
function yoast_wpseo_video_seo_init() {
	global $wp_version;

	if ( ! function_exists( 'spl_autoload_register' ) ) {
		add_action( 'admin_init', 'yoast_phpspl_missing_error' );
	} elseif ( ! version_compare( $wp_version, '3.4', '>=' ) ) {
		add_action( 'admin_init', 'yoast_wordpress_upgrade_error' );
	} else {
		if ( defined( 'WPSEO_VERSION' ) ) {
			if ( version_compare( WPSEO_VERSION, '1.4.99', '>=' ) ) { // Allow beta version
				add_action( 'plugins_loaded', 'yoast_wpseo_video_seo_meta_init', 10 );
				add_action( 'plugins_loaded', 'yoast_wpseo_video_seo_sitemap_init', 20 );
			} else {
				add_action( 'admin_init', 'yoast_wpseo_upgrade_error' );
			}
		} else {
			add_action( 'admin_init', 'yoast_wpseo_missing_error' );
		}
	}
	add_action( 'init', array( 'WPSEO_Video_Sitemap', 'load_textdomain' ), 1 );
}

/**
 * Initialize the video meta data class
 */
function yoast_wpseo_video_seo_meta_init() {
	WPSEO_Meta_Video::init();
}

/**
 * Initialize the main plugin class
 */
function yoast_wpseo_video_seo_sitemap_init() {
	$GLOBALS['wpseo_video_xml'] = new WPSEO_Video_Sitemap();
}

if ( ! defined( 'WP_INSTALLING' ) || WP_INSTALLING === false ) {
	add_action( 'plugins_loaded', 'yoast_wpseo_video_seo_init', 5 );
}

/**
 * Self-deactivate plugin
 *
 * @since 1.7
 *
 * @param	string	Error message
 * @return	void
 */
function yoast_wpseo_video_seo_self_deactivate( $message ) {
	if ( is_admin() ) {
		add_action( 'admin_notices', create_function( $message, 'echo \'<div class="error"><p>\' . __( \'Activation failed:\', \'yoast-video-seo\' ) . \' \' . $message . \'</p></div>\';' ) );

		deactivate_plugins( plugin_basename( __FILE__ ) );
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
	}
}


/**
 * Execute option cleanup actions on activate
 */
function yoast_wpseo_video_activate() {
	$option_instance = WPSEO_Option_Video::get_instance();
	$option_instance->clean();
}
register_activation_hook( __FILE__, 'yoast_wpseo_video_activate' );
