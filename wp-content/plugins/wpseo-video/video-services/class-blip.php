<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Blip.tv Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Blip' ) ) {

	/**
	 * Class WPSEO_Video_Details_Blip
	 *
	 * @todo from before; concerns determine_video_id_from_url():
	 * This isn't active yet as the ID here doesn't translate directly into a Blip ID...
	 * if ( preg_match( '`http://blip\.tv/play/([^\.]+)\.html`', $vid['url'], $matches ) )
	 * $vid['id'] = $matches[1];
	 */
	class WPSEO_Video_Details_Blip extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		protected $id_regex = '`-(\d+)$`';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://blip.tv/rss/view/%s',
			'replace_key'   => 'id',
			'response_type' => '',
		);


		/**
		 * Set the content location
		 */
		protected function set_content_loc() {
			if ( preg_match( '`<enclosure length="[\d]+" type="[^"]+" url="([^"]+)"/>`', $this->decoded_response, $match ) ) {
				$this->vid['content_loc'] = $match[1];
			}
		}

		/**
		 * Set the video duration
		 */
		protected function set_duration() {
			if ( preg_match( '`<blip:runtime>(\d+)</blip:runtime>`', $this->decoded_response, $match ) ) {
				$this->vid['duration'] = $match[1];
			}
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( preg_match( '`<media:player url="([^"]+)">`', $this->decoded_response, $match ) ) {
				$this->vid['player_loc'] = $match[1];
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			if ( preg_match( '`<media:thumbnail url="([^"]+)"/>`', $this->decoded_response, $match ) ) {
				$image = $this->make_image_local( $match[1] );
				if ( is_string( $image ) && $image !== '' ) {
					$this->vid['thumbnail_loc'] = $image;
				}
			}
		}

		/**
		 * Set the video type
		 */
		protected function set_type() {
			$this->vid['type'] = 'blip.tv';
		}

	} /* End of class */

} /* End of class-exists wrapper */