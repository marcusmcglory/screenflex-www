<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Muzu.tv Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Muzutv' ) ) {

	/**
	 * Class WPSEO_Video_Details_Muzutv
	 */
	class WPSEO_Video_Details_Muzutv extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		//protected $id_regex = '';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://www.muzu.tv/api/video/details/%s?muzuid=b00q0xGOTl',
			'replace_key'   => 'id',
			'response_type' => 'simplexml',
		);


		/**
		 * Retrieve the video id from a known video url based on parsing the url and a regex match.
		 *
		 * @param  int  $match_nr [Not used in this implementation]
		 *
		 * @return void
		 */
		protected function determine_video_id_from_url( $match_nr = 1 ) {
			if ( isset( $this->vid['url'] ) && is_string( $this->vid['url'] ) && $this->vid['url'] !== '' ) {
				/* @todo: preg_match does not grab the id properly
					example http://www.muzu.tv/sean-paul-beenie-man/greatest-gallis-music-video/1847016/
					if ( preg_match( '|muzu\.tv/.*([^/]+)$|', $this->vid['url'], $matches ) )
					$this->vid['id'] = $matches[1];
				*/
				// [JRF] suggestion for improved regex based on the presumption that the id is numeric and last:
				// if ( preg_match( '`muzu\.tv/(?:[^/]*/)*([\d]+)[/]?$`', $this->vid['url'], $matches ) )
				$parse = parse_url( $this->vid['url'] );
				if ( isset( $parse['host'] ) && preg_match( '`muzu\.tv$`', $parse['host'] ) ) {
					$pieces          = array_filter( explode( '/', $parse['path'] ), 'strlen' );
					$this->vid['id'] = end( $pieces );
				}
			}
		}


		/**
		 * Set the video duration
		 */
		protected function set_duration() {
			if ( ! empty( $this->decoded_response->content->attributes()->duration ) ) {
				$this->vid['duration'] = (int) $this->decoded_response->content->attributes()->duration;
			}
		}

		/**
		 * Set the video height
		 */
		protected function set_height() {
			if ( ! empty( $this->decoded_response->content->attributes()->height ) ) {
				$this->vid['height'] = $this->decoded_response->content->attributes()->height;
			}
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ! empty( $this->vid['id'] ) ) {
				$this->vid['player_loc'] = 'https://player.muzu.tv/player/getPlayer/i/293053/vidId=' . urlencode( $this->vid['id'] ) . '&autostart=y&dswf=y';
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			if ( ! empty( $this->xml->channel->image->url ) ) {
				$image = $this->make_image_local( (string) $this->xml->channel->image->url );
				if ( is_string( $image ) && $image !== '' ) {
					$this->vid['thumbnail_loc'] = $image;
				}
			}
		}

		/**
		 * Set the video width
		 */
		protected function set_width() {
			if ( ! empty( $this->decoded_response->content->attributes()->width ) ) {
				// @todo Why cast to string ? Int would be more logical
				$this->vid['width'] = (string) $this->decoded_response->content->attributes()->width;
			}
		}

	} /* End of class */

} /* End of class-exists wrapper */