<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Wistia Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Wistia' ) ) {

	/**
	 * Class WPSEO_Video_Details_Wistia
	 */
	class WPSEO_Video_Details_Wistia extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		//protected $id_regex = '';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://fast.wistia.com/oembed?url=%s',
			//'replace_key'   => 'url',
			'response_type' => 'json',
		);

		/**
		 * @var bool   Whether or not to use the fallback method to retrieve certain information on the video
		 */
		protected $use_fallback = false;

		/**
		 * @var string Frame source - used by fallback method
		 */
		protected $frame_source;


		/**
		 * Instantiate the class
		 *
		 * Adjust the video url before passing off to the parent constructor
		 *
		 * @param array  $vid     The video array with all the data.
		 * @param array  $old_vid The video array with all the data of the previous "fetch", if available.
		 * @param string $thumb   The URL to the manually set thumbnail, if available.
		 *
		 * @return \WPSEO_Video_Details_Wistia
		 */
		public function __construct( $vid, $old_vid = array(), $thumb = '' ) {
			if ( isset( $vid['id'] ) ) {
				$vid['url'] = 'http://home.wistia.com/medias/' . rawurlencode( $vid['id'] );
			}
			parent::__construct( $vid, $old_vid, $thumb );
		}


		/**
		 * Use the "new" post data with the old video data, to prevent the need for an external video
		 * API call when the video hasn't changed.
		 *
		 * Match whether old data can be used on url rather than video id
		 *
		 * @param array  $vid       The "new" video array
		 * @param array  $old_vid   The old video array
		 * @param string $thumb     Possibly the thumbnail, if set manually.
		 * @param string $match_on  Array key to use in the $vid array to determine whether or not to use the old data
		 *                          Defaults to 'url' for this implementation.
		 *
		 * @return bool  Whether or not valid old data was found (and used)
		 */
		protected function maybe_use_old_video_data( $vid, $old_vid = array(), $thumb = '', $match_on = 'url' ) {
			return parent::maybe_use_old_video_data( $vid, $old_vid, $thumb, $match_on );
		}


		/**
		 * Retrieve information on a video via a remote API call
		 *
		 * @return void
		 */
		protected function get_remote_video_info() {
			if ( ( is_string( $this->remote_url['pattern'] ) && $this->remote_url['pattern'] !== '' ) &&
				( is_string( $this->vid['url'] ) && $this->vid['url'] !== '' ) ) {

				// Force the embed to be an SEO embed.
				$url = str_replace( array( 'embedType=api', 'embedType=iframe' ), 'embedType=seo', $this->vid['url'] );
				$url = sprintf( $this->remote_url['pattern'], $url );
				$url = $this->url_encode( $url );

				$response = $this->remote_get( $url );
				if ( $response !== false ) {
					$this->remote_response = $response;
				}
			}
		}


		/**
		 * Determine whether or not the use of the fallback method is needed and set video details
		 * to their new values by passing off to the parent method
		 *
		 * @param string $thumb  Possibly the thumbnail, if set manually.
		 *
		 * @return void
		 */
		protected function put_video_details( $thumb = '' ) {
			if ( ! empty( $this->decoded_response->html ) ) {
				$this->decoded_response->html = stripslashes( $this->decoded_response->html );

				if ( strpos( $this->decoded_response->html, '<div itemprop="video"' ) === false && preg_match( '`<iframe src=([\'"])([^\'"\s]+)\1`', $this->decoded_response->html, $match ) ) {

					$this->use_fallback = true;

					$response = $this->remote_get( $match[2] );
					if ( is_string( $response ) && $response !== '' && $response !== 'null' ) {
						$this->frame_source = $response;
					}
				}
			}

			parent::put_video_details( $thumb );
		}


		/**
		 * Set the content location
		 */
		protected function set_content_loc() {
			if ( $this->use_fallback === false ) {
				if ( ! empty( $this->decoded_response->html ) && preg_match( '`<meta itemprop="contentURL" content="([^"]+)" />`', $this->decoded_response->html, $match ) ) {
					$this->vid['content_loc'] = $match[1];
				}
			}
			elseif ( ! empty( $this->frame_source ) && preg_match( '`<a href=([\'"])([\'"\s]+)\1 id=([\'"])wistia_fallback\3`', $this->frame_source, $match ) ) {
				$this->vid['content_loc'] = $match[2];
			}
		}

		/**
		 * Set the video duration
		 */
		protected function set_duration() {
			$this->set_duration_from_json_object();
		}

		/**
		 * Set the video height
		 */
		protected function set_height() {
			$this->set_height_from_json_object();
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( $this->use_fallback === false ) {
				if ( ! empty( $this->decoded_response->html ) && preg_match( '`<meta itemprop="embedURL" content="([^"]+)" />`', $this->decoded_response->html, $match ) ) {
					$this->vid['player_loc'] = $match[1];
				}
			}
			elseif ( ! empty( $this->frame_source ) ) {
				if ( preg_match( '`"type":"flv","url":"([^"]*)"`', $this->frame_source, $match ) ) {
					$flv = $match[1];
				}

				if ( preg_match( '`"type":"still","url":"([^"]*)"`', $this->frame_source, $match ) ) {
					$still = $match[1];
				}

				if ( preg_match( '`"accountKey":"([^"]*)"`', $this->frame_source, $match ) ) {
					$account_key = $match[1];
				}

				if ( preg_match( '`"mediaKey":"([^"]*)"`', $this->frame_source, $match ) ) {
					$media_key = $match[1];
				}

				if ( empty( $this->vid['duration'] ) ) {
					$this->set_duration();
				}

				if ( isset( $flv, $still, $account_key, $media_key ) ) {
					$url = 'https://wistia.sslcs.cdngc.net/flash/embed_player_v2.0.swf?videoUrl=' .
						$flv . '&stillUrl=' . $still . '&controlsVisibleOnLoad=false' .
						'&unbufferedSeek=true&autoLoad=false&autoPlay=true&endVideoBehavior=default' .
						'&embedServiceURL=http://distillery.wistia.com/x&accountKey=' . $account_key .
						'&mediaID=' . $media_key . '&mediaDuration=' . $this->vid['duration'] .
						'&fullscreenDisabled=false';

					$this->vid['player_loc'] = $this->url_encode( $url );
				}
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			$this->set_thumbnail_loc_from_json_object();
		}

		/**
		 * Set the video width
		 */
		protected function set_width() {
			$this->set_width_from_json_object();
		}

	} /* End of class */

} /* End of class-exists wrapper */
