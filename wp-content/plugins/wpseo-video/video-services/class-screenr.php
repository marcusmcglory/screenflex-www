<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Screenr Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Screenr' ) ) {

	/**
	 * Class WPSEO_Video_Details_Screenr
	 */
	class WPSEO_Video_Details_Screenr extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		protected $id_regex = '`screenr\.com/([^/]+)[/]?$`';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://www.screenr.com/api/oembed.json?url=http://screenr.com/%s',
			'replace_key'   => 'id',
			'response_type' => 'json',
		);


		/**
		 * Set the video height
		 */
		protected function set_height() {
			$this->set_height_from_json_object();
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ! empty( $this->vid['id'] ) ) {
				$this->vid['player_loc'] = 'http://www.screenr.com/embed/' . rawurlencode( $this->vid['id'] );
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			$this->set_thumbnail_loc_from_json_object();
		}

		/**
		 * Set the video width
		 */
		protected function set_width() {
			$this->set_width_from_json_object();
		}

	} /* End of class */

} /* End of class-exists wrapper */