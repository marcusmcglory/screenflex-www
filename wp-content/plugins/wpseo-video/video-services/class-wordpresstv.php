<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * WordPress.tv Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Wordpresstv' ) ) {

	/**
	 * Class WPSEO_Video_Details_Wordpresstv
	 *
	 * Retrieve video details from WordPress.tv (well grab the ID and then use the VideoPress API)
	 */
	class WPSEO_Video_Details_Wordpresstv extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		//protected $id_regex = '';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://wordpress.tv/oembed/?url=%s',
			'replace_key'   => 'url',
			'response_type' => 'json',
		);


		/**
		 * @var array Storage for originally passed $vid
		 */
		protected $original_vid;

		/**
		 * @var array Storage for originally passed $old_vid
		 */
		protected $original_old_vid;


		/**
		 * Instantiate the class
		 *
		 * @param array  $vid     The video array with all the data.
		 * @param array  $old_vid The video array with all the data of the previous "fetch", if available.
		 * @param string $thumb   The URL to the manually set thumbnail, if available.
		 *
		 * @return \WPSEO_Video_Details_Wordpresstv
		 */
		public function __construct( $vid, $old_vid = array(), $thumb = '' ) {
			$this->original_vid     = $vid;
			$this->original_old_vid = $old_vid;
			parent::__construct( $vid, $old_vid, $thumb );
		}


		/**
		 * Use the "new" post data with the old video data, to prevent the need for an external video
		 * API call when the video hasn't changed.
		 *
		 * Match whether old data can be used on url rather than video id
		 *
		 * @param array  $vid       The "new" video array
		 * @param array  $old_vid   The old video array
		 * @param string $thumb     Possibly the thumbnail, if set manually.
		 * @param string $match_on  Array key to use in the $vid array to determine whether or not to use the old data
		 *                          Defaults to 'url' for this implementation.
		 *
		 * @return bool  Whether or not valid old data was found (and used)
		 */
		protected function maybe_use_old_video_data( $vid, $old_vid = array(), $thumb = '', $match_on = 'url' ) {
			return parent::maybe_use_old_video_data( $vid, $old_vid, $thumb, $match_on );
		}


		/**
		 * Set video details using the Videopress logic
		 *
		 * @param string $thumb  Possibly the thumbnail, if set manually.
		 *
		 * @return void
		 */
		protected function put_video_details( $thumb = '' ) {
			if ( preg_match( '`v\.wordpress\.com/([^"]+)`', $this->decoded_response->html, $match ) ) {
				$this->original_vid['id'] = $this->vid['id'] = $match[1];

				$video     = new WPSEO_Video_Details_Videopress( $this->original_vid, $this->original_old_vid, $thumb );
				$this->vid = $video->get_details();
			}
		}


		/**
		 * @internal As the VideoPress class is used to set the relevant details,
		 *           this method will never be used.
		 *           However as this class extends an abstract class, it has to be implemented.
		 */
		protected function set_player_loc() {
			return;
		}


		/**
		 * @internal As the VideoPress class is used to set the relevant details,
		 *           this method will never be used.
		 *           However as this class extends an abstract class, it has to be implemented.
		 */
		protected function set_thumbnail_loc() {
			return;
		}
	} /* End of class */

} /* End of class-exists wrapper */