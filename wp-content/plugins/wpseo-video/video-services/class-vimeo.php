<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Vimeo Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Vimeo' ) ) {

	/**
	 * Class WPSEO_Video_Details_Vimeo
	 */
	class WPSEO_Video_Details_Vimeo extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		//protected $id_regex = '';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://vimeo.com/api/oembed.json?url=%s',
			'replace_key'   => 'url',
			'response_type' => 'json',
		);


		/**
		 * Retrieve the video id from a known video url based on a regex match.
		 * Also change the url based on the new video id.
		 *
		 * @param int $match_nr The captured parenthesized sub-pattern to use from matches. Defaults to 1.
		 *
		 * @return void
		 */
		protected function determine_video_id_from_url( $match_nr = 1 ) {
			if ( isset( $this->vid['url'] ) && is_string( $this->vid['url'] ) && $this->vid['url'] !== '' ) {
				if ( preg_match( '`http[s]?://(?:player\.|www\.)?vimeo\.com/(?:video/)?(\d+)`', $this->vid['url'], $match ) ) {
					$this->vid['id'] = $match[ $match_nr ];
				}

				if ( preg_match( '`http[s]?://(?:www\.)?vimeo\.com/moogaloop\.swf\?clip_id=([^&]+)`', $this->vid['url'], $match ) ) {
					$this->vid['id'] = $match[ $match_nr ];
				}
			}

			if ( ! empty( $this->vid['id'] ) ) {
				$this->vid['url'] = 'http://vimeo.com/' . rawurlencode( $this->vid['id'] );
			}
		}

		/**
		 * Use the "new" post data with the old video data, to prevent the need for an external video
		 * API call when the video hasn't changed.
		 *
		 * Match whether old data can be used on url rather than video id
		 *
		 * @param array  $vid       The "new" video array
		 * @param array  $old_vid   The old video array
		 * @param string $thumb     Possibly the thumbnail, if set manually.
		 * @param string $match_on  Array key to use in the $vid array to determine whether or not to use the old data
		 *                          Defaults to 'url' for this implementation.
		 *
		 * @return bool  Whether or not valid old data was found (and used)
		 */
		protected function maybe_use_old_video_data( $vid, $old_vid = array(), $thumb = '', $match_on = 'url' ) {
			return parent::maybe_use_old_video_data( $vid, $old_vid, $thumb, $match_on );
		}


		/**
		 * Set the video duration
		 */
		protected function set_duration() {
			$this->set_duration_from_json_object();
		}

		/**
		 * Set the video height
		 */
		protected function set_height() {
			$this->set_height_from_json_object();
		}

		/**
		 * (Re-)Set the video id
		 */
		protected function set_id() {
			if ( ! empty( $this->decoded_response->video_id ) ) {
				$this->vid['id'] = $this->decoded_response->video_id;
			}
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ! empty( $this->vid['id'] ) ) {
				$this->vid['player_loc'] = $this->url_encode( 'https://www.vimeo.com/moogaloop.swf?clip_id=' . $this->vid['id'] );
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			$this->set_thumbnail_loc_from_json_object();
		}

		/**
		 * Set the video width
		 */
		protected function set_width() {
			$this->set_width_from_json_object();
		}

	} /* End of class */

} /* End of class-exists wrapper */