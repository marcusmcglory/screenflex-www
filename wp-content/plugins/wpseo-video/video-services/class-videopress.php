<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * VideoPress Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Videopress' ) ) {

	/**
	 * Class WPSEO_Video_Details_Videopress
	 */
	class WPSEO_Video_Details_Videopress extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		//protected $id_regex = '';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			//'pattern'       => '',
			//'replace_key'   => '',
			'response_type' => 'json',
		);


		/**
		 * Retrieve information on a video via a remote API call
		 *
		 * @return void
		 */
		protected function get_remote_video_info() {
			if ( ! empty( $this->vid['id'] ) ) {
				$domain         = parse_url( home_url(), PHP_URL_HOST );
				$request_params = array( 'guid' => $this->vid['id'], 'domain' => $domain );

				$url = 'https://v.wordpress.com/data/wordpress.json?' . http_build_query( $request_params, null, '&' );

				$response = $this->remote_get( $url );
				if ( $response !== false ) {
					$this->remote_response = $response;
				}
			}
		}


		/**
		 * Set the content location
		 */
		protected function set_content_loc() {
			if ( ! empty( $this->decoded_response->mp4->url ) ) {
				$this->vid['content_loc'] = $this->decoded_response->mp4->url;
			}
		}

		/**
		 * Set the video duration
		 */
		protected function set_duration() {
			$this->set_duration_from_json_object();
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ! empty( $this->vid['id'] ) ) {
				// @todo: check - original had & encoded as &amp; - is this necessary ?
				$this->vid['player_loc'] = $this->url_encode( 'https://v0.wordpress.com/player.swf?v=1.03&guid=' . $this->vid['id'] . '&isDynamicSeeking=true' );
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			if ( ! empty( $this->decoded_response->posterframe ) ) {
				$image = $this->make_image_local( $this->decoded_response->posterframe );
				if ( is_string( $image ) && $image !== '' ) {
					$this->vid['thumbnail_loc'] = $image;
				}
			}
		}

	} /* End of class */

} /* End of class-exists wrapper */