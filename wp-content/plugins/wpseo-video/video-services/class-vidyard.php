<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Vidyard Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Vidyard' ) ) {

	/**
	 * Class WPSEO_Video_Details_Vidyard
	 */
	class WPSEO_Video_Details_Vidyard extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		protected $id_regex = '`vidyard\.com/(.*?)(?:\.js|\.html|\?|$)`';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://play.vidyard.com/%s',
			'replace_key'   => 'id',
			'response_type' => '',
		);


		/**
		 * Set the content location
		 */
		protected function set_content_loc() {
			$data = $this->get_chapter_data();
			if ( $data !== false && ! empty( $data['sd_unsecure_url'] ) ) {
				$this->vid['content_loc'] = $data['sd_unsecure_url'];
			}
		}

		/**
		 * Set the video duration
		 */
		protected function set_duration() {
			$data = $this->get_chapter_data();
			if ( $data !== false && ! empty( $data['seconds'] ) ) {
				$this->vid['duration'] = $data['seconds'];
			}
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ( is_string( $this->remote_url['pattern'] ) && $this->remote_url['pattern'] !== '' )
				&& ( is_string( $this->vid[ $this->remote_url['replace_key'] ] )
				&& $this->vid[ $this->remote_url['replace_key'] ] !== '' ) ) {

				$url              = sprintf( $this->remote_url['pattern'], $this->vid[ $this->remote_url['replace_key'] ] );
				$this->vid['url'] = $this->url_encode( $url );
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			// preg match for thumbnail
			// @todo [JRF -> whomever] - verify regex - shouldn't the {} be slash escaped ?
			if ( preg_match( '`vidyard_thumbnail_data = ({.*?});`s', $this->decoded_response, $match ) ) {
				$thumbnail_data = str_replace( '\'', '"', trim( $match[1] ) );
				$thumbnail_data = json_decode( $thumbnail_data, true );

				if ( ( is_array( $thumbnail_data ) && $thumbnail_data !== array() ) && isset( $thumbnail_data['url'] ) ) {
					$thumbnail_data = reset( $thumbnail_data ); // get the first element
					$image          = $this->make_image_local( $thumbnail_data['url'] );
					if ( is_string( $image ) && $image !== '' ) {
						$this->vid['thumbnail_loc'] = $image;
					}
				}
			}
		}

		/**
		 * Get decoded vidyard chapter data
		 *
		 * @return bool|array  Data array or false if data could not be found/decoded
		 */
		private function get_chapter_data() {
			static $chapter_data;

			if ( ! isset( $chapter_data ) ) {
				$chapter_data = false;

				// must use preg match because the data is in inline javascript
				if ( preg_match( '`vidyard_chapter_data = (\[[\]]*\]);`s', $this->decoded_response, $match ) ) {

					// replace single quotes with double quotes so it can be json decoded
					$json = str_replace( '\'', '"', trim( $match[1] ) );
					$json = json_decode( $json, true );

					if ( is_array( $json ) && $json !== array() ) {
						$chapter_data = reset( $json ); // get the first element
					}
				}
			}

			return $chapter_data;
		}

	} /* End of class */

} /* End of class-exists wrapper */