<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Animoto Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Animoto' ) ) {

	/**
	 * Class WPSEO_Video_Details_Animoto
	 *
	 * @internal Animoto doesn't provide duration in the oembed API, unfortunately.
	 */
	class WPSEO_Video_Details_Animoto extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		//protected $id_regex = '';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://animoto.com/services/oembed?format=json&url=%s',
			'replace_key'   => 'url',
			'response_type' => 'json',
		);


		/**
		 * Instantiate the class
		 *
		 * Adjust the video url before passing off to the parent constructor
		 *
		 * @param array  $vid     The video array with all the data.
		 * @param array  $old_vid The video array with all the data of the previous "fetch", if available.
		 * @param string $thumb   The URL to the manually set thumbnail, if available.
		 *
		 * @return \WPSEO_Video_Details_Animoto
		 */
		public function __construct( $vid, $old_vid = array(), $thumb = '' ) {
			if ( isset( $vid['url'] ) && ! empty ( $vid['url'] ) ) {
				if ( preg_match( '`http://static\.animoto\.com/swf/.*?&f=([^&]+)`', $vid['url'], $match ) ) {
					$vid['url'] = 'http://animoto.com/play/' . rawurlencode( $match[1] );
				}
			}
			parent::__construct( $vid, $old_vid, $thumb );
		}


		/**
		 * Use the "new" post data with the old video data, to prevent the need for an external video
		 * API call when the video hasn't changed.
		 *
		 * Match whether old data can be used on url rather than video id
		 *
		 * @param array  $vid       The "new" video array
		 * @param array  $old_vid   The old video array
		 * @param string $thumb     Possibly the thumbnail, if set manually.
		 * @param string $match_on  Array key to use in the $vid array to determine whether or not to use the old data
		 *                          Defaults to 'url' for this implementation.
		 *
		 * @return bool  Whether or not valid old data was found (and used)
		 */
		protected function maybe_use_old_video_data( $vid, $old_vid = array(), $thumb = '', $match_on = 'url' ) {
			return parent::maybe_use_old_video_data( $vid, $old_vid, $thumb, $match_on );
		}


		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ! empty( $this->decoded_response->video_url ) ) {
				$this->vid['player_loc'] = $this->decoded_response->video_url;
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			$this->set_thumbnail_loc_from_json_object();
		}

	} /* End of class */

} /* End of class-exists wrapper */