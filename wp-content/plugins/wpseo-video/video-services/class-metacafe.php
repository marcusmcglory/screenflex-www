<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Metacafe Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Metacafe' ) ) {

	/**
	 * Class WPSEO_Video_Details_Metacafe
	 *
	 * @link  http://help.metacafe.com/?page_id=238 Metacafe API docs - no longer available.
	 */
	class WPSEO_Video_Details_Metacafe extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		protected $id_regex = '`/watch/(\d+)/`';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://www.metacafe.com/api/item/%s/',
			'replace_key'   => 'id',
			'response_type' => '',
		);


		/**
		 * Set the content location
		 */
		protected function set_content_loc() {
			if ( preg_match( '`<media:content url="([^"]+)"`', $this->decoded_response, $match ) ) {
				$this->vid['content_loc'] = $match[1];
			}
		}

		/**
		 * Set the video duration
		 */
		protected function set_duration() {
			if ( preg_match( '`duration="(\d+)"`', $this->decoded_response, $match ) ) {
				$this->vid['duration'] = $match[1];
			}
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ! empty( $this->vid['id'] ) ) {
				$this->vid['player_loc'] = 'http://www.metacafe.com/fplayer/' . rawurlencode( $this->vid['id'] ) . '/.swf';
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			if ( preg_match( '`<media:thumbnail url="([^"]+)"`', $this->decoded_response, $match ) ) {
				$image = $this->make_image_local( $match[1] );
				if ( is_string( $image ) && $image !== '' ) {
					$this->vid['thumbnail_loc'] = $image;
				}
			}
		}

	} /* End of class */

} /* End of class-exists wrapper */