<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Cincopa Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Cincopa' ) ) {

	/**
	 * Class WPSEO_Video_Details_Cincopa
	 */
	class WPSEO_Video_Details_Cincopa extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		protected $id_regex = '`cp_load_widget\(\'([^\']+)\',[^\)]*\);`';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://www.cincopa.com/media-platform/runtime/rss200.aspx?fid=%s',
			'replace_key'   => 'id',
			'response_type' => 'simplexml',
		);


		/**
		 * Retrieve the video id from a known video url based on parsing the url and a regex match.
		 *
		 * @param int $match_nr The captured parenthesized sub-pattern to use from matches. Defaults to 1.
		 *
		 * @return void
		 */
		protected function determine_video_id_from_url( $match_nr = 1 ) {
			if ( isset( $this->vid['url'] ) && ( is_string( $this->vid['url'] ) && $this->vid['url'] !== '' ) && $this->id_regex !== '' ) {
				$parse = parse_url( $this->vid['url'] );
				if ( isset( $parse['query'] ) && preg_match( $this->id_regex, $parse['query'], $match ) ) {
					$this->vid['id'] = $match[ $match_nr ];
				}
			}
		}


		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ! empty( $this->decoded_response->content->attributes()->url ) ) {
				$this->vid['player_loc'] = (string) $this->decoded_response->content->attributes()->url;
			}
		}

		/**
		 * Set the thumbnail location
		 *
		 * @todo: thumbnails are not working currently b/c $this->make_image_local() strips query parameters
		 * and this video service needs query params to generate thumbnails look for a more direct approach
		 */
		protected function set_thumbnail_loc() {
			if ( ! empty( $this->decoded_response->thumbnail->attributes()->url ) ) {
				$image = $this->make_image_local( (string) $this->decoded_response->thumbnail->attributes()->url );
				if ( is_string( $image ) && $image !== '' ) {
					$this->vid['thumbnail_loc'] = $image;
				}
			}
		}

	} /* End of class */

} /* End of class-exists wrapper */