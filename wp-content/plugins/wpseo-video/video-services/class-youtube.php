<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * YouTube Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Youtube' ) ) {

	/**
	 * Class WPSEO_Video_Details_Youtube
	 */
	class WPSEO_Video_Details_Youtube extends WPSEO_Video_Details {

		/**
		 * @var    string    Regular expression to retrieve a video id from a known video url
		 */
		//protected $id_regex = '';

		/**
		 * @var    array    Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://gdata.youtube.com/feeds/api/videos/%s',
			'replace_key'   => 'id',
			'response_type' => '',
		);


		/**
		 * Retrieve the video id from a known video url based on a regex match.
		 * Also change the url based on the new video id.
		 *
		 * @param int $match_nr The captured parenthesized sub-pattern to use from matches. Defaults to 1.
		 *
		 * @return void
		 */
		protected function determine_video_id_from_url( $match_nr = 1 ) {
			if ( isset( $this->vid['url'] ) && is_string( $this->vid['url'] ) && $this->vid['url'] !== '' ) {

				$yt_id = WPSEO_Video_Sitemap::$youtube_id_pattern;

				$patterns = array(
					'`http[s]?://(?:www\.)?youtube\.com/(?:watch)?\?(?:.*&)?v=(' . $yt_id . ')`',
					'`http[s]?://(?:www\.)?youtube(?:-nocookie)?\.com/embed/(' . $yt_id . ')`',
					'`http[s]?://(?:www\.)?youtube\.com/v/(' . $yt_id . ')`',
					'`http://youtu\.be/(' . $yt_id . ')`',
				);

				foreach ( $patterns as $pattern ) {
					if ( preg_match( $pattern, $this->vid['url'], $match ) ) {
						$this->vid['id'] = $match[ $match_nr ];
						break;
					}
				}

				// @todo [JRF => Yoast] shouldn't this be checked against $youtube_id_pattern as well ?
				if ( ( ! isset( $this->vid['id'] ) || empty( $this->vid['id'] ) ) && ! preg_match( '`^http`', $this->vid['url'], $match ) ) {
					$this->vid['id'] = $this->vid['url'];
				}
			}
		}


		/**
		 * Set the video duration
		 */
		protected function set_duration() {
			if ( preg_match( '`<yt:duration seconds=([\'"])([0-9]+)\1/>`', $this->decoded_response, $match ) ) {
				$this->vid['duration'] = $match[2];
			}
		}

		/**
		 * Set the video height
		 */
		protected function set_height() {
			$this->vid['height'] = 390;
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ! empty( $this->vid['id'] ) ) {
				// @todo: Check: why is htmlentities used here ? None of the other player_loc's have it
				$this->vid['player_loc'] = htmlentities( 'https://www.youtube-nocookie.com/v/' . rawurlencode( $this->vid['id'] ) );
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			$image = $this->make_image_local( 'http://img.youtube.com/vi/' . $this->vid['id'] . '/maxresdefault.jpg' );
			if ( is_string( $image ) && $image !== '' ) {
				$this->vid['thumbnail_loc'] = $image;
			}
		}

		/**
		 * Set the video view count
		 */
		protected function set_view_count() {
			if ( preg_match( '`<yt:statistics favoriteCount=([\'"])([\d]+)\1 viewCount=([\'"])([\d]+)\3/>`', $this->decoded_response, $match ) ) {
				$this->vid['view_count'] = (int) $match[4];
			}
		}

		/**
		 * Set the video width
		 */
		protected function set_width() {
			$this->vid['width'] = 640;
		}

	} /* End of class */

} /* End of class-exists wrapper */
