<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Viddler Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Viddler' ) ) {

	/**
	 * Class WPSEO_Video_Details_Viddler
	 */
	class WPSEO_Video_Details_Viddler extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		protected $id_regex = '`http[s]?://(?:www\.)?viddler\.com/embed/([^/]+)`';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://api.viddler.com/api/v2/viddler.videos.getDetails.php?key=0118093f713643444556524f452f&video_id=%s',
			'replace_key'   => 'id',
			'response_type' => 'serial',
		);


		/**
		 * Use the "new" post data with the old video data, to prevent the need for an external video
		 * API call when the video hasn't changed.
		 *
		 * Match whether old data can be used on video id or on video url if id is not available
		 *
		 * @param array  $vid       The "new" video array
		 * @param array  $old_vid   The old video array
		 * @param string $thumb     Possibly the thumbnail, if set manually.
		 * @param string $match_on  Array key to use in the $vid array to determine whether or not to use the old data
		 *                          Defaults to 'url' for this implementation.
		 *
		 * @return bool  Whether or not valid old data was found (and used)
		 */
		protected function maybe_use_old_video_data( $vid, $old_vid = array(), $thumb = '', $match_on = 'url' ) {
			if ( ( isset( $old_vid['id'] ) && isset( $vid['id'] ) ) && $old_vid['id'] == $vid['id'] ) {
				$match_on = 'id';
			}
			return parent::maybe_use_old_video_data( $vid, $old_vid, $thumb, $match_on );
		}


		/**
		 * Retrieve information on a video via a remote API call
		 *
		 * Change the $remote_url parameters if id is not available, before passing off to the parent
		 *
		 * @return void
		 */
		protected function get_remote_video_info() {
			if ( empty( $this->vid['id'] ) && ! empty( $this->vid['url'] ) ) {
				$this->remote_url['pattern']     = str_replace( '&video_id=%s', '&url=%s', $this->remote_url['pattern'] );
				$this->remote_url['replace_key'] = 'url';

			}
			parent::get_remote_video_info();
		}


		/**
		 * Set the content location
		 */
		protected function set_content_loc() {
			if ( isset( $this->decoded_response['video']['files'] ) && is_array( $this->decoded_response['video']['files'] ) && $this->decoded_response['video']['files'] !== array() ) {
				foreach ( $this->decoded_response['video']['files'] as $file ) {
					if ( ( $file['ext'] === 'mp4' && isset( $file['url'] ) ) && ( is_string( $file['url'] ) && $file['url'] !== '' ) ) {
						$this->vid['content_loc'] = $file['url'];
					}
				}
			}
		}

		/**
		 * Set the video duration
		 */
		protected function set_duration() {
			if ( ! empty( $this->decoded_response['video']['length'] ) ) {
				$this->vid['duration'] = $this->decoded_response['video']['length'];
			}
		}

		/**
		 * (Re-)Set the video id
		 */
		protected function set_id() {
			if ( ! empty( $this->decoded_response['video']['id'] ) ) {
				$this->vid['id'] = $this->decoded_response['video']['id'];
			}
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ! empty( $this->vid['id'] ) ) {
				$this->vid['player_loc'] = 'http://www.viddler.com/player/' . rawurlencode( $this->vid['id'] ) . '/';
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			if ( isset( $this->decoded_response['video']['thumbnail_url'] ) && is_string( $this->decoded_response['video']['thumbnail_url'] ) && $this->decoded_response['video']['thumbnail_url'] !== '' ) {
				$image = $this->make_image_local( $this->decoded_response['video']['thumbnail_url'] );
				if ( is_string( $image ) && $image !== '' ) {
					$this->vid['thumbnail_loc'] = $image;
				}
			}
		}

		/**
		 * Set the video view count
		 */
		protected function set_view_count() {
			if ( ! empty( $this->decoded_response['video']['view_count'] ) ) {
				$this->vid['view_count'] = (int) $this->decoded_response['video']['view_count'];
			}
		}

	} /* End of class */

} /* End of class-exists wrapper */