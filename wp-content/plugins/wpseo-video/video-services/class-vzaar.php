<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Vzaar Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Vzaar' ) ) {

	/**
	 * Class WPSEO_Video_Details_Vzaar
	 *
	 * @internal We don't strictly need a response, funnily enough, though we lack the duration when we don't get it.
	 */
	class WPSEO_Video_Details_Vzaar extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		protected $id_regex = '`/(\d+)/(?:player|flashplayer|video|download)$`';

		/**
		 * @var	array  Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => '%s%s.json',
			//'replace_key'   => 'id',
			'response_type' => 'json',
		);

		/**
		 * @var string  Vzaar base url
		 */
		protected $base_url = 'http://vzaar.com/videos/';


		/**
		 * Instantiate the class
		 *
		 * Overwrite the vzaar base domain if needed before passing off to the parent constructor
		 *
		 * @param array  $vid     The video array with all the data.
		 * @param array  $old_vid The video array with all the data of the previous "fetch", if available.
		 * @param string $thumb   The URL to the manually set thumbnail, if available.
		 *
		 * @return \WPSEO_Video_Details_Vzaar
		 */
		public function __construct( $vid, $old_vid = array(), $thumb = '' ) {
			$options = get_option( 'wpseo_video' );
			if ( $options['vzaar_domain'] !== '' ) {
				$this->base_url = 'http://' . rawurlencode( $options['vzaar_domain'] ) . '/';
			}

			parent::__construct( $vid, $old_vid, $thumb );
		}


		/**
		 * Retrieve information on a video via a remote API call
		 *
		 * @uses WPSEO_Video_Details::$remote_url
		 * @uses WPSEO_Video_Details::$base_url
		 *
		 * @return void
		 */
		protected function get_remote_video_info() {
			if ( ( is_string( $this->remote_url['pattern'] ) && $this->remote_url['pattern'] !== '' ) && ! empty( $this->vid['id'] ) ) {
				$url = sprintf( $this->remote_url['pattern'], $this->base_url, $this->vid['id'] );
				$url = $this->url_encode( $url );

				$response = $this->remote_get( $url, array( 'referer' => get_site_url() ) );
				if ( $response !== false ) {
					$this->remote_response = $response;
				}
			}
		}


		/**
		 * Set the video duration
		 */
		protected function set_duration() {
			$this->set_duration_from_json_object();
		}

		/**
		 * Set the video height
		 */
		protected function set_height() {
			$this->set_height_from_json_object();
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ! empty( $this->vid['id'] ) ) {
				$this->vid['player_loc'] = 'https://view.vzaar.com/' . rawurlencode( $this->vid['id'] ) . '/flashplayer';
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			if ( ! empty( $this->vid['id'] ) ) {
				$url   = $this->base_url . rawurlencode( $this->vid['id'] ) . '/image';
				$image = $this->make_image_local( $url, 'jpg' );
				if ( is_string( $image ) && $image !== '' ) {
					$this->vid['thumbnail_loc'] = $image;
				}
			}
		}

		/**
		 * Set the video width
		 */
		protected function set_width() {
			$this->set_width_from_json_object();
		}

	} /* End of class */

} /* End of class-exists wrapper */
