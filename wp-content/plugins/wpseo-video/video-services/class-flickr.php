<?php
/**
 * @package    Internals
 * @since      1.7.0
 * @version    1.7.0
 */

// Avoid direct calls to this file
if ( ! class_exists( 'WPSEO_Video_Sitemap' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/*******************************************************************
 * Flickr Video SEO Details
 *******************************************************************/
if ( ! class_exists( 'WPSEO_Video_Details_Flickr' ) ) {

	/**
	 * Class WPSEO_Video_Details_Flickr
	 */
	class WPSEO_Video_Details_Flickr extends WPSEO_Video_Details {

		/**
		 * @var	string	Regular expression to retrieve a video id from a known video url
		 */
		protected $id_regex = '`/(\d+)/?$`';

		/**
		 * @var	array	Information on the remote url to use for retrieving the video details
		 */
		protected $remote_url = array(
			'pattern'       => 'http://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=2d2985adb59d21e6933368e41e5ca3b0&photo_id=%s&format=json&nojsoncallback=1',
			'replace_key'   => 'id',
			'response_type' => 'json',
		);


		/**
		 * Test for the existence of the required details and set video details to their new values
		 * by passing off to the parent method
		 *
		 * @param string $thumb  Possibly the thumbnail, if set manually.
		 *
		 * @return void
		 */
		protected function put_video_details( $thumb = '' ) {
			if ( isset( $this->decoded_response->photo->media ) && $this->decoded_response->photo->media === 'video' ) {
				parent::put_video_details( $thumb );
			}
		}


		/**
		 * Set the video duration
		 */
		protected function set_duration() {
			if ( ! empty( $this->decoded_response->photo->video->duration ) ) {
				$this->vid['duration'] = $this->decoded_response->photo->video->duration;
			}
		}

		/**
		 * Set the player location
		 */
		protected function set_player_loc() {
			if ( ! empty( $this->decoded_response->photo->secret ) && ! empty( $this->vid['id'] ) ) {
				$this->vid['player_loc'] = $this->url_encode( 'http://www.flickr.com/apps/video/stewart.swf?v=109786&intl_lang=en_us&photo_secret=' . $this->decoded_response->photo->secret . '&photo_id=' . $this->vid['id'] );
			}
		}

		/**
		 * Set the thumbnail location
		 */
		protected function set_thumbnail_loc() {
			if ( ( ! empty( $this->decoded_response->photo->farm ) && ! empty( $this->decoded_response->photo->server ) )
				&& ( ! empty( $this->decoded_response->photo->secret ) && ! empty( $this->vid['id'] ) ) ) {

				$url   = 'http://farm' . $this->decoded_response->photo->farm . '.staticflickr.com/' .
							$this->decoded_response->photo->server . '/' .
							$this->vid['id'] . '_' . $this->decoded_response->photo->secret . '.jpg';
				$url   = $this->url_encode( $url );
				$image = $this->make_image_local( $url );
				if ( is_string( $image ) && $image !== '' ) {
					$this->vid['thumbnail_loc'] = $image;
				}
			}
		}

		/**
		 * Set the video view count
		 */
		protected function set_view_count() {
			if ( ! empty( $this->decoded_response->photo->views ) ) {
				$this->vid['view_count'] = (int) $this->decoded_response->photo->views;
			}
		}

	} /* End of class */

} /* End of class-exists wrapper */