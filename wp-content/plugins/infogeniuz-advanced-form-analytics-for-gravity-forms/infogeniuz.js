var InfoGeniuz = {}; //namespace
InfoGeniuz.DetectBrowser = {
	init: function ()
	{
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "an unknown version";
	},
	searchString: function (data)
	{
		for (var i = 0; i < data.length; i++)
		{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString)
			{
				if (dataString.indexOf(data[i].subString) != -1) return data[i].identity;
			}
			else if (dataProp) return data[i].identity;
		}
	},
	searchVersion: function (dataString)
	{
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
	},
	dataBrowser: [
	{
		string: navigator.userAgent,
		subString: "Chrome",
		identity: "Chrome"
	}, {
		string: navigator.userAgent,
		subString: "OmniWeb",
		versionSearch: "OmniWeb/",
		identity: "OmniWeb"
	}, {
		string: navigator.vendor,
		subString: "Apple",
		identity: "Safari",
		versionSearch: "Version"
	}, {
		prop: window.opera,
		identity: "Opera"
	}, {
		string: navigator.vendor,
		subString: "iCab",
		identity: "iCab"
	}, {
		string: navigator.vendor,
		subString: "KDE",
		identity: "Konqueror"
	}, {
		string: navigator.userAgent,
		subString: "Firefox",
		identity: "Firefox"
	}, {
		string: navigator.vendor,
		subString: "Camino",
		identity: "Camino"
	}, { // for newer Netscapes (6+)  
		string: navigator.userAgent,
		subString: "Netscape",
		identity: "Netscape"
	}, {
		string: navigator.userAgent,
		subString: "MSIE",
		identity: "Internet Explorer",
		versionSearch: "MSIE"
	}, {
		string: navigator.userAgent,
		subString: "Gecko",
		identity: "Mozilla",
		versionSearch: "rv"
	}, { // for older Netscapes (4-)  
		string: navigator.userAgent,
		subString: "Mozilla",
		identity: "Netscape",
		versionSearch: "Mozilla"
	}]

};

InfoGeniuz.DetectBrowser.init();

<!-- End Hardware Data -->


<!-- Start Geolocation Data -->
InfoGeniuz.gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write("<script src='" + InfoGeniuz.gaJsHost + "google-analytics.com/ga.js' type='text/javascript'>" + "</sc" + "ript>");

if (typeof (_gat) != "undefined")
{
	InfoGeniuz.pageTracker = _gat._getTracker("UA-1-1");
	InfoGeniuz.pageTracker._trackPageview();
}

InfoGeniuz._uGC = function (l, n, s)
{
	if (!l || l == "" || !n || n == "" || !s || s == "") return "-";
	var i, i2, i3, c = "-";
	i = l.indexOf(n);
	i3 = n.indexOf("=") + 1;
	if (i > -1)
	{
		i2 = l.indexOf(s, i);
		if (i2 < 0)
		{
			i2 = l.length;
		}
		c = l.substring((i + i3), i2);
	}
	return c;
}

InfoGeniuz.addDataToForm = function( form ) {
	// form is a jQuery object
	form = jQuery(form);
	form.find('input[name^="igz_"]').remove();
	
	//now, create a data element for us
	var formbody = form.find('.gform_body')
	
	formbody.append("<input type='hidden' name='igz_g_city' value='"+geoip_city()+"' />");
	formbody.append("<input type='hidden' name='igz_g_country' value='"+geoip_country_name()+"' />");
	formbody.append("<input type='hidden' name='igz_g_state' value='"+geoip_region_name()+"' />");
	formbody.append("<input type='hidden' name='igz_g_longitude' value='"+geoip_longitude()+"' />");
	formbody.append("<input type='hidden' name='igz_g_latitude' value='"+geoip_latitude()+"' />");
	formbody.append("<input type='hidden' name='igz_h_browser' value='"+InfoGeniuz.DetectBrowser.browser+"' />");
	formbody.append("<input type='hidden' name='igz_h_brversion' value='"+InfoGeniuz.DetectBrowser.version+"' />");
	formbody.append("<input type='hidden' name='igz_h_os' value='"+window.navigator.platform+"' />");
	<!-- End Geolocation Data -->


	<!-- Start Analytics Data -->
	var z = InfoGeniuz._uGC(document.cookie, '__utmz=', ';');

	var source = InfoGeniuz._uGC(z, 'utmcsr=', '|');
	var medium = InfoGeniuz._uGC(z, 'utmcmd=', '|');
	var term = InfoGeniuz._uGC(z, 'utmctr=', '|');
	var content = InfoGeniuz._uGC(z, 'utmcct=', '|');
	var campaign = InfoGeniuz._uGC(z, 'utmccn=', '|');
	var gclid = InfoGeniuz._uGC(z, 'utmgclid=', '|');

	if (gclid != "-")
	{
		source = 'google';
		medium = 'cpc';
	}
	var csegment = InfoGeniuz._uGC(document.cookie, '__utmv=', ';');
	if (csegment != '-')
	{
		var csegmentex = /[1-9]*?\.(.*)/;
		csegment = csegment.match(csegmentex);
		csegment = csegment[1];
	}
	else
	{
		csegment = '(not set)';
	}

	var a = InfoGeniuz._uGC(document.cookie, '__utma=', ';');
	var aParts = a.split(".");
	var fVisits = aParts[2];
	var pVisits = aParts[3];
	var cVisits = aParts[4];
	var nVisits = aParts[5];
	
	var first = '';
	var previous = '';
	var current = '';
	if( fVisits != undefined ) {
		var first = new Date(fVisits * 1000);
	}
	formbody.append("<input type='hidden' name='igz_t_original' value='"+first+"' />");
	if( pVisits != undefined ) {
		var previous = new Date(pVisits * 1000);
	}
	formbody.append("<input type='hidden' name='igz_t_previous' value='"+previous+"' />");
	if( cVisits != undefined ) {
		var current = new Date(cVisits * 1000);
	}
	formbody.append("<input type='hidden' name='igz_t_current' value='"+current+"' />");
	
	var a = InfoGeniuz._uGC(document.cookie, '__utmb=', ';');

	var aParts = a.split(".");
	var pViews = aParts[1];
	term = term.split('%20');
	var no = term.length;
	aa = '';
	for (var i = 0; i < no; i++)
	{
		aa = aa + ' ' + term[i];
	}
	term = aa;
	if( pViews == undefined ) {
		pViews = '';
	}
	formbody.append("<input type='hidden' name='igz_a_pageviews' value='"+pViews+"' />");

	formbody.append("<input type='hidden' name='igz_a_source' value='"+source+"' />");
	formbody.append("<input type='hidden' name='igz_a_medium' value='"+medium+"' />");
	formbody.append("<input type='hidden' name='igz_a_term' value='"+term+"' />");
	formbody.append("<input type='hidden' name='igz_a_content' value='"+content+"' />");
	formbody.append("<input type='hidden' name='igz_a_campaign' value='"+campaign+"' />");
	formbody.append("<input type='hidden' name='igz_a_segment' value='"+csegment+"' />");
	formbody.append("<input type='hidden' name='igz_a_visits' value='"+nVisits+"' />");
	var today = new Date();
	formbody.append("<input type='hidden' name='igz_t_time' value='"+today+"' />");
}

jQuery(document).ready(function ($) {
	jQuery(document).bind('gform_page_loaded', function(event, form_id, current_page){
		var form = $('#gform_wrapper_' + form_id);
		console.log('refreshing data for form '+form_id);
		InfoGeniuz.addDataToForm(form);
	});

	$('.gform_wrapper').each(function() {
		// call the fn to create & fill in our data.
		InfoGeniuz.addDataToForm(this);
	});
	
});
