User-agent: *

Disallow: /cgi-bin/
Disallow: /trackback/
Disallow: /wp-admin/
Disallow: /wp-content/plugins/
Disallow: /wp-content/themes/
Disallow: /wp-includes/
Disallow: /xmlrpc.php
Disallow: /xmlrpc.php
Disallow: ?wptheme=
Disallow: /blackhole/
Disallow: /transfer/
Disallow: /assets/
Disallow: /newsletters/
Disallow: /phplive/
Disallow: /aspnet_client/
Disallow: /info/
Disallow: /phone-call/

Sitemap: https://www.screenflex.com/sitemap_index.xml